---
title:   "Animated GIFsanity"
date:    2007-10-08
authors:
  - sad eagle
slug:    animated-gifsanity
---
So I thought I would spend a tiny bit of time on the weekend trying to get khtml 4.0's gif decoder all finished. Yeah, right. Decoding the frames themselves is easy: giflib does it for us. The messy part comes when it comes to putting them together into an animation.

The way this roughly works is the following: gif has a canvas, which it calls the screen. It provides a palette, the index for the background color in it, and of course the dimensions.

For an animated gif, there is then a sequence of frames, each of which specifies the portion of the screen the frame touches. There may also be a per-frame palette, and a transparency colorkey. More interestingly there is a disposal mode, which specifies how to transition to the next frame:

<ol>
<li>unspecified (always a good sign in a spec. Not!)<.li>
<li>leave screen as is</li>
<li>clear to background</li>
<li>revert to state before this frame</<li>
</ol>
Sounds easy enough? Well, too bad it's not the entire picture.
Consider for example the mode 2: clear to background. For most images it behaves by clearing to transparency. What's the big deal? Well, the place the background color is specified is with  the global palette... and there is no transparency in there! Transparency colorkey is specified per-image, so... how exactly is it supposed to work? Does one interpret background color inside the current frame's palette (which may be a local palette, or the global palette with the transparency colorkey overlaid..)? Does one just use transparency for background if there is transparency involved? Beats me.

And that, actually, isn't the worst. The messy part comes from compositing images. The images have transparency, so when one draws the new frame over the current canvas state, one has to combine them together. There are 2 ways of doing it: 
<ol>
<li>only draw non-transparent pixels (the over operator).</li>
<li>copy over everything, overwriting non-transparent 
   pixels with transparency (the src operator).</li>
</ol>
So which one is right? <i>Both</i>. There are images that need one mode, and there are images that need another... and one can easily construct images where both are in use, and it's entirely unclear how e.g. mozilla decides on which way to do things.

Anyone reading this know what's going on there?

<!--break-->