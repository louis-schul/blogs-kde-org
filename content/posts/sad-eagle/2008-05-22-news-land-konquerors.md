---
title:   "News from the land of Konquerors"
date:    2008-05-22
authors:
  - sad eagle
slug:    news-land-konquerors
---
A bunch of exciting things have happened to Konqueror in preparation for 4.1 Beta 1 this weekend. 

First of all, KHTML guru Germain Garand has committed the bulk of the designMode/contentEditable editing code. It's not 100% done yet, but it's a monumental amount of work, and it should enable the various rich-text editor apps to work in KHTML.

In the UI land, Eduardo Elvira has committed some fancy session management features such as crash recovery.

But, I am gonna be bit a bit selfish, and mostly talk about what I did:  my performance/bytecode version of KJS - KJS "Frostbyte" - is now in kdelibs trunk, and part of KDE4.1 Beta 1. This brings some significant pure ES performance improvements over previous versions, and lets us put up some great numbers. On the SunSpider benchmark suite, this version does:
<ul>
<li>2.27x better than Firefox 2</li>
<li>2x better than QtWebKit from Qt4.4</li>
<li>About 1.4x faster than KJS 4.0 "Blizzard"</li>
<li>About 1.152x faster overall than Opera 9.5 beta 2, but it's really a wash, with both engines winning lots of individual benchmarks...</li>
</ul>
.. but about 1.77x slower than Firefox 3rc1. Ouch. Well, at least we beat them on Acid3, and it might just keep my head from getting too big. Great job by the Mozilla guys there, though. And the Apple guys of course aren't standing still either --- the development versions of JavaScriptCore/Safari have excellent optimizations we lack, and will likely be getting their own bytecode engine, SquirrelFish, soon (I name things after weather. They name them after weird animals. I think you can see who lives in California, and who lives in New York State). 

Thankfully, I have some tricks left up my sleeve. Which isn't very comfortable, since I am wearing a t-shirt.

And if you care, or are really bored, the numbers are under the cut.
<!--break-->
<hr>
vs. Firefox 2:
<pre>
TEST                   COMPARISON            FROM                 TO             DETAILS

=============================================================================

** TOTAL **:           2.27x as fast     16813.0ms +/- 0.7%   7409.6ms +/- 4.3%     significant

=============================================================================

  3d:                  2.92x as fast      2612.4ms +/- 0.6%    895.8ms +/- 7.5%     significant
    cube:              2.08x as fast       669.2ms +/- 2.0%    321.0ms +/- 9.9%     significant
    morph:             5.28x as fast      1531.0ms +/- 0.5%    290.2ms +/- 5.9%     significant
    raytrace:          1.45x as fast       412.2ms +/- 0.5%    284.6ms +/- 6.7%     significant

  access:              1.60x as fast      1496.6ms +/- 3.9%    933.2ms +/- 4.8%     significant
    binary-trees:      1.81x as fast       271.8ms +/- 18.3%    150.4ms +/- 2.1%     significant
    fannkuch:          ??                  350.2ms +/- 6.5%    350.6ms +/- 1.9%     not conclusive: might be *1.001x as slow*
    nbody:             1.80x as fast       636.2ms +/- 0.8%    353.2ms +/- 11.7%     significant
    nsieve:            3.02x as fast       238.4ms +/- 0.6%     79.0ms +/- 1.1%     significant

  bitops:              4.51x as fast      4300.6ms +/- 2.6%    952.8ms +/- 2.2%     significant
    3bit-bits-in-byte: 2.41x as fast       334.2ms +/- 0.5%    138.6ms +/- 6.8%     significant
    bits-in-byte:      1.67x as fast       303.8ms +/- 0.8%    182.0ms +/- 5.0%     significant
    bitwise-and:       7.34x as fast      3383.0ms +/- 3.1%    460.6ms +/- 3.7%     significant
    nsieve-bits:       1.63x as fast       279.6ms +/- 2.2%    171.6ms +/- 3.8%     significant

  controlflow:         *1.055x as slow*    185.0ms +/- 0.5%    195.2ms +/- 1.6%     significant
    recursive:         *1.055x as slow*    185.0ms +/- 0.5%    195.2ms +/- 1.6%     significant

  crypto:              1.38x as fast       690.2ms +/- 0.5%    499.2ms +/- 2.1%     significant
    aes:               1.68x as fast       258.4ms +/- 0.9%    153.6ms +/- 1.4%     significant
    md5:               1.21x as fast       213.0ms +/- 0.4%    175.6ms +/- 2.4%     significant
    sha1:              1.29x as fast       218.8ms +/- 0.5%    170.0ms +/- 3.2%     significant

  date:                2.36x as fast      1960.2ms +/- 0.6%    831.6ms +/- 3.7%     significant
    format-tofte:      2.07x as fast       768.2ms +/- 0.2%    371.4ms +/- 3.4%     significant
    format-xparb:      2.59x as fast      1192.0ms +/- 0.9%    460.2ms +/- 4.1%     significant

  math:                1.55x as fast      1466.8ms +/- 0.8%    947.8ms +/- 7.0%     significant
    cordic:            1.49x as fast       555.8ms +/- 1.4%    373.4ms +/- 6.7%     significant
    partial-sums:      1.71x as fast       594.2ms +/- 1.5%    348.0ms +/- 7.4%     significant
    spectral-norm:     1.40x as fast       316.8ms +/- 0.5%    226.4ms +/- 7.1%     significant

  regexp:              1.125x as fast      514.8ms +/- 0.6%    457.4ms +/- 0.7%     significant
    dna:               1.125x as fast      514.8ms +/- 0.6%    457.4ms +/- 0.7%     significant

  string:              2.11x as fast      3586.4ms +/- 0.4%   1696.6ms +/- 4.8%     significant
    base64:            3.75x as fast       995.8ms +/- 0.9%    265.6ms +/- 10.7%     significant
    fasta:             1.57x as fast       683.2ms +/- 0.6%    434.6ms +/- 2.6%     significant
    tagcloud:          1.58x as fast       491.2ms +/- 1.4%    310.4ms +/- 2.8%     significant
    unpack-code:       3.21x as fast      1080.0ms +/- 0.3%    336.6ms +/- 4.7%     significant
    validate-input:    *1.039x as slow*    336.2ms +/- 0.2%    349.4ms +/- 5.2%     significant
</pre>
<hr>
vs. QtWebKit 4.4:
<pre>
=============================================================================

** TOTAL **:           2.02x as fast     14941.0ms +/- 0.3%   7409.6ms +/- 4.3%     significant

=============================================================================

  3d:                  2.21x as fast      1982.4ms +/- 0.5%    895.8ms +/- 7.5%     significant
    cube:              1.94x as fast       621.2ms +/- 0.4%    321.0ms +/- 9.9%     significant
    morph:             2.71x as fast       787.2ms +/- 0.7%    290.2ms +/- 5.9%     significant
    raytrace:          2.02x as fast       574.0ms +/- 0.8%    284.6ms +/- 6.7%     significant

  access:              2.93x as fast      2733.2ms +/- 0.4%    933.2ms +/- 4.8%     significant
    binary-trees:      1.60x as fast       240.4ms +/- 0.6%    150.4ms +/- 2.1%     significant
    fannkuch:          3.30x as fast      1155.4ms +/- 0.8%    350.6ms +/- 1.9%     significant
    nbody:             1.63x as fast       575.6ms +/- 0.3%    353.2ms +/- 11.7%     significant
    nsieve:            9.64x as fast       761.8ms +/- 0.4%     79.0ms +/- 1.1%     significant

  bitops:              2.65x as fast      2520.4ms +/- 1.0%    952.8ms +/- 2.2%     significant
    3bit-bits-in-byte: 3.77x as fast       522.0ms +/- 0.6%    138.6ms +/- 6.8%     significant
    bits-in-byte:      3.64x as fast       663.2ms +/- 1.2%    182.0ms +/- 5.0%     significant
    bitwise-and:       1.34x as fast       617.0ms +/- 3.8%    460.6ms +/- 3.7%     significant
    nsieve-bits:       4.19x as fast       718.2ms +/- 1.0%    171.6ms +/- 3.8%     significant

  controlflow:         1.73x as fast       337.2ms +/- 0.7%    195.2ms +/- 1.6%     significant
    recursive:         1.73x as fast       337.2ms +/- 0.7%    195.2ms +/- 1.6%     significant

  crypto:              2.55x as fast      1270.8ms +/- 0.5%    499.2ms +/- 2.1%     significant
    aes:               2.30x as fast       353.6ms +/- 0.4%    153.6ms +/- 1.4%     significant
    md5:               2.65x as fast       465.4ms +/- 0.6%    175.6ms +/- 2.4%     significant
    sha1:              2.66x as fast       451.8ms +/- 0.7%    170.0ms +/- 3.2%     significant

  date:                2.10x as fast      1742.6ms +/- 0.3%    831.6ms +/- 3.7%     significant
    format-tofte:      1.52x as fast       563.0ms +/- 0.3%    371.4ms +/- 3.4%     significant
    format-xparb:      2.56x as fast      1179.6ms +/- 0.3%    460.2ms +/- 4.1%     significant

  math:                1.86x as fast      1761.0ms +/- 0.4%    947.8ms +/- 7.0%     significant
    cordic:            2.59x as fast       968.8ms +/- 0.6%    373.4ms +/- 6.7%     significant
    partial-sums:      1.160x as fast      403.8ms +/- 0.5%    348.0ms +/- 7.4%     significant
    spectral-norm:     1.72x as fast       388.4ms +/- 0.9%    226.4ms +/- 7.1%     significant

  regexp:              *1.069x as slow*    427.8ms +/- 0.7%    457.4ms +/- 0.7%     significant
    dna:               *1.069x as slow*    427.8ms +/- 0.7%    457.4ms +/- 0.7%     significant

  string:              1.28x as fast      2165.6ms +/- 0.3%   1696.6ms +/- 4.8%     significant
    base64:            1.98x as fast       526.4ms +/- 0.2%    265.6ms +/- 10.7%     significant
    fasta:             1.33x as fast       579.8ms +/- 0.5%    434.6ms +/- 2.6%     significant
    tagcloud:          1.187x as fast      368.4ms +/- 1.6%    310.4ms +/- 2.8%     significant
    unpack-code:       -                   339.6ms +/- 0.5%    336.6ms +/- 4.7%
    validate-input:    -                   351.4ms +/- 0.2%    349.4ms +/- 5.2%
</pre>

<hr>
vs Opera 9.5b2:
<pre>
TEST                   COMPARISON            FROM                 TO             DETAILS

=============================================================================

** TOTAL **:           1.152x as fast    8538.4ms +/- 0.5%   7409.6ms +/- 4.3%     significant

=============================================================================

  3d:                  *1.110x as slow*   807.0ms +/- 1.6%    895.8ms +/- 7.5%     significant
    cube:              *1.37x as slow*    234.0ms +/- 3.6%    321.0ms +/- 9.9%     significant
    morph:             1.142x as fast     331.4ms +/- 2.2%    290.2ms +/- 5.9%     significant
    raytrace:          *1.178x as slow*   241.6ms +/- 1.5%    284.6ms +/- 6.7%     significant

  access:              1.193x as fast    1113.4ms +/- 0.8%    933.2ms +/- 4.8%     significant
    binary-trees:      *1.81x as slow*     83.0ms +/- 2.4%    150.4ms +/- 2.1%     significant
    fannkuch:          1.57x as fast      548.8ms +/- 0.2%    350.6ms +/- 1.9%     significant
    nbody:             *1.45x as slow*    243.0ms +/- 0.9%    353.2ms +/- 11.7%     significant
    nsieve:            3.02x as fast      238.6ms +/- 1.7%     79.0ms +/- 1.1%     significant

  bitops:              1.21x as fast     1154.0ms +/- 1.2%    952.8ms +/- 2.2%     significant
    3bit-bits-in-byte: *1.23x as slow*    113.0ms +/- 0.8%    138.6ms +/- 6.8%     significant
    bits-in-byte:      ??                 179.0ms +/- 2.4%    182.0ms +/- 5.0%     not conclusive: might be *1.017x as slow*
    bitwise-and:       1.21x as fast      557.2ms +/- 1.1%    460.6ms +/- 3.7%     significant
    nsieve-bits:       1.78x as fast      304.8ms +/- 2.1%    171.6ms +/- 3.8%     significant

  controlflow:         *1.72x as slow*    113.2ms +/- 2.1%    195.2ms +/- 1.6%     significant
    recursive:         *1.72x as slow*    113.2ms +/- 2.1%    195.2ms +/- 1.6%     significant

  crypto:              *1.070x as slow*   466.4ms +/- 1.2%    499.2ms +/- 2.1%     significant
    aes:               1.41x as fast      217.2ms +/- 1.0%    153.6ms +/- 1.4%     significant
    md5:               *1.41x as slow*    124.2ms +/- 2.9%    175.6ms +/- 2.4%     significant
    sha1:              *1.36x as slow*    125.0ms +/- 1.4%    170.0ms +/- 3.2%     significant

  date:                1.27x as fast     1054.0ms +/- 0.8%    831.6ms +/- 3.7%     significant
    format-tofte:      -                  378.2ms +/- 1.1%    371.4ms +/- 3.4%
    format-xparb:      1.47x as fast      675.8ms +/- 0.7%    460.2ms +/- 4.1%     significant

  math:                *1.36x as slow*    699.4ms +/- 0.7%    947.8ms +/- 7.0%     significant
    cordic:            *1.23x as slow*    302.8ms +/- 1.1%    373.4ms +/- 6.7%     significant
    partial-sums:      *1.34x as slow*    259.0ms +/- 1.7%    348.0ms +/- 7.4%     significant
    spectral-norm:     *1.65x as slow*    137.6ms +/- 1.8%    226.4ms +/- 7.1%     significant

  regexp:              2.24x as fast     1024.0ms +/- 0.5%    457.4ms +/- 0.7%     significant
    dna:               2.24x as fast     1024.0ms +/- 0.5%    457.4ms +/- 0.7%     significant

  string:              1.24x as fast     2107.0ms +/- 0.8%   1696.6ms +/- 4.8%     significant
    base64:            *1.40x as slow*    190.0ms +/- 2.4%    265.6ms +/- 10.7%     significant
    fasta:             -                  434.4ms +/- 1.3%    434.6ms +/- 2.6%
    tagcloud:          1.24x as fast      383.4ms +/- 0.7%    310.4ms +/- 2.8%     significant
    unpack-code:       2.63x as fast      883.6ms +/- 1.2%    336.6ms +/- 4.7%     significant
    validate-input:    *1.62x as slow*    215.6ms +/- 1.7%    349.4ms +/- 5.2%     significant
</pre>

<hr>
vs Firefox 3.0rc1:

<pre>
EST                   COMPARISON            FROM                 TO             DETAILS

=============================================================================

** TOTAL **:           *1.77x as slow*   4178.4ms +/- 2.0%   7409.6ms +/- 4.3%     significant

=============================================================================

  3d:                  *1.78x as slow*    502.8ms +/- 0.8%    895.8ms +/- 7.5%     significant
    cube:              *1.74x as slow*    184.2ms +/- 0.9%    321.0ms +/- 9.9%     significant
    morph:             *1.85x as slow*    156.6ms +/- 0.4%    290.2ms +/- 5.9%     significant
    raytrace:          *1.76x as slow*    162.0ms +/- 1.4%    284.6ms +/- 6.7%     significant

  access:              *1.63x as slow*    573.8ms +/- 5.1%    933.2ms +/- 4.8%     significant
    binary-trees:      *2.24x as slow*     67.2ms +/- 1.5%    150.4ms +/- 2.1%     significant
    fannkuch:          *1.58x as slow*    221.8ms +/- 0.6%    350.6ms +/- 1.9%     significant
    nbody:             *1.69x as slow*    209.0ms +/- 14.0%    353.2ms +/- 11.7%     significant
    nsieve:            *1.042x as slow*    75.8ms +/- 0.7%     79.0ms +/- 1.1%     significant

  bitops:              *2.43x as slow*    392.4ms +/- 1.3%    952.8ms +/- 2.2%     significant
    3bit-bits-in-byte: *2.13x as slow*     65.0ms +/- 1.4%    138.6ms +/- 6.8%     significant
    bits-in-byte:      *1.86x as slow*     97.6ms +/- 0.7%    182.0ms +/- 5.0%     significant
    bitwise-and:       *4.64x as slow*     99.2ms +/- 2.2%    460.6ms +/- 3.7%     significant
    nsieve-bits:       *1.31x as slow*    130.6ms +/- 2.7%    171.6ms +/- 3.8%     significant

  controlflow:         *3.75x as slow*     52.0ms +/- 0.0%    195.2ms +/- 1.6%     significant
    recursive:         *3.75x as slow*     52.0ms +/- 0.0%    195.2ms +/- 1.6%     significant

  crypto:              *2.02x as slow*    247.2ms +/- 0.7%    499.2ms +/- 2.1%     significant
    aes:               *1.68x as slow*     91.6ms +/- 1.2%    153.6ms +/- 1.4%     significant
    md5:               *2.28x as slow*     77.0ms +/- 0.0%    175.6ms +/- 2.4%     significant
    sha1:              *2.16x as slow*     78.6ms +/- 0.9%    170.0ms +/- 3.2%     significant

  date:                *1.74x as slow*    478.2ms +/- 0.9%    831.6ms +/- 3.7%     significant
    format-tofte:      *1.28x as slow*    290.6ms +/- 1.0%    371.4ms +/- 3.4%     significant
    format-xparb:      *2.45x as slow*    187.6ms +/- 1.0%    460.2ms +/- 4.1%     significant

  math:                *1.87x as slow*    507.8ms +/- 9.0%    947.8ms +/- 7.0%     significant
    cordic:            *1.93x as slow*    193.6ms +/- 0.6%    373.4ms +/- 6.7%     significant
    partial-sums:      *1.69x as slow*    205.8ms +/- 3.9%    348.0ms +/- 7.4%     significant
    spectral-norm:     *2.09x as slow*    108.4ms +/- 38.2%    226.4ms +/- 7.1%     significant

  regexp:              *1.36x as slow*    335.4ms +/- 4.7%    457.4ms +/- 0.7%     significant
    dna:               *1.36x as slow*    335.4ms +/- 4.7%    457.4ms +/- 0.7%     significant

  string:              *1.56x as slow*   1088.8ms +/- 0.6%   1696.6ms +/- 4.8%     significant
    base64:            *2.20x as slow*    120.8ms +/- 2.0%    265.6ms +/- 10.7%     significant
    fasta:             *1.71x as slow*    254.4ms +/- 0.8%    434.6ms +/- 2.6%     significant
    tagcloud:          *1.49x as slow*    208.4ms +/- 1.9%    310.4ms +/- 2.8%     significant
    unpack-code:       1.076x as fast     362.2ms +/- 2.4%    336.6ms +/- 4.7%     significant
    validate-input:    *2.44x as slow*    143.0ms +/- 0.9%    349.4ms +/- 5.2%     significant
</pre>
