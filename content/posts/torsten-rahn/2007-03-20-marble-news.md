---
title:   "Marble News"
date:    2007-03-20
authors:
  - torsten rahn
slug:    marble-news
---
<p>
My <a href=http://blogs.kde.org/node/2429>last blog entry</a> about Marble dates back a few months already, so I'd like to give a small update: Just recently I added a grid layer and a measure tool. For the curious here's how it looks like:

<br>
<a href="http://developer.kde.org/~tackat/marble/marble13.png"><img src="http://developer.kde.org/~tackat/marble/marble13-thumb.png" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marble/marble14.png"><img src="http://developer.kde.org/~tackat/marble/marble14-thumb.png" class="showonplanet" />
</a>
<br>
<p>I also started to work on Wikipedia integration which isn't done yet, though.
If you're still searching for a Google Summer of Code 2007 idea, then maybe Marble is for you. I suggested several Marble ideas on our new technology portal <a href=" http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas#MARBLE_-_Adding_a_2D_View">KDE TechBase</a>. But hurry up, the final deadline for GSoC 2007 application submissions is on Saturday already!
<p>BTW: Quaternion <a href="http://en.wikipedia.org/wiki/Slerp">SLERPs</a> rock ;-)
<p>
<!--break-->

 