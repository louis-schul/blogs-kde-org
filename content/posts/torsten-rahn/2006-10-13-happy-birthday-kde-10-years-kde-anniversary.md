---
title:   "Happy Birthday KDE! 10 Years Of KDE Anniversary"
date:    2006-10-13
authors:
  - torsten rahn
slug:    happy-birthday-kde-10-years-kde-anniversary
---
<p>Yesterday at 10:00 AM the president of the KDE e.V. Eva Brucherseifer welcomed the audience of the presentation track at the KDE anniversary event at the <a href="http://www.tae.de">Technische Akademie Esslingen</a> (TAE) in Ostfildern near Stuttgart, Germany. Keynote speakers were <a href="http://en.wikipedia.org/wiki/Matthias_Ettrich">Matthias Ettrich</a>, founder of the KDE project, as well as <a href="http://en.wikipedia.org/wiki/Klaus_Knopper">Klaus Knopper</a> of Knoppix distribution^wfame. During their presentations they looked back at KDE's successful past 10 years and they offered their thoughts about the future of KDE and <a href=http://www.fsf-europe.org>Free Software</a>. Jono Bacon, Canonical's community manager of Ubuntu / <a href="http://www.kubuntu.org/">Kubuntu</a>, congratulated KDE with his own presentation about Kubuntu and KDE. Jan Mühlig from <a href="http://www.relevantive.de/">Relevantive</a> and Daniel Molkentin, KDE e.V. talked about Usability and KDE 4. In the afternoon the audience of the presentation track met for a group photo and celebrated the event with sparkling wine and a big birthday cake. Even the dragon mascot of the project was present - this time made of marzipan, right on the top of the birthday cake.
<br>
<a href="http://developer.kde.org/~tackat/kde10party/kde10years1.jpg"><img src="http://developer.kde.org/~tackat/kde10party/kde10years1-thumb.jpg" class="showonplanet" /></a>
<br>
<a href="http://developer.kde.org/~tackat/kde10party/kde10years2.jpg"><img src="http://developer.kde.org/~tackat/kde10party/kde10years2-thumb.jpg" class="showonplanet" /></a>
<br>
<a href="http://developer.kde.org/~tackat/kde10party/kde10years3.jpg"><img src="http://developer.kde.org/~tackat/kde10party/kde10years3-thumb.jpg" class="showonplanet" /></a>
<br>
Right after the afternoon break the presentation track continued with a speech from Heinz-M. Gräsing from the city of Treuchtlingen who gave some insights about the successful migration of <a href=http://www.treuchtlingen.de/>Treuchtlingen</a> to KDE.
The presentation part of the anniversary was concluded by Knut Yrvin, community manager at Trolltech, who surveyed the KDE 10 Years raffle together with Eva Brucherseifer. As prizes Trolltech offered a <a href=http://www.qtopiagreenphone.com/>Qtopia Greenphone</a> and <a href=https://www.opensourcepress.de/>Open Source Press</a> offered Daniel Molkentin's <a href= http://www.amazon.de/Qt-4-Einfhrung-die-Applikationsentwicklung/dp/3937514120/ref=sr_11_1/302-0390506-7200839?ie=UTF8>new book about Qt4 programming</a>.
<p>
Further KDE people arrived at the TAE in the evening when people met in the cottage for a delicious big "italian" dinner. People clinked glasses later at midnight when the party started to last into the actual anniversary.
<p>
The KDE project would like to thank the speakers of the presentation track, the Technische Akademie Esslingen, <a href=http://www.trolltech.com>Trolltech</a> and Open Source Press for their support of the whole event. A special "thank you" goes to the confectioner who created that awesome marzipan dragon for the cake ;-) 
<p> And of course we would like to thank all the people who were not able to attend the event and who sent their nice wishes to the KDE Project. 
<p>
<!--break-->