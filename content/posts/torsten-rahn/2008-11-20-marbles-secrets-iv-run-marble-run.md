---
title:   "Marble's Secrets IV:  Run Marble, Run!"
date:    2008-11-20
authors:
  - torsten rahn
slug:    marbles-secrets-iv-run-marble-run
---
<p>KDE 4.2 is in bug fixing mode and so is <a href="http://edu.kde.org/marble">Marble</a>. Time to have a look at things that got implemented right in time for Marble 0.7: <b>Henry de Valence</b> has been one of the most active Marble core developers during the last few months: He has implemented several exciting Marble features already.</p>
<p>
For Marble 0.7 (KDE 4.2) he implemented <i>MarbleRunners</i>. This is a pretty nice feature, which is still a bit hidden, but that is probably going to change soon:</p>
<p><ol>
<li> Make sure you have internet -- Oh, ok, yes, I just wanted to make sure ... 
<li> Start Marble (e.g. from KDE 4.2 Beta1 or trunk) 
<li> Find the "Search" field and type in: Playmobil
<li> Wait for the results to arrive!
</ol>
<p>
What happens is that on pressing the Enter key Marble will send the search query to several "service" threads in the background called "MarbleRunners". These threads will return the result as soon as it is available. In this case the <a href="http://www.openstreetmap.org">OpenStreetMap</a>-Runner has returned four matching results from the OpenStreetMap-Server!</p>
<p>
<a href="http://developer.kde.org/~tackat/runmarblerun/runmarblerun1.jpg"><img src="http://developer.kde.org/~tackat/runmarblerun/runmarblerun1_thumb.jpg" class="showonplanet" /></a></p>
<p>
Ok, let's try another one: Imagine, you are somewhere in Finland on holidays (<i>"Tervetuloa!"</i>), it snows and you feel really hungry. In this situation Marble can be a true life-saver (<i>"Hyvää ruokahalua!"</i>):</p>
<p>
<a href="http://developer.kde.org/~tackat/runmarblerun/runmarblerun2.jpg"><img src="http://developer.kde.org/~tackat/runmarblerun/runmarblerun2_thumb.jpg" class="showonplanet" /></a></p>
<p>
The source code for the OpenStreetMap-Runner is based on source code by Jens-Michael Hoffmann who also created the OpenStreetMap integration for Marble. Henry de Valence has taken it and created a Marble runner out of it. Additionally Henry has created a Coordinate-Runner:
</p>
<p>
Enter e.g. 
<code>
46°14'00" N 06°03'00" E
</code>
<p>
<p>
or just:
<p>
<code>
46 14 00 N 06 03 00 E
</code>
<p>
<p>
and press the Enter key. The latter will almost immediately return a result from the GeoCoordinate-Runner and several results from the OpenStreetMap-Runner trailing in a few seconds afterwards:</p>
<p>
<a href="http://developer.kde.org/~tackat/runmarblerun/runmarblerun3.jpg"><img src="http://developer.kde.org/~tackat/runmarblerun/runmarblerun3_thumb.jpg" class="showonplanet" /></a></p>
<p>
The best thing is however that Henry has made a <a href="http://techbase.kde.org/Projects/Marble/RunnerHOWTO">HOWTO for creating MarbleRunners</a>. So now you can create your own <i>MarbleRunner</i> yourself! The example shows how easy it is! And the description ensures that you can do this <i>even if you are a beginner</i>.
<p>
<b>Just go <a href="http://techbase.kde.org/Projects/Marble/RunnerHOWTO">here</a> to get the HOWTO</b> and just look <a href="http://edu.kde.org/marble/obtain.php">here</a> to get instructions on how to compile Marble.</p> 
The best Runners that you'll come up with will get shipped with the next version of Marble (your chance to enter <a href="http://edu.kde.org/marble/getinvolved.php">history</a>)! 
<p>
I could imagine lots of runners: What e.g. about a <i>Wikipedia-Runner</i> (we don't have that one yet, ...)? What ideas do you have? Please let us know or even better: send us your patch! We are reachable via marble-devel@kde.org. Or join us on IRC ( #kde-edu, Freenode: irc.kde.org )! Of course we also appreciate all kinds of patches that improve the current MarbleRunners as well! 
</p>
<!--break-->
