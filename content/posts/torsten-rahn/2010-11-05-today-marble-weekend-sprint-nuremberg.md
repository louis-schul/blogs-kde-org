---
title:   "Today: Marble Weekend Sprint  in Nuremberg"
date:    2010-11-05
authors:
  - torsten rahn
slug:    today-marble-weekend-sprint-nuremberg
---
<p>The Marble Sprint Weekend is about to start at the <b><a href="http://www.basyskom.com">basysKom</a> Office in Nuremberg</b>. If you're around and if you're curious about development of the <a href="http://edu.kde.org/marble">Marble Virtual Globe</a> then don't hesitate to join us. This is your best chance to get involved! We'll have lots of interesting topics about Marble presented on Saturday. See our <a href="http://community.kde.org/Multimedia/MarbleWeekend">Marble Sprint Wiki</a> for more information. Thanks go to the <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.basyskom.com">basysKom</a> for sponsoring this event. 
<br><img src="http://developer.kde.org/~tackat/marble-shirt.jpg" class="showonplanet" />
<p>
<p>In other news <b><a href="http://peregrine-communicator.org/">Peregrine</a></b> got released today! See the basysKom <a href="http://www.basyskom.de/index.pl/peregrine_announced">announcement</a>. <b>Peregrine</b> is a crossplatform real-time communication client that integrates all daily needed communication services for VoIP, Video and Chat in one solution. I'll cover this release and project more extensively in a different blog entry.
<br><a href="http://peregrine-communicator.org"><img src="http://peregrine-communicator.org/images/logo.jpg" class="showonplanet" /></a>
<!--break-->
