---
title:   "Marble C++ Tutorial Part 2"
date:    2010-08-14
authors:
  - torsten rahn
slug:    marble-c-tutorial-part-2
---
<p><b>Marble 0.10.0</b> has been released as a major update last week together with <b>KDE SC 4.5</b>. As a user you might be interested in our <a href="http://blogs.kde.org/node/4306">Visual ChangeLog</a> which is also available in spanish over at <a href ="http://www.muylinux.com/2010/08/13/marble-un-desarrollo-prodigioso-se-renueva">muylinux.com</a>.

<p>But <i>Marble is also a library</i>. So it can be used as a widget in <a href="http://techbase.kde.org/Projects/Marble/MarbleUsedBy">other applications</a>. Today I'd like to show you how.
<p>In our <a href="http://blogs.kde.org/node/4297">previous tutorial</a> I already introduced you to the very first steps of <a href="http://doc.trolltech.com/widgets-tutorial.html">Qt Programming</a> and <b>Marble Programming</b>. Now the last few Summer days in Germany have been rather rainy. So in the second part of our tutorial I'd like to show you how to create a weather map! 

<p><b>Creating a weather map</b></p><br>
<p>We'd like to display a small weather map. So we need to modify the map defaults of <a href="http://api.kde.org/4.x-api/kdeedu-apidocs/marble/html/classMarble_1_1MarbleWidget.html">MarbleWidget</a>. And we need to turn on the satellite view, enable the clouds and enable the country border lines.

<p>Again <a href="http://api.kde.org/4.x-api/kdeedu-apidocs/marble/html/classMarble_1_1MarbleWidget.html">MarbleWidget</a> provides a convenient way to make these changes to the overall look and feel of the map.

<p>By default Marble shows a few info boxes: <b>Overview Map</b>, <b>Compass</b> and <b>ScaleBar</b>. But the size for the widget is very limited. Therefore we want to shrink the compass. And we want to get rid of all the clutter, so we turn off the Overview Map and the ScaleBar. In the source code the class <a href="http://api.kde.org/4.x-api/kdeedu-apidocs/marble/html/classMarble_1_1AbstractFloatItem.html"> AbstractFloatItem</a> is used to display all kinds of <b>Info Boxes</b>. All the Info Boxes are derived from the AbstractFloatItem class. Now we get a list of all the float items that are known to MarbleWidget and we go through it. Once we reach the float item which has got the name id <i>compass</i> we make all the changes we want to it (this has been simplified in Marble pre-0.11.0 where you will be able to access AbstractFloatItems directly via their nameId): 

<p><code>
#include <QtGui/QApplication>

#include <marble/global.h>
#include <marble/MarbleWidget.h>
#include <marble/AbstractFloatItem.h>

using namespace Marble;

int main(int argc, char** argv)
{
    QApplication app(argc,argv);

    // Create a Marble QWidget without a parent
    MarbleWidget *mapWidget = new MarbleWidget();

    // Load the Satellite View map
    mapWidget->setMapThemeId("earth/bluemarble/bluemarble.dgml");

    mapWidget->setProjection( Mercator ); 
    
    // Enable the cloud cover and enable the country borders
    mapWidget->setShowClouds( true );
    mapWidget->setShowBorders( true );
    
    // Hide the FloatItems: Compass and StatusBar
    mapWidget->setShowOverviewMap(false);
    mapWidget->setShowScaleBar(false);
    
    foreach ( AbstractFloatItem * floatItem, mapWidget->floatItems() )
        if ( floatItem && floatItem->nameId() == "compass" ) {
            
            // Put the compass onto the left hand side
            floatItem->setPosition( QPoint( 10, 10 ) );
            // Make the content size of the compass smaller
            floatItem->setContentSize( QSize( 50, 50 ) );
        }
    
    mapWidget->resize( 400, 300 );
    mapWidget->show();

    return app.exec();
}
</code></p>
<br>

<p>Save the code above as <tt>marble_weather.cpp</tt> and compile it:

<p><code>
 g++ -I /usr/include/qt4/ -o marble_weather marble_weather.cpp -lmarblewidget -lQtGui
</code></p>
<br>

<p>Instead of calling the compiler directly you can also create a <a href="http://doc.trolltech.com/qmake-tutorial.html">qmake project file</a>:

<p><code>
TEMPLATE = app
TARGET = marble_weather
DEPENDPATH += .
INCLUDEPATH += .
SOURCES += marble_weather.cpp
LIBS += -lmarblewidget 
</code></p>
<br>

<p>Store it as <tt>marble_weather.pro</tt> in the same directory and call 

<p><code>
qmake marble_weather.pro
make
</code></p>
<br>

<p>If things go fine, execute <tt>./marble_weather</tt> and you end up with a map application that displays clouds on top of a flat map: 

<p>
<br><img src="http://developer.kde.org/~tackat/marble_tutorial/lesson2.png" class="showonplanet" />

<p>That's all for today. In our third chapter we'll show how to load KML and GPX files into Marble. So stay tuned. If you need help join us on our mailing list <a href="http://edu.kde.org/marble/getinvolved.php">marble-devel@kde.org or on #marble</a> (IRC on Freenode). If you want to obtain the latest Marble source code have a look at <a href="http://edu.kde.org/marble/obtain.php">Marble's website</a>.

<p>If you are interested in more news about Marble then join us and feel welcome in our <a href="http://www.facebook.com/group.php?gid=346064806033">Marble Facebook Group</a>!
<!--break-->
