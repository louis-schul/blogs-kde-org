---
title:   "Google Summer of Code 2008 - Marble Projects"
date:    2008-03-26
authors:
  - torsten rahn
slug:    google-summer-code-2008-marble-projects
---
<p>
If you're a student and if you're still searching for a suitable GSoC 2008 idea then Marble might offer a chance to participate in a young, vivid and interesting project. We suggest that you have a look at the <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#Marble">Google Summer of Code Ideas</a> page. There are especially three ideas which seem to be exciting and very important:
<ul>
<li> <b>Vector Tiles</b>: We'd like to see a similar concept implemented for all geographical features as the one we are using for Textures: Usage of <a href="http://blogs.kde.org/node/3269">tiles</a>. Especially for vectors this would be interesting as we could live render e.g. <a href="http://www.openstreetmap.org">Open Street Map</a> or <a href="http://en.wikipedia.org/wiki/Vector_Map">VMap 0</a> data. 
<li> <b>Panoramio/Wikipedia photo support</b>: This would enable Marble to show thumbnails from georeferenced photos such as those from <a href="http://www.panoramio.com">Panoramio</a>, <a href="http://www.wikipedia.org">Wikipedia</a> (or whatever image source is suggested by the applicant). Technically this work would extend Marble's KML support (which is currently being ported to a new QXmlStreamReader based framework). 
<li> <b>OSM Annotations</b>: This project was suggested by Adriaan de Groot and Armijn Hemel and is meant to provide on-screen OSM Annotation support. Of course providing OpenStreetMap support is an important goal for Marble and the suggestions for this idea seem pretty reasonable and realistic for the given timeframe (and dealing with UMPC devices seems to promise lots of fun).
</ul>
Of course we appreciate any other ideas for Marble development as well. Creative ideas get extra bonus points. If you want to find out what Marble Development is about then I recommend that you check out my "Marble's Secrets" blog entries:
<ul>
<li><a href="http://blogs.kde.org/node/3269">Part I</a> was a Do-It-Yourself course about creating <a href="http://blogs.kde.org/node/3269">maps for Marble</a>. We've seen how Marble manages to even display features such as aerial photos or <a href="http://www.openstreetmap.org">OpenStreetMap</a>. 
<li><a href="http://blogs.kde.org/node/3272">Part II</a> showed how Marble paints the different map layers. We've also seen why Marble only needs very little disk space and memory for its default map - making it an ideal choice for solutions that involve little hardware resources (like the Asus EeePC and the OLPC).
<li><a href="http://blogs.kde.org/node/3275">Part III</a> looks beyond Marble's offline mode: It describes how Marble fetches its data from the internet.
</ul>
<p>
But hurry up: Deadline for GSoC 2008 student applications is on Monday!  
<p>
For help or questions you can join us on IRC ( irc.kde.org, #kde-edu ) or send a mail to our <a href="https://mail.kde.org/mailman/listinfo/marble-devel">mailing list</a>. 
<!--break-->
