---
title:   "Planning for CeBIT: Kubuntu CDs!"
date:    2006-02-14
authors:
  - torsten rahn
slug:    planning-cebit-kubuntu-cds
---
A new year -- a new chance to spread the word about our favourite desktop environment at linux events. In Germany I already had the pleasure to attend the <a href="http://www.skolelinux.de/wiki/Erkelenz2006">SkoleLinux Gathering 2006</a> in the sprawling city of Erkelenz. Carsten Niehaus, author of the popular <a href="http://dot.kde.org/1139779450/">award-winning</a> chemistry application Kalzium, held a nice Qt 4 Workshop as well as a presentation about KDE-EDU there. After the <a href="http://chemnitzer.linux-tage.de/2006/info/">Chemnitzer Linuxtage 2006</a> the next stop for a pretty huge event will be CeBIT 2006!
<p>
<img src="http://developer.kde.org/~tackat/kubuntubox.jpg" align=center class="showonplanet" />
<p>
 Visitors usually ask for CDs at the KDE booth and this time we are proud to be able to provide the user friendly <a href="http://www.kubuntu.org/">Kubuntu</a> operating system during <a href="http://www.linux-events.de/LinuxPark_2006/OpenBooth">CeBIT</a> fairs. Just recently <a href="http://distrowatch.com/weekly.php?issue=20050801#interview">Jonathan Riddell</a> sent me two large packages which contain almost 1500 Kubuntu CDs! A big "Thank You!" goes to <a href="http://www.iol.co.za/index.php?set_id=1&click_id=139&art_id=vn20050209102227763C337661">Mark</a> <a href="http://loktarogar.blogspot.com/2006/02/have-you-seen-this-man-lately.html">Shuttleworth</a> and <a href="http://www.canonical.com/">Canonical</a> for supporting KDE. And a special "Thank you!" to Jonathan Riddell for delivering this awesome fine product :jawdrop: !

If you are an experienced user from Germany and would love to help us to demo KDE and Kubuntu at CeBIT fairs (Hannover, 9-15 March 2006) don't hesitate to write an e-mail to our mailing list <a href="mailto:kde-promo@kde.org">kde-promo@kde.org</a>!

... To be continued ...
<!--break-->
