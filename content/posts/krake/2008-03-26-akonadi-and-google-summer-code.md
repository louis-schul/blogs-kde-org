---
title:   "Akonadi and the Google Summer of Code"
date:    2008-03-26
authors:
  - krake
slug:    akonadi-and-google-summer-code
---
Since the Akonadi related applications for GSoC so far have been mostly about interfacing with external PIM data storage systems, e.g. Google's web based apps, I'd like to a bit of advertising for two other things we'd really like to have somebody working on.

The first one is the test framework idea I added to <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#Akonadi">our list</a> (second item). True, testing and stuff related to testing are usually quite boring, but it is nevertheless necessary and would be a great help for any one working on or with Akonadi.

To not leave the fun aspect out of the equation, one idea how to implement it would be to create a pair of scriptable Akonadi Agent and Resource, e.g. through a <a href="http://api.kde.org/4.x-api/kdelibs-apidocs/kross/html/index.html">Kross</a> plugin, effectively making it possible to work on Akonadi managed PIM data from any language Kross has support for.

The second project I'd like to get more widespread publicity is basically a matter of extending the target audience by getting support for developers on other desktop environments or tool stacks.
The idea is to implement a GLib based Akonadi client library, i.e. an GLib equivalent to KDE's Akonadi client library libakonadi-kde.

Unless other KDE related GSoC projects this would not require C++ and Qt skills, but rather C and GLib skill.
<a href="https://blogs.kde.org/blog/432">Till Adam</a> would be available as a mentor, though we would really also have a GLib expert as an additional co-mentor or advisor.

Any student interested in either project can contact us on the kde-pim mailinglist or on IRC channel #akonadi on the freenode network.
<!--break-->
