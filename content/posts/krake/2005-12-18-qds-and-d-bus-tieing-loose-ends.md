---
title:   "QDS and D-BUS, tieing up loose ends"
date:    2005-12-18
authors:
  - krake
slug:    qds-and-d-bus-tieing-loose-ends
---
Today I reworked the <a href="http://qds.berlios.de/">QDS</a> architecture so it doesn't require the application developer to delegate QApplication creation to QDS.

The main reason for this requirement has been the desire to use a KDE service implementation, which needs a KApplication instance.

The new architecture uses an out-of-process approach for implementing the desktop API access with <a href="http://www.freedesktop.org/Software/dbus">D-BUS</a> being used as the commmunication layer through the Qt3 binding backport I have blogged <a href="http://blogs.kde.org/node/1687">about</a> the <a href="http://blogs.kde.org/node/1681">last</a> few <a href="http://blogs.kde.org/node/1673">days</a>.

The new QDS-Daemon provides the services based on the same desktop wrapper plugins formerly used directly by the QDS library, but the D-BUS approach also opens the door for implemeting the service directly in the respective desktop environment.

Moreover it allows non-Qt applications to access the service as well :)

Currently the only implemented service is the Launcher, i.e. the interface to start the default handler for a file or an URL, with and without specifying the MIME type.

I hope it still compiles Windows and OS X, I had to rename a few functions and haven't had the opportunity to test there yet.

In case you are interested, these three archives contain the current <a href="http://www.sbox.tugraz.at/home/v/voyager/dbus+qt3.tar.bz2">D-BUS + Qt3 bindings</a> (1MB) <b>or</b> <a href="http://www.sbox.tugraz.at/home/v/voyager/qt3-20051217.tar.bz2">just the Qt3 bindings</a>(14KB) and a <a href="http://www.sbox.tugraz.at/home/v/voyager/qds-20051217.tar.bz2">QDS snapshot</a>(240KB)

You'll need --enable-qt3 when configuring D-BUS

Remember to set LD_LIBRARY_PATH and PATH (for the D-BUS daemon and tools) if necessary

If you have the qds-daemon running and connected to the D-BUS session bus you can use the launcher like this from commandline:

First make sure the daemon has loaded the launcher:

<pre>
$ dbus-send --print-reply --dest="de.berlios.QDS" /QDS "de.berlios.qds.Factory.InitLauncher"
</pre>

Then launch a file

<pre>
$ dbus-send --print-reply --dest="de.berlios.QDS" /QDS/Launcher "de.berlios.qds.Launcher.LaunchFile" string:"/tmp/test.html"
</pre>

Or an URL

<pre>
$ bus-send --print-reply --dest="de.berlios.QDS" /QDS/Launcher "de.berlios.qds.Launcher.LaunchURL" string:"http://www.kde.org/"
</pre>
<!--break-->
