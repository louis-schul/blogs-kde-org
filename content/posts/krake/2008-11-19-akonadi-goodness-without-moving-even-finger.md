---
title:   "Akonadi goodness without moving even a finger"
date:    2008-11-19
authors:
  - krake
slug:    akonadi-goodness-without-moving-even-finger
---
Sound like magic you say?

I'd say you're right!

The White Wizard (also known as Volker Krause) has embedded a powerful spell in the KResource framework which summons a golem (also known as kres-migrator) and commands it to carefully transform your contact and calendar resources into a respective Akonadi setup.

As of last night the magical barrier holding the spell's power has been removed, so that all brave souls testing our snapshots and the upcoming 4.2 beta1 can help us improve the minion's capabilities to everbody's satisfaction.

Since a carefully crafted illusion is more popular with the common folks than just songs and stories, lets see what we can do about that.

<img src="https://blogs.kde.org/files/images/kaddressbook-before.png" alt="address book before migration" title="address book before migration"  class="image image-_original " width="755" height="684" />
KAddressBook when started with a traditional setup: three KResource plugins for different data backends.


<img src="https://blogs.kde.org/files/images/start.png" alt="Akonadi startup progress" title="Akonadi startup progress"  class="image image-preview " width="400" height="68" />
Progress of Akonadi server startup

<img src="https://blogs.kde.org/files/images/migrator.png" alt="migration report" title="migration report"  class="image image-_original " width="666" height="361" />
Report of the migration results: The "dir" and "LDAP" plugin based resources have been migrated using our <a href="http://blogs.kde.org/node/3286">compatibility bridges</a>, the "file" plugin based resource has been migrated to the native Akonadi VCard handler.


<img src="https://blogs.kde.org/files/images/kaddressbook-after_0.png" alt="addressbook after migration" title="addressbook after migration"  class="image image-_original " width="755" height="684" />
KAddressBook after the migration: a new Akonadi based plugin containing all three data sources as "folders". The three traditional plugins are still present but have been deactivated.


Beware! The dark side hasn't slept either!
We have heard rumors that the following jinx can put an unholy barrier around our spell, effectively keeping the poor victim deprived of any Akonadi goodness:

<pre>% kwriteconfig --file kres-migratorrc --group Migration --key Enabled --type bool false</pre>
<!--break-->
