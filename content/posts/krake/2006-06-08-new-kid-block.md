---
title:   "New kid on the block"
date:    2006-06-08
authors:
  - krake
slug:    new-kid-block
---
Yesterday I got my new laptop. I got it in the meanest possible way I could have imagined: two minutes before I had to leave for work
<b>sigh</b>

I barely had time to check the contents of the box and it didn't matter what kind of things I got to work on at the office, most of my thoughts were occupied with my precious.

It's a VAIO SZ1XP, according to the reviews a subnotebook, but its keyboard feels huge compared to my current laptop, a HP Omnibook 6100 (15" model).

Since it its specs indicate that it is meant for work, Sony would have had a difficult time deciding which operating system to install, since they can't possibly imagine what kind of work their customers are going to use it for.

But Sony is quite clever and they remembered that their core strength is entertainment. So instead of installing a general purpose operating system they installed a variant of Microsoft's XP entertainment system.
Even its limited capabilities are capable of showing that all hardware works within acceptable parameters and I read that by installing some third party tools, albeit through a crude mechanism that requires downloading and installing each of them separately, you can even use it for common internet tasks like web browsing, email and instant messaging.

However, since I intend to use it for real work, in my case software development, I am going to install a general purpose operating system as well.
Before I go on, I will now test the VAIO recovery tool, to check if it works like documented, i.e. has the option to just restore the pre-installed entertainment pack and not touch the main operating system while doing that.

Stay tuned!
<!--break-->
