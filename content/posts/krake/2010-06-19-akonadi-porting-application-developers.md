---
title:   "Akonadi porting for application developers"
date:    2010-06-19
authors:
  - krake
slug:    akonadi-porting-application-developers
---
After all the blogging about our (as in KDE PIM developers) Akonadi porting, I thought I'll address a couple of things other application developer might consider for the next release cycle.

The most prevalent use of PIM API seems to be <a href="http://api.kde.org/4.x-api/kdepimlibs/kabc/classKABC_1_1StdAddressBook.html">KABC::StddAddressBook</a>.

<b>Find By Uid</b>

A common use case is to look for a contact object by its identifier, acquired by user selection somewhen in the past.
The code to do that usually looks like this:
<pre>
KABC::AddressBook *addressBook = KABC::StdAddressBook::self();

KABC::Addressee contact = addressBook->findByUid( uidString );
</pre>

This is quite easy but it always has a couple of issues, mainly the call to <pre>KABC::StdAddressBook::self()</pre> because its first invocation will load the whole address book and it will do so synchronously.
Potentially blocking the application for the whole duration or, sometimes even worse, introducing unexpected reentrancy in single threaded applications due to use of nested event loops.

<em>Note:</em> while the same sequence is possible with Akonadi, due to Akonadi jobs being based on KJob and it having the capability of "blocking" by use of a nested event loop, I really recommend that you use the porting effort as an opportunity to get rid of such emulated synchronousity whenever possible.

The equivalent using Akonadi API looks like this
<pre>
Akonadi::ContactSearchJob *job = new Akonadi::ContactSearchJob( this );
job->setQuery( Akonadi::ContactSearchJob::ContactUid, uidString );

connect( job, SIGNAL( result( KJob* ) ), SLOT( contactSearchResult( KJob* ) ) );
</pre>
and
<pre>
void contactSearchResult( KJob *job )
{
  if ( job->error() != 0 ) {
    // error handling, see job->errorString()
    return;
  }

  Akonadi::ContactSearchJob *searchJob = qobject_cast<Akonadi::ContactSearchJob*>( job );

  const KABC::Addressee::List contacts = searchJob->contacts();
}
</pre>

Also see API docs for <a href="http://api.kde.org/4.x-api/kdepimlibs/akonadi/contact/classAkonadi_1_1ContactSearchJob.html">ContactSearchJob</a>

<b>Find by Email</b>

Another use case I came across is searching by Email. The KABC code usually looks pretty identical:
<pre>
KABC::AddressBook *addressBook = KABC::StdAddressBook::self();

KABC::Addressee contact = addressBook->findByEmail( emailString );
</pre>

Unsurprisingly, the Akonadi code looks also quite similar to the previous use case:
<pre>
Akonadi::ContactSearchJob *job = new Akonadi::ContactSearchJob( this );
job->setQuery( Akonadi::ContactSearchJob::Email, emailString );

connect( job, SIGNAL( result( KJob* ) ), SLOT( contactSearchResult( KJob* ) ) );
</pre>

The code for the result slot is the same.

<b>Who Am I</b>

Another use case for StdAddressBook is not directly available in Akonadi:
<pre>
KABC::AddressBook *addressBook = KABC::StdAddressBook::self();

KABC::Addressee contact = addressBook->whoAmI();
</pre>

Even this had its shortcomings, because this depends on the users adding themselves to the address book and marking the respective entry as "this is me".

The concrete use cases I've seen this deployed for are getting the user's full name and/or their email address.

It might be better to use <a href="http://api.kde.org/4.x-api/kdepimlibs/kpimidentities/classKPIMIdentities_1_1IdentityManager.html">KPIMIdentities::IdentityManager</a> instead.
It contains the identity users can configure in the Systemsettings module for personal information, so if there is data yet, you could embed the respective KCM in a dialog and let the user configure this centrally for all of KDE.

<b>Modifying a contact</b>

Using KABC::StdAddressBook modifying a contact (for example one retrieved by findByUid()) worked like this:
<pre>
addressBook->insertAddressee( contact );

KABC::Ticket *ticket = addressBook->requestSaveTicket();
if ( !ticket ) {
  // error
} else if ( !addressBook->save( ticket ) ) {
    // error
    addressBook->releaseSaveTicket( ticket );
  }
}
</pre>

With Akonadi, you will need the Akonadi::Item or at least the item's identifier.
Assuming you retrieved the contact using ContactSearchJob, consider keeping the item or Item::id() around, see <pre>ContactSearchJob::items()</pre> (inherited).

Lets say you have just the item's identifier:
<pre>
Akonadi::Item item( itemId );
item.setPayload<KABC::Addressee>( contact );
item.setMimeType( KABC::Addressee::mimeType() );

Akonadi::ItemModifyJob *job = new Akonadi::ItemModifyJob( item );

connect( job, SIGNAL( result( KJob* ) ), SLOT( contactModifyResult( KJob* ) ) );
</pre>

Slot code similar to the one for search.

<b>Buildsystem adjustments</b>
Since you are already linking against KDE PIM Libs classes, you already have the necessary <pre>find_package( KdepimLibs 4.5 REQUIRED )</pre>

However, your target_link_libraries need to be extended to contain
<pre>
${KDEPIMLIBS_AKONADI_LIBS}
</pre>

for base things like Akonadi::Item and

<pre>
${KDEPIMLIBS_AKONADI_CONTACT_LIBS}
</pre>
for things like <pre>Akonadi::ContactSearchJob</pre>.

Includes for Akonadi core classes are like
<pre>
#include &lt;Akonadi/Item&gt;
</pre>

for contact related specializations they are like
<pre>
#include &lt;Akonadi/Contact/ContactSearchJob&gt;
</pre>

In both cases also available in lowercase variants with ".h" suffix.

<b>Other useful classes</b>

Our Akonadi porting efforts have resulted in a couple of useful classes for various aspects of dealing with PIM data.
For contacts such classes would be: <a href="http://api.kde.org/4.x-api/kdepimlibs/akonadi/contact/classAkonadi_1_1ContactEditor.html">ContactEditor</a> (also available as a dialog), <a href="http://api.kde.org/4.x-api/kdepimlibs/akonadi/contact/classAkonadi_1_1ContactViewer.html">ContactViewer</a> (again also available as a dialog), <a href="http://api.kde.org/4.x-api/kdepimlibs/akonadi/contact/classAkonadi_1_1ContactsTreeModel.html">ContactsTreeModel</a> and others.

As usual: in case of questions, our IRC channel is #akonadi, freenode network, or send an email to our mailinglist (kde-pim@kde.org).

Also available on <a href="http://techbase.kde.org/Development/AkonadiPorting">TechBase</a>
<!--break-->
