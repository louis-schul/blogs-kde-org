---
title:   "KDE PIM Meeting 2009 Debriefing"
date:    2009-01-11
authors:
  - krake
slug:    kde-pim-meeting-2009-debriefing
---
So I am in my hotel room here in Nürnberg now and a bit tired from the long days over the weekend, however I think a bit of coverage of the KDE PIM meeting is in order.

Tom already covered some of the aspects of the <a href="http://www.omat.nl/drupal/content/Random-bits-Osnabruck-day-01">first two days</a> so he probably will do another blog about the third one as well. Therefore I'll concentrate more on the overall aspects.

The annual PIM meeting is quite different to the two Akonadi developer sprints we had during the year, because while there is still quite some hacking going on, a lot of time is spent on planning for future releases, discussing strategies evaluating the current situation and so on.

It is also a great time to share knowledge among participants like the introduction to the project our French guests are doing as part of an University course or hearing about the code quality project Bertjan is working.
Obviously Volker got loads of questions on Akonadi details, but I also tried to be of some help to our application developers who were pondering porting strategies.

If you are interested in details, check our <a href="http://techbase.kde.org/Projects/PIM/Meetings/Osnabrueck_7#Saturday">meeting page</a> on TechBase.

The travelling to Nürnberg was actually quite interesting.
During the weekend I already got to know that Cornelius would be on the same trains, so I was already looking forward to not travelling alone.
As it turned out, both Frank and Tobias were on the same train as far as Hannover.

We managed to go back in time and take the train scheduled for 14:08 at about 16:15 :)
Unfortunately this didn't help us in any way, so Cornelius and me still missed our connection from Hannover to Nürnberg and we had to wait for about an hour to get a less ideal connection.
Forunately Deutche Bahn compensated this inconvenience with a voucher <b>and</b> we managed to get upgraded to Frist Class on the next ICE to Würzburg <b>and</b> we used the opportunity to have a beer at Hannover main station ;)

I love it when things go wrong, or not as planned, in a positive way.
<!--break-->
