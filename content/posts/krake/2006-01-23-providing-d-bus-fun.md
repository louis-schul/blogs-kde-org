---
title:   "Providing D-BUS Fun"
date:    2006-01-23
authors:
  - krake
slug:    providing-d-bus-fun
---
Last week Cornelius blogged about having some <a href="http://blogs.kde.org/node/1752">D-BUS Fun</a> using the Qt3 D-BUS bindings I had backported from Harald Fernengel's Qt4 bindings.

Later that week an email conversation between Cornelius, Will Stephenson and myself resulted in the decision to put the bindings into KDE's SVN repository for shared development.
They do now live <a href="http://websvn.kde.org/branches/work/dbus-qt4-qt3backport/">here</a> and eagerly await your contributions :)

In the meanwhile I have finished their <a href="http://www.englishbreakfastnetwork.org/apidocs/apidox-playground/work-apidocs/dbus-qt4-qt3backport/html/index.html">API DOX</a> and <a href="http://people.fruitsalad.org/adridg/bobulate/">Mr. APIDOX</a> himself put them up at EBN.
Thanks Adriaan!

A first release of the bindings suitable for the current D-BUS release (0.60) can be found <a href="http://www.sbox.tugraz.at/home/v/voyager/dbus-1-qt3-0.2.tar.bz2">here</a> (about 400KB)
<!--break-->