---
title:   "Portland - lets do it"
date:    2006-02-26
authors:
  - krake
slug:    portland-lets-do-it
---
<a href="http://lists.freedesktop.org/archives/portland/2006-February/000131.html">Lubos woke</a> the dormant Portland project by creating a first implementation of a possible desktop adapter API.

Having some experience due to my work on <a href="http://qds.berlios.de/">QDS</a> I volunteered to put some work into it as well, especially KDE related backend code.

One of the <a href="http://www.freedesktop.org/wiki/PortlandIntegrationTasks">Portland tasks</a> is <a href="http://www.freedesktop.org/wiki/PortlandTaskAddressbook">addressbook related</a>, e.g. querying for contact information and such things.

The task description is not very detailed so I thought that by adding some query and access functions would give interested developers the chance to provide feedback rather than coming up with a specification without knowning what we, the desktop enviroment developers, can implement without too much trouble.

Last week I quickly added a couple of addressbook related functions to Lubos' code and implemented the KDE backend for them.

However, DE integration APIs are only good for evaluation if you can at least test with two backends, so I checked how difficult it would be to do a GNOME backend for it as well.

But unfortunately the EDS client API is, uhh, under-documented :)
I better leave this to someone who has already worked with it, but I think the simple semantics of the dapi addressbook functions should be easy to implemented with EDS as well.

Speaking of GNOME: I used some of the GNOME related code in QDS to <a href="http://lists.freedesktop.org/archives/portland/2006-February/000154.html">bootstrap</a> a dapi_gnome daemon, or rather a copy of Lubos' generic daemon extended to use GNOME-VFS functions for opening and executing URLs.

Lets see if this really ugly abuse ;) of GNOME's API lures some of their developers into the trap^W fun

P.S.: According to ajax this should make it to planet-fdo, so hello to all readers there
<!--break-->