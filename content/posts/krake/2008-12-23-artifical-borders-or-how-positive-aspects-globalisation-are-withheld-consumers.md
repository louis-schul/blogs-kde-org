---
title:   "Artifical borders or how positive aspects of globalisation are withheld from consumers"
date:    2008-12-23
authors:
  - krake
slug:    artifical-borders-or-how-positive-aspects-globalisation-are-withheld-consumers
---
The human race has been living and thinking in terms of borders for a long time, since the first people decided to settle down instead of keeping a nomadic life style.

Ideally each individual's respect for everyone else's needs and personal boundaries would be sufficient to control access to shared resources and such things, however I am not holding my breath that this would happen any time soon (soon as in terms if centuries).

Therefore we happen to need borders and ways to enforce them, such as protecting political borders through military forces, legal borders through law enforcement, etc.
Sometimes people separated by such borders decide that whatever reason they had to create these in the first place are no longer sufficiently imporant (if they exist at all) and work on removing these obstacles reap the benefits of what basically is a reunion.

I know that all of you reading this blog understand this very well, because we, communities like KDE, are an unprecedented reunion of people separated by all kind of borders.
This is why people within these communities are or really should be offended by any attempt to enforce borders that have actually been removed even outside our own spheres.

Attempts like artificially segmenting markets for the sole benefit of big corporations which want to keep control over what consumers are allowed to purchase beyond their financial restrictions.

Obviously these corporations are really good at hiding this, quite often by mollifying consumers using half-truths.
One such half-truths is the often encountered "localization problem", i.e. the extra effort of getting things translated, correctly layouted and formatted, etc.

The nice aspect of this half-truth is the true part, i.e. that it is quite some considerable work to do this properly and that basically everybody knows that first hand, e.g. by having been abroad in a country with a different native language.

The false part is implied consequences.

For example lets have a look how this is applied on TV shows.
It usually takes a year(!) or even longer after the original airing in North America until the episodes reach Europe.

True: doing good translations and dubbing takes time

False: it doesn't take a year (or longer) and episodes aren't broadcasted right after being shot either

One actual reason: "test drive" in one market and quit if it tanks. The studios don't want to get into a situation where one market likes the show and another doesn't, i.e. actually let the consumers decide, $DETIY beware!

"Unfortunate" results:
<ul>
<li>consumers outside the "test" market don't even get a chance of liking something that didn't fit the expectations of the test group

<li>genre or franchise fans outside the "test" market are either cut off from participating in international fan communities or need to use illegal back channels to stay up-to-date
</ul>

Or as an more on-topic example for an IT related blog, check how OEMs like Dell abuse this excuse to sell certain configuration in one country but deny delivery to customers of a neigbouring country dispite this two countries having <b>exactly</b> the same localization and despite all trade barriers having been abolished between those countries for years.

The very same companies will happily move production to countries with cheap labour, consolidate preivously distributed support centers, etc.
In other words they will take the benefits of globalization and not let you have any of it!

Therefore it is imporant that we as a really global community demonstrate continuously that thinking and acting globally is not only doable but also possible to do in a timely manner!

Because despite being largely based on voluntary work, the KDE community manages to release products and their localizations synchronously across the globe.
<!--break-->