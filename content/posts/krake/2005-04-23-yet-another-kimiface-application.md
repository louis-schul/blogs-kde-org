---
title:   "Yet another KIMIface application"
date:    2005-04-23
authors:
  - krake
slug:    yet-another-kimiface-application
---
KDE, the integrative desktop environment.

I can't remember who came up with this but it is so true.
By providing interfaces (kdelibs/interfaces) KDE enables choice without negatively affecting KDE interoperability.

One fo those interfaces is kde:KIMIface, a set of DCOP functions KDE applications can use to talk to instant messenger applications in an uniform way.

About a week ago I finished an implementation of this interface for the KDE GUI plugin of <a href="http://www.licq.org/">Licq</a>
A couple of screenshots showing it in action can be found <a href="http://www.sbox.tugraz.at/home/v/voyager/licq-kimiface/">here</a>

Kudos to Will Stephenson and the other Kopete developers for thinking the KDE way by creating the interface and thus enabling users to choose their preferred IM application while not loosing any KDE integration!

Now we just need to come up with some cool ideas on how to use all this :)