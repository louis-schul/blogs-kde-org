---
title:   "Secondary development"
date:    2006-10-22
authors:
  - krake
slug:    secondary-development
---
Most development I do lately is not directly visible in KDE's source repository, but rather development for other projects connected to or interface with KDE.

Since I am a Debian user, a very happy one :), I am following the debian-qt-kde mailinglist, to know about issues our Debian packagers might encounter.
Sometimes this requires just adding some information to a bug report, sometimes it requires doing some research in bug tracking systems and code respositories and sometimes it requires coding.

One of those which involve coding has been <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=350589">this one</a>: removing the requirement for the SuperKaramba package to depend on XMMS by dlopen'ing libxmms instead of having it as a build-time option.

Those involving doing bug research are obviously less fun, nevertheless quite educating. The one I am currently on is <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=392807">this one</a>:
the packages for KDE breaking KDE's compliance with the freedesktop.org menu specification.
Turns out the reason for the breakage is a hotfix/workaround for a conflict with a GNOME package ages ago!

Since it is very likely that the respective GNOME package has also been fixed by now, the correct KDE behavior can be restored in time for the Debian Etch release :)

So, off to check if I can fix the <a href="https://bugs.freedesktop.org/show_bug.cgi?id=8694">xdg-utils bug</a> I discovered a few days ago.

P.S.: if you know somebody with knowledge about KURL internals, maybe we can even fix <a href="http://bugs.kde.org/show_bug.cgi?id=135852">this problem</a> 
<!--break-->
