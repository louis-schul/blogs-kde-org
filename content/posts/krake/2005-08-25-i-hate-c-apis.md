---
title:   "I hate C APIs"
date:    2005-08-25
authors:
  - krake
slug:    i-hate-c-apis
---
Well, not all of them.
Those that create tons of structs for datatypes and even more functions to work with them.
I hate them because they almost look like sane OOP APIs but are awfully to use compared to
what real OOP languages would offer

Consider this example:<!--break-->
<code>
void foo()
{
    CFStringRef cfstring =
        CFStringCreateWithCString(NULL, "http://wwwkde.org/", kCFStringEncodingUTF8);

    CFURLRef cfurl = CFURLCreateWithString(NULL, cfstring, NULL);

    // use the URL

    free((void*)cfurl);
    free((void*)cfstring);
}
</code>

Compare that to this:
<code>
void foo()
{
    QUrl url("http://www.kde.org");

    // use the URL
}
</code>
The C++ compiler can convert the const char* into an QUrl by going via the QString
constructor without having to be told.

It also takes care about freeing the objects once they are out of scope.
Oh well.

Anyway, in case you are wondering about my above Carbon exercise:
I have been trying to implement the Launcher service for <a href="http://www.sbox.tugraz.at/home/v/voyager/qds/">QDS</a> for the Mac OS X platform.

Not having direct access to such a system made that a little awkward, but a friend has been really
helpful and tried to compile anything I sent his way ;)

Unfortunately neither of us was able to figure out why the above code results in weird linker
errors. Googling brought up several other reference for similar problem, seems it is not possible
to mix Carbon and Qt code or, more precisely, ceased to work on more recent OS X versions.

So if you are looking for a challenge and have access to a Mac with Qt3, get <a href="http://www.sbox.tugraz.at/home/v/voyager/qds/downloads/qds-0.4.1.tar.bz2">qds-0.4.1.tar.bz2</a>
and give it a shot. Your help will be greatly appreciated!
