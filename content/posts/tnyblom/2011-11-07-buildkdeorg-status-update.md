---
title:   "build.kde.org - status update"
date:    2011-11-07
authors:
  - tnyblom
slug:    buildkdeorg-status-update
---
A long over due update on the status of build.kde.org.

It now builds most of the old core KDE modules. Both the stable, currently the KDE/4.7 branch and the master branch. Sadly I haven't gotten Qt5 or kdelibs frameworks branch to build as a result nepomuk-core doesn't build either.

There is also an IRC channel for notifications on the build status, #kde-builds.

A second build node has been added that kicks in when the master node is busy, as this node is low on bandwidth it is set to only try and build if the master has been busy for some time.

Current list of jobs:
* akonadi (1.6 master)
* extra-cmake-modules (master)
* kactivities (master)
* kde-baseapps (KDE/4.7 master)
* kde-runtime (KDE/4.7 master)
* kde-workspace (KDE/4.7 master)
* kdelibs (frameworks KDE/4.7 master)
* kdepim-runtime (KDE/4.7 master)
* kdepim (KDE/4.7 master)
* kdepimlibs (KDE/4.7 master)
* kdeplasma-addons (master)
* libqzeitgeist (master)
* nepomuk-core (master)
* qt5 (master)