---
title:   "QtScript Web Browser"
date:    2008-03-24
authors:
  - rich
slug:    qtscript-web-browser
---
As some of you may have seen, Kent has released the QtScript binding generator on Troll Tech labs. I've been playing with the code for a bit, and as with KJSEmbed one of the first tests was to make sure you could use it to write a simple web browser. Kent recently added support for QUiLoader to the bindings and as a result, I can use the QWebView designer plugin to make things simple. The result is a basic web browser in less than 10 lines of javascript:
<pre>
var loader = new QUiLoader(null);
var file = new QFile("browser.ui");
file.open(QIODevice.OpenMode(QIODevice.ReadOnly, QIODevice.Text));

var web = loader.load(file,null);
web.show();

QCoreApplication.exec();
</pre>
There's also a very simple designer ui file that just contains the web view and a layout. Here's the obligatory screenshot:
<img src="http://xmelegance.org/devel/qtscript-browser.png">
<!--break-->
