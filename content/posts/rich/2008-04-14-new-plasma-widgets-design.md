---
title:   "New Plasma Widgets Design"
date:    2008-04-14
authors:
  - rich
slug:    new-plasma-widgets-design
---
This is the draft design for a new widget API for plasma, it will be appearing on techbase later, but here's what we're thinking.

<h2>General Notes</h2>

The intention of this API is to provide a very simple way for users to create
plasma applets. The API can be used both for scripting and from C++. For the
simple javascript API, this API will be all that is provided allowing us to
run untrusted applets as they will not have access to any dangerous
facilities.

<p>

From C++ or more advanced scripting APIs (such as the 'full' Javascript
bindings) you can gain access to a pointer to the underlying Qt widget
contained within the QGraphicsProxy allowing you to access all of its
methods. For most simple uses however, this should be unnecessary as you can
do most of the customisation using the Qt stylesheet facilities.

<p>

All widgets have a stylesheet, which is a string property containing a Qt
stylesheet. This allows for simple but powerful configuration of the widgets -
for example you can configure the alignment of the text in the label using the
text-align property, or the font using the font property. Using this you can
do some extremely advanced interfaces without having to learn how Qt works.

<p>

A nice side effect of the way this API is defined is that it is possible to
implement the simple widget API inside a web browser using HTML, javascript
and HTML stylesheets to provide the implementation.
<h2>API Reference</h2>

<h3>Label</h3>

<h4>Constructors</h4>

<ul>
<li>Label(Widget)
</ul>

<h4>Properties</h4>
<ul>
<li>Widget parent
<li>String text
<li>String image
<li>String stylesheet
<li>QWidget nativeWidget
</ul>

Note that the image property is a string and specifies the name of the
image within the plasmoid package. The image can be either a bitmap (in any
supported format) or an SVG image.

<h3>PushButton</h3>

<h4>Constructors</h4>

<ul>
<li>PushButton(Widget)
</ul>
<h4>Properties</h4>
<ul>
<li>Widget parent
<li>String text
<li>String image
<li>String stylesheet
<li>QWidget nativeWidget
</ul>

<h4>Signals</h4>
<ul>
<li>clicked()
</ul>

<h3>CheckBox</h3>

<ul>
<li>CheckBox(Widget)
</ul>
<h4>Properties</h4>
<ul>
<li>Widget parent
<li>String text
<li>String image
<li>String stylesheet
<li>bool checked
<li>QWidget nativeWidget
</ul>

<h4>Signals</h4>
<ul>
<li>toggled(bool)
</ul>

<h3>RadioButton</h3>

<ul>
<li>RadioButton(Widget)
</ul>

<h4>Properties</h4>
<ul>
<li>Widget parent
<li>String text
<li>String image
<li>String stylesheet
<li>bool checked
<li>QWidget nativeWidget
</ul>
<h4>Signals</h4>
<ul>
<li>toggled(bool)
</ul>

Note that a RadioButton must exist within a ButtonGroup, the group box ensures
that only one radio button is set at any one time.

<h2>Widgets To Do</h2>

<ul>
<li>WebContent
<li>GroupBox
<li>ButtonGroup
<li>ComboBox
<li>LineEdit
<li>TextEdit
<li>Meter
<li>Graph
<li>Throbber
<li>Simplified versions of the QGraphicsLayout classes
</ul>

<h2>Non-Widgets To Do</h2>

<ul>
<li>URLOpenner
</ul>
<!--break-->