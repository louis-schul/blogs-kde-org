---
title:   "The trouble with DConf"
date:    2005-04-15
authors:
  - rich
slug:    trouble-dconf
---
There's been lot of discussion on the XDG list about a proposal called 'DConf'. Unfortunately, it seems to me to have missed some rather important issues. The most obvious question in any sort of common configuration is system is 'What configuration can be shared?'.

It can't be application configuration because this is generally tied into the functionality offered by that particular application - unless two apps share exactly the same feature set they can't really share configuration. For example if application A has 3 ways of viewing a piece of data, and app B has 4 then as soon as you choose option 4 you can no longer share the configuration.

Ok, lets choose something more easily defined - HTTP proxy settings. The semantics of this are pretty well defined, right? Well, actually no. Many proxies need a user name and password, and can use one of many authentication mechanisms (eg. basic password, NTLM, Kerberos). Again, unless the functionality offered by the apps is the same we can't do it.

The above ignores completely the problem of several apps using configuration in ways that look the same, but offer different semantics. When I was working on EDIF, we dealt with this issue by using information modelling to define the agreed semantics of the file format (and provided an extension mechanism for vendor specific extensions). The file format itself is easy - definining the semantics in a meaningful way was the result of years of effort.

So, I fail to understand what DConf is supposed to achieve. It seems to be defining a mechanism to express semantics that have not been defined, and a way to interoperate with them. This is putting the cart a long way in front of the horse.
