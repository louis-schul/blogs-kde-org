---
title:   "Nat, Miguel, FUD and other buzzwords"
date:    2004-03-29
authors:
  - rich
slug:    nat-miguel-fud-and-other-buzzwords
---
It's hard to see what's going on at Novell right now, and recent comments by Nat and Miguel aren't making things any clearer. They're claiming that they're not aware of any SuSE development using Qt other than YaST, ignoring all the SuSE sponsored KDE development (including new hires such as the developer of the KDE-OpenOffice GUI glue). Nat says he's 'Novell/SuSE Desktop Lead' hmm. I wonder could that be 'Novell/SuSE Ximian Desktop Lead' perhaps - after all he said he was VP of Novell Desktop Services a few months ago, missing out the crucial word Ximian which indicated the limitation of his scope.
<br>
<br>
Unfortunately Nat and Miguel don't seem concerned about confining what they say to the declared policy of the company they work for, perhaps they know something we don't, perhaps they just wish it was true?
<br>
<br>
If any of the Novell guys attending the Linux Expo UK would like to explain what on earth they're doing I would be most happy to listen, either as a UK KDE Representative, an editor of the Dot or a user and developer of KDE.
