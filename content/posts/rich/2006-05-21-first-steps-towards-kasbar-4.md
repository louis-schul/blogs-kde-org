---
title:   "First steps towards Kasbar 4"
date:    2006-05-21
authors:
  - rich
slug:    first-steps-towards-kasbar-4
---
Over the last few days I've started working a rewrite of Kasbar for Qt 4. This is a rewrite rather than a straight port because lots of the code in kasbar was there to work around limitations in X11 that will not apply once we have a composition manager. In addition, the old code was massively over complex because the features got added over time until the original design was swamped. I am however able to make use of some of the old code as basis for the new version, and many of the ideas will remain the same.

So far, things seem ok though I have hit some issues - it seems it is no longer possible to use QLCDNumber as a way to draw LCD numbers unless you actually use it as a widget. In Qt 3 you could use the drawContents() method to paint an LCD number onto a user provided QPainter, but that facility seems to have been lost. This means that the clock item I had in Kasbar 3 could not be easily implemented. If anyone knows how to handle this, please let me know.

The other issue that bothers me is that documentation on how to use the new QPainter APIs efficiently seems to be lacking. For example, if I keep hold of a QGradient and reuse it, is that efficient? or should I draw it to a pixmap and reuse that instead? From the documentation I'm not sure. It is also not clear when you should be using QPainterPath, and what the benefit of using it is.

For now though, I have a simulated KasLoadItem working, and I think it's looking quite good:

[image:2030 size=original]
<!--break-->
