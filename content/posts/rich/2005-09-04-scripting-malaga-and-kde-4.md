---
title:   "Scripting, Malaga and KDE 4"
date:    2005-09-04
authors:
  - rich
slug:    scripting-malaga-and-kde-4
---
I'm now back from Malaga and have just about recovered, so I thought I'd blog a little about the discussions etc. I've been involved in over there. For me, obviously, the big questions were about scripting. Until now, KDE has been weak in the area of scripting applications - this will not be true of KDE 4.

First of all, we need to define what we mean by application scripting: In the discussions we've had at akademy we've limited it to ways to script the functionality of applications, and the ability to add minor customisations. We're not talking about full on RAD development here (that's Richard Dale's department). Obviously there is cross-over between application scripting and RAD as the two are opposite ends of a continuum, but the requirements and target audience are quite different. We want application scripting to be usable by system administrators who want to customise applications (eg. to fit with their corporate environment), by normal users, and by beginners. A few of use cases should help clarify this:

1. Consider a company that requires external email to be archived to a central repository. This not an appropriate piece of code to go in the general release of KMail, but it is a pain to handle it outside of kmail. By letting the sys admin code a script we should make it easy to make this supported.

2. Consider plasma - this will be the replacement for kicker and kdesktop in KDE 4. It should be possible for users to develop simple plasmoids (applets) without learning C++. These should be able to make use of the plasma libraries and be able to look 'cool' but will usually have only basic programming logic requirements.

3. Consider a user who is performing a repetitive task. They should be able to record it once then simply replay it for the subsequent times.

This is what we are talking about when we say 'application scripting'.

In order to achieve these goals, we need several things. Firstly we need a scripting language, next we need bindings both to commonly used parts of qt/kdelibs and to the applications themselves. Finally, we need a workbench in which these scripts can be developed. There are other components we'd like too - a macro recorder and KHotNewStuff integration (so people can download the scripts) spring to mind.

The challenge now is to develop these tools to a high quality as early as we can in the KDE 4 release cycle so that we can tightly integrate them into the applications.
<!--break--> 