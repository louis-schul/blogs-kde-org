---
title:   "KJSEmbed now supports enums!"
date:    2003-08-28
authors:
  - rich
slug:    kjsembed-now-supports-enums
---
I've just committed some basic support for enums to kjsembed. This means that enums published by QObjects using the Q_ENUM declaration can be used by scripts. At the moment, the constants are properties of the proxy object itself rather than the class, but the basics are there.
<!--break-->
This means  that scripts can now do things like:
<pre>
     f = new QFrame();
     f.frameShape = f.WinPanel | f.Sunken;
</pre>
instead of needing to use the int values of the enum. As you can see the enum constants can be or'd together just as they are in C++.