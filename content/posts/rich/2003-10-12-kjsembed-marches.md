---
title:   "KJSEmbed marches on"
date:    2003-10-12
authors:
  - rich
slug:    kjsembed-marches
---
I haven't written much about KJSEmbed for the last couple of weeks because I've been concentrating on squashing bugs in kasbar, but over the last couple of days I've had a chance to work on some kjsembed stuff that has been becoming more and more important.
<p>
The biggest change is that slots can now return QObjects with KJSE automatically creating the right wrapper classes. This change makes the process of writing QObject based wrapper much easier as there is no need to add custom method implentations if you have nested objects. While I was at it, I extended the code for calling slots from scripts so you can pass these objects back to your application. For completeness, I made it work for any opaque pointers or unsupported QVariant types that might be used too.
<p>
During this period of activity, there have also been a bunch of improvements to the documentation and to the flexibility of the JSConsoleWidget class.
