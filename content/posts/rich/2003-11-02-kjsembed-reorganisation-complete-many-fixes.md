---
title:   "KJSEmbed reorganisation complete - many fixes"
date:    2003-11-02
authors:
  - rich
slug:    kjsembed-reorganisation-complete-many-fixes
---
This didn't seem to work first time, so I'll try again:

I did a major reorganisation of the code just before the freeze hit. This caused a lot of breakage (eg. the wrapper classes were borked, as was KPart support), fortunately things are working again now. One important change is the use of separate classes to handle proxies for QVariant types and opaque pointers, this makes the code a hell of a lot easier to understand. Now that the structural stuff is done, Ian Geiser's SQL bindings seem to be basically working (though the QObject wrapper class needs a couple of bugs squashing) so the only thing that's left actually broken is the QPainter wrapper class. All in all, this code is looking good for the 3.2 release.
