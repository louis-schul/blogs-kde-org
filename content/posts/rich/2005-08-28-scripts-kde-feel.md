---
title:   "Scripts with a KDE Feel"
date:    2005-08-28
authors:
  - rich
slug:    scripts-kde-feel
---
At the party last night I got asked to put my presentation from yesterday online. So, for now I've put it online on my website <a href="http://xmelegance.org/">http://xmelegance.org/</a>. Thanks to everyone who came, I hope you found it useful.
