---
title:   "its coming along..."
date:    2004-04-23
authors:
  - geiseri
slug:    its-coming-along
---
Wow this week was productive.  I added transparent Javascript Array to QStringList handling and transparent Javascript Date to Qt DateTime classes with the help of Harri.  Rich added support for setting the application name off of the script, so things like config files, xmlgui, and standard icons work as they do in normal KDE apps.  Slowly we are even gaining some users.  Even one that is using it for a commercial product.  I'm impressed. 

Rich continues to work on the Qt only version, while I keep banging on odds and ends.  I added a few helper functions that will streamline the XML generated bindings.  I'm still playing way too much daleks, but hey its causing me to fix bugs in KJS too =)

Now on the horizon... zack and i have some evil KJSEmbed plans.  Stay tuned... Gadgets and Javascript and other evilness will soon take over your desktop!