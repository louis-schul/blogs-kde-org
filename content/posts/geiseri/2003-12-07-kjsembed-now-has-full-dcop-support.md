---
title:   "KJSEmbed now has full DCOP Support"
date:    2003-12-07
authors:
  - geiseri
slug:    kjsembed-now-has-full-dcop-support
---
Finally after a long time of messing with this, DCOP now works in KJSEmbed.  
<!--break-->
Currently we support any type that can be used inside of a qt:QVariant.  KJS functions can also be exported to DCOP interfaces now.  This also gives us the ability to have connect dcop signals to local KJS functions.
<br>
A good example that shows off how this all plays out is below:
<pre>
function newWeather( station )
{
        var temp = client.call("KWeatherService", "WeatherService", "temperature(QString)", "KMKE");
        var name = client.call("KWeatherService", "WeatherService", "stationName(QString)", "KMKE");
        var label = new QLabel(this);
        label.text = "The temperature at " + name + " is " + temp;
        label.show();
}

var client = new DCOPClient(this);
if ( client.attach() )
{
        var dcop = new DCOPInterface(this, "weather");
        dcop.publish("void newWeather(QString)");

        client.connectDCOPSignal("KWeatherService", "WeatherService", "fileUpdate(QString)",
                 "weather","newWeather(QString)");


        client.send("KWeatherService", "WeatherService", "update(QString)", "KMKE");

        application.exec();
}
</pre>
This will yeild the following window: [image:258]

So now this is cool, because we can now write KJSEmbed scripts that completely integrate with KDE.  Systems administrators can now write .login scripts, that will be more tighly couppled with KDE, and will perform much better than their bash counterparts.