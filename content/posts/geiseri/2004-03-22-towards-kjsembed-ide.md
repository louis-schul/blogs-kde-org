---
title:   "Towards a KJSEmbed IDE..."
date:    2004-03-22
authors:
  - geiseri
slug:    towards-kjsembed-ide
---
I think for about 6 months now I have been toying with a KJSEmbed IDE and failing at most turns... 
<!--break-->
I'm still not convinced this [image:203,middle] although now I have more Javascript language support [image:386,middle]. It still falls short of my expectations.  From what I see there are two modes that people will use KJSEmbed 1) as a macro language for KDE apps and 2) as a "visual lets get something out in the next 2 hours" language.  From what I see here KDevelop is just not going towards either direction.  

This leaves me with my current dilemma, where to go from here.  I have no mood to rewrite or fork Qt Designer to make a visual KJSEmbed IDE, and likewise, I have no energy to write the ECMA Parser needed to do code completion.  The joys of a randomly typed language :)

Ideas from the peanut gallery?