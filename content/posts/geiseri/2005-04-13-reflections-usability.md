---
title:   "Reflections on usability..."
date:    2005-04-13
authors:
  - geiseri
slug:    reflections-usability
---
this was a comment to [Usability, Usability, Usability] but it grew too big and figured it would best be in a blog.

Note I am not a usability expert, as I only took two courses in "Human factors in engineering". These where geared more towards industrial automation, where the users usually had minimal education, and the repercussions of "hitting a wrong button" usually resulted in death, dismemberment, or a very nasty mess to clean up.  Computer usability is a bit different, but I feel some elements tie over.

It thus far has been my experience that developers who are forced to follow the tomes that MS and Apple have churned out for years usually won't.  they tend to get creative, and usually don't want to sit with a pixel ruler trying to line crap up.

this is how most of the nuggets at the interface all of shame got there.  not because the developer said "i wish to make the crappiest interface known to man" but because they didn't have the time or energy to read through the lengthy and complex style guide rules of their platform.

now there are two parts to usability, content and layout.  now layout is something that we can and should enforce in code.  there is no reason that we cannot define "standard layouts" that can be applied to a UI to provide things like center justification, balance, and just general alignment.  having "standard buttons" to enforce a standard sizes and spacing.  having "standard button groups" to enforce placement, order and size. we already have a great start with this in the area of kde:KStdGuiItem, and the kde:KDialogBase.

if as a developer i have a choice of using a class vs reading some thick book, i will use the class and be done with it.  most normal humans can tell when something looks "off" but not all of them can figure out how to make it "look good".

for content imho this would best be served by using a process similar to i18n. developers give the controls their names, and a comment on what stuff should do.  then we can have our usability engineers "translate" the UI into something usable.  i am not sure how well this will work, but the usability people have to get more involved with the development as a start.  we have enough armchair oss developers at osnews, we don't need any more :P

that said, I am pleased that usability is moving off of some silly flame infested list, and moving more into the forefront of kde development.