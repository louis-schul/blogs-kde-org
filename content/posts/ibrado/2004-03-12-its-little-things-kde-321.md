---
title:   "It's the little things (KDE 3.2.1)"
date:    2004-03-12
authors:
  - ibrado
slug:    its-little-things-kde-321
---
I upgraded to KDE 3.2.1 when the RPMS appeared on <a href="http://kde-redhat.sf.net/">kde-redhat</a>'s repositories. It seems snappier, and a lot of my pet peeves with 3.2 have disappeared. Here are some notes... some may be kde-redhat specific, or may actually have already been in 3.2.
<!--break-->
<ul>
<li>Apparently, the KHTML developers must be reading <a href="http://www.megatokyo.com/">MegaTokyo</a> after all -- the huge empty space after the strip has finally disappeared.</li>
<li>KCalc accepts pasted values again. I wonder how such a basic function could go sour...</li>
<li>The workaround to bug #75201 (one of my reports) has been incorporated -- no more Cervisia KPart crashes so far.</li>
<li>Umbrello seems a lot more stable, though I'll probably keep using the CVS version for now.</li>
<li>KMail really rocks. One "little thing" I especially like is the message count feedback when expiring messages.</a>
</ul>

On the other hand,

<ul>
<li>ExeCompletion on KURLRequester/KURLCompletion (bug #77169) still has problems, showing directories instead of apps.</li>
<li>kget got horribly broken, crashing and hanging like never before. Apparently this is related to Qt 3.3. Thankfully, I checked out the CVS code and it's fixed now.</li>
</ul>

Since this is my very first post on kdedevelopers.org, I'd like to invite everyone using Bluetooth to check out the <a href="http://kde-bluetooth.sf.net/">KDE Bluetooth Framework Project</a> on <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdeextragear-3/kdebluetooth/">KEG-3</a>. If you have a UIQ or Series 60 phone, you'll find kbemusedsrv rather useful for controlling your PC (Noatun, XMMS, generic scripts, with more to come).<br /><br />

BTW, since I now maintain the main <a href="http://bemused.sf.net/">Bemused</a> codebase as well, new features will probably be tested first on the KDE server. :)
