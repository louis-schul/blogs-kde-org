---
title:   "Back to College; Updating a running KDE; KDE 4 Name"
date:    2006-04-10
authors:
  - martijn klingens
slug:    back-college-updating-running-kde-kde-4-name
---
Today I went back to the college where I got my Bachelor's degree a couple of years ago. This time to watch a good friend's graduation talk. It's almost impossible to compare school systems between countries, so even while the EU has unified on Bachelor's and Master's degrees it's hard to say where they are in other countries.

(Heck, if you go to university here in the Netherlands you also get a Bachelor's degree after completing the first 3 years or so of a Master's education, but that degree is not comparable to a college where it's the end result of the education. A university's BS will have taught you more theory but little practice in the business world, while a college's BS degree includes about a year's worth of internships, the last half of which is the subject of the graduation talk.)

Anyway, it was a very interesting talk. Half of Bauke's internship (and talk) was about moving all IT and communication infrastructure from two office buildings in one new building for a company with about 50 people working in those two offices. He has been responsible for most of the logistics, design decisions and other project management work, which is really cool for an internship -- most people don't manage but are being managed during that period :)

The second part was about consolidating the server park on VMWare ESX where, again, Bauke has been doing most of the coordination and design work. All in all it has been a smooth talk and he knew what he was talking about, prompting the exam committee to give him a very nice '9' mark. (10 being the highest, 1 the lowest, but you need at least a 6 to pass at all.)

So, Bauke, congratulations with your degree!

<b>Updating a running KDE</b>
Earlier today I continued the update of my KDE 3.4 at work where I left off last Friday. Even while most of it can be replaced in place these days we're not at the point yet where we can really do hot updates. I wonder how much of the stuff I noticed is distributor's work (post-install scripts in RPMs and DEBs for instance) and how much is really KDE's own task.

While any update to .desktop or XDG-menu files correctly caused kbuildsycoca to update I have been less succesful with other parts of the sycoca. During YaST's work I was doing my own job, but at some point I could no longer open a PDF anymore because the mime type database was unusable. Nothing a kbuildsycoca can't fix, but which non-developer knows that?

A bit later I needed to restart kdeinit when kdelibs was updated. Again, I've done this lots of times when doing SVN builds, but when updating through a package manager it would be nice to have this done for you. In theory a 'dcop --all-users kdeinit '' restart' from the RPM's post-install would have done, apart from the fact that kdeinit is not a DCOP client.

When the update was complete I had a desktop where Konqueror was still the KDE 3.4 version with a builtin widget style. Apparently some preloaded Konq was being reused. Kontact crashed on startup when I wanted to use the new desktop. A logoff and relogin later my desktop worked mostly as expected. Only artsd keeps crashing because libarts_mpeglib.so is nowhere to be found. Packaging error for Suse's 9.2 rpms? I don't need arts all that much at work apart from an occasional beep, so I'll look into that later. Everything I really need works now, it's just that it hasn't been a completely transparent upgrade while the process was busy.

<b>KDE 4 Name</b>
And to finish this long post, the KDE 4 slogan discussion that spontaneously started on IRC last week (and that <a href="http://aseigo.blogspot.com/2006/04/houston-we-have-liftoff.html">Aaron</a>, <a href="http://tom.acrewoods.net/node/418">Tom</a> and <a href="http://wadejolson.blogspot.com/2006/04/kde-promo.html">Wade</a> already mentioned) is now continuing by mail between the marketing team members and a couple of others as Aaron already said he would do. Expanding on the basic idea that we pursued in that discussion Aaron has been thinking about some of the higher-level concepts that need to go with it. Currently we're tearing apart Aaron's bad ideas, refining the mediocre ones and keeping the great ones. All in all there's been a lot of useful input from all involved. So far we haven't decided anything for real yet, but given the progress that might not take long anymore. Looks like we will have some great concepts to show KDE to a bigger audience. Can't wait! :)
<!--break-->