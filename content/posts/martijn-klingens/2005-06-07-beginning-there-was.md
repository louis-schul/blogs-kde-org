---
title:   "In the beginning there was..."
date:    2005-06-07
authors:
  - martijn klingens
slug:    beginning-there-was
---
... my first blog entry. Well, beginning, judging by the age of the 'blog' concept it's probably closer to the end of times, but hey, I needed a catchy title :)<br>
<br>
Now, why do I want to add yet another blog to the growing collection? Do I have anything of value to add?<br>
<br>
Until now I've never seen a good reason to blog. Two years ago all I did for KDE was doing the grunt library work of <a href="http://kopete.kde.org">Kopete</a>. Important stuff, and a pretty cool way to experience yourself in API design and code cleanup/refactoring, but rather boring for the general public, definitely not blog-worthy.<br>
<br>
The last year for me was even worse, a job that was stressful and getting more and more on my nerves, so I was too tired at home to get much KDE stuff done, if anything at all.<br>
<br>
Times seem to have changed. Last month I started my new job, and I have a lot more energy than I've had in a long time. Still, my personal pleasure doesn't warrant a blog, so why the heck is this guy blogging?<br>
<br>
Well, recently I have become a lot more active within KDE again, with subjects that are a lot more suitable for a blog. For this first blog entry just some one-line teasers of stuff that I expect to be blogging about the coming weeks:<br>
<ul>
<li>I finally started my <a href="http://websvn.kde.org/trunk/playground/sysadmin/kextprocess/">extended KProcess</a> library after I got the idea almost 3 years ago</li>
<li>My interest in KMail has been increased once again after the <a href="http://kdepim.kde.org/development/meetings/nlpim1/index.php">Dutch KDE PIM developer meeting</a> in Achtmaal, so I've been doing all kinds of small changes there</li>
<li>Taking KMail even further, I have started working on server-side mail filter rules through Sieve</li>
<li>At my new <a href="http://www.yacht.nl">employer</a> it's getting quite clear that Unix and Linux are new focal points, with lots of interesting developments there (not the least of them being the fact they hired me, and are still hiring too BTW ;))</ul>
<br>
So yes, all in all quite some stuff that's KDE-worthy. Don't expect me to write Aaronesque daily novels, I don't have <i>that</i> much to tell. Besides, my mother tongue is not English, so my storytelling isn't as entertaining to read.<br>
<br>
To conclude, hi all, and hope you'll like my upcoming contributions :)
