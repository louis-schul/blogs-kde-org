---
title:   "New Laptop"
date:    2004-08-21
authors:
  - daniel molkentin
slug:    new-laptop
---
Tonight I got my new laptop, which HP offered at a very very low price. My own laptop, finally! That was a dream I actually had since a long time, but I never got to buy one because it was "too expensive". Only wireless doesn't work yet, but there are rumors that there is a driver for that.

In order to set it up for the week, I decided to do tonights nightshift -- and I'm not alone :-). Two hours ago I fetched Daniel Stone from the train station. I was pretty impressed that trains still stop in LuBu at such times.

In other news I am really looking forward to todays talks. Some of them sound pretty interesting, even though I will be pretty busy with orga stuff like lending laptops and stuff.