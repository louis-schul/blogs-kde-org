---
title:   "Kontact: Eyecandy / Exchange"
date:    2005-02-03
authors:
  - daniel molkentin
slug:    kontact-eyecandy-exchange
---
[image:837,right]
Today, I finished two the new intro screen that was fairly overdue already. It's meant to look fancy and be useful (to a certain extend at least ;)

In other news I finally committed the new exchange wizard with hopefully all relevant strings. I'll be working on it tomorrow to make sure to have something working for the final release. Right now it simply does nothing, so noone is gets hurt, just a bit disappointed maybe.

That said, Exchange is Groupware Server Number five that is going to be officially supported, although it still way to go until maturity. OpenGroupware.org is next I guess.

I'll send out a list with known and critical issue to the list tomorrow, I really need to investigate the reason for the toolbar mess in Kontact that happens semi-randomly. But now it's time for me to go bed, have a good night/day/whatever.
<!--break-->