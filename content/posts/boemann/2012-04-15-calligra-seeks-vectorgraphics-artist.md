---
title:   "Calligra seeks vectorgraphics artist"
date:    2012-04-15
authors:
  - boemann
slug:    calligra-seeks-vectorgraphics-artist
---
At Calligra we are mising several icons, including several applications icons.

So we are seeking an artist that are capable of creating oxygen compatible icons.

You will become a member of our very cool community, and take part of the revolution we are determined to make in the world of office and creative applications.

Contact us on irc channel #calligra or on our mailing list calligra-devel at kde.org
<!--break-->
