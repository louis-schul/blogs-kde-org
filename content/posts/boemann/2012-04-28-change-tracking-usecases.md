---
title:   "Change tracking usecases"
date:    2012-04-28
authors:
  - boemann
slug:    change-tracking-usecases
---
In Calligra 2.4 we disabled change tracking because it didn't have the quality we felt comfortable releasing.

Now I ask you dear users to share your requirements for the ultimate change tracking systemt. But also describe how you have been using change tracking in the past (in any application) and what was good and bad, what you felt missing, etc.

I'd like personal accounts and anything you share should be free from interlectual property claims.

Go to the <a href="http://forum.kde.org/viewtopic.php?f=203&t=101811">forum post</a> that I have also made.
<!--break-->
