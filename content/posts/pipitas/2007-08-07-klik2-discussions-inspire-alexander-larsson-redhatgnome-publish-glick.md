---
title:   "klik2 discussions inspire Alexander Larsson (Redhat/Gnome) to publish 'glick'"
date:    2007-08-07
authors:
  - pipitas
slug:    klik2-discussions-inspire-alexander-larsson-redhatgnome-publish-glick
---
<p>My last blog <A href="http://blogs.kde.org/node/2920">outlining some of the upcoming klik2 goodness</A> has attracted some rather surprising readers -- and even seems to have inspired some to do their own brainwork and come up with ideas how to implement the base paradigm of <i>"1 application == 1 file"</i> even more elegantly, and with less dependencies.</p>

<p>One such reader was <a href="http://blogs.gnome.org/alexl/2007/08/07/experiments-with-runtime-less-app-bundles/">Alexander Larsson</a>, a Gnome developer working for Redhat. Read <a href="http://blogs.gnome.org/alexl/2007/08/07/experiments-with-runtime-less-app-bundles/">his blog entry</a> about his proof-of-concept implementation of 'glick', and give is (working) examples a try (hey, he gave up 2 days of his vacation to get some peace of mind for finding out if his beach-inspired concept does work at all&nbsp; &nbsp;;-) &nbsp; &nbsp;). </p>

<p>If you read his short explanations about the implementation details, your head probably will become a bit dizzy (for me, the fusecram/fuseiso union mount is more easy to grasp, since I'm a bit familiar with how it works on most of the more recent Linux Live CDs): to forego the need for union mounting (and using only standard fuse mounting), his hack uses the 'rodata' section of the ELF binary format to embed a file system, and the startup code fusemounts that filesystem and uses some magic glue code to create a pipe, fork the process (where the pipe is used to allow parent and child to talk to each other). Next, the parent.... bah, you better read it in <a href="http://blogs.gnome.org/alexl/2007/08/07/experiments-with-runtime-less-app-bundles/">Alex's own words</a>.</p>

<p>The beauty of his idea however lies in the following: it will allow a user to run the embedded bundle <b>without requiring any runtime klik/glick code</b> (the thing we are currently calling 'the klik client'). To the user, the new binaries can be executed directly, just like any other.</p>

<p>On my SuSE-10.2 notebook, Alex's 'fancy_greeter' bundle did indeed start without me needing to take any additional preparations. (Not sure if all I was expected to see was this static fullscreen 'greeter' thingie, though...) His 'katachi' example however did yield an error message about 'libselinux.so.1' not being found... but I don't have SELinux installed on this system.</p>

<p>The question to me now remains: how 'complicated' is the creation of glick bundles that work like Alex designed his concept? Can this process be easily automated?</p>

<p>Alex, can you explain a bit more? And can you provide a few more example bundles to play with? Preferrably one that does save its settings to some ~/.configfile, and that creates data (e.g. an simple text editor) for a user to 'SaveAs...'? </p>
<!--break-->
