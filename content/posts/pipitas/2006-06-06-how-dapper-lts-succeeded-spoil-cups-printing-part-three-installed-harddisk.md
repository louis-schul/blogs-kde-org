---
title:   "How Dapper LTS Succeeded To Spoil CUPS Printing (Part Three -- Installed on Harddisk)"
date:    2006-06-06
authors:
  - pipitas
slug:    how-dapper-lts-succeeded-spoil-cups-printing-part-three-installed-harddisk
---
<p>I installed Kubuntu Dapper now from the Live CD to the harddisk.</p>

<p>The hardware is very "low end": Intel P IV 1.80 GHz, 256 MByte RAM, 20 GByte HD. But it should be good enough to check the printing capabilities of Dapper, and see how the packagers modified the CUPS setup away from how CUPS.org's defaults. After all, the complaints and calls for help I received must have a reason...</p>

[image:2074 align="left" hspace=8 vspace=4 border=0 size="original" class="showonplanet"]

<p>The installer worked flawlessly for me; took about 25 minutes until it was done. I had to use the "cfdisk" commandline tool in oder to get the partitioning done the way I wanted it. The GUI installer calls a partitioner that reminds of qparted -- but that doesn't seem to support ReiserFS. The only other thing I changed from the default: I created a 1 GB swap partition, so that I counterbalance a bit on my rather poor RAM facilities.</p>

<p>I am still heavily wondering why Ubuntu developers do give away many MByte (to include Firefox and OpenOffice packages for MS Windows(!)) of their precious Linux install CD space, but do not seem to have enough room left for inclusion of openssh-server (which only takes 205 kByte).... The lack of an option to startup sshd and securely login over the network to a freshly installed Dapper (or one running from Live CD) severly limits any possibility for professional helpdesk people (or personal friends) to support someone experiencing problems.</p>

<p>One other sidenote -- I found one more glitch, that developers may want to fix in the next release: the installer should ask for an eventual proxy to be used. In my case, the installer's initial attempt to run an immediate security update failed, because it could not connect to the repository. I knew how I could work around that -- but will any newbie know?</p>

<p>Now I have a KDE 3.5.2 running on the desktop; for printing there's a CUPS version which dpkg identifies as "1.2.0-0ubuntu5". Before I do any update or upgrade, I'll first inspect the default packages and printing setup, and run a few tests. My hope is that all problems suffered through so far are only due to the peculiarities involved with running a computer from a Live CD, and the complications stemming from overlaying the read-only CD ROM iso image with a writeably layer that is hosted in the system's RAM, using UnionFS. </p>

<p>...</p>

<p>Surprise, surprise! Installing printers into a "real" Dapper (meaning it is installed on harddisk) is the same mis-success as was when it ran from the Live CD. Now let's update and upgrade what's updateable and upgradeable, and see if this changes anything for the better....</p>

<p>...</p>

<p>Nope. No cigar. So far no official package update fixes my problem.</p>

<hr>
<p><b>[ <i> <b>&lt;-----</b> Since most (K)Ubuntu users probably do not know what kind of features are available via the CUPS web interface (if it was not artificially crippled by the packagers), <b>&lt;-----</b> here is a screenshot that shows just one example. It exposes the "Set Printer Options" page, that may be used by to set print option defaults for all users. I'll add some more feature screenshots in my next blog entry. </i> ]</b></p>
<hr>

<p>As I already mentioned before, the CUPS web interface is heavily crippled in its functionality: the original CUPS printer web administration is disabled. Thankfully, on the new shiny Dapper, <a href="http://localhost:631/">http://localhost:631/</a> has at least a hint telling users so.</p><dl>
<dt><b>It says:</b></dt>
    <dd><i>&nbsp;&nbsp;&nbsp;&nbsp;"Administrative commands are disabled in the web interface for security reasons."</i> </dd>
</dl>

<p>Great! So, for the sake of a (doubtful) increase in security, users are taken away their ability to easily add and configure printers. Hmmm... let's read on; there are two more sentences. They read:</p><dl>
<dt><b>First:</b></dt>
    <dd><i>&nbsp;&nbsp;&nbsp;&nbsp;"Please use the GNOME CUPS manager (System > Administration > Printing)." </i></dd>
</dl>

<p>WTF? I have a <i>Kubuntu</i>, and it tells me to use <i>GNOME CUPS manager</i>?? Alrightiiee then, let's try it.... Gosh! Can't find it. There is no menu with an entry like that; and there is neither the commandline tool gnome-cups-manager on Kubuntu... </p>

<p>(I did of course try to run <tt>kaddprinterwizard</tt> and other KDEPrint tools, hoping that, in lieu of the missing gnome-cups-manager, these would do the job; do I need to tell you that these didn't work either?)</p><dl>
<dt><b>Then:</b> </dt>
    <dd><i>&nbsp;&nbsp;&nbsp;&nbsp;"<tt>/usr/share/doc/cupsys/README.Debian.gz</tt> describes the details and how to reenable it again."</i></dd>
</dl>

<p>OK, move on to see what that README.Debian.gz tells us. Supposedly, running the commands  <tt>adduser cupsys shadow</tt>  and   <tt>adduser kurt lpadmin</tt> should do it. The second one should add user kurt to the <i>lpadmin</i> group and establish his ability to administer CUPS; the first one should let him also do so via the official CUPS web interface, not just the commandline. The reasons behind these steps are: since Dapper maintainers patched CUPS to run cupsd from the "cupsys" account, that user needs to be made part of the "shadow" group whose members are allowed to read the shadow password file. And user kurt needs to be in the "lpadmin" group so he can manage printers without needing full root privileges. </p>

<p>So I did run these two commands; and as commands, they succeeded; but their intented purpose they did not achieve. While kurt is now part of the lpadmin group, and cupsys is part of shadow... you guessed it: the CUPS web interface still does not let me add a new printer, or delete an existing one, or setup the default printjob options.</p>

<p>So Ubuntu packagers went to the length of patching that sentence into the CUPS local documentation. That's nice; they seem wanting to keep me informed about their changes. But apparently they did not check if their advice also works...</p>

<p>Please fix that! And please, please, please: do not send your users to a gzip-compressed README, that is not directly accessible, and that confuses them with all kinds of unrelated content. <b><i>*If*</i></b> you insist upon crippling CUPS in the name of supposedly increased security, please add a proper documentation how to re-establish its full functions. By proper documentation I mean: add a HTML page that is part of the <A href="http://localhost:631/">localhost:631</A> CUPS documentation. Put your explanation into that HTML document, add a hyperlink, make that document accessible directly from the same sentence where you try to justify your user-unfriendly actions. Thank you! </p>

<p>I could even stop my series of rants now; but I decided to instead dissect Dapper's CUPS setup a bit. I'll use it as an example to highlight some of the new features offered by CUPS to end users (though they will not yet be missed by most (K)Ubuntu users -- because packagers are hiding them very well from them). I'll outline how <b><i>I</i></b> would like to have CUPS configured for maximum usability, without neglecting security. Maybe it helps one user or the other to fix things for himself?  (Hrmmm... and maybe one or the other editor of <i>$online_or_print_publication</i> is interested in a solid review article that features the new stuff that is shipping with CUPS 1.2.0? Yes, I may be looking for a new job quite soon...)</p>

<p>See you!</b>

<!--break-->