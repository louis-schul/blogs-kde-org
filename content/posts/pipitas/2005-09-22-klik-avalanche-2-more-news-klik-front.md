---
title:   "klik avalanche (2): more news from the klik front"
date:    2005-09-22
authors:
  - pipitas
slug:    klik-avalanche-2-more-news-klik-front
---
<A href="http://www.valdyas.org/fading/index.cgi/2005/09/21#klik">Boudewijn</A>, the <A href="http://http://www.koffice.org/krita/">Krita</A> maintainer has his first <A href="http://www.xs4all.nl/~bsarempt/krita.cmg">krita-latest.cmg</A> on offer. It seems to be a resounding success to him, with the download number approaching 300 within the first 24 hours already, and some dozen feedback mails. He even found bugs (and fixed them too) which could not have been found without users actually starting to use the current Krita snapshot for real -- by kliking on the krita.cmg file. (Yes, we are also aware of some bugs which are due to the .cmg file -- missing libraries, missing symlinks; these will be fixed with the next krita.cmg). Boudewijn had success reports on Mandriva, SUSE 10, SuSE 9.3, Kanotix, Debian (with kde 3.4, so fairly adventurous debian users) -- but not all could actually import files. Since the package uses a compilation done on a KDE-3.4 system, there seem to be problems on running it on KDE-3.3 systems (So much for our own ABI backward compatibility, no? Uhmmm... or we better blame GCC for this, because it breaks the ABI of code compiled with each new minor version release). We will try and compile one of the next versions on a 3.3 system, and see if that helps (need to find or setup one first). Due to the success, Boudewijn is now a bit concerned, that the 2 Gig downloads caused by his krita.cmg may after all be giving his provider second thoughts. That's why we are looking into providing a klik://krita-latest shortcut via the klik server, that will fetch the copy of the krita bundle from another source than boud's server.</p>

<p>More news items:</p>

<ul>
<li>Various KDE hackers have queried me in IRC or by mail about how to create <i>my-cool-application-the-latest-development-snapshot.cmg</i> files. I'll publish a short HOWTO about that soon.</li>

<li>probono is pondering to introduce a system of <A href="http://134.169.172.48/">klik</A> package.... no, <A href="http://klik.atekon.de/scribus.recipe.example">klik recipe</A> maintainers, whose job it is to track all the <A href="http://134.169.172.48/comments.php">feedbacks</A> that come in (nearly by the minute) on the klik website (well, not right now -- because of the DNS problem mentioned in my <A href="http://blogs.kde.org/node/1464">last blog</A> entry), and fix the problems that occur. You can imagine that most of the 4.000+ recipes offered by the klik server are not yet tested at all, or even manually tweaked. They are auto-created by scripts. And yet they work on many systems already, very often even extremely well. I've seen <A href="https://blogs.kde.org/blog/175">[fab]</A> expressing his satisfaction about how well <A href="klik://apollon">klik://apollon</A> worked for him. </li>

<li>We had some great <A href="http://www.opensuse.org/Talk:SUPER_KLIK">input of ideas</A> in the <A href="irc://irc.freenode.net/klik">#klik channel</A> on <A href="http://www.Freenode.net/">Freenode</A> about how to <A href="http://www.opensuse.org/Talk:SUPER_KLIK#Thumbnails_.2F_Icons">include application icons</A> and display them for the .cmg files while still sticking to the paradigm that makes klik such a big success: <b><i>"1 application == 1 file"</i></b>. </li>

<li>I am working on two other klik recipes (for <A href="http://www.KOffice.org/">KOffice</A> and <A href="http://edu.kde.org/">KDEedu</A>) which would put multiple applications into one file even, and still make it possible to access them individually. Let's see how far this leads... I'm sure probono has a solution for this already.</li>
</ul>


<!--break-->