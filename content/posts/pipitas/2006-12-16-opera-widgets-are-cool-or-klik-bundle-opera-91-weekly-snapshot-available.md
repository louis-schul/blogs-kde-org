---
title:   "\"Opera widgets\" are cool... (Or: klik bundle of Opera 9.1 weekly snapshot available)"
date:    2006-12-16
authors:
  - pipitas
slug:    opera-widgets-are-cool-or-klik-bundle-opera-91-weekly-snapshot-available
---
<p>This evening I've created a <a href="http://134.169.172.48/apt/?package=opera91">new klik recipe</a>, for <a href="http://opera91.klik.atekon.de/">Opera 9.1</a>. It makes the klik client fetch from Opera's download site their current weekly snapshot of the upcoming 9.1 release and transform it into a typical <i>"1 application == 1 file == 1 click to download+run"</i> klik bundle.</p>

<p>It was cool to see <a href="http://widgets.opera.com/rated/">Opera Widgets</a> work flawlessly on this new klik bundle. Their widget creating community has produced an amazing number of small applications that run inside Opera -- and many of them are truely awesome and creative.</p>

<p>I'm looking forward to see the same type of applications and extensions appearing on the KDE4 platform (probably running on top of Plasma, Aaron?).</p>

<p>I've added a <a href="http://klik.atekon.de/wiki/index.php/Wiki_page_for_opera91_klik">klik Wiki page for Opera 9.1</a> with hints for klik newbies, and also some pre-cautionary measures to backup their <i>$HOME/.opera/</i> directory containing personal settings and other stuff they probably won't like to loose if something goes wrong.</p>

<p>The <a href="http://lists.freestandards.org/pipermail/lsb-discuss/2006-December/date.html">lsb-discuss</a> list got a <a href="http://lists.freestandards.org/pipermail/lsb-discuss/2006-December/003370.html">mail describing the bundle</a>, so that interested ISVs can look at the packaging and software distribution technology around klik.</p>

<p>At this time, <a href="http://opera91.klik.atekon.de/">10 users have run</a> it, and 5 have <a href="http://opera91.klik.atekon.de/comments/">given feedback</a>, none of them indicating any problem. Looks like it runs without any flaw on SUSE-10.0, (K)Ubuntu Dapper and Ubuntu Edgy.</p>

<p>Would you please let us know about your distro too? Does it work for you? Do the Opera Widgets also work?</p>

<hr><p><small>(In case of problems, make sure you read the <a href="http://klik.atekon.de/wiki/index.php/User%27s_FAQ">klik User's FAQ</a>)</small></p>
<br>&nbsp;

<!--break-->