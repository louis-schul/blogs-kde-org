---
title:   "Recommendations to (K)Ubuntu Dapper users: How to restore an uncrippled CUPS [3: network printer discovery with SNMP]"
date:    2006-06-26
authors:
  - pipitas
slug:    recommendations-kubuntu-dapper-users-how-restore-uncrippled-cups-3-network-printer
---
<p><b>[3] Enabling network printer auto-discovery (with new "snmp" backend of CUPS 1.2)</b></p>

<p>My last two blog entries explained....</p>

<ul>
  <li>...how Ubuntu users can <A href="http://blogs.kde.org/node/2117/">restore the CUPS web interface</A> to full functionality, and</li>
  <li>...how Ubuntu user can <A href="http://blogs.kde.org/node/2117/">re-enable the client-side printer browsing</A> so they may automatically discover and use printers shared by CUPS servers on the network.</li>
</ul>

<p>This one will deal with restoring network printer auto-discovery goodness into their CUPS installation. </p>

<p>First have a look at this screenshot. It shows the <A href="http://localhost:631/admin/">http://localhost:631/admin/</A> web interface of a CUPS 1.2.0 server with a successfull network printer discovery. (Do I need to explicitely state, that this discovery will only work if there <i>are</i> actually network printers in your office to be discovered?):</p>

[image:2126 align="center" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>Please take good note of this fact: CUPS <i><b>network printer auto-discovery</b></i> is something <i><b>completely different</b></i> from the CUPS client site <i><b>printer browsing</b></i> we discussed last time. To re-iterate again: </p>

<ul>
   <li><b>client site printer browsing</b> discovers printqueues that are announced (and usually also shared) by other CUPS servers on the network. These include network-attached (IPP, AppSocket/JetDirect or LPR/LPD-connected) as well as locally connected (with parallel, USB, serial, IrDA, FireWire, or SCSI links) devices which are installed as printqueues on the servers. Printing to queues discovered this way will make your box act as a CUPS print client; jobs will go through the CUPS print server, before arriving at the printing device.</li>
   <li><b>network printer auto-discovery</b> scans for printing devices that are connected to the LAN with an ethernet cable or a WiFi interface and which respond to the SNMP broadcast query made by CUPS. The produced list will silently leave out any printers that already have a local queue installed. After discovery, an easy to use "Add Printer Wizard" lets users install new print queues with 2 simple mouse clicks. Printing to queues installed this way will make your computer act as its own print server; jobs will go directly to the printing device.</li>
 </ul>

<p>This method of network printer discovery is a completely new feature in CUPS 1.2, enabled by default.  Ubuntu Dapper <i><b>does</b></i> include CUPS 1.2 -- so Dapper users should already have seen this, no? Let's see:</p>


[image:2127 align="center" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>No! The network printer discovery feature was amputated by Ubuntu's packagers. Therefore it will be hardly known to any Ubuntu user. However, they are lucky: it is extremely easy to restore. Just run one command, to get this sweetness up and running:</p>

<pre> sudo ln -s  ../backend-available/snmp /usr/lib/cups/backend/snmp</pre>

<p>Let's have a closer look. The page shown in the first screenshot at the top took about 3 seconds to build. (With the snmp backend disabled, it takes less than 1 second). What does it show? In addition to the other new stuff in the /admin part of the web interface it lists a bunch of uninstalled network printers, and offers to install them by clicking on the "Add This Printer"-button.</p>

<p>How did it work?</p>

<p>The new "snmp" backend (usually hosted in <i>/usr/lib/cups/backend/</i>) is run each time you access the /admin page. The snmp backend did perform the following operations to create the list:</p>

<ul>
  <li>send an SNMPv1 broadcast to the LAN, querying all nodes to ask if they are printers <small><br>(for SNMP-knowledgeable geeks out there: it uses the <i>Host MIB</i>; future releases will also support scanning for vendor-specific SNMP OIDs and the new PWG Port Monitor MIB);</small></li>
  <li>memorize all nodes' IP addresses which respond;</li>
  <li>individually query each memorized node for their specific protocol capabilities;</li>
  <li>first try if the device supports Internet Printing Protocol (IPP);</li>
  <li>if not successful in getting an IPP response, try AppSocket (a.k.a. "HP JetDirect" or "Direct Network" or "TCP/IP printing");</li>
  <li>if AppSocket fails, try if the node supports LPR/LPD;</li>
  <li>once there is a response, query for the exact "printer make and model" string.</li>
</ul>

<p>Clicking one of the "Add This Printer"-buttons will yield this page:</p>

[image:2128 align="center" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>CUPS has preselected the PPD (that represents the "driver") it thinks is best for the discovered model; but of course, you can override that pre-selection if you want. Confirming the PPD with the "Add Printer"-button, will shortlydisplay a confirmation message first:</p>

[image:2129 align="center" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>and then automatically redirect to the page where you can "Set Printer Options":</p>

[image:2130 align="center" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>The "Set Printer Options" page is important for various reasons:</p>

<ul>
 <li>to correctly fill in the "Options Installed" part [the device may or may not have an optional duplexing unit built in, or additional paper trays,...],</li>
 <li>setting the default paper size [Letter or A4],</li>
 <li>other settings may be desired as defaults, such as Duplex printing</li>
</ul>

<p>One cool feature of this: it even notices if you try to set forbidden combinations of print options, such as <a href="http://blogs.kde.org/node/2131?size=_original">"Duplex printing" on "Transparency" media</a>. (This combo wouldn't make much sense, even if it worked -- but more importantly, it does not even work, because it would jam in the "fuser" system of the laser printer and cause damage to it, when the transparency is passing the heat burn-in unit twice within a short time -- hence, the PPD forbids this combination by enumerating it in its "UIConstraints" section):</p>

[image:2131 size="preview" align="center" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>Of course, the snmp backend is not only beneficial for web interface users. You can also run it from the commandline:</p>

<pre>  /usr/lib/cups/backend/snmp 2&gt;&amp;1 | tee ~/cups-snmp.log</pre>

<p>Or, if you are an Ubuntu user with the snmp backend still disabled, try:</p>

<pre>  /usr/lib/cups/backend-available/snmp 2&gt;&amp;1 | tee ~/cups-snmp.log</pre>

<p>This will show you the result in the terminal window, and at the same time store it in the <i>${HOME}/cups-snmp.log</i> file.  Each printer discovered on the net, and not yet installed locally will be displayed on a line of tis own, showing this info:  </p>

<ul>
  <li><i>"Device" "URI" "ID" "make-and-model IP-address" "printer-info"</i></li>
</ul>

<p>Example:</p>

<pre>
 network ipp://10.162.2.92:631/ipp "Ricoh IPP Printer v2.0" "Ricoh IPP Printer v2.0 10.162.2.92" ""
 network socket://10.162.2.93 "infotec  ISC824" "infotec  ISC824 10.162.2.93" ""
 network ipp://10.162.2.94:631/ipp "Canon iR5000-6000" "Canon iR5000-6000 10.162.2.94" ""
</pre>

<p>SNMPv1 is not exactly famous for its built-in security features. Quite the contrary. It uses something dubbed "community name" for authentication and transfers that in clear text across the wire. By default, 99% SNMP devices are factory-set to use the SNMPv1 community name "public".  And for 98% of those, this setting was never changed after the printer was installed. (Most network admins don't bother to read the printer manuals). So "public" is what the CUPS utility also probes by default in order to query the printer.</p>

<p>Of course, responsible network admins in security conscious environments do set a different community name, or do even disable SNMPv1 altogether (and go for SNMPv3 if their printer supports it). However, CUPS currently only supports SNMPv1 with this utility. This is not an additional security risk -- if your printer allows SNMP queries with SNMPv1 it is not CUPS' fault... </p>

<p>So if an admin has set different community names for his devices, he can also tell CUPS about it. He should look into the /etc/cups/snmp.conf; there he can set different community names to use...</p>

<p>If you want to see more output for debugging purposes, try one of these commands:</p>

<pre>  CUPS_DEBUG_LEVEL=1 /usr/lib/cups/backend/snmp 2&gt;&amp;1 | tee ~/cups-snmp-l1.log
  CUPS_DEBUG_LEVEL=2 /usr/lib/cups/backend/snmp 2&gt;&amp;1 | tee ~/cups-snmp-l2.log
  CUPS_DEBUG_LEVEL=3 /usr/lib/cups/backend/snmp 2&gt;&amp;1 | tee ~/cups-snmp-l3.log</pre>

<p><b>I really do wonder what the reasoning of the CUPS packagers for Dapper was when they decided to hide the snmp backend from their users and disable it. </b></p>

<p><b><i>I think this decision should be reverted!</i></b></p>

<br>
<hr>
<small><dl><dt><b>Amendment:</b></dt>
    <dd><p>Mike Sweet has recently <A href="http://www.cups.org/articles.php?L387">published an article</A> about the snmp backend with more debugging hints. CUPS.org is interested in feedback to improve the utility for cases where it failed to discover an existing printer. Especially helpfull will be the output of <pre>  snmpwalk -Cc -v 1 -c public 192.168.1.101 | tee snmpwalk.log</pre>(Replace "public" by any other SNMP community name you may be using, and "192.168.1.101" with the printer's or embedded print server's IP address).</p></dd>
</dl>
</small>


<!--break-->
