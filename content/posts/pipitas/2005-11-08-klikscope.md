---
title:   "klik://scope"
date:    2005-11-08
authors:
  - pipitas
slug:    klikscope
---
<p><A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik User's FAQ</A> work continued. Today "Scope" section. The IT and Linux press is slowly becoming aware of klik. They visit us in #klik, ask questions, write us e-mails, want interviews, request articles, publish articles themselves. Like the nice one that is <A href="http://www.linux.com/article.pl?sid=05/10/27/1948248">currently on Linux.com</A> (even picked up by <A href="http://osnews.com/comment.php?news_id=12566">OSnews</A>). -- Today's local preview:</p>


<TABLE width="95%" cellspacing="2" border="1" cellpadding="2" align="center" bgcolor="#d3d3d3">
  <tbody>
    <tr>
      <td>
<b>Scope</b>

<p>
<b>Q: </b><i>What does the word "klik" stand for?</i>
<b>A:</b> Originally, it was an abbreviation for <i>"<b>K</b>DE-based <b>L</b>ive <b>I</b>nstaller for <b>K</b>noppix+Kanotix"</i>. But klik has grown and developed beyond that meaning.</p>

<p>
<b>Q: </b><i>Does klik only work for KDE?</i>
<b>A:</b> No. klik works for Gnome too now.</p>

<p>
<b>Q: </b><i>Does klik only work for Knoppix and Kanotix?</i>
<b>A:</b> No. klik now works for all Debian-based systems (surely for Debian Sarge, Debian Sid, Ubuntu, Kubuntu, Xandros and simplyMepis) as well as for SUSE 9.3 and 10.0.</p>

<p>
<b>Q: </b><i>Do klik applications work for Redhat, Fedora, Mandriva and Slackware too?</i>
<b>A:</b> Try it. It differs from case to case. We have had reports that many applications work for users of these distros out of the box. Others don't.</p>

<p>
<b>Q: </b><i>Can you make klik to fully support Redhat, Fedora, Mandriva and Slackware too?</i>
<b>A:</b> Yes. Do you want to help us with that effort?</p>

<p>
<b>Q: </b><i>Are there more browsers supporting klik:// type of links, other than Konqueror?</i>
<b>A:</b> Yes. klik://appname links can be made to work for Mozilla, Firefox, Opera and elinks now.</p>

<p>
<b>Q: </b><i>How do I make klik://appname links work for Mozilla?</i>
<b>A:</b> This should have been automatically enabled by the klik client install script. If not, do this: [FIXME].</p>

<p>
<b>Q: </b><i>How do I make klik://appname links work for Firefox?</i>
<b>A:</b> This should have been automatically enabled by the klik client install script. If not, do this: [FIXME].</p>

<p>
<b>Q: </b><i>How do I make klik://appname links work for Opera?</i>
<b>A:</b> This should have been automatically enabled by the klik client install script. If not, do this: [FIXME].</p>

<p>
<b>Q: </b><i>How do I make klik://appname links work for elinks?</i>
<b>A:</b> This should have been automatically enabled by the klik client install script. If not, do this: [FIXME].</p>

<p>
<b>Q: </b><i>I try to install xvier by following a klik://xvier link via the elinks browser that runs from a Linux console. I get  [FIXME]</i>
<b>A:</b> xvier is a GUI application. GUI applications need an X server to run. Try to use elinks from a Konsole or xterm window inside your favourite desktop environment, and it will work.  [FIXME]</p>

</td>
    </tr>
  </tbody>
</TABLE>

<p>Tomorrow's topic: Usage. Now done with 24 out of 105 questions; my notes for tomorrow list 19 questions re. "Usage". Have you ones of your own you are interested in? Please submit them.</p>
<!--break-->