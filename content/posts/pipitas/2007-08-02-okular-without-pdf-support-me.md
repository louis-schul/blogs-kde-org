---
title:   "oKular without PDF support (for me)"
date:    2007-08-02
authors:
  - pipitas
slug:    okular-without-pdf-support-me
---
My freshly installed kde4-okular package (grabbed from the openSUSE build service repository, using the 'smart' package management tool) seems to be without PDF support.

It doesn't even display PDFs for selection in the FileOpen dialog. Cheating the filter line, by entering *.pdf makes the files show up -- but open they don't ("unsupported format").

I remember this was already the case when I had occasion to look at a KDE4 pre-Alpha build on a customer site 2 or 3 months ago. But now we have Cnuth, a 'Beta 1'. So I expected it to work, somehow....

pinotree told me on IRC that there should be a file named <i>"$(kde4-config -prefix)/lib/kde4/libokularGenerator_poppler.so"</i> on my system.

Well, on openSUSE 10-2 there is none. And not even in an different path either. And not in any of the packages that carry 'poppler' or 'okular' in their name (and I installed all of them).

So what is the solution?

<!--break-->