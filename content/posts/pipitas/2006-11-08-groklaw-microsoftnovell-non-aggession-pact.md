---
title:   "Groklaw + the Microsoft/Novell Non-Aggession Pact"
date:    2006-11-08
authors:
  - pipitas
slug:    groklaw-microsoftnovell-non-aggession-pact
---
Novell has <a href="http://www.sec.gov/Archives/edgar/data/758004/000075800406000109/novl-8k_110706.htm">published some more details</a> about their recent business agreement(s) with Microsoft. This time it is about some financial details involved. (Looks like they are required by law to reveal these details to the <a href="http://www.wikipedia.org/wiki/SEC">SEC</a>.)

I found <a href="http://www.groklaw.net/article.php?story=20061107194320461"><b>an excellent commentary</b></a> about this operation on a website called "Groklaw". I've read it -- and I recommend it as required reading to anyone inside the communitiy trying to wrap his mind around this particular "affair".....

*Sigh*.... still need to find time to read more than the headlines of last week's Groklaw article on that subject. If today's is any indicator, the previous one must have been thoughtful and very revealing as well.

<!--break-->