---
title:   "Help! KDEPrint on KDE-3.x will break with CUPS-1.2 (or with 1.3 at the latest)!"
date:    2006-04-03
authors:
  - pipitas
slug:    help-kdeprint-kde-3x-will-break-cups-12-or-13-latest
---
<p>I'm suffering from another CUPS+KDE frustration right now.</p>

<p>Today I learned that there are two bug reports in our bugzilla which I had missed to see before. They were submitted by Mike Sweet from CUPS. See yourself: <A href="http://bugs.kde.org/show_bug.cgi?id=115891">#115891</A> and <A href="http://bugs.kde.org/show_bug.cgi?id=124157">#124157</A>. The first one was submitted last November, the other one a few days ago.</p>

<p>As you may know, Michael Goffioul, our KDEPrint guru, has already announced 2 years ago that he doesn't have the time any longer to maintain the code. He also does not have regular access to a Linux/KDE system any more. Thankfully, he still is keeping an eye on the KDEPrint mailing list, and gives some great advice there every time a more complicated problem or user question pops up. <A href="https://blogs.kde.org/blog/311">Cristian</A> has taken over maintainership now -- but he also is very short on resources, and said that he'd have almost no time at all until Autumn, and even then could not devote more than a few hours per week.</p>

<p><b>The pitch:</b> It looks like KDEPrint is using some CUPS API functions which are explicitely declared <i><b>private</b></i>. Now, CUPS-1.2 introduces a new (additional) Unix domain socket interface to speed up local printing of large numbers of jobs to large numbers of printers (this will be a huge boon to all those installations which are out there running 1000s or even tens of 1000s of printqueues on one print server host).</p> 

<p>Had we used the official and public CUPS API, we would not have to care at all. Mike and the CUPS developers go to great lengths in order to keep the public CUPS API stable and backwards compatible throughout the 1.x development series. Those who use the CUPS API will automatically get access to the now built-in Unix domain socket support. Those who stepped over the boarders, get biten.</p>

<p>A short term emergency salvation is available, but will go away within 6 months: thanks to Mike, who is a very cooperative guy, CUPS-1.2 will now keep (for a this limited period), a special KDE exception in its code: he added some stub functions with the old names which call the new functions that serve the same purpose -- all that just so that KDEPrint doesn't break if a user upgrades to CUPS-1.2!</p>

<p>Of course, there are two sides to this coin too, like mostly in life: Michael Goffioul had introduced the "ugly hacks" [his own words] in order to get rid of blocking dialogs, and what users experienced as "frozen interfaces", which were caused by relying on synchronous CUPS calls; in order to reduce the level of user frustrations, which also expressed themselves in a particular type of bug reports, he had temporary resorted to this workaround that now turns its character and becomes a new bug...</p> 

<p>Mike has expressed his willingness to work with KDEPrint developers, and adapt some more CUPS internal things (like introducing worker threads, or a new asynchronos API) if it helps KDE better.  Please let's not have that opportunity pass! Please help.</p>

<p>So.... please, please, please: if you are interested to keep KDEPrint in sync with CUPS-1.2, have a deep look at the CUPS-1.2 code and API, as well as at KDEPrint. <b><i>And don't delay it!</i></b> CUPS-1.2 will be out within a few weeks. CUPS-1.3 will happen within 6 months after that (in any case very likely before KDE-4.0.) Users will upgrade. Their current KDEPrint systems will break, or they will not be able to use most of the new, cool CUPS features. Frustration will abound. Do you want that to happen?</p>

<!--break-->