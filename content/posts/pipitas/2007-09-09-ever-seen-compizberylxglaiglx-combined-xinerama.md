---
title:   "Ever seen Compiz/Beryl/XGL/AIGLX combined with Xinerama?"
date:    2007-09-09
authors:
  - pipitas
slug:    ever-seen-compizberylxglaiglx-combined-xinerama
---
 I've never [image:2980 align="left" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"] seen Xinerama combined with Compiz/Beryl/XGL (or AIGLX) in action. This morning, when checking out <a href="http://openprinting.blogspot.com/">a printing-related blog</a>, I stumbled upon a little YouTube <a href="http://www.youtube.com/watch?v=DUSn-jBA3CE">video showing exactly that</a>. It does look amazing indeed.

Looked like all the effects did work in the demo-ed setup. Duh!, the video is even a year old. I wonder when I'll be able to get meself a new (!) notebook that can do at least non-Xinerama 3D desktop stuff. (It's not that I'm convinced it will make me more productive... but it at least makes for nice "demoware" to impress relatives, friends, customers or complete strangers in the airport lounge).

(Note: the image is only a screenshot. Don't send a bug report if clicking it won't <a href="http://www.youtube.com/watch?v=DUSn-jBA3CE">play the video</a>.)

<!--break-->