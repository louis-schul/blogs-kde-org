---
title:   "KDE on Windows -- Deadly to OpenSource in General?"
date:    2004-12-09
authors:
  - pipitas
slug:    kde-windows-deadly-opensource-general
---
<BR>
My friend <A href="http://aseigo.bddf.ca/">Aaron</A> fears <A href="http://aseigo.blogspot.com/2004/12/how-to-kill-open-source-on-desktop.html">Open Source on the Desktop gets killed</A>. He's not afraid of Microsoft though. He thinks it's the "enemy within". Nah, not his words. I'll rephrase it: He thinks, the deadly danger comes from <A href="http://webcvs.kde.org/kdelibs/win/">some little efforts</A> going on in <A href="http://wiki.kde.org//tiki-index.php?page=KDElibs+for+win32">some corner</A> of the KDE project to <A href="http://kde-cygwin.sourceforge.net/">port some KDE applications</A> to the MS Windows platform. Hmm.... again my words... OK, you better go and <A href="http://aseigo.blogspot.com/2004/12/how-to-kill-open-source-on-desktop.html">read his blog entry</A> yourself.

For now, let me say this: I always valued my friend Aaron's balanced way of reasoning in past "opinion battles" (oh, yeah -- those occur very frequently on KDE mailing lists). But this time I think he is very un-balanced, one-sided, black-and-white only, static in his thinking. Maybe more on that later. (Now first to finish this customer job -- cloning a <a href="http://www.cups.org/">CUPS</a> master daemon from system space to run several copies in userspace, all with different security settings and custom options, on different non-privileged TCP/IP ports....)
<br>
<!--break-->