---
title:   "Why reading text when you can recognize a face?"
date:    2004-11-29
authors:
  - duncan mac-vicar
slug:    why-reading-text-when-you-can-recognize-face
---
When <a href="http://www.apple.com/ichat/">iChat</a> was released and I saw the <a href="http://images.apple.com/ichat/images/buddylist061903v2.jpg">contact list</a> with big face avatars I remembered inmediately that humans can recognize faces much faster than reading a name. Since that moment I wished that <a href="http://www.kde.org">KDE</a> could drop text everywhere it is not needed and start using faces (w/text as an addition if it makes sense).
As I don't have too much time to even hack in <a href="http://kopete.kde.org">Kopete</a> because University and thesis, my time got limited to build HEAD and read all the lists. 

One day I jumped around the room when I saw avatar support in kabc. As Kopete already has kabc integration (you can associate a kopete metacontact with a addressbook entry) , my wish (also in my 2005 TODO) is to add support for a very visual contact list, where big faces (128 or more pixels) are displayed in the contact list. I wonder why kabc avatar support is limited to 140 pixels.


Those face avatars should be everywhere, balloon popups, kmail, bday reminders, korganizer meetings, etc. We don't like to read, we like to recognize and associate. 

Selecting avatars is boring. The new face-crop dialog from Larrosa and others solves most of this problem.
