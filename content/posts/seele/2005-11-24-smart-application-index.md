---
title:   "Smart Application Index"
date:    2005-11-24
authors:
  - seele
slug:    smart-application-index
---
Theres been lots of talk and speculation what a new KMenu would look and act like.  I presented some background research as well as some of my own ideas at aKademy in August.  I had sketches, diagrams, and notes of what a new system could look and act like, but nothing too visual which didnt require reading my book (literally a notebook full) of notes.

<i>If you missed the presentation at aKademy but are interested in a summary of the research I reviewed, <a href="http://www.obso1337.org/hci/kmenu/akademy2005.ps">here are the slides</a> (Postscript) from my talk.</i>

So, I'll skip some of the background information and continue to the technical details.  My solution for the KMenu comes in three parts.  First, we need an interface with which the user can easily browse/search all software.  Second, we need an interface which the user can quickly and easily launch applications without having to put forth much effort.  Third, we need "smart" customizable menus to help the user relate data and applications together as tasks.

Here is part 1:  The Application Repository

Lets address need number One: an interface with which the user can easily browse/search all software.  This interface would be used for a number of tasks.  The two major user tasks associated with this utility would be to 1) search for an unknown application with a known task in mind, and 2) browse and discover the existing software installed on the system.

<a href="http://gallery.lebwog.com/seele/albums/Screenshots/index.png"><img src="http://gallery.lebwog.com/seele/albums/Screenshots/index.sized.png" width="630" height="370" alt="index of the application repository.  click for larger screenshot" /></a><br />Application Repository -- Index (Click for larger view)

You may have seen something that looks similar, however the major differences are in the Information Architecture. The important issues addressed are the quality (of information), context (of what the user is trying to accomplish), and the relation (of the information to itself, other information, and the user). The other GUIs basically copies the structure of the KMenu (or whatever menu they are using) in to a filesystem hierarchy of linear categories with no real relations.

The main screen of the application browser (the root or default screen) includes pertinent information the user will be able to use to search or browse applications.  It includes user-influenced applications such as those frequently used by the user, system-influenced applications such as those recently installed, and the index of all available software.

An index is one of the best ways to present users with a library of information.  Typical properties of an index includes a category label, examples of contents, and often cross-referencing.  They allow the user to browse sample contents without having to take action before they make a decision.  If the labels fail to reach the user, the example applications may help the user decide if the contents fit their goals.

Category indexes provide several types of information, 1) all applications listed in the category, 2) related applications not in the category, and 3) related categories.

<a href="http://gallery.lebwog.com/seele/albums/Screenshots/category.png"><img src="http://gallery.lebwog.com/seele/albums/Screenshots/category.sized.png" width="630" height="370" alt="sample category screen of the application repository.  click for larger screenshot" /></a><br />Application Repository -- Category Screen (Click for larger view)

Similar and related applications are listed to provide the user with more information in case the current application is not what they were looking for.  Related categories are also provided in which the application could be classified in certain contexts.  This helps break down possible differences in the system's semantics vs. the user's semantics. If the system and user think the same way, the user should be able to find the application with in a few clicks.  If they do not think the same, the user should be able to flow through the system's suggestions to find the application they had in mind.

AFAIK this kind of information is not yet available in KDE, but it may be available in KDE 4.  It is an integral part of the interaction with the information, but can be better explained later in the third part of the final solution.

All applications are click-icon-to-launch, preserving the behavior in the rest of the environment.  'More information...' under an application will take the user the application screen with more information.

<a href="http://gallery.lebwog.com/seele/albums/Screenshots/app.png"><img src="http://gallery.lebwog.com/seele/albums/Screenshots/app.sized.png" width="630" height="370" alt="sample application screen of the application repository.  click for larger screenshot" /></a><br />Application Repository -- Application Screen (Click for larger view)

This screen gives some system information about the application, files (if any) last accessed with the application, and most importantly related information to help the user continue their search.

I didnt create a mockup of a search results page, but it would basically return applications and categories which match the search criteria.  The look and feel of the GUI can change as long as the quality, context, and relation  of the information provided is effectively displayed.

Keep in mind that this utility alone does not replace the KMenu or solve its problems.  It is only one part of the solution, addressing the users need to search and browse.  When the application for completing a goal is unknown, the current task is <i>searching for the application</i> and not the original goal.  As such, the user is more willing to put forth effort and time to find the application.

This differs from when the user <i>knows</i> the application they need for completing a goal.  The task at hand is not searching through applications to find one that fits, the task is to <i>complete the goal</i> with the known application.  This requires different information structuring for a quick search and response.  What we need is an interface which the user can quickly and easily launch applications without having to put forth much effort.

I will address this in part 2: The Application Launch Menu

<!--break-->