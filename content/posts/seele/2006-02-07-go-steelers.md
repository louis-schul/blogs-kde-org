---
title:   "GO STEELERS!"
date:    2006-02-07
authors:
  - seele
slug:    go-steelers
---
sorry for the caps, but i *am* yelling :)

this weekend justin and i went home and watched (and later celebrated) the steelers play in the super bowl.

for those of you who are not familiar with the super bowl, it is the championship game for american football (you know that weird game that is nothing like soccer).  for those of you who are not familiar with me or the steelers, you should know that i am from pittsburgh and so are they ;)

saturday night in the south side (one of the bar districts) reminded me of mardi gras.  people dressed up in black and gold, drunk in the streets singing "Here we go STEEEEEELERS, HERE WE GO!", cars blasting steelers polka (yes polka, dont ask) and blaring horns down carson street, and almost every store/bar/restaurant/house decorated in black and gold for the Big Game.

why was it such a Big Game (other than it being the championship of course).  well, the super bowl is a big deal to pittsburgh for many reasons.  *) it is a family owned team (which is getting rarer these days) of which the father of the current owner won 4 superbowls with. *) jerome 'the bus' bettis has been inducted in to the hall of fame, yet had not won a super bowl AND has been hanging in with the team for 2 years trying to get a ring (he is a broken man) AND is from detroit (he is from detroit). *) ben roethlesberger is the second youngest starting quarterback (and now youngest winning quarterback) *) bill cowher has been coaching the steelers for something like 15 years and has taken them to the playoffs something like 9 or 10 times with only 1 other appearance (loss) in the superbowl (1996).

so it was time for 'one for the thumb' (the super bowl prize is a ring and trophy for the team)

oh and strangely condoleeza rice (U.S. secretary of state) was at the game and publicly picked the steelers to win (this is strange in part because a) she was at the super bowl and b) public figures do not usually publicly pick sides in sports unless they are a hometown team and she is from Alabama).

anyway.. being more interested in getting drunk and staying warm to watch the game than 'freezing our nuts off' (as justin would put it), we watched the game from the comfort of a house party in north pittsburgh rather than going down town in the craziness.  popular bar districts in pittsburgh were closed for the game (south side, strip district, station square, oakland..) to allow people to wander the streets and celebrate (or riot) after the game.  

people from all around the country came in to pittsburgh just to watch the game here.  pittsburgh has a great following, once from pittsburgh, always from pittsburgh.  i have friends in California who have found a steelers bar, a friend in Boston who has found one, and Justin and i know of at least three in DC/Balitmore. (as for stats, i think Florida is the #1 place for pittsburghers to move to, DC-Metro #2)

the game was full of ups and down, ill let you read commentary to see the close calls and bad plays from both sides, but overall it was a great game to watch.

the street scene broadcasted to the post-game news was insane.  the streets were littered with cheering fans, far from sober but all smiles.  luckily most of the streets around the districts were closed as well and would not be opened until 3am, which gave people plenty of time to party (er.. sober up).  

im afraid to know what the scene would have been like if we lost.  something would have been torn down, set afire, blown up, who knows.  but who cares, bettis has a fairy tale ending, the rooney's have one for the thumb, pittsburgh native coach cowher has a superbowl win, ben is the youngest winning qb in superbowl history, and pittsburgh will be a very happy place to be for a long time.

GO STILLERS! (I is an intentional accent ;))

<!--break-->