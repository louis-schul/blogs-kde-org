---
title:   "Usability Review of KBruch"
date:    2005-10-22
authors:
  - seele
slug:    usability-review-kbruch
---
This weekend I finally finished writing my review for KBruch.  KBruch is an Edutainment application which allows students to practice fractions.  It has several modes of operation: Evaluation of multiple terms, Factorization of numbers, Comparison between two terms, and Conversion to decimal.  

Overall I was impressed with the application, its variety of task modes allow users to cover all necessary basics to gain a good understanding for fractions. (Basically if I had kids, I'd make them use this ;))

Most of the issues were minor to moderate, usually having to do with size, position, or labeling of an item.  There were a few severe issues I was concerned about which could seriously effect game-play, but no changes were so serious that it would be very difficult to fix.  I also provided some interface suggestions on how some of the usability issues could be fixed.  

There was one concern I had about the interface I am not completely sure how to fix.  In the old interface, the mode buttons were along the left in a menu, similar to other KDE interfaces (such as Control Center).  The concern was the layout of the interface (which was in 3 columns) and the obscurity of the mode options in the toolbar (difficult to interpret immediately, always available even when not in use).  There may be a better solution than putting the modes in the toolbar, and I am open for suggestions.

I love working with the Edutainment packages, because children are a very peculiar audience.  More freedom is given in the interface to include color, graphics, and sounds to make them more engaging for younger users.  Children are also a challenge to design for because they have special needs in regards to language, context, font size, etc.

The report can be found <a href="http://www.obso1337.org/hci/kbruch/">here</a>.