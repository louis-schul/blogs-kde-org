---
title:   "KDE MUD Clients"
date:    2003-07-30
authors:
  - berkus
slug:    kde-mud-clients
---
So okay, what do I want from a MUD client?

a) Necessities
a1) Complete scriptability (better in Ruby or Perl, then Python, custom languages won't do)
a2) Bindable keys. Thats crucial since typing commands when you can bind it is a straight road to rip.
a3) Triggers. While present in most clients they need be fast and easy to add.
a4) Support for encodings other than Latin-1.
a5) MCCP, Mud Client Compression Protocol, saves up to 95% of traffic.
b) Fancies
b1) Automapper
c) My ideas
c1) Battle screen. More on this later.

And here are my nominees

i) Kmud2
ii) KMC
iii) mmc
iv) Xpertmud
v) KMuddy

None of them meets all criteria for a good mud client =(. KMud2 is a rewrite of KMud, so it lacks lots of stuff present in former KMud. None of those clients but mmc support cyrillic. Mmc lacks proper keybindings under X and also lacks a mapper. Of those a mapper is present only in KMud2, but it is so glitchy i wouldn't use it in real life =)

mmc and KMC feature internal Perl scripting, something very much anticipated since it makes life oh so easier.

None of the clients has the battle screen, something I'd like to see supported by the server someday.
Battle screen is a region on-screen which displays both your and enemy party as they fight. You can shortcut to your party members with keys 1..9 and to enemy party members with keys Q..P and cast spells and exec commands with the rest of the keys... The battle screen also shows stats for each party member, hp, mana, affects, spells... So you have all info at hand and can manipulate it quickly. This requires a server output parser unless server supports this directly in its protocol. Mud2 protocol supports sending stats and sounds between client and server, but imo this is not enough.

This all calls for writing another client; probably based on KMC? But first i think to test out the battle screen feature....