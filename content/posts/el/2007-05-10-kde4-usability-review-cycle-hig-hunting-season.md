---
title:   "KDE4 Usability Review Cycle & HIG Hunting Season"
date:    2007-05-10
authors:
  - el
slug:    kde4-usability-review-cycle-hig-hunting-season
---
Yesterday, the usability review cycle for KDE4 started. As the HCI working group is poor on man-power, we started an experiment to include the community into the search for obvious infringements of the KDE Human Interface Guidelines: The <b><a href="http://dot.kde.org/1178743323/">HIG Hunting Season</a></b>. 

If you recently had a look at the <a href="http://wiki.openusability.org/guidelines">HIG's start page</a>, you'll quickly spot that it's far from being finished. That's why we are preparing <b>HIG checklists</b> which list the most common user interface and interaction problems in KDE3. 

Such issues can be stated like bugs, and that's what we ask the community for: 

<i>Please review KDE4 applications along the HIG checklists - and report bugs whenever you find an infringement!</i>

Reviews can be performed by anyone - users, developers, technical writers, translators, developers, usability people.. anyone! If you don't have a running KDE4 environment, check out beineri's <a href="http://blogs.kde.org/node/2785">KDE four live</a> CD or wait for the alpha release which should be available tomorrow or so.


The checklists are published on a weekly basis on the dot. For the first checklist about configuration dialogs and instructions how to proceed, please have a look at this week's <a href="http://dot.kde.org/1178743323/">dot story</a>.


Developers who receive HIG-related bug reports which require further usability analysis please send a mail to <a href="https://mail.kde.org/mailman/listinfo/kde-usability-devel">kde-usability-devel</a>. 


Happy hunting!

<!--break-->
