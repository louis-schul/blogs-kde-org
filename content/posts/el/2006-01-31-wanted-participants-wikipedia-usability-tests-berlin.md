---
title:   "Wanted: Participants for Wikipedia usability tests in Berlin"
date:    2006-01-31
authors:
  - el
slug:    wanted-participants-wikipedia-usability-tests-berlin
---
<i>[.. as this test is for German-speaking participants only, I'll proceed in German]</i>


<b>Teilnehmer für Wikipedia-Usabilitytest in Berlin gesucht!</b>

Um die Einstiegsschwelle zur Mitarbeit an der freien Enzyklopädie <a href="http://www.wikipedia.org">Wikipedia</a> zu reduzieren, führen wir im Rahmen von <a href="http://www.openusability.org">OpenUsability</a> nächste Woche (6. bis 10. Februar) Usabilitytests mit der deutschen Version der Wikipedia durch. Dazu suchen wir TeilnehmerInnen!

Die Tests finden in Berlin Mitte statt - du solltest also nächste Woche in der Gegend sein - und dauern ca. eine Stunde. Als Aufwandsentschädigung erhältst du 15 Euro. 

Um mitmachen zu können, solltest du dich einer dieser Gruppen zugehörig fühlen:

<ol>
<li>Ich bin <i>Wikipedia-Leser</i>, habe aber <i>noch nie versucht</i>, einen Artikel zu bearbeiten oder zu verfassen.</li>

<li>Ich bin <i>Wikipedia-Leser</i>, habe schonmal versucht einen Artikel zu bearbeiten oder zu verfassen, aber <i>etwas ging schief</i>.</li>

<li>Ich bin <i>Wikipedia-Leser</i>, und habe auch schon einige Male Artikel bearbeitet, <i>nie jedoch selbstständig einen neuen Artikel verfasst</i>.
</ol>

Wenn eine dieser Beschreibungen auf dich zutrifft, <a href="mailto:ellen at relevantive de">[ melde dich bitte ] </a>

<!--break-->



