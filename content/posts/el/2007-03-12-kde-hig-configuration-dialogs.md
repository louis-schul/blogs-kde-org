---
title:   "KDE HIG: Configuration dialogs"
date:    2007-03-12
authors:
  - el
slug:    kde-hig-configuration-dialogs
---
I started to write down some <a href="http://wiki.openusability.org/guidelines/index.php/Practical_Examples:Configuration_Dialogs">guidelines for configuration dialogs in KDE4</a>. The major differences to KDE3 are:

<ul>
<li> Do not overload your configuration dialogs!</li>
<li> Avoid the usage of tree views in the sidebar of paged dialogs.</li>
<li> Set the dialog's maximum size to 800x600 Pixels.</li>
<li>Make sure all dialog pages are sized equally, so the dialog does not resize.</li>
<li>Provide vertical and horizontal scrollbars on each tab (hide them by default). This allows users who require big font sizes to reach all contents while they can see the tab titles and reach the OK/Cancel buttons.</li>
<li>Separate advanced preferences from frequently used ones.</li>
</ul>

Please review the guidelines and give me feedback. I'm basically interested in everything - if you agree on the contents, the format, if there are parts missing, if you need more examples, more concrete guidelines etc. Also, in the lower part of the page, there are some suggestions for new widgets in the standard sections I'd like to see feedback for  (font and color requester). You can either use this blog's comment section or write an email to ellen kde org).


<!--break-->