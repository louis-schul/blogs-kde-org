---
title:   "\"Summer of Usability\" ?"
date:    2006-01-26
authors:
  - el
slug:    summer-usability
---
In my <a href="http://blogs.kde.org/node/1724">previous entry</a>, I  mentioned that there are still too few usability people involved with  OSS  usability - both in the scope of <a  href="http://www.openusability.org">OpenUsability</a> as well as in other  efforts like the <a href="http://www.flossusability.org/">FLOSS Usability</a>  group.

On Monday, I had a long call with <a href="http://www.janethaven.com/">Janet Haven</a>. Janet is working for the <a href="http://www.soros.org/initiatives/information">Open Society Institute Information Program</a>. She runs the Civil Society Communications program, which enhances the ability to access, exchange and produce information by civil society constituencies. In this scope, she is promoting OSS software. 

When implementing OSS solutions in real life projects, Janet experienced strong differences in the usability of the applications, and at one point she wondered: "What can I do to make the software better?". She started thinking of how to integrate usability and design with the OSS development process. That's how she came across OpenUsability. 

During our chat the idea came up to start something similar to  Google's <a href="http://code.google.com/summerofcode.html">Summer of Code</a> to get usability students involved with OSS projects - a "Summer of Usability" (don't worry, this is just a working title). 

As Janet, I like this idea a lot...


<!--break-->