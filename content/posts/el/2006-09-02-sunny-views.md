---
title:   "sunny views"
date:    2006-09-02
authors:
  - el
slug:    sunny-views
---
After three years at relevantive, I decided to work as a freelancer again - and moved in to a shared office in Berlin Kreuzberg.

While the relevantive office was beautiful, this is sure a small upgrade: 


[image:2308 width=200 class=showonplanet align=center]

[image:2309 width=250 class=showonplanet align=center]


It's the first time in my job life that I do not share an office with other usability people, but with people of many different backgrounds: Hardware developers, security consultants, web and software developers, and a trick film company. I'll sure miss the cross-table usability talks we had at relevantive... but am also looking forward to see how a trick film is produced, hehe!


<!--break-->