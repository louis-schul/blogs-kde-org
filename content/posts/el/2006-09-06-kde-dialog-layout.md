---
title:   "KDE Dialog Layout"
date:    2006-09-06
authors:
  - el
slug:    kde-dialog-layout
---
<a href="http://blogs.kde.org/node/2182">A few weeks ago</a>, I complained about the possibilities to align and group elements when designing KDE dialogs with Designer. But in order to make Designer better meet our requirements, we first have to define what KDE dialogs should look like: We need to define guidelines.

In a first step, I had a look at dialogs and guidelines of the four major desktops: KDE, Gnome, Mac and Windows. 


<b>Comparison of the major existing Dialog Layouts</b>

<b>Status quo: KDE</b>

The new guidelines will have to work with existing KDE dialogs. There are (at least) three aspects we have to consider which hinder us in simply copying other guidelines (even if we wanted..). 

<ul>

<li><i>Wide range of provided functionality.</i>
KDE dialogs vary strongly in their provided functionality. There are dialogs pages which provide two or three options only, while there are others which look bloated. A general dialog layout should be able to handle the whole range.</li>

<li><i>Nested pages.</i>
Some KDE dialogs have up to four levels of navigation (left bar + sublevels + tabs). The number of tabs and the existence of a left sidebar affects the size of the window, and thereby the layout.</li>

<li><i>Orphaned options.</i>
There is a high number of options which is not directly related to others, so they can't be grouped in meaningful clusters. </li>

</ul>




<b>How do the other layouts look in Qt?</b>

I applied the other desktops' guidelines to Kontacts "New Event" dialog. Even if they do not correspond 100% to each other and to the guidelines, I think you can see a difference:


[image:2322 width=500 class=showonplanet align=center]

<b>KDE.</b> Basic characteristics of the dialog are the five tabs and four "orphaned" options which are not grouped with others: Busy, Public, Comment, Categories.

This is not the best-designed KDE dialog out there, and there have been several attempts to redesign it. None of them was too successful - possibly because of the missing guidelines. So let's see if the others can help!


[image:2323 width=450 class=showonplanet align=center]

<b>Gnome Simulation.</b> Gnome has a very clear layout that shows is very easy to understand and to implement (straight top-to-down approach). It requires to create meaningful groups - you can see that the orphaned options are grouped logically with other elements. 

Disadvantages of this approach are the missing scalability in width - if there are more tabs, ui elements either will have to be stretched or there is a lot of empty space on the right. Also, the strict placement of options below each other limits the maximum number of options on a page (the dialog becomes simply too long). 

[image:2324 width=500 class=showonplanet align=center]

<b>Mac Simulation.</b> Due to the center-equalisation, this layout also works when the dialog becomes broader. It simply recenters.

However, this layout is not so easy to understand and implement as the Apple HIG is very complex with regard to dialog layout. Also, the look is less clean than in the Gnome layout as there are more visual axes.


[image:2325 width=500 class=showonplanet align=center]


<b>Windows Simulation.</b> The advantage of this layout is that it is very compact and many options fit onto a relatively small page. 

However, the layout is not so easy to implement as there are various options how to arrange the single groups and options. The developer has to spend more thoughts in it which unnecessarily costs time. Also, some elements have to be stretched (e.g. the line edit) to fit the overall layout.





<b>What options do we have?</b>


To find the optimal layout for KDE, we will have to combine the advantages of different layouts and probe carefully if they fit with our existing dialogs. Before I post my suggestions, I'd be happy to read your opinions here.


<!--break-->

