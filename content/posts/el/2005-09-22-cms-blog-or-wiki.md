---
title:   "... cms, blog or wiki?"
date:    2005-09-22
authors:
  - el
slug:    cms-blog-or-wiki
---
I've quite often thought of getting my own webspace. But what next? cms, blog or wiki? which software to use? what fits my purpose?

Bjoern yesterday pointed this site to me:

<a href="http://www.opensourcecms.com">www.opensourcecms.com</a>

Click <a href="http://www.opensourcecms.com/index.php?option=content&task=view&id=388&Itemid=143"> CMS Ratings</a> in the navi and you'll get a long list of cms, blogs, wikis, groupware, etc.

The best about it: Most of them offer a test installation providing a frontpage and an admin login. All collected in one place - no more need to struggle through an endless number of web pages and search for demo intallations :)

Also, this is especially useful for usability experts. Its applicability for expert inspections and usability testing comes close to <a href="http://dot.kde.org/1126867980/">klik</a> and <a href="http://blogs.kde.org/node/1440">NX technology</a> promoted by <a href="https://blogs.kde.org/blog/418">Kurt Pfeifle</a>.

<!--break-->