---
title:   "sshd kicking you out? Check your SELinux labeling!"
date:    2012-09-02
authors:
  - spstarr
slug:    sshd-kicking-you-out-check-your-selinux-labeling
---
So, on my Fedora Rawhide box, somehow it refused to allow logins via ssh, somehow SELinux in permissive mode was spewing failures left and right.

<b>Aug 28 13:35:27 panic kernel: [   67.224233] type=1400 audit(1346175322.774:78): avc:  denied  { read } for  pid=618 comm="fedora-storage-" name="ld.so.cache" dev=dm-0 ino=1271 scontext=system_u:system_r:initrc_t:s0 tcontext=system_u:object_r:file_t:s0 tclass=file
Aug 28 13:35:27 panic kernel: [   67.226381] type=1400 audit(1346175322.778:79): avc:  denied  { open } for  pid=618 comm="fedora-storage-" name="ld.so.cache" dev=dm-0 ino=1271 scontext=system_u:system_r:initrc_t:s0 tcontext=system_u:object_r:file_t:s0 tclass=file</b>

And so forth, now even though SELinux was in permissive mode, sshd was dropping all connections, including to localhost with <i>write error: broken pipe</i>.

If you encounter such situation, do a <b><i>yum reinstall selinux-policy selinux-policy-targeted -y </i></b>, reboot and at grub prompt, edit selection and append to linux options with <b><i>autorelabel=1</i></b> to force selinux to relabel to fix any broken permissions. It's possible my specific rawhide snapshot broke in some spectacular way (which I wouldn't expect to affect when in permissive mode!)

*phew*! This is what you get for using Rawhide on a dev box, but it's also a good exercise if you really want to learn how to recover from unusual system issues.

Enjoy!

Shawn