---
title:   "A lament for the system tray"
date:    2008-07-03
authors:
  - spstarr
slug:    lament-system-tray
---
O woe is me, your poor systray.
I work so hard to show you statuses,
and even then I get no respect.
I make your panel shine with glee,
O please say a prayer for me.

O woe is me.
I am the systray,
just don't make me display things in disarray.