---
title:   "A new year and a whole lot of stuff (part 1 of 2)"
date:    2015-01-25
authors:
  - spstarr
slug:    new-year-and-whole-lot-stuff-part-1-2
---
It's been an awful while since I last blogged, we all go though periods where things get rather crazy all at once or boring, mine went though both.
 

<font size="3"><b>Jobs</b></font>
It took awhile (try 9 months!), but I started one job but decided to resign half a year later when I felt things didn't connect well. I still wish them all the best, they were nice people. Now, I work at a great Open Source company, <a href="http://www.remote-learner.net">Remote Learner</a>, we focus on <a href="https://moodle.org/">Moodle</a> and provide upstream patches, new features and addons. I spend my time in the DevOps/Infrastructure side of things with Puppet and random scripts. I have a great team and we do awesome things with git too.

<font size="3"><b>School</b></font>
I took some night school classes while among the unemployed, aced them and brought my overall GPA way up, quite pleased about that.
 
<font size="3"><b>Unexpected Trip</b></font>
Spent a month in Cape Town, South Africa, went up Table Mountain, climbed down it (using wrong shoes lead to some scary moments!), had a wonderful experience there, and met some people along the way.

<font size="3"><b>Open Source</b></font>
I didn't run away from the FLOSS community, I've been helping the Fedora QA Team for a couple releases, mostly thrashing Anaconda to improve our installation experience after seeing Fedora 18 and the horror of installation, I jumped in and have helped since.

As for KDE, I'm waiting for Plasma 5.x to be officially released <b>Edit: as default in Fedora</b> before I look at moving any of my code over (if it's still applicable).

<font size="3"><b>"Stuff"</b></font>
I never usually buy from the TV but picked up a nifty vacuum that even has a reverse blower so I can clean dust out of computer parts,  thanks mom! :)

More to come..

[ Part 1 / 2 ]

