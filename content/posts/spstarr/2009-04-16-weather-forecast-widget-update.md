---
title:   "Weather Forecast Widget - Update"
date:    2009-04-16
authors:
  - spstarr
slug:    weather-forecast-widget-update
---
There will be some new weather providers coming soon. I am currently working on adding a Netherlands source (current conditions only provided free).  Someone is working on a German source and I plan on adding 7 day forecast info to the NOAA provider hopefully soon.

Just be aware, I do read what's out there from googleland and yes we do need more weather providers for you folks complaining ;-)
As for using Marble, I'd like to, as NOAA provides .kml files which would make radar display very nice to have. But I'm only one person you know, help me out and things happen faster!

Thanks to aseigo, we have Solid Network awareness support for the dataengine.

Thanks to Petri Damstén, we have a cleaner interface for weather data handling.

It's very nice to see the weather wallpaper!

More to come... :)