---
title:   "APKs now built on invent"
date:    2023-11-13
authors:
  - mahoutsukai
slug:    apks-now-built-invent
categories:
  - CI/CD
  - Android
---
The migration of jobs from <a href="https://binary-factory.kde.org">Binary Factory</a> to <a href="https://invent.kde.org">KDE's GitLab</a> continues. Last week almost all jobs that built APKs were migrated to invent.kde.org. The only remaining Android job on Binary Factory doesn't use Craft.

The <a href="https://invent.kde.org/sysadmin/ci-utilities/-/blob/master/gitlab-templates/README.md?ref_type=heads#android-1">Android APK jobs</a> running on invent are the first jobs that make use of our <a href="https://invent.kde.org/sysadmin/ci-notary-service">CI Notary Services</a> delegating tasks that require sensitive information (e.g. the keys to sign the APKs or the credentials to upload the APKs to our F-Droid repositories) to services running outside of GitLab. Currently, the <a href="https://invent.kde.org/sysadmin/ci-notary-service/-/blob/master/doc/apksigner.md?ref_type=heads">apksigner</a> service and the <a href="https://invent.kde.org/sysadmin/ci-notary-service/-/blob/master/doc/fdroidpublisher.md?ref_type=heads">fdroidpublisher</a> service are live. The configuration of the services can be found in the <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/signing?ref_type=heads">sysadmin/ci-utilities</a> repository.

As before, the APKs are published in our F-Droid repositories:

<ul>
<li><a href="https://cdn.kde.org/android/stable-releases/fdroid/repo/">KDE Android Release Builds</a> provides APKs built from the 23.08 branches.</li>
<li><a href="https://cdn.kde.org/android/nightly/fdroid/repo/">KDE Android Nightly Builds</a> provides APKs built from the master branches.</li>
</ul>
Many apps have already switched from Qt 5 to Qt 6, but the Qt 6 APKs are not yet ready for public consumption. Not even as nightly builds. Therefore many &quot;nightly&quot; builds are many weeks old.

The migration to invent has a few implications. On Binary Factory all APKs (release and nightly) were rebuilt once a day even if nothing changed. On invent the APKs are rebuilt whenever a change is pushed to the release/23.08 branch or the master branch of a project. Another change is that on invent any KDE developer can manually trigger a new pipeline to build the APKs (e.g. if a bug in a dependency was fixed). On Binary Factory you often needed to ask someone to trigger a build for you.

<b>Building APKs</b>
If you want to start building APKs for your project then add the <code>craft-android-apks.yml</code> template for Qt 5 or the <code>craft-android-qt6-apks.yml</code> template for Qt 6 to the <code>.gitlab-ci-yml</code> of your project. Note that you must use the <code>include:project</code> format (<a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/gitlab-templates?ref_type=heads#our-gitlab-cicd-pipelines">example</a>).

The jobs will build unsigned APKs. To enable signing your project (more precisely, a branch of your project) needs to be cleared for using the signing service. This is done by adding your project to the <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/signing?ref_type=heads#apksigner">project settings of the apksigner</a>. The master branch is cleared by default for all applications listed in the project settings, so that you only need to add two lines: The repo path on invent and the ID of your application. The key used to sign your application will best be created by us directly on the machine that does the signing.

Once you think the APKs are ready for publication you can enable publishing in our F-Droid repositories by adding your project to the <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/signing?ref_type=heads#fdroidpublisher">project settings of the fdroidpublisher</a>. Because some Qt 6 APKs are not yet ready for publication although their build succeeds, by default the master branch is not cleared for publishing. This means you will have to add five lines to the project settings of fdroidpublisher.

<b>Outlook</b>
To complete the Android jobs/services a job/service for submitting APKs to Google Play will go live soon. It's currently only used by <a href="https://apps.kde.org/itinerary">Itinerary</a> but, in view of our <a href="https://www.youtube.com/watch?v=MzpoTuHuciE">Make a Living</a> initiative, we hope to publish more apps on Google Play to create some revenue to fund our work. Creating and publishing Android Application Bundles (AAB), the new packaging format required for new applications published on Google Play, will follow soon after.

Then we'll add signing of Windows artifacts and installers and submission to the Microsoft Store, signing and publishing of Flatpaks, and signing and publishing of builds for macOS.
