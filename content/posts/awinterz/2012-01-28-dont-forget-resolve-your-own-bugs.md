---
title:   "Don't Forget to Resolve Your Own Bugs"
date:    2012-01-28
authors:
  - awinterz
slug:    dont-forget-resolve-your-own-bugs
---
A gentle reminder:  please occassionally check back on bugs you've reported and resolve them if they have been fixed in recent versions. 

Sometimes we developers fix bugs but forget to resolve them ourselves; sometimes bugs are fixed by newer versions of other software we depend on; sometimes bugs are fixed as consequences of other fixes.

So help us out and resolve your own bugs when possible.

Thanks.

