---
title:   "Quick Tip: Gitorious Email Notifications"
date:    2010-11-03
authors:
  - awinterz
slug:    quick-tip-gitorious-email-notifications
---
You won't find any (obvious) link on Gitorious, but if you want to enable email notifications you need to go to <a href="http://gitorious.org/favorites">http://gitorious.org/favorites</a>.

Thanks to Mike McQuaid for telling me about this hard-to-find feature of Gitorious.
