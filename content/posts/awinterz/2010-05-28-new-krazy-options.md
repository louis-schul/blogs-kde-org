---
title:   "New Krazy Options"
date:    2010-05-28
authors:
  - awinterz
slug:    new-krazy-options
---
I added some new options to Krazy in the past few days. Hope folks find them useful.

<ol>
<li> You can now control the types of files to process with the new <b>--types</b> and <b>--exclude-types</b> options.

Use the <b>--list-types</b>  option to see what file types are supported.

Examples:
<code>
# Run all checks against desktop and designer files in this directory:
% krazy2 --types=desktop,designer  *
</code>

<code>
# Run all checks on all file types except C++ files:
% krazy2all --exclude-types=c++
</code>
</li>

<li>Krazy now supports "check-sets" with the new <b>--check-sets</b> option.  Check-sets are a collection of checkers with a similar high level purpose in mind.
Check-sets currently available:
* c++, pure C++ checks
* qt4, Qt4 source (C++, QML, Qt Designer files)
* kde4, everything (the default)
* foss, checks specific to FOSS (eg. license and copyright)


Use the <b>--list-sets</b>  option to see what check-sets are supported.

Examples:
<code>
# Only check for pure C++ issues:
% krazy2 --check-sets=c++ *
</code>

<code>
# Only check for pure Qt4 issues:
% krazy2all --check-sets=qt4
</code>

</ol>

Wait! There's more... you can control all these new features by using TYPES, EXCLUDETYPES, and CHECKSETS directives in your .krazy file.  Please see the krazy2, krazy2all and krazyrc man pages for more info.

Pick up the new Krazy in $KDESVN/trunk/quality/krazy2 and run ./install.sh from there.
Please tell me about bugs and feature requests by submitting a report to the Krazy component in bugs.kde.org