---
title:   "Fedora Complaints"
date:    2006-06-10
authors:
  - awinterz
slug:    fedora-complaints
---
I still haven't managed to get sound working on my new HP Pavilion a1320y desktop with Fedora Core 4.  Read a lot of suggestions in <a href="http://fedoraforum.org">Fedora Forum</a> without any luck so far.

<p>
Figured why not just upgrade to Fedora Core 5?  So I did, with less than optimal results.  The graphics went all haywire.  Even the console graphics were unreadable.  Guess the installation made some assumptions about my graphics card that aren't true.

<p>
Anyhow, Fedora Core 4 has been re-installed.

<p>
Help getting sound working greatly appreciated.