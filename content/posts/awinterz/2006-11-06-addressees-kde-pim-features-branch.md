---
title:   "Addressees in the KDE PIM Features Branch"
date:    2006-11-06
authors:
  - awinterz
slug:    addressees-kde-pim-features-branch
---
Thanks to Christian we have a bunch of clean-ups in the way KMail's addressee and recipient editors work.  This new code will probably be fast-tracked[1] into the 3.5 branch, but for now we have it only in the features branch for testing.  So, please do test.

Feedback to kmail-devel@kde.org please.  Not all of our new contributors are reading my blog.

In particular, this patch addresses the following bug reports (at least):

<a href="http://bugs.kde.org/show_bug.cgi?id=98691">Bug 98691: tab completion last name / nickname</a><br>
<a href="http://bugs.kde.org/show_bug.cgi?id=76739">Bug 76739: Partial string search for contacts in address fields</a><br>
<a href="http://bugs.kde.org/show_bug.cgi?id=77342">Bug 77342: include nicknames in address lookup</a><br>
<a href="http://bugs.kde.org/show_bug.cgi?id=109798">Bug 109798: apply distinct on address in dropdown for autocomplete</a><br>
<a href="http://bugs.kde.org/show_bug.cgi?id=107945">Bug 107945: wrong/strange autocomplete in composer addressfield</a><br>

[1] I mean that the KMail core developers already have reviewed the code and seem to like it, and this is really bug fixing and not a set of new features.