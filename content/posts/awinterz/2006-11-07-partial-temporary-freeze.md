---
title:   "Partial, Temporary Freeze"
date:    2006-11-07
authors:
  - awinterz
slug:    partial-temporary-freeze
---
Based on past experience we can project the KDE 3.5.6 tagging about 1 month from now.

So, today I put a freeze on any new features in the KDE PIM Features Branch -- the stuff I have been babbling about non-stop of late.  We can still test bug fixes in this branch.  But we need to put our efforts into finishing off and polishing the existing new goodies (KMail templates, custom tagging, TOFU, HTML signatures, ...).  And negotiate with the developers over which of the goodies we will move into the 3.5 branch for the 3.5.6 release.  And hope that the translators don't mind the new strings.

Really not much time.


