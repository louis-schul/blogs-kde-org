---
title:   "Quickies but Goodies"
date:    2008-02-03
authors:
  - awinterz
slug:    quickies-goodies
---
A list of good stuff that I was involved with in the past week:

* The PIM team (mostly Till) posted the <a href="http://techbase.kde.org/Projects/PIM/Features_3.5.9">KDE PIM 3.5.9 Features List</a>.  A huge list of exciting, new features that will be released with KDE 3.5.9 resulting from the merge of the enterprise branch.

* The Release Team started a <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">KDE 4 Feature Plan</a>. The items on the plan are things we hope to accomplish during the KDE 4 lifecycle.  Don't take it too seriously; but it does give a glimpse into what developers are thinking/hoping to accomplish over the next few years.

* I imported Oral's Planner summary for Kontact from the old kdepim-3.5.5+ branch into trunk. It's ported to Qt4/KDE4 and it mostly works now. Danimo made a nice configuration dialog with Qt designer that I need to hook up, and there are some missing icons, but it is coming along.

<b>Oral: if you are reading this... please contact me about Planner.<b>