---
title:   "Extended Warranties (Not!)"
date:    2006-05-10
authors:
  - awinterz
slug:    extended-warranties-not
---
My main system is an HP Pavilion laptop.  I bought it refurbished a couple years ago, direct from HP.  And, since it as a refurb, I figured it would be wise to also get the extended warranty.

<p>
But now the harddrive seems to be failing.  smartd says so.  And every day or two the system hangs.

<p>
So I call technical support.. I just want a new harddrive.

<p>
"Oh, we're sorry -- you have installed a non-qualified operating system on your laptop.  See section #7 in your warranty agreement. But, if you still experience problems with your system after un-installing Linux we'd be glad to help."  <b><i>Do'h!</i></b>

<p>
Moral of the story: Don't waste your money on extended warranties if you intend to install a non-Gates OS

<p>
I think I'll buy a brand new system now... desktop or laptop... laptop or desktop... decisions, decisions...