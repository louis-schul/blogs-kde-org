---
title:   "Pretending..."
date:    2007-10-17
authors:
  - frederik gladhorn
slug:    pretending
---
... is what <a href="http://edu.kde.org/parley">Parley</a> started doing lately. Pretending to have some very basic limited intelligence. Extending that to some more still very limited and basic intelligence will be easy now that the groundwork has been done.
And I hope it's getting better at pretending then ;)

Parley is becomming more configurable in an easier way, gains features almost daily, be it weird stuff like support for dual conjugations (speak up, if you speak a language that has these, I'd like someone to tell me more about it) or a new fun and easy "get to know new words" practice mode. I don't know how much stuff has been fixed or improved - especially thanks to three tireless people patching fixing and reviewing a bunch of stuff I changed, so somehow now it still works despite all my efforts.
Apart from that we now have a little summary telling you how bad you did, served right after your practice session.

<a href="http://blogs.kde.org/node/3039">
<img src="https://blogs.kde.org/files/images//temp/practice_image_tomato.preview.png"></a>
It's not only rotten tomatos at all, but also some fresh stuff.<br/>
Back to pretending. Last week I went over the correction code. Thanks to Thiago, I finally managed to get unicode decomposition working to be better in dealing with accents. So your favorite vocabulary trainer will now nicely tell you how dumb and mistaken you are. Doesn't that make you feel happy too?
<br/>
You put wrong accents? Misspelled your word or even made Capitalzation Errors? Ha, you will be lectured on that right away... you may even choose how you will be punished (count accent mistakes as right or wrong for example).
<a href="http://blogs.kde.org/node/3037">
<img src="https://blogs.kde.org/files/images//parley-accentuation.preview.png"></a>
</br>
And not only do you get this stuff for free, it's even crashing on a Windows near you.
<a href="http://blogs.kde.org/node/3038">
<img src="https://blogs.kde.org/files/images//windows-edit-entry-parley.preview.png"></a>
(Windows screenshot by Saro Engels)

So consider learning a new language, to give Parley a try, will you?
<!--break-->