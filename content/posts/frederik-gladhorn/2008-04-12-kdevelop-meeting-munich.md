---
title:   "KDevelop meeting in Munich"
date:    2008-04-12
authors:
  - frederik gladhorn
slug:    kdevelop-meeting-munich
---
Today (for some yesterday I guess) the KDevelop meeting started.
After arriving a little late, I came just in time to grab something to eat after listening to the end of Aleix's talk about KDE 4 in general.
So far we are still at the "formal" part, discussing general direction of KDevelop in KDE 4.
[image:3391]