---
title:   "freedom and liberty"
date:    2008-03-23
authors:
  - frederik gladhorn
slug:    freedom-and-liberty
---
The time to paint easter eggs has not come yet. It will though, in about half an hour. Also I don't really know why I'll do it. Maybe to reflect on my art skills once more and be happy that other people user their ability to create great artwork for KDE and Parley.
Instead I yesterday started something that had been rotating in the back of my head for about a year now. Since I started working on KVocTrain/Parley one problem has been how to deal with synonyms (to a lesser degree antonyms and false friends, which will be done with in the same instant).
Trying to check in at four in the morning probably doesn't help me fighting git. But I wanted a branch locally. I won, with only <a href="http://websvn.kde.org/?view=rev&revision=789082">little side effects</a> for the Kalzium plasmoids Carsten made me hack on earlier.
Also the first version of the gui was somewhat scary, including three buttons, a lineedit and a listview plus two labels. After getting some sleep, it's down to a label, button and a list that shows the synonyms of the currently selected word.
Also having to fix a bug in "The Raven" by Poe, no wait, in the importer for KDE3 vocabulary documents I decided that ember and radiant had to be synonyms for today, as I wasn't in the mood to look for better ones.
[image:3345]
If you feel your vocation is to suggest me a more clever way of setting up synonyms, make yourself heard.

I could add to the git section of Carsten:
I needed a svn revert, which is just not the same as git revert.
Being clever (and this being my second attempt to use git), I tried "git checkout" instead.
Good idea, but 
<code>git checkout HEAD kalzium/plasmoid/engine/kalzium_engine.cpp</code>
was what I needed (I assume because of the git-svn stuff). Since I was missing my HEAD, it just did nothing. Ouch.
Also when using git-mergetool, I'd recomend a little the addition of "-t kdiff3" or some other sane 3-way merge tool.
Unless you know how to use vim-merge.

And finally <a href="http://cheat.errtheblog.com/s/git/">cheat git!</a> because cheat sheets are so handy.