---
title:   "Smart Card"
date:    2008-11-20
authors:
  - frederik gladhorn
slug:    smart-card
---
A few weeks ago I got a <a href="http://fsfe.org/card">smart card</a> to use with gpg for hardware encryption. I'm no security fanatic but I like the idea, so I bought a "lots of different cards all in one" reader. I got a MSI <a href="http://global.msi.com.tw/index.php?func=proddesc&maincat_no=132&prod_no=1411">StarReader SMART</a> which should support smart cards and was available locally (strange habit, I like to go to real shops instead of the online competition sometimes).
I played around with it, but it seemed to just sit there and do nothing (except read every variant of useless memory card). What made my day is that after only one mail to Ludovic Rousseau with some info about the device and getting a response the same evening, it started working. After adding its usb id it's listed on the <a href="http://pcsclite.alioth.debian.org/ccid.html">ccid driver page</a> :)
A big thank you to Ludovic Rousseau!
Time to get it to work with gpg and mail now.
<!--break-->