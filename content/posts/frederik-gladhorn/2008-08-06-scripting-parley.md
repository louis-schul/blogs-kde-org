---
title:   "Scripting Parley"
date:    2008-08-06
authors:
  - frederik gladhorn
slug:    scripting-parley
---
Akademy is coming closer and I still haven't managed to blog about current stuff happening in Parley.
Instead I was <i>wasting</i> my time outside, climbing, and lately again (last weekend at least) some coding.
Lately I leaned back and enjoyed other people working on parley *a lot*.
Our two summer of code projects are becoming ready for broader testing and have been merged into trunk. New faces show up improving Parley, Javier Goday got started with two patches to improve the ui :)
Daniel Laidig is working on a welcome screen that hopefully will make new users feel more welcome and not scare all long time users away ;)
One sad topic that came up is the state of the handbook, which is not as nice as it should be. I cleaned out lots of old stuff from it a week ago, so now it's time to fill it with actual information again. Since huge parts will have to be rewritten, I was wondering if there is a good way to collaborate on the handbook. Any tips are more than welcome. I think we have enough interested people willing to write some piece and improve parts here and there, but the current docbook sources are not very easy to work on with different people that don't necessarily have svn accounts.

Now it's time for me to start reading a book in Spanish again, so I will profit from Avgoustinos' project: Scripting in Parley.
Originally the intention of his Summer of Code project was to implement a way to make it easy to fetch vocabulary and additional contents online. After discussing and planning we finally shifted his goal. To be more flexible Avgoustinos implemented a scripting interface for Parley.
Kross was an obvious choice for us since it supports many scripting languages with little effort.
Now we have some working Python scripts (and I bet more great stuff coming up).

It's your opportunity to be the first to implement a script in a different language (Java Script? Ruby?) for more languages (we have what Google translation has to offer, Spanish-German from leo.org, but there have to be other languages that need support).

While the interface will still receive some polishing, I'm happy to let you sneak peak at what Parley in KDE 4.2 will offer :) (or grab it NOW from svn).

<center>                                                            <script type="text/javascript" src="http://blip.tv/scripts/pokkariPlayer.js?ver=2008010901"></script>                   <script type="text/javascript" src="http://blip.tv/syndication/write_player?skin=js&posts_id=1155207&source=3&autoplay=true&file_type=flv&player_width=&player_height="></script>                   <div id="blip_movie_content_1155207">                   <a rel="enclosure" href="http://blip.tv/file/get/Fregl-KDE42ParleyVocabularyFetching963.ogv" onclick="play_blip_movie_1155207(); return false;"><img title="Click to play" alt="Video thumbnail. Click to play"  src="http://blip.tv/file/get/Fregl-KDE42ParleyVocabularyFetching963.ogv.jpg" border="0" title="Click To Play" /></a>                   <br />                  <a rel="enclosure" href="http://blip.tv/file/get/Fregl-KDE42ParleyVocabularyFetching963.ogv" onclick="play_blip_movie_1155207(); return false;">Click To Play</a>                   </div>                                      
</center>

<a href="http://blip.tv/file/1149076/">Scripting screencast</a>
Ok, this is the first time I tried making a screencast and while I don't think it's very bad, there certainly is room for improvement. Let me know if this is interesting at all (and working), otherwise I can save myself some trouble ;)

To get you started on scripting Parley (much more than vocabulary fetching is possible), Avgoustinos also wrote a little intro and some nice documentation: <a href="http://api.kde.org/4.x-api/kdeedu-apidocs/parley/html/namespaceScripting.html">Parley scripting api dox</a>.
<!--break-->