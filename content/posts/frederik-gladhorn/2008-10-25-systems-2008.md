---
title:   "Systems 2008"
date:    2008-10-25
authors:
  - frederik gladhorn
slug:    systems-2008
---
Today the Systems Fair in Munich ended. I only spent yesterday and today there helping at the KDE booth. Eckhart and Holger spent all week there and I am deeply impressed that they still were in very good spirits and were lots of fun to manage the booth with :)
<a href="http://ktown.kde.org/~gladhorn/blog/systems2008/booth.jpg">
<img src="http://ktown.kde.org/~gladhorn/blog/systems2008/thumb/booth.jpg" alt="booth">
</a>

The Systems is a rather business oriented fair, so knowledge about KDE and open and free software is not as common as at some other fairs. Still we talked to many other great projects and people. It was good to talk to enthusiastic users nonetheless.

Eckhart gave a KDE Education presentation showing 
Kalzium, Kig, Step and Marble in depth.
<a href="http://ktown.kde.org/~gladhorn/blog/systems2008/eckhart.jpg">
<img src="http://ktown.kde.org/~gladhorn/blog/systems2008/thumb/eckhart.jpg">
</a>
To my surprise many visitors showed up for this presentation, more than for the presentations before or after it - yay! Showing the <a href="http://windows.kde.org">Windows port</a> on this occasion was interesting, since many people are reluctant to install Linux, though we also gave away quite a few live CDs.
At the booth a few people showed up interested in getting involved more with the KDE project which I think is absolutely great. Not only could we happily point at <a href="http://techbase.kde.org">techbase</a> but also I hope we will see new faces for promotion showing up soon :)

I also enjoyed talking to the <a href="http://www.fsfe.org">Free Software Foundation Europe</a> people whos booth was not far from ours. This even got me a free (as in gas) ride home. Thanks a bunch Björn. Since we were hitting off with the collaboration and promo things so well, Eckhart and I decided to leave Holger alone for quite some time and even steal his usb stick for a fun project of our own. When we arrived, we noticed some large printers which were in the same hall as our booth. He started the fun contest of finding which would be the most friendly company donating some posters to KDE. In the end we were very happy to bring home more than 10 fresh new posters featuring <a href="http://picasaweb.google.com/wadejolson">Wade's great work</a>.
<a href="http://ktown.kde.org/~gladhorn/blog/systems2008/systems_printer.jpg">
<img src="http://ktown.kde.org/~gladhorn/blog/systems2008/thumb/systems_printer.jpg">
</a>
They will find their way into the booth box soonish. We would like to say thank you to Michael Häusler and Thorsten Seyffarth for shiny new KDE posters.

<!--break-->
