---
title:   "Blue Numbers"
date:    2008-05-16
authors:
  - uga
slug:    blue-numbers
---
KDE has always been a community that I saw myself very happy with. Open, friendly and cheerful to newcomers. To improve that, and not to diminish the quality of coding while doing so, #kde-devel is better used for coding only, and suggests #kde-cafe for a socialisation channel. I promoted the use of that channel in the past.

I can't anymore though. Blue Numbers is an op at that channel. Everyone knows her... errrm... sorry, She knows everyone. Whenever she sees a newcomer to that channel, rather than being happy about a new person, she feels the need to follow up an interrogation session. Who he/she is, how he/she found the channel, why is he/she not in #kde-devel (we don't welcome gnomees now, no collaboration supported), what are their real names... oh wait, wasn't Blue Numbers herself using her fake name even in the kde mailing lists? only recently she added the real one to the planet even. And yes, if the person doesn't answer, Blue will /op herself and yes... you know the rest.

So yes, the USA will be happy that a new FBI member is doing the customs check works, and soon we'll all need to use biometric IDs to be able to enter #kde-cafe. So great!

I had complained about it... and now each time somebody joins, Blue Numbers will accuse somebody else (me) of being responsible of that interrogation session. Great! So first she'll go paranoid, and then I'll be the one responsbible.

I live in a country (Spain) that used to be a Dictatorship, just barely 30 years ago. We welcomed democracy. I don't want the same for a kde socialization channel.

If there are two things I really hate are 1) dictators 2) lies