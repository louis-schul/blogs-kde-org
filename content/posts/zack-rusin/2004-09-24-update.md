---
title:   "Update"
date:    2004-09-24
authors:
  - zack rusin
slug:    update
---
<p>Lately I didn't have too much spare time, but I've been working a little bit on the Qt Mozilla port yesterday and today. I finally figured out why we've been getting paint event storms but I haven't yet implemented it as it's a fundemental flaw in the way I wrote nsCommonWidget. I'm still waiting for a super-review to get CVS write access on mozilla.org to commit this stuff. Hopefully it's going to happen over the weekend. Also hopefully by then I'll have the paint event storms and offset bug in the toolbar fixed.</p>

<p>From the integration front we've been looking into integrating Helix into KDE. Ryan Gammon, Ian and I are all about it. And it looks like soon we'll be able to show people some cool things. </p>

<p>I have quite some things here locally that I'd like to show at some point. I've spent Saturday reading up on Ruby. It's very interesting. I did it mainly because during aKademy I kept hearing how great it is from Richard and Alex. The language in itself is very neat, but what they did with KDE/Qt bindings is just amazing. It's so cool that I started writting an app using it for the hell of it. </p>

<p>People are complaining about gmail not working with KHTML. They do have a point, we need to fix it. I think I completed the xmlhttprequest synchronous implementation, but there's still a lot of things that are broken. Personally I'm being bothered by the fact that we do destroy khtmlview associated with an iframe when its display is set to none from JS (hence the "open with konqueror" dialog). That one will be hard to fix.</p>

<!--break-->