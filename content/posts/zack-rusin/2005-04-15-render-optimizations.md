---
title:   "Render optimizations"
date:    2005-04-15
authors:
  - zack rusin
slug:    render-optimizations
---
Last week I went to Norway to do the paperwork and see the Trolltech office. I loved Oslo which bodes well for the next few months. I'm back in States but will be moving to Norway in the next few weeks. 

When I got to the office Lars and I started talking a little bit about Render composition code and before I knew it on Wednesday morning we started hacking on it. By Friday we rewrote the compositing code. Instead of operating on a per-pixel basis we work on scanlines. Speedup varies between 2x and 10x times dependingly on many factors. Unfortunately this doesn't mean that you can just be running compositing managers quite yet but it's a step in the right direction. Framebuffer reads are once again the limiting factor of the whole thing. So there's still lots and lots of work to do. We have some ideas for other things and we'll start working on them soon. So yeah, fun :)

Either way I'd be really happy to just drop a lot of old and unused stuff from the X server for 7.0. Unfortunately it's very likely that this will never happen. Even Render has paths which are never used. How can I be so certain of it? Because when working on the rewrite we noticed that the old code for some formats was just wrong and could never work. And to be honest the biggest challenge of rewriting the compositing code wasn't the actual code, it was testing of it. There are things that I'm pretty sure no one ever used so we've spent a while just writing examples. 

I worked a little bit on Impresario2 lately. Impresario2 will be basically a 3D window manager + 2D window manager in one. I started on merging in the 3D code in the simple 2D infrastructure. All that forced the split of it into: XMirror - which monitors all toplevel Window's and syncs them into client side data, with that data you can do anything you want (create textures or just plain images and manipulate them), libpixml - which is basically an XML drawing library, I'm contemplating making it compatible with at least some basic Avalon XAML semantics, it will be used to render themes, core - which is the part of the window manager that can adapt to both 2D and 3D environment. 

Also people have been asking me about Cairo so lets get something clear: right now there's no point in Arthur using Cairo. There's just absolutely no good reason for it. I will be closely watching Cairo and if the need arises I'll start working on fixing the issues we have with it, but as of right now there's just no good reason for KDE to be using Cairo. None. None whatsoever. Please do reread the last paragraph.  
<!--break-->