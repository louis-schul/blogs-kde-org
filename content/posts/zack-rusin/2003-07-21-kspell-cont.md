---
title:   "KSpell cont."
date:    2003-07-21
authors:
  - zack rusin
slug:    kspell-cont
---
I don't like the fact that I have to be coming up with topics for those entries. I think I'll just start putting dates for them. People who want to know what I'm doing will find them anyway. 
I haven't done anything too interesting today. Worked on KSpell a little more. Cleaned it up a little and closed two bugs, from which the more interesting is that you can now check HTML/SGML/XML and Tex/LaTeX documents with KSpell and it will automatically skip the tags/commands and it will spell check only the actual contents. It should make spellchecking in such applications as Quanta and KTextMaker/Kile a lot more friendly. 
I'm trying to come up with a new layout for the spell checking dialog. If you're missing something in our current implementation let me know. Oh, and you can safely skip the fact that you want to be able to select a language right from the dialog. I have two reports about that and that fact alone is one of the reasons I'm planning to redesign it. Maybe our usability team will make itself useful ;)
I put an image of me in my hacking corner <a href="https://blogs.kde.org/image/uid/14">here</a> or simply check my photo gallery. For some reason people who have never seen me imagine me differently. Not sure if that's good :) Also not really KDE, more my-life, related, still a great <a href="http://www.automatix.de/~zack/0000003.JPG">picture</a>. I wasn't really supposed to be posting it on the net, but I liked it too much.