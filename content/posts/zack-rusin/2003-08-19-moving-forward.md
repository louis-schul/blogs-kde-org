---
title:   "Moving forward"
date:    2003-08-19
authors:
  - zack rusin
slug:    moving-forward
---
I haven't added any blog entries for a while now. I'm in Germany, eagerly awaiting n7y. The basic functionality of KConfEdit is there. I think it's pretty much ready to move it to kdeadmin. I disabled the saving of configs because I don't want it to be doing that till I'm confident everything is working fine. I got the new ui made by Aaron in KSpell, it still needs some slot connections (most notably the Suggest stuff). <br/>
In my spare time I'm playing with my pet project: Gadget. It's a services/search oriented application, completely plugin based. If right now you're going: "a what?". You need to look at <a href="http://www.apple.com/macosx/jaguar/sherlock.html">Sherlock</a> or <a href="http://www.karelia.com/watson/">Watson</a> applications. It havily uses kjsembed. All the plugins are written in javascript. I might add a native interface after I'm happy with the javascript one. Why would you want to use javascript in plugins? Because it's super easy. With kjsembed you can load Designer generated ui files, connect signals to your js slots, load read-only kparts and use dcop - all in a very few lines of code. Cool stuff. Quite possibly I'll add support for Sherlock plugins, but for that XQuery would be required. Rich has got an alpha XPath implementation and Tim expressed interest in such a beast as well, so maybe the three of us can come with something nice up.