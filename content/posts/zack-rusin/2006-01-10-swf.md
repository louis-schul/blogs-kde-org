---
title:   "SWF"
date:    2006-01-10
authors:
  - zack rusin
slug:    swf
---
Lets talk SWF. A lot of people seem to be mentioning <a href="http://www.gnu.org/software/gnash/">Gnash</a> today. Some seem to think it's a giant step for FSF. I think it's really funny. It's really funny (or sad - it depends how you look at it) how a giant win for FSF is taking a wonderful Public Domain <a href="http://www.tulrich.com/geekstuff/gameswf.html">project</a> and basically just releasing it under GNU GPL.

So <a href="http://blogs.gnome.org/view/uraeus/2006/01/10/0">Christian</a>, GNU Flash and Gnash at least were technically two different projects. GNU Flash is (was?) technically GPLFLash2 which is LGPL licensed (how confusing can you get, eh?), while Gnash is using a relicensed Public Domain SWF decoding library making the whole thing GPL. gameswf has been put in the Public Domain so assigning copyrights to FSF and relicensing everything without making any substantial changes is completely legal. In my opinion morally it's not the most wonderful thing one could do though. 

<a href="http://ariya.blogspot.com/2006/01/arthuring-gnash.html">Ariya</a> mentions that we could be Arthuring Gnash. Unfortunately, no we really couldn't. Most of the greatness of gameswf, which Gnash is using, is that it implements its own rendering model on top of OpenGL. That rendering model is rather tightly coupled with gameswf (as it's the core of it) so abstracting it would involve refactoring most of the code. 

As to whether having SWF support in KDE would be beneficial let me just mention that of course we have it. I did it about two months ago and I have been contemplating putting it along QtSvg but decided that it's pointless (a little bit about reasons later). SWF is very different from SVG though. Yes, they both are technically vector graphics formats, but their usage is completely different. SWF is used to create complete applications. How many applications written in SVG is there?  
There's hundreds of different custom installers for SWF files which turn them into .exe files and allow companies to easily deploy those files as standalone apps. 

Not having an open library fully implementing SWF is not a problem. FSF created a flawed problem definition. Gnash, in my opinion, is a flawed solution to a self-created problem. 

Format is not a problem. People don't care about formats at all. How many people use hex editors to create SWF files? Right, zero. The reason people use SWF is because the creator for them is simply really good. It's very simple and allows them to create compelling GUI's in no time with very little, if at all, programming knowledge. The fact that we don't have an answer to that is the problem. The fact that we don't have anything that could compete with it is exactly the problem. Use of SWF across the web is just a symptom. 

And by no means am I implying that creating a SWF editor (e.g. Flash4Linux) would be a solution. It would be a very neat project (like Flash4Linux is) but it is not a solution. The reason is following: when you're competing with a product you have to give people a compelling arguments to switch. Therefore having exactly the same functionality as the application you are competing against is not enough. Why would people switch if they already have everything you provide? Why would they switch if you're implementing just a little more? If you're trying to compete with a product by trying to copy its  functionality - you already lost. Furthermore by consistently following vision of another project, you lose your own and you willingly part with the only thing that could make you competitive which is originality. 

The way to compete is by going back and asking yourself what was the scope of the problem the original project was trying to solve? What did they manage to solve? What are the main complains of people who are using it? What are people using it for? What are the main features people love about it? If you have those you are well on your way. Next year it's not Macromedia Flash and SWF anymore but <a href="http://www.microsoft.com/products/expression/en/graphic_designer/gd_features.aspx">Microsoft Expression</a> and XAML, the year after it could be something else. We need to provide a product that stands on its own merits and presents a better solution to the problem of quick and easy creation of easily deployable applications (well, that and stupid little animations ;) ). Qt Designer or Glade are not enough because they allow very limited artistic freedom and in comparison to what you can do in Flash or Expression apps created in Designer/Glade just look ugly.  

I guess now it's obvious why Trolltech will be working on scene graph for the graphical basis of the toolkit. 

<!--break-->