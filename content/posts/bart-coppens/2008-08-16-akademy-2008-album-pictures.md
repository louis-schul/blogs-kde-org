---
title:   "Akademy 2008: Album with pictures"
date:    2008-08-16
authors:
  - bart coppens
slug:    akademy-2008-album-pictures
---
After a lot of deliberating this, I decided to put online an album with a lot of the pictures I took at Akademy. Since most people at (big) conferences and community gatherings like it when they can have some form of memento (including me!) of themselves and the event, I think this outweighs the (very few) people who didn't seem to like their picture being taken. Especially since most of the people actually tried posing in different positions when I came near them with my camera, which I think means obviously that they would want to see themselves afterwards :P

To the best of my abilities of remembering those who did not like pictures being taken, I have not put online the pictures of those people, neither have I uploaded the pictures of somewhat embarrassing nature (like people sleeping during the presentations ;)). I've also not uploaded the worst out-of-focus ones, blurry ones, moved ones, etc. This brings me to a total of 685 pictures currently being available, out of 939 in my local album, which I still think is rather much ;) (This means it's also rather much bandwith that this will suck, so please be kind to my webhost ;))

If you really don't want to be in one of the pics I put online, give me a sign and I'll try to fix it (to make sure it's not being indexed by the web archive and then you start complaining your pic is being archived over there, I put up a robots.txt over this, which is very very sad, but I really don't want people complaining afterwards). Similarly, if you really like a certain picture of you and want the original-sized one, also don't hesitate to ask for it.

<b>So, after all that, here are <a href="http://www.bartcoppens.be/photos/Akademy2008/index.html">my photos of Akademy 2008</a></b>

PS: The weird ordering of the pictures (seemingly not chronological or alphabetically sorted by filename) is not my fault: I blame digiKam

Update 17/08/08: Removed 5 photos but added a few more, bringing us to a total of 690 pictures. Also added an explicit license: Creative Commons Attribution-Noncommercial-Share Alike 3.0.
<!--break-->