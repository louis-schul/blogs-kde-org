---
title:   "Back in Fuerth"
date:    2005-09-07
authors:
  - coolo
slug:    back-fuerth
---
I don't know about other cities, but for Nuernberg/Fuerth the definite sign for being home is entering the metro and the driver saying "Pleahz schteb bek" (trying to mimik franconian accent in English isn't really easy :) as the train leaves the stations. I know several people (including my mother) that insist on taking the metro if they are around just for that to happen. And just today I was in a train where the driver tried to speak plain german, but then again in the tone of a city guide. Very nice touch for those getting up early actually :)

I just missed to hear about the weather conditions at Lorenzkirche, that would have polished it all. I will really miss this daily comedy show when they switch to all automatic trains (still some time ahead though). 

How about this one, Miss Varcuzzo?
A guy walks into a bar and orders a drink. After a few more he needs to go to the can. He doesn't want anyone to steal his drink so he puts a sign on it saying, "I spat in this beer, do not drink!". After a few minutes he returns and there is another sign next to his beer saying, "So did I!"
<!--break-->
