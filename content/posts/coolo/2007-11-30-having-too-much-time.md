---
title:   "Having too much time"
date:    2007-11-30
authors:
  - coolo
slug:    having-too-much-time
---
<p>Due to our little <a href="http://idea.opensuse.org/content/">hackweek project</a> winning in two categories, I have the rest of the year so called "Innovative time off". In the beginning 10 days ITO sounded like nothing, but now I'm kind of forced go give up on real work and do fancy experiments :)</p>

<p>
So I tried something. Certain voices claimed, porting Yast to Qt4 would be almost impossible due to the different event loops and threading and all that. But thanks to Thiago's little explanations on how things to go together, it was actually a job done pretty quickly (it was once tried in july, so I didn't start from scratch). Damn! Did I say, I have the rest of the year?</p>

<p>Anyway, here you can compare qt3 to qt4 when running the infamouse Wizard3.ycp example:</p>
<!--break-->
<p>Qt3:
<a href="http://ktown.kde.org/~coolo/yast1.png"><img src="http://ktown.kde.org/~coolo/yast1.png" width="512"></a></p>
<p>Qt4:
<a href="http://ktown.kde.org/~coolo/yast4.png"><img src="http://ktown.kde.org/~coolo/yast4.png" width="512"></a></p>

<p>As you can easily see, I have one bug remaining: the cool gradients are missing. So I had two choices: either fix it or remove them all together. Asking around I couldn't find someone too attached to them, so I went for the latter:</p>

<p><a href="http://ktown.kde.org/~coolo/yast2.png"><img src="http://ktown.kde.org/~coolo/yast2.png" width="512"></a></p>

<p>This looks pretty plain I thought, but browsing on planet.kde.org (don't blame me, the O in ITO stands for "off"!) I happened to saw girish's blog entry about theming Qt, so I thought I give my theme designer capabilities a try. Thanks to Robert for showing me once more how to use inkscape I wrote a little qss file. And now if you start the above yast with -stylesheet <a href="http://svn.opensuse.org/svn/yast/branches/tmp/coolo/wizard-rework/theme/style.qss">style.qss</a>, it will look like this:</p>

<p><a href="http://ktown.kde.org/~coolo/yast3.png"><img src="http://ktown.kde.org/~coolo/yast3.png" width="512"></a></p>.

<p>
As you can see, I'm project manager for a reason: This looks pretty ugly. But the good news is: A designer can go from here and make the qss good looking. The bad news though: it's still not even december and I need to find another pet project to hack on.</p>
