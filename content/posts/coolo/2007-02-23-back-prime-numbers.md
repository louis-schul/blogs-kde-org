---
title:   "Back to prime numbers"
date:    2007-02-23
authors:
  - coolo
slug:    back-prime-numbers
---
Last year I turned 30 and I know people still wonder when I end up being as old as I look. But on the way to this I pass today the next prime number - and I must say it feels <b>much</b> better than last year. The main reason is that I have a new job. This may suprise one or two, but it shouldn't. 

Don't panic, I still work for SUSE. But I teamed up with my wife, who is also a software engineer and we decided to create a product on our own in the after hours. This is also the reason you may have missed me on the release-team mailing list so far. I just don't know how much time I will have left for KDE and openSUSE over this other project. It's a pretty radical change, so please bear with me if you expect me to do something that I don't do in the timeframe you're used to.

Anyway, what is this project about? I can't really say much about it at this point. It basically just passed early beta tests (which is also the reason I had some time for KConfig love) and we're both already pretty happy with it. I uploaded an early <a href="http://ktown.kde.org/~coolo/newjob.jpg">Screenshot</a> in case you wonder what it looks like. 

Of course it will take some time before we can release it - but as you know me, I'm the born release dude: we schedule for early august and perhaps KDE 4.0 can rally with it - I wouldn't mind if KDE beats us.
<!--break-->