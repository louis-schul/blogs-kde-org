---
title:   "Akonadi OpenChange (Exchange) status report"
date:    2008-11-07
authors:
  - brad hards
slug:    akonadi-openchange-exchange-status-report
---
The hard-working Danny Allen put out a new Commit Digest, and the resulting discussion on the Dot raised a few comments about Microsoft Exchange support in kdepim.

The short version is that it isn't going to make it for KDE 4.2. Maybe 4.3. Maybe not for 4.3 either. Sorry. 

The longer version is that I had some real-life work intrude (I'm only a volunteer developer), and also did some work on something I really do plan to use (being the .snp generator for Okular). More recently I've also been working on the underlying libraries (<a href="http://www.openchange.org">OpenChange</a>).  Those are hopefully going to be of some use to some of you too. I do believe this stuff is important, but it isn't the most important thing to me all of the time (in particular, when I have to work, or travel, or catch up on some sleep).

When dealing with MS Exchange, people often suggest "just use IMAP". I don't think that mail is the real reason why people use Exchange and Outlook. There are lots of ways to use IMAP clients (including Outlook) and servers (including Exchange) for mail. If it was just email, there wouldn't be much point to OpenChange. The real lock-in for most Outlook / Exchange houses is the shared calendaring (including Free/Busy), big use of contact lists, and Public Folders.  

The good news is that OpenChange is well advanced in supporting those, and the work that Alan Alvarez has done on the Akonadi OpenChange resource has set us up well for making some use of that.

The bad news is that right now the resource won't even compile (because of some changes of API in OpenChange), and  Free/Busy isn't supported in Akonadi yet. There is a long way to go.

It does seem to be taking a long time, and that is probably a fair criticism. Please understand that the Exchange protocol is very complex (some 20+ documents in the <a href="http://msdn.microsoft.com/en-au/library/cc425499.aspx">specification set</a>, and some of them are into the hundreds of pages) and no-one is working on this stuff full time. Julien Kerihuel is the closest we have to a full time developer of OpenChange, and he is mostly doing it on a self-funded basis. Certainly a lot of time invested and some great work, but still only a single developer. I'd also like to recognise the work of Jelmer Vernooij. So, not many people working on it, and a lot of work to do. When we get OpenChange into shape, I still have to put the work in the Akonadi resource.

Sorry.