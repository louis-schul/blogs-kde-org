---
title:   "Another update for mx610hack"
date:    2006-05-21
authors:
  - brad hards
slug:    another-update-mx610hack
---
If you don't care about Logitech MX610 mouse support, you can safely stop reading now...

I released an update to the mx610hack code about two months ago, but forgot to blog about it. I did announce it on the lomoco-dev mailing list, but there doesn't appear to be an archive. So you get it again.

I've figured out the IM led now. As before, you send 6 bytes to report ID 0x10 
(16), followed by the "confirm" message.

The magic values are:
unsigned char confirm[6] = { 0x01, 0x80, 0x52, 0x00, 0x00, 0x00 };
unsigned char email_off[6] =     { 0x01, 0x80, 0x52, 0x06, 0x00, 0x00 };
unsigned char email_on[6] =      { 0x01, 0x80, 0x52, 0x05, 0x00, 0x00 };
unsigned char email_pulse[6] =   { 0x01, 0x80, 0x52, 0x04, 0x00, 0x00 };
unsigned char email_flash[6] =   { 0x01, 0x80, 0x52, 0x03, 0x00, 0x00 };
unsigned char email_instanton[6] = { 0x01, 0x80, 0x52, 0x02, 0x00, 0x00 };
unsigned char email_instantoff[6] = { 0x01, 0x80, 0x52, 0x01, 0x00, 0x00 };

unsigned char im_off[6] =     { 0x01, 0x80, 0x52, 0x00, 0x06, 0x00 };
unsigned char im_on[6] =      { 0x01, 0x80, 0x52, 0x00, 0x05, 0x00 };
unsigned char im_pulse[6] =   { 0x01, 0x80, 0x52, 0x00, 0x04, 0x00 };
unsigned char im_flash[6] =   { 0x01, 0x80, 0x52, 0x00, 0x03, 0x00 };
unsigned char im_instantoff[6] = { 0x01, 0x80, 0x52, 0x00, 0x01, 0x00 };
unsigned char im_instanton[6] = { 0x01, 0x80, 0x52, 0x00, 0x02, 0x00 };

The normal on/off commands (0x05/0x06) give you a slow (ramp-like intensity 
change) transition. The 0x02/0x01 values give you step response.

There is a new version (0.3) of mx610hack available:
http://www.frogmouth.net/mx610hack-0.3.tar.gz