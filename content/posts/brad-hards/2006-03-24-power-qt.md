---
title:   "Power Qt"
date:    2006-03-24
authors:
  - brad hards
slug:    power-qt
---
The Canberra LUG and the <a href="http://cs.anu.edu.au">Department of Computer Science at the Australian National University</a> (in particular, Bob and Steve) have done some very nice work with a couple of Power5 boxes that <a href="http://www.ibm.com">IBM</a> has loaned out.

I took a chance to do some benchmarks of compile speed. Qt is important, so I decided to see how long it takes to build Qt (using qt-x11-opensource-src-4.1.3-snapshot-20060323). 
Answer:
real    26m19.481s
user    94m6.628s
sys     5m24.688s

That is on a four way partition, using gcc-4.0.3 from Debian etch (testing).