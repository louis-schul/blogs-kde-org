---
title:   "OpenChange at Exchange RPC Plugfest (24-27 Jan 2011)"
date:    2011-02-12
authors:
  - brad hards
slug:    openchange-exchange-rpc-plugfest-24-27-jan-2011
---
Late last month, I was fortunate to be invited to Microsoft for the Exchange RPC Plugfest as part of the OpenChange team.

I decided to arrive into Redmond a few days early to get over the worst of the jetlag, and to spend some pre-plugfest hacking time with Julien Kerihuel and Jelmer Vernooij. That produced some excellent planning work, and a bit of new code. I also caught up with Tom Devey (a Microsoft consultant, who is basically the customer representative for Exchange RPC protocols) and enjoyed a nice wood-fired pizza lunch, and some night skiing at Stevens Pass. My skiing was never really good, and lack of practice and some pretty unusual conditions didn't help. It was still a lot of fun though.

The Exchange RPC plugfest kicked off on Monday (24 January 2011), with breakfast and getting oriented in the lab environment. The labs we used were in the "Platform Adoption Center" (Building 20 on the Microsoft campus at Redmond), which is a nice facility for this kind of thing. Monday afternoon had presentations from Simon Xiao and Xianming Xu, who were part of the team from Microsoft (well, a contract / outsource organisation paid by Microsoft) on the test suite we'd be using to test OpenChange with for the rest of the week. In this context, the testing is aimed at protocol specification compliance. Microsoft has similar tests for some of the windows protocols, but the suite we used was specific to the Exchange RPC ("MAPI") wire-protocol.

Over the course of the week, the test suites showed us quite a number of issues (from minor non-compliances through to crashing the server, and occasionally causing problems with the test suite). We fixed some of it during the plugfest, and we also did some refactoring of the backend store that will allow us to address other issues and build a more stable and reliable server.

We typically ran tests from 0830 through to about 1900 (sometimes a bit earlier / later) on Tuesday, Wednesday and Thursday. Microsoft had also arranged Exchange developers (Darrell Brunsch, Joe Warren and Juan Pablo Muraira) to give presentations, which helped enormously with understanding the protocol. Although it took time away from the testing, it was still well worth while to both attend the talks and to discuss things before and after the talks.

There we also "shared" talks with the Active Directory plugfest that was going on at the same time, and presentations by Nick Meier (one of only a few Linux guys inside Microsoft) and Paul Long (on Netmon) amongst other talks helped a lot. Seeing the inside of the "Enterprise Engineering Center" was also illuminating - thanks to Darryl Welch for organising the tour and the EEC team for hosting us.

It was a real shame to miss LCA 2011 (since I'd already committed to the Plugfest when the LCA dates changed), but given what we learned about the OpenChange server, then I'd make the same decision again.

Thanks everyone who attended and contributed, but particularly to Virginia Bing and Tom Devey for all the organisational work and putting on the event.