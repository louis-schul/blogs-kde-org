---
title:   "dealing with Microsoft Exchange, when you really want to use SMTP"
date:    2010-12-09
authors:
  - brad hards
slug:    dealing-microsoft-exchange-when-you-really-want-use-smtp
---
I've been doing some work on <a href="http://www.openchange.org>OpenChange</a>, including parsing RFC2822 format messages into Exchange RPC properties. One of the test tools I have can parse up some kinds of RFC2822 / MIME messages (plain text, HTML, some mime/alternative and text/calendar) and upload the results to a Microsoft Exchange server as a particular user.

With a little bit of unix-style tool combination, you can plug this into something like Postfix. So if you're in a situation where you want to integrate a tool that wants to send mail via SMTP, but your network is pretty much "Exchange." (and you can't send SMTP directly), then this might be useful.
<!--break-->
When using Postfix, you'd add a transport entry, either a wildcard:
<pre>
* oc:localhost
</pre>
or a restricted list if you like.

Enable the transport mapping in main.cf:
<pre>
transport_maps = hash:/etc/postfix/transport
</pre>

Then add an entry to master.cf to match the required transport:
<pre>
oc	unix	-	n	n	-	1	pipe
   flags= user=bradh argv=/path/to/script.sh
</pre>

where the script receives the mail, and invokes the oxcmail_test application:
<pre>
#!/bin/sh

SCRIPT_TMP_DIR=/var/spool/myscript
OC_SEND="/home/bradh/openchange/branches/oxcmail/liboxcmail/test/oxcmail_test --dump-data -d9 --database=/home/bradh/.openchange/profiles.ldb"

# you could also choose to remove $SCRIPT_TMP_DIR/oc_send.$$.log if you don't need the records
trap "rm -f $SCRIPT_TMP_DIR/$$.msg" 0 1 2 3 15

cat > $SCRIPT_TMP_DIR/$$.msg

$OC_SEND $SCRIPT_TMP_DIR/$$.msg > $SCRIPT_TMP_DIR/oc_send.$$.log

exit $?
</pre>

I would caution that the code is pretty experimental - its still in a branch for a reason! However I would appreciate some external testers if you're feeling particularly brave, and have the right kind of environment for testing this. You can pick up the code from svn (http://svnmirror.openchange.org/openchange/branches/oxcmail) and build it using the <a href="http://tracker.openchange.org/projects/openchange/wiki/HowTo_Install_OpenChange_From_Source">instructions on the OpenChange wiki</a>. In addition, note that you can't usually set the From address, so you'll probably be restricted to sending from one pre-configured user (or perhaps a small set of users)

If you have any questions, try to catch me on IRC (#openchange on freenode), leave blog comments or use the <a href="http://mailman.openchange.org/listinfo/devel"> devel mailing list</a>. 

Future plans involve a sendmail-like application for those applications that want to sendmail instead of SMTP. Hopefully that will allow me to send from KMail without needing to go via the SMTP path.