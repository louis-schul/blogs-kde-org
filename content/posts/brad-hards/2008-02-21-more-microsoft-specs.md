---
title:   "More microsoft specs"
date:    2008-02-21
authors:
  - brad hards
slug:    more-microsoft-specs
---
For those that missed it, Microsoft recently released some of the specs relating to their office suite (<a href="http://www.microsoft.com/interop/docs/OfficeBinaryFormats.mspx">http://www.microsoft.com/interop/docs/OfficeBinaryFormats.mspx</a>).

Naturally, they probably aren't everything we'd like to have (various people have suggested that they aren't complete, and I'd be surprised if there wasn't something omitted, more likely because of incomplete records than because of deliberate omission - the PR downside to deliberate omission becoming public would be a disaster).

However for KOffice, the biggest problem isn't going to be incomplete specs - it is more likely going to be insufficient developers. Right now, it looks like there is a huge amount of work going into ODF, and .doc is clearly a lower priority.

So if you have time and interest, ODF or import for the legacy Microsoft stuff would be a great thing to work on!

Edit: forgot to mention that the format options are PDF and XPS. Interesting to see Microsoft continuing to push XPS.