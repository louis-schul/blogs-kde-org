---
title:   "Compound File Binary Format"
date:    2007-12-06
authors:
  - brad hards
slug:    compound-file-binary-format
---
Dear lazyweb,

I'm doing some research for a weekend KDE project, and you're all invited to help me :-)

Is there anything in KDE (or Qt4) that can read the <a href="http://en.wikipedia.org/wiki/Compound_File_Binary_Format">Compound File Binary Format</a>?

I'm also looking for code to borrow that can extract a file out of a CAB format file (I know about cabextract and its library version libmspack, but no-one packages libmspack, and I don't want to depend on a binary).

Suggestions welcome :-)