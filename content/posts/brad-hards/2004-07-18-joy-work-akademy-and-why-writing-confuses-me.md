---
title:   "The joy of work, aKademy, and why writing confuses me"
date:    2004-07-18
authors:
  - brad hards
slug:    joy-work-akademy-and-why-writing-confuses-me
---
I've been slack, busy and confused. Hence the lack of blog.

The one thing that I have done, based on a likely need to refer to it to write up my talk for <a href="http://conference2004.kde.org">aKademy</a> is to mostly complete the KFile meta-data tutorial. The bit that got added yesterday and today is on <a href="http://developer.kde.org/documentation/tutorials/kfile-plugin/x503.html">using meta-data plugins in your application</a>. So it is basically a description of the other side of the kde:KFileMetaInfo interface. In some respects, its a bit lame, because I didn't really have a good insight into what you might need beyond kde:KPropertiesDialog in a real application. Maybe I can learn more at Wheels' presentation at <a href="http://conference2004.kde.org">aKademy</a>. If anyone has something to add to either of my tutorials, just commit in the SGML docbook source for the tutorial (and CCMAIL: me, if applicable).

I have been doing a bit of thinking about why I write documentation. I think it is different to the motivation to code, in that coding is an intellectual challenge, while documentation is more of a community service. That isn't to say that there isn't substantial intellectual challenge in documentation (especially documentation written about programming, like the <a href="http://developer.kde.org/documentation/tutorials/kfile-plugin/t1.html">Meta data tutorial</a>), but that it feels different when writing it. Even bug-fixing provides a little thrill when things get to work, but documentation mostly leaves me feeling ambivalent about the outcome. Maybe I only write documentation because I need it so badly when I code...

