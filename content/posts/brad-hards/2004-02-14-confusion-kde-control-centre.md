---
title:   "Confusion in KDE Control Centre"
date:    2004-02-14
authors:
  - brad hards
slug:    confusion-kde-control-centre
---
I've been thinking about some of the stuff that Frans English has brought up about KDE Control Centre. While I don't agree with some of the things he's written, there is certainly a case for cleanup in some of the hardware settings.

Some of the cleanup is going to mean breaking up some of the existing kcm's. The real culprits are some of the KMilo plugins. Make no mistake - I love the support for special Vaio features - Windows reconfigures the screen brightness on my laptop depending on power profile, and it stays stuck at that brightness level through a full reboot. Without Vaio support, I can't see what is on the screen if the last time I used Windows it was on battery (say I pulled the power supply connector just before shutdown).

But finding the module in the Control Centre still confuses me sometimes. Changing the screen brightness, or checking battery power isn't a System Administration function. Bits of it need to go into the Power Control->Laptop Battery module, and into the Peripherals->Display module. For things like the battery status, it needs proper integration (eg so it only polls APM for battery level if there isn't a smarter notification element). Similarly, some of it might need to go into Peripherals->Mouse.

I guess that there are likely some similar issues for other similar things (eg the IBM thinkpad). Certainly I expected to find keyboard shortcuts (and KHotKeys, and keyboard layout) within Peripherals, not within Regional and Accessibility.

A first step would be to make a clear definition of what belongs in what top level division within the KDE Control Centre. Re-aligning the functionality to match that division can then follow on.