---
title:   "API docs and the joy of Notes"
date:    2004-10-13
authors:
  - brad hards
slug:    api-docs-and-joy-notes
---
I've been doing some work on the API doco, while I try to figure out what is going wrong with Linux hotplug support. API doco is pretty easy, but tedious work. Mostly I've just been doing cleanups, with a few new widget images. Unfortunately it takes a while for http://api.kde.org to catch up.

The Logitech mouse stuff is now working (sans smartscroll support and the manual update), but I really need to fix the automagical permissions stuff, so you don't have to expose the whole of the USB filesystem. Then I'll commit, and start looking for some usability support on it. How are we going with a usability support team anyway?

Also been working a lot, getting tired with the change of seasons. Most out-there "what the hey!" moment was having Lotus Notes crash. It says (in a dialog):

The exception Breakpoint
A breakpoint has been reached.
(0x8000003) occured in the application at location ....

