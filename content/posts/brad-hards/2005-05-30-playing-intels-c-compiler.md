---
title:   "Playing with Intel's C Compiler"
date:    2005-05-30
authors:
  - brad hards
slug:    playing-intels-c-compiler
---
Building the Qt4 snapshot from today with -platform linux-icc :
real    52m9.573s
user    46m17.119s
sys     4m38.787s

Building the same Qt4 snapshot with -platform linux-g++ :
real    72m12.090s
user    67m30.458s
sys     3m58.214s

g++ is version 3.4.3 (from FC3), icc is version 8.1. Not a very scientific test of tests, but
a fairly large margin.

There are some nice looking features in the ICC suite, especially in terms of code coverage. I haven't been having a lot of success with gcov, and although I haven't tried it yet, if the <a href="http://www.intel.com/software/products/compilers/code_test/">documentation</a> is to be believed, then it has some interesting possibilities for code coverage and test prioritisation. It would be ideal if we could compile test and run the applicable test cases after every commit to the subversion repository, getting immediate feedback. Perhaps one day we will have a large enough icecream farm, enough tests, enough workload data and the right tools.

I also like the idea of having more than one compiler - more likely that two different compilers will find something silly that I've done. I did note a few warnings with ICC on the Qt4 build that I hadn't noticed with gcc. I don't have an AMD machine, and while I understand that there may be issues with Intel's compiler and AMDs instruction set, I don't plan to release binaries, so no big deal to me (although I do see both sides of the argument).

If you're interested, you can get a non-commercial license for ICC from Intel for the cost of the download - <a href="http://www.intel.com/software/products/noncom/index.htm">http://www.intel.com/software/products/noncom/index.htm</a>

There are a couple of other itnresting things in there too - Intel Performance Primitives (basically hand tuned libraries for some operations) might be a potential back-end for Qt Cryptographic Architecture.


