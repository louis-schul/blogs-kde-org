---
title:   "New C++/Qt code checks in clazy static analyzer"
date:    2015-11-15
authors:
  - sergio.martins
slug:    new-cqt-code-checks-clazy-static-analyzer
---
About two months ago I blogged about clazy,  a <a href="https://www.kdab.com/use-static-analysis-improve-performance">Qt oriented static analyser</a>.

Since then it has moved to an official KDE repo <a href="http://anongit.kde.org/clazy">http://anongit.kde.org/clazy</a>, got an IRC channel (#kde-clazy @ freenode) and also many fun new checks.

Here's a quick dump of the new features (be sure to see the README for the old checks too):

<b> **** Performance related checks ****</b>

<b>qstring-arg</b>: (saves allocations)
<code>s5 = QString("%1 %2 %3 %4").arg(s).arg(s1).arg(s3, s4);</code>
main.cpp: warning: Use multi-arg instead [-Wclazy-qstring-arg]

<b>qstring-ref</b>: (saves allocations, fixit included)
<code>int n = str.mid(1, 1).toInt(&ok);</code>
main.cpp: warning: Use midRef() instead [-Wclazy-qstring-ref]

<b>qdatetime-utc</b>: (saves allocations)
<code>QDateTime::currentDateTime().toUTC();</code>
main.cpp: warning: Use QDateTime::currentDateTimeUtc() instead [-Wclazy-qdatetime-utc]

<b>qgetenv</b> (saves allocations):
<code>qgetenv("Foo").isEmpty();</code>
main.cpp: warning: qgetenv().isEmpty() allocates. Use qEnvironmentVariableIsEmpty() instead [-Wclazy-qgetenv]

<b>range-loop</b>:
<code>for (int i : getQtList()) {</code>
range-loop.cpp warning: c++11 range-loop might detach Qt container [-Wclazy-foreach]

<b>foreach</b>:
<code>foreach (int i, v) {</code>
main.cpp: warning: foreach with STL container causes deep-copy [-Wclazy-foreach]

<b>old-style-connect</b>:
<code>connect(ptr, SIGNAL(signal1()), SLOT(slot1()));</code>
main.cpp: warning: Old Style Connect [-Wclazy-old-style-connect]

<b>qDeleteAll</b>:
<code>qDeleteAll(s.values());</code>
main.cpp: warning: Calling qDeleteAll with QSet::values, call qDeleteAll on the container itself [-Wclazy-qdeleteall]

<b> **** Crash related checks ****</b>

<b>temporary-iterator</b>:
<code>for (auto it = getList().begin(); it != getList().end(); ++it) {}</code>
main.cpp: warning: Don't call QList::end() on temporary [-Wclazy-temporary-iterator]

<b>auto-unexpected-qstringbuilder</b>:
<code>const auto s1 = "tests/" + QString::fromLatin1("foo");</code>
main.cpp: warning: auto deduced to be QStringBuilder instead of QString. Possible crash. [-Wclazy-auto-unexpected-qstringbuilder]

<b> **** Misc related checks ****</b>

<b>writing-to-temporary</b>:
<code>widget->sizePolicy().setVerticalStretch(1);</code>
main.cpp: warning: Call to temporary is a no-op: QSizePolicy::setVerticalStretch [-Wclazy-writing-to-temporary]

<b>implicit-casts</b>:
<code>explicit ComposerLineEdit(bool useCompletion, QWidget *parent = Q_NULLPTR);
        RecipientLineEdit::RecipientLineEdit(QWidget *parent) : ComposerLineEdit(parent)  <-- oops, passing pointer as firt argument, which is a bool</code>
main.cpp warning: Implicit pointer to bool cast (argument 1) [-Wclazy-implicit-casts]

Also, <i>old-style-connect</i> and <i>qstring-ref</i> checks got fixits, so you can fix your code just by recompiling it with clazy.

Big thanks to everyone who contributed ideas and even code!

Please report bugs at <a href="https://bugs.kde.org/enter_bug.cgi?product=clazy">b.k.o</a>, happy hacking!