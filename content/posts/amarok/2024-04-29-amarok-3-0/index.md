---
title: Amarok 3.0 "Castaway" released!
date: "2024-04-29T20:00:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
images:
 - https://cdn.kde.org/screenshots/amarok/amarok30.png
---

The Amarok Development Squad is happy to announce the immediate availability of [Amarok 3.0 "Castaway"!](https://apps.kde.org/amarok/)

![Amarok 3.0 screenshot](https://cdn.kde.org/screenshots/amarok/amarok30.png)

The new 3.0 is the first stable Qt5/KDE Frameworks 5 based version of Amarok, and first stable release since 2018,
when the final Qt4 based version 2.9.0 was released.

The road to 3.0 has not been a short one. Much of the Qt5/KF5 porting was done in 2015 already, but finishing and polishing everything up has been
a slow, sometimes ongoing and sometimes stalled process ever since.
3.0 Alpha was released in February 2021 and has been since used by many people, as have been nightly builds of git master available for various distributions.
Now in the past few months, an effort was made to get everything ready for a proper 3.0 release.

Common usecases should work quite well, and in addition to fixing KF5 port related regressions reported in pre-releases,
3.0 features many bugfixes and implemented features for longstanding issues, the oldest such documented being from 2009.
However, with more than 20 years of development history, it is likely that not every feature Amarok has been tested thoroughly in the new release, and specifically
some Internet services that have changed their API in recent years are not available, at least for now.
It might well be that getting them in better state wouldn't require huge effort, however, so if you know your way with Qt and KDE Frameworks and your favourite Internet music service does not work with Amarok 3.0,
you are extremely welcome to join in and help!

In the following months, minor releases containing small fixes and additions, based on both newly reported and longer-standing bug reports and feature requests, are to be expected.
Work on porting to Amarok to Qt6/KDE Frameworks 6 should start in the following months, the goal being to have a usable Qt6/KF6 based beta version in 2024 still.



One should observe that due to scripting framework port from QtScript to QJSEngine still being a work in progress,
previous Amarok 2.x scripts are often not compatible. The script API documentation at [community wiki](https://community.kde.org/Amarok/Development/Script_API) is also partially out of date.
Additionally, due to incompatibilities and other issues, KNewStuff downloading of scripts is disabled for the time being.
Having script support in more polished shape is something to work on after an initial Qt6/KF6 version starts to be usable.
It is also evident that [the web site](https://amarok.kde.org) and [community wiki pages](https://community.kde.org/Amarok) largely originate from more than ten years ago,
and contain partially outdated information. Some work on refreshing them and pruning the documentation to make it more maintainable is likely to happen during the following months.

### *Now it's time to Rediscover Your Music in the 2020's!*

#### Changes since 3.0 Beta (2.9.82)

##### FEATURES:
* Added a visual hint that context view applets can be resized in edit mode.
* Display missing metadata errors in Wikipedia applet UI.
* Add a button to stop automatic Wikipedia page updating. ([BR 485813](https://bugs.kde.org/485813))

##### CHANGES:
* Replace defunct lyricwiki with lyrics.ovh as lyrics provider for now. ([BR 455937](https://bugs.kde.org/455937))
* Show only relevant items in wikipedia applet right click menu ([BR 323941](https://bugs.kde.org/323941)), use
 monobook skin for opened links and silently ignore non-wikipedia links.
* Don't show non-functional play mode controls in dynamic mode ([BR 287055](https://bugs.kde.org/287055))

##### BUGFIXES:
* Fix loading of some Flickr photos in the photos context view applet and show more relevant photos. ([BR 317108](https://bugs.kde.org/317108))
* Fix playlist inline play control slider knob & draw playlist delegate icons with
 higher DPI.
* Fix searching for composer and album info for local files in Wikipedia applet.
* Don't remove wrong songs from collection when contents of a folder, whose name is
 a substring of another collection folder, are changed ([BR 475528](https://bugs.kde.org/475528))
* Prefer symbolic systray icon to fix colours in Plasma6 systray ([BR 485748](https://bugs.kde.org/485748))

The complete ChangeLog, which includes the pre-releases, is available
[in the git repository](https://invent.kde.org/multimedia/amarok/-/blob/master/ChangeLog).

To provide some insight on the road from 2.9.0 to 3.0.0,
statistics collected from git repository are presented:

##### Commits and added/removed lines of code between 2.9.0 and 3.0 alpha (2.9.71)
l10n daemon script:       117 commits, +898, -192  
Heiko Becker:             72 commits, +5641, -2112  
Laurent Montel:           69 commits, +9478, -9697  
Aroonav Mishra:           65 commits, +15474, -6808  
Pino Toscano:             31 commits, +6892, -1637  
Malte Veerman:            30 commits, +19466, -29990  
Olivier CHURLAUD:         27 commits, +1106, -474  
Yuri Chornoivan:          19 commits, +966, -806  
Pedro de Carvalho Gomes:  8 commits, +145, -407  
Pedro Gomes:              7 commits, +7222, -805  
Luigi Toscano:            7 commits, +15, -14  
Mark Kretschmann:         6 commits, +27, -17  
Wolfgang Bauer:           5 commits, +31, -7  
Tuomas Nurmi:             4 commits, +39, -23  
Stefan Derkits:           4 commits, +20, -19  
Andreas Sturmlechner:     3 commits, +189, -75  
Aditya Dev Sharma:        3 commits, +47, -46  
Stephan Wezel:            2 commits, +12, -7  
Andreas Sturmlechner:     2 commits, +8, -6  
Andreas Hartmetz:         2 commits, +2, -2  
Victor Mataré:            1 commits, +7, -3  
Tobias C. Berner:         1 commits, +5, -1  
Thiago Sueto:             1 commits, +1, -1  
Sven Eckelmann:           1 commits, +5, -3  
Somsubhra Bairi:          1 commits, +1, -1  
Simon Depiets:            1 commits, +2, -2  
Rishabh Gupta:            1 commits, +1, -4  
Nicolas Lécureuil:        1 commits, +4, -2  
Nate Graham:              1 commits, +7, -7  
Johnny Jazeix:            1 commits, +2, -2  
Elbin Pallimalil:         1 commits, +11, -5  
Christophe Giboudeaux:    1 commits, +1, -2  
Antonio Rojas:            1 commits, +1, -0  
Alexandr Akulich:         1 commits, +1, -1  
Albert Astals Cid:        1 commits, +1, -0  

##### Commits and added/removed lines of code between 3.0 alpha 2.9.71 and 3.0.0
l10n daemon script:       317 commits, +1597783, -75585  
Tuomas Nurmi:             147 commits, +3813, -1550  
Friedrich W. H. Kossebau: 9 commits, +1075, -1044  
Jürgen Thomann:           8 commits, +130, -101  
Heiko Becker:             8 commits, +187, -19  
Pino Toscano:             6 commits, +3361, -24  
Toni Asensi Esteve:       4 commits, +100, -13  
Pedro de Carvalho Gomes:  4 commits, +51, -9  
Mihkel Tõnnov:            4 commits, +4486, -800  
Zixing Liu:               2 commits, +140, -8  
Fabian Vogt:              2 commits, +9, -0  
David Faure:              2 commits, +4047, -15  
Damir Islamov:            2 commits, +401, -420  
Yuri Chornoivan:          1 commits, +1, -1  
Sebastian Engel:          1 commits, +21, -21  
Nicolas Fella:            1 commits, +1, -1  
Nicolás Alvarez:          1 commits, +7, -7  
Nate Graham:              1 commits, +1, -0  
Matthias Mailänder:       1 commits, +5, -0  
Jonathan Esk-Riddell:     1 commits, +2, -6  
Jakob Meng:               1 commits, +1, -1  
Heiko Becker:             1 commits, +17, -17  
Christophe Giboudeaux:    1 commits, +3, -4  
Carl Schwan:              1 commits, +7, -2  
Boris Pek:                1 commits, +1, -1  
Andreas Sturmlechner:     1 commits, +2, -0  

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/amarok/3.0.0/amarok-3.0.0.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
