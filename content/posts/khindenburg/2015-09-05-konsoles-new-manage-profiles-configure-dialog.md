---
title:   "Konsole's new \"Manage Profiles\" Configure dialog"
date:    2015-09-05
authors:
  - khindenburg
slug:    konsoles-new-manage-profiles-configure-dialog
---
One of the goals I've had with Konsole was to combine all the configure dialogs: 1) Configure Konsole; 2) Manage Profiles; 3) Edit Profile.  Currently, in the branch config_dialog, the Manage Profiles is now combined with the main Configure dialog.  The only downside is that the Konsole part can't open the Manage Profiles dialog.  If and when the Edit Profile gets combined, I'll have to make sure the parts can open those dialogs.

If you use the Konsole kpart (Dolphin, yakuake, KDevelop, etc) and being unable to right-click Manage Profiles is something you can't do without, please let me know ASAP.   After I commit this, you would need to open Konsole and manage the profiles from there.

After this, the actual dialog UI needs to be re-coded for better usage.



