---
title:   "Nethack wtf?"
date:    2006-01-29
authors:
  - khindenburg
slug:    nethack-wtf
---
Why does this even mean?!?!?


  |./!+# 
  |.i.| 
 #+...| 
If I were a NetHack monster, I would be a tengu. I'm always in the right place at the right time, and am quick to avoid people that I'd rather not be with.