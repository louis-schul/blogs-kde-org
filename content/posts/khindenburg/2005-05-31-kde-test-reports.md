---
title:   "KDE test reports"
date:    2005-05-31
authors:
  - khindenburg
slug:    kde-test-reports
---
Since I still can't get kde4/qt-copy to compile I decided to look at creating some new scripts for icefox's KDE Automated Test Report (http://www.icefox.net/kde/tests/report.html).
<br>
I've found the tests to be an easy way to fix simple bugs.
<br>
Anyway, I create emptykddebug, uniquekddebug and checksemicolons.  You should see the first two in the morning run.  The last script creates too many false positives as of yet.
<br>
I also went through and added an empty line after each warning; this should help with the visual grouping.
<br>
The scripts are in playground/base/kdetestscripts/ if anyone is interested... ;-)
<br>
<!--break-->