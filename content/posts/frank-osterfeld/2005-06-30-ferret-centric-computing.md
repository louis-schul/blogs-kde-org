---
title:   "Ferret-centric computing"
date:    2005-06-30
authors:
  - frank osterfeld
slug:    ferret-centric-computing
---
As it turned out yesterday, even ferrets like KDE:
<a href="http://bugs.kde.org/attachment.cgi?id=11622&action=view">
<img src="http://www-user.rhrk.uni-kl.de/~f_osterf/blog/ferret-small.jpg" width="400" height="300" alt="Ferrets love KDE!" class="showonplanet" />
</a>

But unfortunately they make KDE apps <a href="http://bugs.kde.org/show_bug.cgi?id=108312">crash</a> sometimes, so KDE can't be certified as ferret-proof yet. Which brings us to the fact, that the research field of <i>animal-centric computing</i> was neglected for too long. What tremendous opportunities we have here! Just compare the numbers: There live trillions of animals on planet earth, where the homo sapiens population is ridiculously small. You can't seriously talk about world domination if you don't take this into account.

And yeah, <a href="http://www.dfki.uni-kl.de/~miller/photos/Frettchen/index.html">know your user</a>! 
<!--break-->