---
title:   "KDevelop Assistant"
date:    2004-05-07
authors:
  - adymo
slug:    kdevelop-assistant
---
Every time I wanted to look at documentation, I started Qt Assistant for Qt API and Konqueror for KDE API. This annoyed me for quite a long time. I always dreamed about a program like Qt Assistant capable of browsing KDE API and indexing it. KDevelop had something like that for ages, it was an old documentation plugin. But it has two problems - it's index was really slow and unusable and the viewer could not be run standalone.<br>
Eventually I decided to fix that problems and now I'm proud to present KDevelop Assistant (kdevassistant). It is the only documentation viewer that is capable of browsing, indexing and full text searching various types of API docs in your system. Currently it supports Qt API, KDE API and Devhelp (GTK/GNOME) docs, support for new types can be added in a hour by creating a documentation plugin. Check out KDevelop CVS HEAD to get not only the best IDE in the world but also the best API documentation viewer ;).<br><!--break-->
Screenshot:<br>[image:458]