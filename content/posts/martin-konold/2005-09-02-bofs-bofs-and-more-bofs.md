---
title:   "BoFs, BoFs and more BoFs"
date:    2005-09-02
authors:
  - martin konold
slug:    bofs-bofs-and-more-bofs
---
The nice things when meeting everyone at aKademy is that you can very easily talk face to face to people.

I attended many BoFs including the working group meeting. I liked that everyone was sharing a common goal but it showed that if the group becomes too big making progress is not simple. Funny enough to me this BoF showed the need for structure within the KDE project very much and the intermediate result that a small subgroup shall draft a proposal is showing promise. Mirko did a great job in moderating the discussion.

My personal favourite was of course the RuDI BoF. This group was much smaller and therefore very efficient and easier to do moderation of the discussions.

RuDI got many supporters inkluding Matthias Ettrich who said that he would love to support RuDI for Qt developers. Basically it boils down that Qt developers can keep their current  developement model and only need to link to RuDI. This allows all Qt applications to integrate nicely into KDE and default to the native Qt functionality in case RuDI is not available. Qt developers don't need deep KDE knowledge to use RuDI. Matthias should know as Trolltech just recently conducted a [http://wiki.kde.org/tiki-index.php?page=Trolltech%20Keynote%20Talk|market research] with feedback from many thousands of ISV which built upon Qt technology. Even though many of these are targeting the Linux desktop they didn't integrate their applications in the desktop at all.

As [http://lists.kde.org/?l=kde-core-devel&m=112565459811357&w=2|Benjamin Meyer] wrote on the kde-core-devel mailing list reducing the complexity of the current kdelibs and make it easier for non-core developers to use the KDE platform can help us to gain more ISVs, testers and finally more contributors. After all reducing the complexity will be the critical point when recruiting new KDE developers.

Due to the fact that a [http://blogs.kde.org/node/1396|service oriented] approach to the Linux desktop is not limited to KDE but also of big benefit to [http://www.gnome.org|GNOME] we have to think how to best contact the GNOME developers about proposing a common RuDI protocol. IMHO RuDI can also be a suitable technology to integrate [http://www.mono-project.com/Mono_for_Gnome_Applications|Mono] applications in both Linux desktop in a clean and robust manner. 

I am now wondering whom to approach for this?
<!--break-->




 