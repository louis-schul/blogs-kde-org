---
title:   "Kommander"
date:    2004-11-10
authors:
  - henrique pinto
slug:    kommander
---
A while ago, it was questioned on kde-devel why was KFileReplace in the KDE WebDev package. The reasoning is that no one looking for that application specifically would think it was inside a web development package. The same holds true for Kommander. It is not really a web development tool, so it is kind of weird to have it in WebDev. Kommander has great potential. It is easy to use, and is getting even more powerful.

During the discussion, I asked whether the Kommander executor could be moved to kdelibs. That would mean that although you'd still need WebDev installed to create Kommander "scripts" (what should I call them?), these "scripts" could be run "out-of-the-box" on any machine that has KDE installed. While no one agreed with me that the move could happen already for KDE 3.4, it seems that it is actually the intention of the developers to do this, but later. Nice. Kommander is one among the incredibly powerful features of KDE few people know about (KIO-Slaves are another of those, but it seems there is <a href="http://blogs.kde.org/node/view/700">discussion</a> <a href="http://lists.kde.org/?l=kfm-devel&m=109982487311129&w=2">about</a> how to expose them better to the users going on).

There's just one small problem in the Kommander Executor move to kdelibs: it is not a lib, as it was pointed. Yet I still think it deserves being there. IMHO, the kdelibs package should provide the basic framework needed to run any KDE application, and I believe Kommander dialogs can be counted as "simple KDE apps".

If you don't know Kommander yet, or aren't aware of how useful can it be, check Quanta. They use it for quite a lot of nice things :)