---
title:   "Rant of the day (30/3)"
date:    2004-03-31
authors:
  - zogje
slug:    rant-day-303
---
I said Rant because this thing is clearly marked as "personal blog" and yet it forces me to pick a KDE category, that's hardly personal now is it?

A lot of people are curious about the New Project. I can't say much about it at these early stages but I did some digging today and made good progress, I hope to have phase 1 completed by the end of this week. The patch starts to look good as well.

Today was patch day, it's a shame that we get perfectly good patches and let them sit on bugs.kde.org for so long. That's not a good way to motivate people to get involved. Which reminds me of the cache-control patch that was send to kfm-devel, tomorrow I will now really look into it! I promise.
