---
title:   "Rant of the day (5/4)"
date:    2004-04-05
authors:
  - zogje
slug:    rant-day-54
---
Boy ksim was in a mess, fixed the obvious issues with it over the weekend. I somewhat fail to see why ksim is implemented as a kicker extension, wouldn't it make more sense as a normal stand-alone application? I guess I should see if I can make the disk-log-widget-thingy scale to full width, I use the "Aliens" GKrellMM-theme (who comes up with these names?) and around the disk-log-widget-thingy I have purplish uglyness.

A lot of people (2) have asked about the New Project. Yes, I'm excited too. I will no longer let you in the dark about it. Since a picture is worth a thousand words I will just point you to <a href="http://blogs.kde.org/node/view/405">the picture that explains it all</a>.

Nice to see some fresh blood looking into <a href="http://lists.kde.org/?l=kfm-devel&m=108100892517070&w=2">performance issues</a> this weekend.