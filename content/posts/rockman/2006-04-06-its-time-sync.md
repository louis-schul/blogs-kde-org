---
title:   "It's time to sync.."
date:    2006-04-06
authors:
  - rockman
slug:    its-time-sync
---
After some boring works on GSM and UCS2 encodings, bug fixing, PDU sms, finally i could spend some time on implementing a couple of nice new features.
<a href="http://img206.imageshack.us/my.php?image=20060406kmobiletoolskorg5js.png" target="_blank"><img src="http://img206.imageshack.us/img206/1613/20060406kmobiletoolskorg5js.th.png" border="0" alt="Free Image Hosting at www.ImageShack.us" /></a><br>
Starting with <a href="http://img206.imageshack.us/my.php?image=20060406kmobiletoolskorg5js.png" target="_blank">Calendar Syncing</a>, it was quite easy by using KParts, to embedd a KOrganizer view.. it was less easy to read data, due to stupid AT commands.. but at least now it's working fine, even if read-only, and unfortunately only on Motorola Phones.

Here it is the new one: <a href="http://img141.imageshack.us/my.php?image=20060406kmobiletoolskmail3pq.png" target="_blank">exporting SMS Messages into KMail.<br><img src="http://img141.imageshack.us/img141/9674/20060406kmobiletoolskmail3pq.th.png" border="0" alt="Free Image Hosting at www.ImageShack.us" /></a><br>
I used email format for backing up sms because it's easly extensible, i could add some custom tags for storing memory infos, and also because it was suggested on kde bugzilla.
I think KMobileTools is going to be feature-complete for 0.5.. hoping i can release soon, so :)
p.s.: i know what you're thinking.. YES, i really should clean my email folders.. :(