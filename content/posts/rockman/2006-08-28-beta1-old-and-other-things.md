---
title:   "Beta1? Old (and other things...)"
date:    2006-08-28
authors:
  - rockman
slug:    beta1-old-and-other-things
---
Long time since last blog post..
But i've been quite busy with <a href="http://www.kmobiletools.org/kmt_0.5_beta1" target="_BLANK">KMobileTools 0.5 beta1</a> release.
Pre-release time was a hell.. i had to rewrite the whole theme for the homepage (looks really good now, anyway :P ) and some new reference pages, since a lot changed since 0.4.
But post-release was busy too: trying to get the best visibility as possible, writing here and there, but above all answering to all the emails, bug reports, and feedbacks.
Well.. sometimes it's annoying, and it takes a lot of time, but hey.. we love our users :P
Even if tired, i'm very satisfied by users interest, and so i was a bit more motivated than before on bugfixing. Now it's still a bit early for beta2, but seing a lot of code fixed, i feel it's not even too far...
So just a polite reminder: beta1 has still old and annoying bugs, try <a href="http://svn.kmobiletools.org" target="_BLANK">current svn</a> instead.
I was also working with Marcin Przylucki for the <a href="http://developer.kde.org/seasonofkde/project.php?kmobile_filesystem.xml" target="_BLANK">Season of KDE</a> project regarding mobile filesystems.
We started with obex:/ protocol, rewriting it in place of kdebluetooth (outdated) one. It started working good, and it has also write support, now. Also, i implemented a more complex url syntax: obex://transport@device:port/ where transport can be
<ul><li>bluetooth</li>
<li>irda</li>
<li>inet</li>
<li>usb</li>
<li>fd</li>
<li>custom</li></ul>
Everything following <a href="http://openobex.triq.net/obexftp/obexftp" target="_BLANK">ObexFTP</a> behaviour.
That's because this new kioslave is now based on obexftp, instead of using an internal library (qobex) like kdebluetooth's one. And since now obexftp can support usb devices, you could give it a try..
I had a couple of bugs, anyway, like uploading and downloading corrupted files.. maybe obexftp it's still too young, since it seemed that the bugs weren't in our kioslave (oh well don't worry, we've our bugs too ;) like connection management, it's still to be improved A LOT ).
I'm quite happy about our job.. Marcin is learning a lot of new stuff, and i'm learning how to help new developers :P