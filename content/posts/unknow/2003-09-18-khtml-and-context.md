---
title:   "KHTML and context"
date:    2003-09-18
authors:
  - unknow
slug:    khtml-and-context
---
So, with revision of 1.25 to kdelibs/khtml/khtml_popupmenu.rc, Stephan Binner removed a function from the context menu in KHTML that I happen to use quite frequently. This commit was the one that took away the 'View document source' action from my right-click menu in Konqueror, and I can't begin to tell you how unhappy that makes me.

First off, as a web developer, 'View document source' is probably one of the single most valuable debugging tools that a web browser provides to me. No matter where I am on the page, I don't have to move the mouse much to get to the background, and from there, it's a single click, drag, and release away before I'm viewing the source to the web page that I want to see. It's the fastest way to do it if one hand is on the mouse. Since we don't have a keyboard accelerator OR a keyboard shortcut for the 'View document source' action in the menu, either, there's no easy way to get to it via the keyboard. (see <a href="http://c133.org/files/vds_keyboard.patch">my patch</a> to fix this issue)

So, in the interim, while I wait and see how strongly Stephan feels about his commit, I'm also distributing a much better (imho) <a href="http://c133.org/files/khtml_popupmenu.rc">popup menu configuration file</a> on my site as well. This menu is much leaner than the one that's included by default in KDE CVS - it has the 'Stop Animations' and 'Set Encoding' entries taken out, as well as rearranging the order of the link-related menu items. AND it has the 'View document source' item back where it belongs.

I'd love to hear your comments on this. Do you care about this? Am I getting irrationally annoyed? Does it really make that much of a difference? (My opinion, of course, is that it does, but I'd love to hear other peoples' viewpoints on this situation.)

Also, feel free to <a href="mailto:clee@kde.org">mail me</a> with your thoughts as well as posting them here.