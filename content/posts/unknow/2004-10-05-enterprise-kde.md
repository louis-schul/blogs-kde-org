---
title:   "Enterprise KDE"
date:    2004-10-05
authors:
  - unknow
slug:    enterprise-kde
---
I've just read <a href="http://quote.bloomberg.com/apps/news?pid=10000103&sid=aZ2JnBlm5tOs">this story</a> about AT&T trying linux. The story tells us that they might deploy linux to 70,000 computers. Of course, this might be the case that AT&T is just trying to get some leverage against <a href="http://www.microsoft.com">Microsoft</a> to get a better deal, but I began imagining 70,000 <a href="http://www.kde.org">KDE</a> boxes runing.

Enterprise-wise, I don't this there is any match for KDE. <a href="http://www.gnome.org">GNOME</a> may have a lot of good things, but it is surely not as good as KDE for the enterprise. The kiosk architecture is in itself enough to put KDE ages ahead of GNOME. Also, I always felt as if the core of the developers are very interested in helping develop whatever the enterprise needs implemented. Very cool.

Perhaps this is one thing the (dead?) <a href="http://www.kdeleague.org">KDE League</a> might work in. Having AT&T adopting KDE as the default desktop would be great to showcase.

Unfortunately, <a href="http://enterprise.kde.org">KDE::Enterprise</a> is longing for announcements of KDE adoption. I'm going to search around for news regarding KDE in the enterprise to update the site. We've left it untouched for some time now :(

If anyone has anything even slightly related to KDE in the enterprise, p-l-e-a-s-e send it to <a href="mailto:roberto@kde.org">roberto@kde.org</a> and I will compile everything and place it on KDE::Enterprise.