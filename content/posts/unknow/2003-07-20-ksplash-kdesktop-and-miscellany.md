---
title:   "KSplash, kdesktop, and miscellany"
date:    2003-07-20
authors:
  - unknow
slug:    ksplash-kdesktop-and-miscellany
---
KSplash plugin loading scheme changes, and probably will change again before 3.2. Grid spacing on kdesktop needs to be made flexible. No Perl: Python or Ruby?<br>
<!--break-->
<ul>
<li>Am I happy that I am the only one hacking on the new ksplash plugins! Probably going through a few more BIC changes and naming scheme changes before we see a final stable API. No time for that to happen before N7y as I am defending my doctoral dissertation on the 12th of next month. I wish I was going to N7y - no money, no time, no job yet.</li>

<li>The other thing to do in the short term is to provide configurable grid spacing for kdesktop - quite a few bug reports about it. Unfortunately, the current icon layout algorithm, while nice, does not lend itself greatly to configurability. Let's see what comes of it. Also, based on an informal survey of people in my lab, there is no need for three alignment options on the RMB menu: horizontal, vertical, grid. Everyone prefers something, and uses <i>only</i> their preferred means. This means that we could move the options into the KControl module, and have just one <b>Align</b> item on the RMB menu.</li>

<li>Now that I have given up on Perl (impossible to maintain code for any long period of time, see am_edit for an example of well-known mess), the question is: Python or Ruby? Or are there other languages out there that require consideration?</li>

<li>Stuff I hope someone else will fix/create:
<ul>
<li>ksmserver - saving multiple sessions</li>
<li>kmail - crash on session exit</li>
<li>qdockwindow - sane API designed for subclassing</li>
<li>kmdi - working IDEAl mode</li>
<li>khtml - pluggable modules to add support on demand for MathML, etc.</li>
<li>KEmacs :-)</li>
</li>
</ul>
</ul>