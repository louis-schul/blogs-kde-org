---
title:   "evil hacks and silly things like that"
date:    2003-12-22
authors:
  - unknow
slug:    evil-hacks-and-silly-things
---
So Eugenia's proposal for a new "usable" widget style (what is dotNET, anyway, chopped liver?) was wrapped in an envelope today as a "KDE style challenge", whatever that's supposed to mean, by someone I happened to see on IRC. I then explained that, despite the fact that the snake menus she put on her mockup are nifty looking, they are in fact insanely difficult to code, but not impossible. Just ugly and scary.

Others were interested. "So you have written snake menus before?" And I explained. Yes. In fact, with Qt, you can do all sorts of crazy shit that most people wouldn't imagine. But I used dotNET as the basis for the snake menus that I implemented and after I had them working halfway (and I probably could have finished it, but I was scared of the code at this point) I deleted the fork and tried to forget about my sins.

But the discussion continued. And I pointed out that I had written another neat hack style before, one that I still haven't seen anybody else do anything like. And just to prove that I'm not always full of shit (only when I've had waaay too much to eat) I went ahead and dug out the code to my se7en style. And after about half an hour of fighting differences with the old QStyle API (this was code that was written against Qt 3.0beta5 if I remember correctly), it compiled once again, and worked. So it is now off in the wild, to be loved or shunned. And I want feedback on it.

http://www.kde-look.org/content/show.php?content=9549