---
title:   "anybody for rich text?"
date:    2004-05-21
authors:
  - mattr
slug:    anybody-rich-text
---
I added support for receiving bold, italic, and underline text in yahoo today. It was pretty easy. The next step is to now implement the sending of it, which will probably be a tad harder, especially considering how brain-dead some clients tend to be when handling rich text. Should be lots of fun.<br><br>
I've decided that i'm going to try and write here more often. Now let's just hope I have more interesting things to write about. :-)
<!--break-->