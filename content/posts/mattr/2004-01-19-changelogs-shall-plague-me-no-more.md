---
title:   "ChangeLogs shall plague me no more"
date:    2004-01-19
authors:
  - mattr
slug:    changelogs-shall-plague-me-no-more
---
I've started working on a (currently small) app that will parse the ChangeLog files created by cvs2cl and display them in a KListView and allow me to edit them and produce a new ChangeLog file from that. <!--break--> The current code is pretty rough and is something I did this weekend to be more of a proof of concept just to see how hard it would be. Why was I worried about how hard it would be? Well, I wrote a somewhat similar program (it also parsed text output) in Windows w/o using QT and it's about 75 times bigger than the program that I just wrote this weekend. :) I love QT. Anyways, now that I'm off my soapbox about that crappy windows program, I had a few more things in mind for this ChangeLog parser thingie (currently, it's name is KChangeLogMaker, but well, names change :) ). Here are some of the ideas:

<ul>
<li>Generalize the text parser so that users can specify different file formats, via XML or some other means
<li>Output a new file based on a file format that can be chosen arbitrarily (i.e. take a file output by cvs2cl and make it a KSpread file or simliar)
<li>Make it look prettier :)
</ul>

I imagined I had some more ideas, but i can only remember these right now.

And in other news, 3.2 branched this weekend. For me, personally, it's quite exciting since this is the first full KDE release cycle that I've been through where I've actually been a developer, rather than just somebody who runs the latest RCs to see what they've put in the next release. 