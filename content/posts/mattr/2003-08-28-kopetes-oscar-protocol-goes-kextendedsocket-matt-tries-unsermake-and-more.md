---
title:   "Kopete's Oscar protocol goes KExtendedSocket, Matt tries Unsermake, and more!"
date:    2003-08-28
authors:
  - mattr
slug:    kopetes-oscar-protocol-goes-kextendedsocket-matt-tries-unsermake-and-more
---
I've been converting Kopete's Oscar protocol to KExtendedSocket over the past week or two. Needless to say, it hasn't been without it's headaches, but I have finally conquered it!!! The testing that I've done so far seems to indicate that it still works the way it did before the conversion, although now I can't test it to make sure an issue I had with disconnecting is gone because of the KUtils stuff. I'm not quite sure I know what to think about the KUtils stuff, just basically because I feel like it limits us to using HEAD for kopete now. Oh well, I guess I shouldn't care too much since I use HEAD on a daily basis anyways.
<!--break-->
I also tried unsermake today. It's really nice. Kudos to coolo for such an awesome replacement. I don't know why I didn't start using it sooner.

I also don't know what else I was going to write here and since it's now 2am, I think I'll go to bed. :)