---
title:   "so i guess i should write something"
date:    2004-05-19
authors:
  - mattr
slug:    so-i-guess-i-should-write-something
---
It was about a year ago that I got my CVS account after posting a patch to the kopete-devel fixing some issue in the Yahoo plugin. I don't even remember what it was.  I sent the patch and Duncan Mac-Vicar Prett was like, "You should get a CVS account.". A few days later and I had one. Needless to say, I was quite pleased with myself.  So anyways, it's been a year now, and I'd say I've been pretty active, although not as active as some people. For those who like lots of stats, here's some numbers since I got my CVS account on 05/13/2003
<ul>
<li>Number of commits: 251 or so. 251 is what the <a href="http://cia.navi.cx">CIA</a> bot has.
<li>Number of bugs closed: 651
</ul>
<br><br>
Speaking of commits, I doubt anybody follows what I do on a consistent basis (if they do, i doubt i'll find out), but I've been a bit inactive lately.  I always seem to find something better to do, like play SimCity :-).  I haven't quite figured out what it is that's keeping me from coding. I've got it narrowed down to a few things though. My job search hasn't been going particularly well, so that could be part of it. I've been really tired lately too, which I haven't been able to figure out yet either, since from my POV at least, I seem to be getting enough sleep. I guess this would be the break I eluded to in my <a href="http://blogs.kde.org/node/view/448">last post</a>. Oh well, I'll pick it back up again, and everybody better watch out. ;-)<br><br>
Too bad I don't have any cool projects like zogje with his dog race track. That's some pretty cool stuff right there. :-) 

