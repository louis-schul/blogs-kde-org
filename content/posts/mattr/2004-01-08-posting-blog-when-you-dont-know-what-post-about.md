---
title:   "posting a blog when you don't know what to post about."
date:    2004-01-08
authors:
  - mattr
slug:    posting-blog-when-you-dont-know-what-post-about
---
So, yup, this is my blog entry, and I have no idea what to write about. Maybe it's because I'm not doing anything, or maybe it's because I'm doing too much, or.... and the list could go on and on.<!--break-->  Or maybe, it's because I'd like to try fixing Xinerama support, but I haven't a clue where to start, and I went back down to a single monitor a couple of weeks ago because having that second monitor left no room on my desk. :( Anyways, I've rearranged the desk and so now I may try again after I get home. I'm stuck at work right now, and it really sucks since I don't have a whole lot of time to work on KDE here anymore because we're always so busy. :( Maybe it'll slow down some though and I can get back to hacking.

And in other news, I've contacted Shawn Gordon about a job at theKompany. I enjoy hacking on KDE, so I figure I'll enjoy hacking on applications for KDE too. He said things were kinda slow then, so i'll probably email him in the next couple of days to see if he's got something that I can do. I'd really love to do just programming work, instead of what I'm doing now, which is run a whole bunch of SQL queries to fulfill orders. It's quite mindnumbingly boring, and I have to use windows :P

Anyways, i just found out that gentoo took out the speedup patch with the upgrade to binutils-2.14.90.0.7 so I get to emerge binutils-2.14.90.0.6 to get my linking speed back up to par. 

And now, I think I've done enough rambling on about stupid crap nobody wants to know about anyways, time to go back to work.
