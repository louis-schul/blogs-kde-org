---
title:   "KDevelop 4 and Qt's new dockwidget tabs"
date:    2006-07-28
authors:
  - manyoso
slug:    kdevelop-4-and-qts-new-dockwidget-tabs
---
With the release of Trolltech's java bindings it kinda feels like Christmas.  I wanted to point out some more presents under the tree...

<img src="https://blogs.kde.org/system/files?file=images//qt_designer_ideal.preview.png" />

Look closely at the dockwidgets on the right of the image.  See that?  Recent versions of Qt 4.2 snapshot include a new Ideal like feature.  When you drag a dockwidget completely on top of another an Ideal like tabbed bar is created.  This is done entirely within Qt and any application which uses QMainWindow will have this functionality.  Pretty cool, eh? :)

<img src="https://blogs.kde.org/system/files?file=images//kdevelop_with_designer4.preview.png" />

This is a screenshot of KDevelop 4 as it currently stands.  As you can see, we now natively integrate Qt 4 designer.  You can thank Matt Rogers and Roberto Raggi for this one.  KDevelop 4 is going through some pretty incredible changes right now.  Hopefully the dot is going to host an interview with some updates on what is going on.  Stay tuned!

<!--break-->