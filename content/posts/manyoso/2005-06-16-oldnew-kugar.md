---
title:   "Old/New Kugar"
date:    2005-06-16
authors:
  - manyoso
slug:    oldnew-kugar
---
As I said below, I just assumed maintainership of Kugar from adymo and have been working on re-integrating his local tree into koffice svn.  Well, that is now complete, although there is still a whole lot of work to be done: d-pointifying the library, getting rid of the Qt-only shell, etc, etc.  I also have a number of new goals for Kugar including greater control over grouping and sorting, making it easier to embed both the viewer and designer into other apps (dataKiosk and Kexi come to mind) and just general bug fixing and so on.  

The good news is that adymo was sitting on a whole host of improvements and additions.  Especially to the designer.  After integrating his local tree into koffice svn I also switched out the property editor for the new and improved one in kdenonbeta/propertyeditor which has now been moved to koffice/lib/koproperty temporarily.  It'll hopefully be moved to KDE4libs eventually.  Anyway, here are some screenshots of the old Kugar (basically unchanged for the last few years) and the new Kugar just committed to svn (warning: still some bugs with the propeditor).

<b>Old:</b>

<img class="showonplanet" src="https://blogs.kde.org/images/ed315cdeaf9bb2cd02b2d8248b1b90a0-1164.png" alt="Kugar as of 06/14/05" />

<b>New:</b>

<img class="showonplanet" src="https://blogs.kde.org/images/1fa2a5c6d934990ac4e023f4f044bca7-1166.png" alt="Kugar as of 06/15/05" />

Cheers :)
<!--break-->