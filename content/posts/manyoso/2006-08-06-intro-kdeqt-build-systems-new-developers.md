---
title:   "Intro to KDE/Qt build systems for new developers"
date:    2006-08-06
authors:
  - manyoso
slug:    intro-kdeqt-build-systems-new-developers
---
In the recent <a href="http://dot.kde.org/1154654739/">KDevelop dot interview</a> I noticed a very astute comment by someone named borker:

"One of the things I've loved about picking up Qt/KDE was how quickly you can make non-trivial applications, but I found learning Qt/KDE was a weird reverse... rather than writing code first and then learning the complexities of the build process later, as required, I found myself needing to come up to speed with the build process first and then write code. Not being overly familiar with the GNU tool chain didn't help speed up this process either. I found it to be a frustrating blocker to actually writing code and it wasn't helped by most of the available tutorials being great on coding advice but very slim on build advice, so when I wanted to go from 'hello world' to something a bit more 'real world' I ran smack into the build system and all my momentum died completely. Anything that could help a beginner like me out in the way of teach as you go type stuff would be great." -- <a href="http://dot.kde.org/1154654739/1154711434/1154712573/1154714347/">link</a>

I can't tell you how many times I've seen someone new to KDE/Qt development come into the #kdevelop channel after being absolutely floored by Autohell.  Well, as you might have heard, KDE is going to be moving away from Autotools, so hopefully that will help.  Here is a short introduction to all the build systems you might encounter in KDE land.

<ol>
<li><a href="http://doc.trolltech.com/3.3/qmake-manual.html">QMake Version 3</a> -- Used by Qt 3.x applications.
Brain dead easy to learn.<br><br>
<li><a href="http://doc.trolltech.com/4.2/qmake-manual.html">QMake Version 4</a> -- Used by Qt 4.x applications.
Brain dead easy to learn.<br><br>
<li><a href="http://wiki.kde.org/tiki-index.php?page=KDECMakeIntro">CMake</a> -- Used by KDE 4.x and Qt 4.x applications.
Fairly easy to learn, but more powerful.<br><br>
<li><a href="http://sources.redhat.com/autobook/">Autotools</a> -- Used by KDE 3.x applications.
This will make your eyes bleed and your brain beg for mercy.<br><br>
<li><a href="http://lwn.net/Articles/188693/">Scons</a> -- Previously considered for KDE 4.x applications, but ultimately dropped for KDE/Qt development.
</ol>

If you are a new developer and have no previous experience with KDE/Qt then learn #2 first.  You need to learn how to use Qt 4 and this will be the most pain free way to do so.  Once you've done that, then you can venture into KDE 4 land and learn how to use #3.  It is not that hard and your experience with QMake 4 will probably help.

If you are familiar with Qt and how to use it, but you are thinking that you want to develop KDE 4 applications, then by all means start with #3.

Unless you are currently hacking on KDE 3 or have knowledge of Autotools, you have no reason to suffer through #4 especially if you are new to KDE/Qt development.  Save your energy and learn QMake 4 first and then CMake.

If you like scons, great! But, please keep in mind that most KDE applications will be using CMake.
<!--break-->