---
title: Bugzilla Bot improvements in the Automation Sprint
authors:
  - traceyc
date: 2024-04-29T21:30:00Z
---

I'm happy to have been able to attend my first in-person KDE event, the Automation & Systematization Sprint in Berlin. Previously, my contributions to KDE have consisted of submitting and triaging bug reports. During this weekend, I was able to meet some of the KDE team in person, and become more involved. I've started working with the Bugzilla Bot code, and plan to start digging into the automated test code.

The Bugzilla product list had fallen out of date, so first I [updated that](https://invent.kde.org/sysadmin/bugzilla-bot/-/merge_requests/23) (yay, my first accepted MR!). I also started working on using the GitLab API to automate these updates. In the near future, I'll be tackling some requested improvements to the Bugzilla Bot. This will lessen the amount of boring manual bug chores and free people up to do more valuable work.

Thanks to the KDE team for being so friendly and willing to help me learn the development environment. I'm happy to have found more ways to contribute that I enjoy, and will be valuable to the project.
