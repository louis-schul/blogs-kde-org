---
title:   "Should I SoC this year?"
date:    2006-05-03
authors:
  - carewolf
slug:    should-i-soc-year
---
Last year I completed the Google SoC with an entry about CSS 2.1 in KHTML printing. The project was a great succes despite low visability. (You can see the result by printing documents in Konqueror and notice that headers usually follows their bodies and text is rarely broken so one line is left alone a page).

Well, because I such a lazy student, I still havent finished my master thesis, so I am technically still allowed to do a SoC-project this year. So should I SoC?
<!--break-->
First of all it is a great opportunity to get paid for something you would do anyway, the only hard part is figuring out what you _would_ do anyway. 
It is quite hard to predict what you want to do(atleast for me); for instance I had a number of small projects I listed for the spring a few months ago, but I haven't really finished any of them, but instead worked on other stuff like better XHTML support and dynamic restyling in KHTML (in 3.5.3 you will be able to do all kinds of crazy things with CSS and dynamic HTML). 

But SoC is also a great opportunity to get some hard cash reason to commit yourself to a slightly larger project than you usually would.  I have two potential large projects, that are too large for me to commit myself to them in my spare time, but might just be interesting to be paid to do to help KDE on:
<dl>
<dt> Something KHTML ... 
<dd> For instance implement XForms or merging DOM editing from WebCore, or even the wild crazy one: Porting WebCore to KDE.
<dt> avKode ...
<dd> An old abandoned project-idea reborn as a FFMPEG-backend for Phonon. This is actually something we need a lot, and would allow me to finish something I hinted at doing a couple of years ago.
</dl>