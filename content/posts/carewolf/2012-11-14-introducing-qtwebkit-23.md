---
title:   "Introducing QtWebKit 2.3"
date:    2012-11-14
authors:
  - carewolf
slug:    introducing-qtwebkit-23
---
Welcome back, it is has been a long long time since I last made a blog post. Since then I have moved from working on KHTML in my free time to being payed to work on QtWebKit full time. I was hired by Nokia last December and was transferred to Digia in September together with most of the Qt team. At Digia most of our work goes into getting ready for Qt 5. We do however have a small side project for Qt 4.x I would like to tell you about, QtWebKit 2.3.

QtWebKit 2.3 was branched from WebKit trunk in early August, but has been merging almost all commits since, only skipping those that removed Qt4 support or used Qt5 specific features. It is our plan to stop following WebKit trunk once QtWebKit for Qt5 also branches, QtWebKit 2.3 will then follow the Qt5 branch, so that it will provide roughly the same underlying features for Qt4 that the next QtWebKit version will provide for Qt5. 

It should be pointed out that QtWebKit 2.3 is intended as a community project, it is not part of any official Qt release and is not scheduled to be. QtWebKit 2.3 got started because there was a show of interest from the community to continue to support WebKit for Qt 4.x, so please note that we rely on your help make it work. From the feedback we already get on the webkit-qt mailing list it looks like there are already several developers trying it out.

<i>So what is actually new?</i> The same things as in QtWebKit for Qt 5 minus the QQuickWebView interface or the rename of libraries. 

New features:
<ul compact>
<li> WebGL
<li> CSS shaders/filters
<li> CSS regions/exclusions
<li> window.requestAnimationFrame
<li> Remote web inspector
</ul>

All the small stuff:
<ul compact>
<li> Latest WebKit (bleeding edge in fact)
<li> Lots of performance improvements and reduced memory usage.
<li> Incremental image loading by prefering WebKit image-decoders over Qt image decoders.
<li> Interface for automatic spell-checker (which wasn't ready for QtWebKit2.2 which is in Qt 4.8).
<li> Better support for "BLOB" URLs (Used by some Google services when uploading images among other things).
<li> Caret navigation (option)
<li> Smooth animated scrolling (option)
<li> MHTML coming in a merge soon (multipart MIME documents such as emails),
</ul>
<i>So where can you find it?</i>
https://gitorious.org/+qtwebkit-developers/webkit/qtwebkit-23

<i>How do I build it?</i>
Set QTDIR (even it is to /usr), call Tools/Scripts/build-webkit --qt --no-webkit2.

<i>How do I build kwebkitpart against my new build of QtWebKit2.3?</i>
You add these flags to cmake:
    -DQT_QTWEBKIT_INCLUDE_DIR={{your QtWebKit 2.3 dir}}/WebKitBuild/Release/include/QtWebKit/ \
    -DQT_QTWEBKIT_LIBRARY_RELEASE={{your QtWebKit 2.3 dir}}/WebKitBuild/Release/lib/libQtWebKit.so 

<i>How do I file bugs? </i>
Bugs for QtWebKit 2.3 are currently tracked in the WebKit bug tracker (bugs.webkit.org). When making a bug report for QtWebKit 2.3, please add [Qt4] to the beginning of the bug title, and make it block bug 88186 which tracks QtWebKit 2.3 issues. Note that since QtWebKit 2.3 lives in a separate branch, features such as the Early-Warning System will not work, and the regular review process does not strictly apply, so you should never request reviews with r? or commits with cq? on any patches for the branch.

I guess that is all. If you have more questions, ask them in the comments.
