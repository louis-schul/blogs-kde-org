---
title:   "daniels has entered the building"
date:    2004-08-21
authors:
  - daniels
slug:    daniels-has-entered-building
---
So, I'm writing this entry from aKademy -- all thanks to the eV for giving me a subsidy so I could come over here and speak about freedesktop.org.

If you see a tall Aussie guy with a beard and short, spiky hair wandering around looking confused because he didn't have enough time to learn German before left, and you want to talk to him about freedesktop.org, or just say hi, please grab him and talk to him. Apparently he's a pretty cool dude. :)

Oh, and in other news, danimo sucks as a tour guide. ;)<!--break-->