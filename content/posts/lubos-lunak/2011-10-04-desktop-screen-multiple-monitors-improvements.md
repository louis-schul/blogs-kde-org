---
title:   "Desktop-per-screen (multiple monitors improvements)"
date:    2011-10-04
authors:
  - lubos lunak
slug:    desktop-screen-multiple-monitors-improvements
---
There's been a "small" upgrade to my desktop machine at work, and as a part of that I got my hands on a 1920x1080 Dell monitor and couldn't help placing it as a secondary monitor, rotated. And I couldn't help noticing various problems with this setup.

<img src="http://home.kde.org/~seli/blog/monitor_setup.png">

A screen rotated to become 1080x1920 should be quite useful for a developer - let's face it, 1920x1080 is often good just few displaying a narrow text column between two huge margins anyway (hello dot.kde.org and other sites), so why not rather use the extra size for the direction that matters. But in practice, it didn't quite work, not really. Getting (some) driver to work somewhat decently in this setup wasn't exactly trivial and even then, there were various problems and the extra monitor seemed more like added weight than useful addition.

There were bugs of course, the unusual geometry can lead to strange results pretty easily, with the center of the right monitor being at the same height like the bottom edge of the left monitor. But, more importantly, it didn't seem to work right as a whole. I use virtual desktops a lot, and each virtual desktop spans the whole area of all monitors combined. Which makes the monitors very dependent, linked together, and so trying to take advantage of them for viewing something results in a struggle to arranging windows properly.

So, this <a href="http://hackweek.opensuse.org/">Hackweek</a> came about in the right time to allow me to do something about it as my <a href="https://features.opensuse.org/312860">project</a>. I have fixed a number of multi-monitor bugs that bothered me, and I have a usable implementation of a desktop-per-screen feature for KWin. My changes for it are in the KDE git repo in clones/{kdelibs|kde-workspace}/lunakl/desktopperscreen, and let's see how it works in practice.

<img src="http://home.kde.org/~seli/blog/desktopperscreen.png">

The picture above is from my testing (the black is the dead area). After selecting either of screens as the active switching desktops switches just what that screen shows. That means that with this setup its much simpler to have e.g. 4 virtual desktops on the left screen for development and 2 virtual desktops on the right screen for documentation, and using keyboard shortcuts one can get rapidly to anything wanted quite easily.

Of course, there is still room for improvements, or perhaps better said fixes for this mode. It can quickly get confusing about which virtual desktop is shown on which screen, so the picture shows an applets I quickly hacked up for this (I needed it quick&easy, so this one is for <a href="http://kde-apps.org/content/show.php/Kor+Testudo+Shell?content=126302">Kor</a>). The pager now also shows incorrect information, since now each virtual desktop spans just one screen, e.g. desktop 4 should be shown in pager as having the KWrite window in its bottom-right corner, like it is in reality.

But well, it works, or so it seems so. I'll add the patches to my 11.4 KDE packages in the <a href="http://download.opensuse.org/repositories/home:/llunak:/my/">home:llunak:my</a> OBS repo, time to try it out for real.

