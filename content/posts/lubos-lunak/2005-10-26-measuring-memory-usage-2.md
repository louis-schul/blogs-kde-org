---
title:   "On measuring memory usage #2"
date:    2005-10-26
authors:
  - lubos lunak
slug:    measuring-memory-usage-2
---
Hmm ... I should wish things more often. I <a href="http://blogs.kde.org/node/1445">complained</a> about the lack of any usable memory reporting tool on Linux, and only a couple of days later I discovered <a href="http://homepage.ntlworld.com/john.berthels/exmap/">exmap</a>. Unlike e.g. top with its number of useless memory usage values, exmap has only three of them.
<ul>
<li>First of them is virtual memory, i.e. top's VIRT. Not really that useful in practice except for some specific cases.</li>
<li>Second is mapped memory, i.e. memory that the process actually really uses. This value still includes memory that's shared with other processes.</li>
<li>Last and definitely not the least but rather the other way around, there's the effective size. It is mapped size, but for shared memory the shared portion are divived into all processes that use it. To give a simplified example, if there are 10 KDE apps running, then for each of them the effective size is its unshared memory plus 1/10 of memory used by KDE libraries.</li>
</ul>
The effective memory size is so far the best approximation of the one-number-for-memory-usage-of-an-app value I've seen that even Joe User could use without getting it horribly wrong. Not that it'd be impossible to get this value wrong too, you still need to understand what this is about, but if Joe User says "app XYZ uses too much memory according to top" you can ignore this as being completely clueless and wrong; with exmap, Joe User would have a number that really kind of means what they think it means.
Too bad exmap needs to insmod a kernel module and run under root. I wish it replaces top and top remains only as a (very) experienced user's tool.
<!--break-->
