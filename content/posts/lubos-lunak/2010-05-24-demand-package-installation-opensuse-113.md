---
title:   "On-demand package installation in openSUSE 11.3"
date:    2010-05-24
authors:
  - lubos lunak
slug:    demand-package-installation-opensuse-113
---
<p>
You most probably have already run into this at least once. You use the computer, try to do something and you get an error message saying "sorry, application <i>foo</i> is not installed", "the required plugin <i>bar</i> is not installed" or similar. And that's it, there it stops. You have to find out what package the required functionality is in, install it manually and try again. Like if the computer couldn't ask "but maybe I can install that, do you want me to try?" and handle it itself.</p>

<p>
And that's what my goodie for this openSUSE release is about. I've been examining a bit about what various parts of the desktop could do this and there indeed are some cases. For example, clicking in Dolphin on a file that has no associated application installed usually results in the "Open with?" dialog. And that dialog has nowhere in it the option "the application that can open it, silly". Especially given that if the installation medium is accessible (i.e. usually if the network connection is up), it's rather easy to find out the right application for the file and install it:</p>
<a href="https://blogs.kde.org/files/images/snapshot9_0.png">
<img src="https://blogs.kde.org/files/images/snapshot9_0.preview.png" alt="KSUSEInstall #8" title="KSUSEInstall #8"  class="image image-preview " width="640" height="512" /></a>
<p>
This specific case is actually a bit tricky. The file, example.kvtml, is a file with pairs of words or expressions that e.g. KWordQuiz can use for teaching (words for language lesson, for example). The problem is that technically the data is stored as XML and the mimetype (file types) specification has a concept of subclasses that is and in not useful depending on how you look at it. A C++ source file is a subclass of a plain text file, so you can edit it just like a plain text. Good. An XML file is also a subclass of a plain text file, so you can view XML as plain text. Good? I'm not quite sure on this one, since while XML is human-readable, any decent up-to-date XML is certainly not human-understandable anyway, so I fail to see the point. But, since .kvtml files are XML files, they are a subclass of them, and that means you can view them just like a plain text.</p>
<a href="https://blogs.kde.org/files/images/snapshot8_0.png">
<img src="https://blogs.kde.org/files/images/snapshot8_0.preview.png" alt="KSUSEInstall #7" title="KSUSEInstall #7"  class="image image-preview " width="640" height="512" /></a>
<p>
Good??? Probably not. They are supposed to be opened in an application that can show the lesson nicely, who'd be crazy enough to decipher it from the XML? Well, but that's the reason for the dialog looking this way, the above is what the dialog is trying to tell you. Sorry :). This should get eventually sorted out somehow in the mimetype specification, but for now I had to go with this.</p>
<p>
The short version is: Just say you want an application that can handle exactly the file type, that's usually the right choice here. And if there are more applications that can handle it, you'll get a choice:</p>
<a href="https://blogs.kde.org/files/images/snapshot10.png">
<img src="https://blogs.kde.org/files/images/snapshot10.preview.png" alt="KSUSEInstall #9" title="KSUSEInstall #9"  class="image image-preview " width="640" height="512" /></a>
<p>
There is another, rather obvious case, where this can be useful. Amarok on its first start usually likes to complain about lack of support for certain well-known and widely used multimedia format and 11.3 will be no different. However, Amarok has also some support for solving this problem and has this dialog:</p>
<a href="https://blogs.kde.org/files/images/snapshot1_3.png">
<img src="https://blogs.kde.org/files/images/snapshot1_3.png" alt="KSUSEInstall #1" title="KSUSEInstall #1" width="597" height="447" /></a>
<p>
And that is where the new feature comes into play.</p>
<a href="https://blogs.kde.org/files/images/snapshot2_0.png">
<img src="https://blogs.kde.org/files/images/snapshot2_0.png" alt="KSUSEInstall #2" title="KSUSEInstall #2"  class="image image-preview " width="598" height="448" /></a>
<p>
This time, however, there is the usual problem: The openSUSE distribution is not allowed to include the necessary support, because &lt;a lot of ugly legal babble that causes headache&gt;. Some distributions may try to include it and hope that residing in a country without such laws solves the problem. Or, even better, not having a load of money in bank avoids a lot of trouble too (what's the point of sueing somebody who can't pay afterwards, these merry patent folks don't do it just for the sports). That doesn't quite work for openSUSE, being supported by Novell, and I bet every big company has been already sued for much more stupid things than this, just in case it'd work out. So openSUSE simply can't include the support and can't even really tell you where to get it. As long as the world is the way it is, there can't even be any "install all I need" button in openSUSE. Sorry. That's the way it is :(.</p>
<p>
So what happens in this case it that the required packages will not be found. However, there are many repositories for openSUSE not provided by openSUSE, and you can add the right one and try again (BTW, the URL in the link doesn't work yet, that will be fixed in time for 11.3).</p>
<a href="https://blogs.kde.org/files/images/snapshot3_0.png">
<img src="https://blogs.kde.org/files/images/snapshot3_0.png" alt="KSUSEInstall #3" title="KSUSEInstall #3"  class="image image-preview " width="598" height="447" /></a>
<p>
Choosing to enable additional repositories will simply launch the YaST module for configuring repositories. And, as I said, it cannot point you "here" and tell you which repository to add (because, if nothing else, it doesn't know anyway). But adding a repository is not really that hard.</p>
<a href="https://blogs.kde.org/files/images/snapshot4_0.png">
<img src="https://blogs.kde.org/files/images/snapshot4_0.preview.png" alt="KSUSEInstall #4" title="KSUSEInstall #4"  class="image image-preview " width="640" height="512" /></a>
<p>
After adding the right repository it will proceed with installation. Again, the usual allmighty YaST. Nothing hard about it, and this part should be mostly automatic anyway.</p>
<a href="https://blogs.kde.org/files/images/snapshot5_2.png">
<img src="https://blogs.kde.org/files/images/snapshot5_2.preview.png" alt="KSUSEInstall #5" title="KSUSEInstall #5"  class="image image-preview " width="640" height="512" /></a>
<p>
There it is. As simple as possible (sigh) and now it's ready (but the dialog is actually right, restart is required for technical reasons).</p>
<a href="https://blogs.kde.org/files/images/snapshot6.png">
<img src="https://blogs.kde.org/files/images/snapshot6.png" alt="KSUSEInstall #6" title="KSUSEInstall #6"  class="image image-preview " width="598" height="447" /></a>
<p>
There of course can be more places where this could be useful. The crash handler has already support too, so generating proper bugreports with full backtraces should be now much simpler as well. This is so far just experimenting with the feature and seeing how it works out, if it works well, even more can be added after 11.3.</p>

<p>
<a href="http://en.opensuse.org/openSUSE_11.3"><img src="http://countdown.opensuse.org/11.3/small" border="0"/></a></p>
<!--break-->
