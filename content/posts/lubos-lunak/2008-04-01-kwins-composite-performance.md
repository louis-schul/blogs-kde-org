---
title:   "On KWin's composite performance"
date:    2008-04-01
authors:
  - lubos lunak
slug:    kwins-composite-performance
---
As every year, one can see all kinds of articles related to today's date everywhere, ranging from quite amusing ones (it's a pity I knew what day it was when visiting dot.kde.org) to really old boring ones without anything interesting in them. I guess many people are running out of ideas or something. Rather that doings things like that, I think such people should simply stay serious. Like myself, I'm a pretty boring person usually, so I won't join the crowd, but I'll rather try to fight back by trying to be serious.

People who have tried KWin's compositing have different views on how well it performs. I have seen comments starting from "it's so unusably slow, and Compiz runs fine here" to "it runs so great, unlike Compiz". I guess it really depends on one's luck and the gfx card, X version and drivers in use. Here KWin runs comparably to Compiz, and that's with several different setups (and gfx cards, and drivers). Quite hard to do something for people where it's bad for some reason, really. My crystal ball refuses to work whenever I try to use it on compositing problems, and here compositing works for me (TM).

Actually, that's only with either the development version of KWin or the to-be-4.0.3 version. Some days back I found and fixed some inefficiencies in KWin that were way too often causing pixmap-to-texture conversions, slowing it down, or other problems (see the 4.0.3 changelog when it's out). These fixes also allowed me to again enable KWIN_NVIDIA_HACK trick (i.e. __GL_YIELD=NOTHING, see Nvidia's README), making KWin actually seem to perform even better there.

<b>Update:</b> Saying that one is not going to fool anybody does not necessarily mean this saying is just not part of the fooling. It was for a reason why the first paragraph and the very last sentence were somewhat ambiguous. It seemed funny at that point for some reason. But I guess I took it a bit too far with pretending all of this is serious, most people probably don't have the knowledge to know that, since compositing is an additional pass, it cannot make things faster in general. Although, and that should be the lesson, things rarely in practice magically become five times faster. Glxgears is not going to actually get more work done just because you postprocess it. So yes, sorry, the part below and the video are a fake.

<b>However</b>, the parts above are not, they are true. I really have recently done some optimizations in KWin and it runs here comparably to Compiz (on some setups slightly faster, one some somewhat slower, but it's not a significant difference). And, with the Nvidia trick, the <i>perceived</i> performance for some users in some cases actually may seem to be five times better. And, this time unfortunately, the parts about it mostly working for me and it being difficult to do something about problems other have that I don't see are true as well.

<b>End</b> of update. The video that follows is a fake.

And, speaking of tricks, as a result of seeing and fixing those problem, in my local version I've tried some more performance improvements based on that. By applying even more optimizations to KWin's handling of pixmaps and OpenGL textures, KWin now can avoid even more unnecessary redraws or pixmap-to-texture conversions, leading to a rather noticeable difference. In fact, since it's an accelerated desktop, glxgears running with KWin's compositing now even runs faster than without compositing (I know glxgears is not that great a benchmark, but it's good enough for some uses). You can see it for yourself <a href="http://www.youtube.com/watch?v=RDF_SFE_oOc">here</a>, or if you've found out that <a href="http://blogs.kde.org/node/3162">Flash sucks</a>, then you can download the video <a href="http://home.kde.org/~seli/download/kwin-performance.avi">here</a> (you'll probably need to set the video to fullscreen to see the FPS numbers in Konsole). As you can see, the gfx card is not very fast, and the recording of the whole screen for the video makes it appear even worse (throughout the whole video, unfortunately, that's why some parts are quite choppy).

That's all, the usual old boring stuff ;).
<!--break-->
