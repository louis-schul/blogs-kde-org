---
title:   "Kivio development"
date:    2003-09-13
authors:
  - psn
slug:    kivio-development
---
It's been a while since my last entry here, so I thought I'd tell everyone what I've been up to. :)

As there is a feature freeze right now I haven't commited anything, but I thought people wanted to know how things are progressing. :)

* Linestyles: The backend are implemented in 1.3, but I've added it to the gui localy (I didn't have time before the message freeze)

* Gradients: I have implemented support for filling stencils with gradients and pixmaps.

* Stencil rotation: This is my current project. (and it's a lot harder then I first thought!)