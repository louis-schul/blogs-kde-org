---
title:   "Munich Beastie Squishing Party Begins"
date:    2013-11-22
authors:
  - jriddell
slug:    munich-beastie-squishing-party-begins
---
We're meeting in Munich at the offices of LiMux who have just completed their transition to convert the whole of public administration in Munich to Kubuntu, over 15,000 computers.

<a href="http://www.flickr.com/photos/jriddell/10998741295/" title="DSCF8741 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3691/10998741295_390475543b.jpg" width="500" height="375" alt="DSCF8741"></a>
Tux is supporting the wrong football team

<a href="http://www.flickr.com/photos/jriddell/10998962273/" title="DSCF8744 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7296/10998962273_b6388c1e10.jpg" width="500" height="375" alt="DSCF8744"></a>
Beastie Squishers

<a href="http://www.flickr.com/photos/jriddell/10998960443/" title="DSCF8743 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2892/10998960443_a315ea82ed.jpg" width="500" height="375" alt="DSCF8743"></a>
Quick, there's a bug with Elodi!

<a href="http://www.flickr.com/photos/jriddell/10998747945/" title="DSCF8745 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7318/10998747945_c51564938d.jpg" width="500" height="375" alt="DSCF8745"></a>
Bug fixed! 
