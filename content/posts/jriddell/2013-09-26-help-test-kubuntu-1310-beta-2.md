---
title:   "Help test Kubuntu 13.10 Beta 2"
date:    2013-09-26
authors:
  - jriddell
slug:    help-test-kubuntu-1310-beta-2
---
Download and test the <a href="http://iso.qa.ubuntu.com/qatracker/milestones/303/builds">beta 2 candidates</a> and report on that iso testing site.  Upgrades also need testing.  If you're wondering how to help Kubuntu, this is how.  Virtual machines and real hardware needed.  Join #kubuntu-devel to chat.

<img src="http://www.muylinux.com/wp-content/uploads/2013/02/Konqi.png" width="500" height="413" />
