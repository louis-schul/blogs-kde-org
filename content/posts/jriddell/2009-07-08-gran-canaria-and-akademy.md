---
title:   "Gran Canaria and Akademy"
date:    2009-07-08
authors:
  - jriddell
slug:    gran-canaria-and-akademy
---
The main talks are over for Gran Canaria Desktop Summit and Akademy.  Glyn Moody wins the prize for best keynote, I always did admire him as the only writer worth reading in Computer Weekly and he proved himself to be an inspirational but thoroughly grounded speaker.  I spent much of the time running between rooms manning three of the video camers, and at times acting as session chair too.  Nobody from Gnome volunteered to help out so their videos will be badly cut in places, eventually Robert Knight was good enough to help.  Running around gave me a good feel for a lot of the talks which were varied and interesting. Turns out there is more to Gnome 3 than cleaning up some APIs (infact GTK 3 may not happen in time for Gnome 3 although nobody seemed to be sure).  Marble had the most bling demo and now features other planets too.  The Plasma Netbook talk was interesting, the developers have done a lot of research into the user interface possibilities, they critised the Ubuntu Netbook setup for only using the desktop as a simple launcher and not anything else it could be used for (of course this is changing, it'll be integrating Plasma soon).  The track on money in free software gave various thoughts: Till had the problem of KDAB hiring all the PIM maintainers so there is the danger that nobody is left to maintain the bits they are not so interested in, the Amarok guys seem unsure how to get started making money although they seem to have plenty of ideas on it, and Frank Karlichek announced the openDesktop.org App Store which will be an interesting experiment in making money by selling binaries.

Today was the e.V. meeting.  Your new KDE board members are: Adriaan de Groot (re-elected), Celeste Lyn Paul and Frank Karlichek, well done on getting a free meal to them.

Lots of parties happened, after the Canonical one, Nokia and Basyskom both had nights of free beer leaving everyone quite out-partied (until the Collabora one tomorrow anyway).  And of course the  beach is a nice way to spend the lunch break during the e.V. meeting.

<img src="http://farm3.static.flickr.com/2504/3698720453_89dc687bb2_m.jpg" width="180" height="240" />
Why Eva, with these free drinks you are really spoiling us no?

<img src="http://farm4.static.flickr.com/3530/3699530814_990c77cc18_m.jpg" width="240" height="180" />
Till gets philosphical

<img src="http://farm4.static.flickr.com/3646/3699655698_566791ddf0_m.jpg" width="240" height="180" />
Our conference can be as good as the one in Jamaica!
<a href="http://www.flickr.com/photos/jriddell/sets/72157620955618284/">photos on Flickr</a>
<!--break-->
