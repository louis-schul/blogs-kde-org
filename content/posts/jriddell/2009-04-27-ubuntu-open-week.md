---
title:   "Ubuntu Open Week"
date:    2009-04-27
authors:
  - jriddell
slug:    ubuntu-open-week
---
<a href="https://wiki.kubuntu.org/UbuntuOpenWeek">Ubuntu Open Week</a> has just started in the #ubuntu-classroom IRC channel.  I seem to be down for doing a Kubuntu Introduction talk in a few hours.  Tomorrow Mark Shuttleworth gets quizzed by the community.  On Wednesday there's a Kubuntu Q & A with nixternal and elite Kubuntu Ninja Nathan talks about Staying up-to-date with what is going on in the community.  Plenty of talks on all other areas of Ubuntu too.
