---
title:   "PyKDE WebKit Tutorial"
date:    2008-10-13
authors:
  - jriddell
slug:    pykde-webkit-tutorial
---
For last Kubuntu Tutorals Day and again at Ubuntu Developer Week I gave a quick introduction to PyKDE creating a simple web browser using Qt's WebKit widget.

Now I have <a href="http://techbase.kde.org/Development/Languages/Python/PyKDE_WebKit_Tutorial">put the Python KDE tutorial on techbase</a> so you can learn how to make a web browser too.
<!--break-->
