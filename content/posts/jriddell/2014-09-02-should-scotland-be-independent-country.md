---
title:   "Should Scotland be an Independent Country?"
date:    2014-09-02
authors:
  - jriddell
slug:    should-scotland-be-independent-country
---
<img src="https://farm6.staticflickr.com/5553/14929299698_d031f941ec_o.jpg" width="431" height="321" />

Today my postal vote in the referendum for Scottish independence was sent off.  I usually use <a href="http://jriddell.org/diary/">my personal blog</a> for non technical bits</a> but I thought some readers of my KDE Blog might be interested in this as it does affect the geopolitics of pretty much the whole world.

<b>What's going on?</b>

300 years ago Scotland was broke and England was getting rich from its empire so England gave Scotland a wad of money and created the United Kingdom.  That served Scotland pretty well for some centuries as Scots were able to trade and move freely across the empire and plenty of Scots were happy to take part in that (my old school still has a load of <a href="http://en.wikipedia.org/wiki/James_Haldane_Stewart_Lockhart">Hong Kong gold in its attic</a> as a result).  Time passed and the empire was shut down, the UK joined the EU and some oil was found in the sea off the coast of Scotland.  After many decades of Scottish public services being run by some people appointed by the UK government, 15 years ago a new Scottish pariament was reconvened to control public services and laws in Scotland, but not tax or anything international.  

In Scottish politics we have not liked the Conservative (and Unionist or Tory) party since Margaret Thatcher destroyed Scottish industry.  Then Labour went out of fashion after Tony Blair started some random wars and Scottish administration (not even wanting to call itself a government) decided to market Scotland by <a href="http://news.bbc.co.uk/2/hi/uk_news/scotland/6964609.stm">calling it small</a>.  Then their coalition partner party the Liberal Democrats fell out of fashion by getting into the UK government and dropping all their liberal principles (GCHQ has been doing mass surveilance on the population and they haven't said a word against it).  So at the last election the only large party left that had not lost all respect, the Scottish National Party, was voted in with a majority and their Scottish Government is now organising this referendum.

<b>What will happen if the vote is Yes?</b>

Both sides have said they will respect the result.  If over 50% of people vote yes then the Scottish Government will start to negotiate with the UK government on the details to make Scotland an independent country.  Depending on your point of view this is either a new country (Scotland) and a continuing state (rest of UK) or dissolving the 300 year old union to make two new countries.  The proposed timetable is to make Scotland independent by March 2016 (which is done to fit in with the election timetable of the Scottish parliament, it's made somewhat more complex by a UK election happening in 2015).

<b>Why vote for Yes?</b>

There is a democracy in Scotland which has been notably different from England and the rest of the UK for some time.  The two parties that make up the UK government (Conservative and Liberal Democrat) are the two least popular parties in Scotland.  Political borders are circles on a map which can be arbitrary or based on some nasty tribal allegance but here's a redrawing which makes government better follow the demos of the population.

<b>Is this anti-English?</b>

We can be proud in Scotland that independence is being done through a peaceful political process.  That was not the case in Ireland where they had effectively a guerrilla civil war until recently over incomprehensible tribal allegances.  And it's not the case that Scottish nationalism is in any way anti any other nation, Scotland would continue to be best friends with England and the other counties of the British Isles.

The Yes campaign has sensibly avoided any call to patriotism, kilts and Irn Bru tend to get old quickly.  By contrast the No campaign has tried quite a lot of patriotism talking about shared ties and gosh remember the first world war wasn't it glorious?  Meanwhile English politics is getting more and more little-Englander.  The Prime Minister has said he will give a referendum on membership of the EU, he wants to pull the UK out of the Court of Human Rights and he is making immigration much harder, all good reasons to vote to stay away from that sort of politics.

<b>But isn't it better to work together?</b>

Yes, and Scotland has benefited from the union with England to not have barriers of trade or movement.  Now we have a larger union, the EU, to sort out the boring stuff around trade and movement we have no need of that middle layer of government.  Scottish independence is an efficiency drive.

<b>Can Scotland afford to be independent?</b>

This is a common worry.  Scots are often not very confident in their own country.  We're small and need the help of something larger is a common thought.  Of course it's not true, Scotland is exactly in the middle if you place it in a list of countries by size or population.  We have plenty of industry and natural resources.  And then we have the largest oil reserves in Europe.  Currently the UK spends all the money that comes in from the oil while any other well run country with oil creates a soverign wealth fund, a common argument against independence is that the oil will run out sometime, my argument for independence is we need to be independent toot sweet so we can start a soverign wealth fund before it runs out.

<b>Will Scotland be allowed to join the EU?</b>

There's no precendent for this happening and no rules governing this process.  Those against independence say Scotland would need to apply to join the EU and the Spanish might block it to stop the Catalans getting ideas above their station.  Those for independence point out that Scotland is already a member of the EU, that all the citizens have EU citizenship and there is no rule to kick us out.  In the end politics will decide and the EU has a good record of welcoming in people rather than shunning them.

<b>What Currency will Scotland use?</b>

Ah, yes, slightly more tricky this and the No campaign has been playing it to the full.  The Scottish Government wants to continue to use the pound sterling with a formal currency union with the rest of the UK.  The current UK government says this would not happen but it's not clear why the rest of the UK gets to keep it and Scotland not, this does suggest the last 300 years of union have really been a sham.  The Scottish Government in response points out that as a freely tradeable currency Scotland can use the pound if it wants and nobody can stop it (this is what Ireland did when it became independent and what the Isle of Man and Gibraltar still do) and without a currency union Scotland would have no need to pay the massive national debt the UK has.  Personally I'd like to use the Euro but that doesn't seem very fashionable these days for some reason.

<b>What will happen to the BBC.. to the NHS.. won't the terrorists love it.. will Scotland have to join Schengen..  will immigrants steal our jobs and our women... will the UK get to keep its nuclear bombs?</b>

The Scottish Government published a really long <a href="http://www.scotland.gov.uk/Publications/2013/11/9348/0">book with answers to all these details</a>.  They're just starting positions of course, after any Yes vote there will be lots of negotiation to work out how everything will be set up, but there is little that seems insurmountable.

They want to start a Scottish equivalent of the BBC which would mean we would actually get Scottish news in Scotland, currently half the news shown to use is irrelevant.  It would make a formal agreement to allow us to keep important stuff like the World Service and Doctor Who.  The National Health Service gives us free use of doctors and hospitals and any politician which says anything against it will be voted out quickly, the NHS is separate in Scotland than that in England so there's no change here, although some no campaign adverts have tried to claim otherwise.  There has been some nonsense about terrorists loving it and we won't have MI5 and GCHQ to look after us all, to me the mass surveilance of the spy agencies is very much a reason to get away from that.  I'd be all for Scotland joining Schengen and having closer ties with Europe but I expect we'll remain part of the British Isles Common Travel Area, it's only sensible politically.  Scotland needs more immigrants for various economic reasons but in England there's a large political wish against it, another good reason to vote Yes.  The UK nuclear bombs are kept north of Glasgow and <a href="http://news-beta.slashdot.org/story/14/08/22/0151253/would-scottish-independence-mean-the-end-of-uks-nuclear-arsenal">sadly this is the only issue that has interested Slashdot</a>.  There is no desire to keep these in Scotland and the Scottish Government has promised to get rid of them mucho rapido, what the rest of the UK does with them is a problem for the rest of the UK.

<b>All very exciting..</b>

The vote is on 18th September and the polls show the No campaign stay steady at a bit below 50% and the Yes campaign started low at 30-odd percent but has been gaining ground as undecided voters move to Yes.  There's now only a few points separating them and there are still plenty of undecided voters.  The trend is for undecideds to move to Yes so it's very much all to play for.  It's fun to live in interesting times.
