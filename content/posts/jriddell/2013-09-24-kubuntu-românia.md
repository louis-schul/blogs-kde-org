---
title:   "Kubuntu România"
date:    2013-09-24
authors:
  - jriddell
slug:    kubuntu-românia
---
<a href="http://ro.kubuntu.org"><img src="http://ro.kubuntu.org/sites/default/files/logo-kubuntu-small.png" width="70" height="71" />Kubuntu România</a> launches today, a Romanian language website with Kubuntu news and support.
