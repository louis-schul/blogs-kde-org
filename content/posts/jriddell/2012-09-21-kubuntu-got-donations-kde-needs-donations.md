---
title:   "Kubuntu got Donations, KDE needs Donations"
date:    2012-09-21
authors:
  - jriddell
slug:    kubuntu-got-donations-kde-needs-donations
---
<a href="http://www.flickr.com/photos/jriddell/8002726157/" title="DSCF7055 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8438/8002726157_14ed280f59.jpg" width="500" height="375" alt="DSCF7055"></a>
There we were starting discussions for the <a href="http://uds.ubuntu.com/">Ubuntu Developer Summit</a> at the end of October in Copenhagen and wondering who we could afford to send when I got a nice e-mail asking what it would cost to send everyone who wanted to go.  I replied with a rough budget of what the flights and hotel would be and next thing I know that had appeared in my Paypal account.  Now I like to think Kubuntu is a friendly and trusting community but this is generosity beyond my expectations.  I'm constantly amazed with how much people like Kubuntu and how they show it.

Now KDE needs your donation for the annual Randa Sprint of kde-core developers.  It's nearly at the target so let's see if we can push it over the line today!
<a href='http://www.pledgie.com/campaigns/18045'><img alt='Click here to lend your support to: KDE Randa Meetings and make a donation at www.pledgie.com !' src='http://www.pledgie.com/campaigns/18045.png?skin_name=chrome' border='0' /></a>
<!--break-->
