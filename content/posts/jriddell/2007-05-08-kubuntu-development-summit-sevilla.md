---
title:   "Kubuntu Development Summit in Sevilla"
date:    2007-05-08
authors:
  - jriddell
slug:    kubuntu-development-summit-sevilla
---
UDS is back underway, this time in the sunny city of Sevilla in southern Spain.  As usual we are working on specifications for the next six months of Kubuntu.  Areas being looked at include desktop search, dolphin in KDE 3, bluetooth support (it's all going to break apparantly), making sure the user is properly informed of removable media, improving Guidance and moving it to Qt 4, adding an on screen keyboard for accessibility and of course packaging KDE 4.

The Kubuntu team are all lovely, Tonio, Michael, Sebas, Ade, Achim, Sarah and John all rock.  It's been especially nice to have Cristo and Augustin from Meduxa here too, they're rolling out their Kubuntu derivative to 35,000 desktops in the Canary islands.  There are another two Kubuntu and Ubuntu based distros in the Canary Islands and about half a dozen more throughout Spain with so far very little collaboration.  The Meduxa dudes are going to move to hosting their changes and scripts on Launchpad and package them in Ubuntu so their setup will be available to all.

<a href="http://www.flickr.com/photos/jriddell/489839886/"><img src="http://farm1.static.flickr.com/206/489839886_ae6065c09a_m.jpg" width="240" height="180" class="showonplanet" /></a>

Lots of geeks.

<a href="http://www.flickr.com/photos/jriddell/489867649/"><img src="http://farm1.static.flickr.com/210/489867649_497bc802e9_m.jpg" width="180" height="240" class="showonplanet" /></a>

Debian Developer in Boozer Scandal!
<!--break-->
