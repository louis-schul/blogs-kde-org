---
title:   "Plymouth and Boston"
date:    2007-11-11
authors:
  - jriddell
slug:    plymouth-and-boston
---
Ah, New England in the Autumn.  Lets of reds and yellows and greens.  It's a desktop art theme waiting to happen.  Went canoeing on the river and enjoyed it much.  No photos of that but here's some exciting other ones.

<a href="http://jriddell.org/photos/2007-11-kev-jonathan-canonical-500-miles.jpg"><img src="http://jriddell.org/2007-11-kev-jonathan-canonical-500-miles-wee.jpg" width="300" height="201" /></a><br />Big Kev and myself contributing Proclainers songs to Canonijam07.

<a href="http://jriddell.org/photos/2007-11-fosscamp-kde-yuiry-jonathan-celeste-troy-jeff-ken.jpg"><img src="http://jriddell.org/2007-11-fosscamp-kde-yuiry-jonathan-celeste-troy-jeff-ken-wee.jpg" width="300" height="210" /></a><br />Slightly delayed KDE at FOSSCamp lignup, Yuriy, Me, Celeste, Troy, Jeff and Ken, but missing Leo and Robert.

<a href="http://www.flickr.com/photos/jonmasters/1957731631/"><img src="http://farm3.static.flickr.com/2079/1957731631_e2751a0f36.jpg" width="500" height="375" /></a><br />Linux hacker Jon Masters falls in love with the new Apple keyboards and impulse buys.

<a href="http://www.flickr.com/photos/jonmasters/1958394516/"><img src="http://farm3.static.flickr.com/2203/1958394516_3960d14343.jpg?v=0" width="500" height="375" /></a><br />And in the spirit of cross distro cooperation I was given a spin in <a href="http://www.flickr.com/photos/jonmasters/1451098158/in/set-72157602188786735/">this car</a>
<!--break-->
