---
title:   "KDE's New Licence Policy"
date:    2008-01-12
authors:
  - jriddell
slug:    kdes-new-licence-policy
---
As one of the archive admins for Ubuntu I often get to see programmes which are in some way improperly licensed.  Often the licence text itself is missing, and sometimes it doesn't match the header file.  So I end up having to care about licensing.  

KDE's licence policy was created some years ago and has worked well in specifying what is for the most part pretty obvious, all KDE code should be free.  With the new GNU GPL it was time to update the policy to ensure all code can link to GPL 3.  I also added policies for non-code (icons, docs) which had not been specified before.  Old licences (Artistic, QPL) were removed and there are versions of the header texts for those who don't trust the FSF to keep future GPL versions free but still allow us to use them when they are.

KDE e.V. today adopted <a href="http://techbase.kde.org/index.php?title=Policies/Licensing_Policy">the new Licensing Policy</a>.  Take a look at it and do remember to use the new header texts in new files.

Writing the policy has given me a new founded respect for lawyers, it's very difficult to get a text which covers all the possibilities.  

This is probably a good opportunity to remind the planets that if you have any code in KDE please sign up to the <a href="http://toma.kovoks.nl/kde">relicensing page</a> to get rid of those last bits of GPL 2 only code.
<!--break-->
