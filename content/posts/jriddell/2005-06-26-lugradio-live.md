---
title:   "LugRadio Live!"
date:    2005-06-26
authors:
  - jriddell
slug:    lugradio-live
---
LUGRadio Live happened yesterday. At last Britain gets a community run expo/conference for Free Software geeks.  Me and some Glasgow LUG geeks took a roadtrip down.  Ben Lamb came along to help run the KDE stall and we handed out lots of Kubuntu CDs and showed people the goodness of KDE.  Credativ UK dude and Kubuntu devel Chris Halls turned up Mark Shuttleworth talked about Ubuntu and going into space, Colin Watson talked about the Ubuntu installer, Christian Schaller talked about GStreamer and Jon Masters talked about embedded Linux.  I've put in notes I took on the <a href="http://wiki.lugradio.org">LugRadio Wiki</a>, scroll to the bottom.  I gave a talk on Kubuntu and KDE, <a href="http://jriddell.org/programs/kubuntu-lugradio-talk-06-2005/">slides are up</a>.  I'm not a good public speaker, it'll be interesting to watch myself once the videos are available.  It also doesn't help that KPresenter 1.4 can't open it's own files without crashing (OpenDocument files are fine).  

When visiting foreign countries it's always good to sample the local cuisine, so we took over a very nice Indian restaurant for lots of curry and beer.  Red Hat kindly paid the bill.  Wolverhampton turned out to be a perfectly nice English city with a cool semi-outdoors nightclub.

The show finished with a live recording of LUGRadio which will be out soon.  Anything they say about Pickle and the Kubuntu CDs in the back of the car is unlikely to be true.

<img src="http://jriddell.org/photos/2005-06-25-lugradio-live-jonathan.jpg" width="320" height="240" class="showonplanet" />
<!--break-->