---
title:   "13.04 Testers Needed!"
date:    2013-04-24
authors:
  - jriddell
slug:    1304-testers-needed
---
We need you to test candidates for the 13.04 Kubuntu release.  <a href="http://iso.qa.ubuntu.com/qatracker/milestones/269/builds">ISO tracker</a> has the tests needed and links to the images.  Chat in #kubuntu-devel to coordinate.  Happy testing!

<img src="http://i1285.photobucket.com/albums/a594/AaronHoneycutt/kubuntu-banner-1304-2_zpsf76e6d5e.png" width="944" height="231" />
