---
title:   "A curious thing"
date:    2004-08-21
authors:
  - jriddell
slug:    curious-thing
---
Further to my last entry, I was wandering to the hostel yesterday evening when I passed a woman who said something in German to me.  "Sorry I don't speak German" I said as I passed and she replied "Sir sir! we have a newspaper in Stuttgart that you can sell, they give it to you free to make money".  Genuinely, she said this.

I need a shave and a haircut.
<!--break-->