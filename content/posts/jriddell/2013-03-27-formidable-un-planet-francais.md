---
title:   "Formidable!  Un Planet Francais"
date:    2013-03-27
authors:
  - jriddell
slug:    formidable-un-planet-francais
---
Bienvenue au <a href="http://planetkde.org/fr/">Planet KDE Francais</a>, un grand planet pour tout la monde francophone.  

<a href="http://planetkde.org/fr/">
<img src="http://planetkde.org/fr/images/48banner_fr.png" width="800" height="140" />
</a>
 
"C'est cool ça un planet en français pout KDE!" Cédric Bellegarde

"Une planète en français s’il vous plait!" Jean-Nicolas Artaud

"So that is the place to go for reading montel blogpost" afiestas