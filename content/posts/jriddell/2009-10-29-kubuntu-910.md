---
title:   "Kubuntu 9.10"
date:    2009-10-29
authors:
  - jriddell
slug:    kubuntu-910
---
9.10 is out.  KDE 4 is really taking shape now with more of the important gaps getting filled.  We ported OpenOffice integration to KDE 4, the installer got some beautiful love from Nuno, social apps integation in various places, there's a handy message indicator and notifications work cross desktop.  We have the latest KDE apps of course, K3b gets a KDE 4 port, user config got ported too.  Rather importantly network connects work and intel graphics drivers aren't rubbish.

There's a shiny new Netbook edition thanks to the Plasma Netbook guys, it's not complete yet but it shows off a lot of nice ideas.

Looking at the future there's still some obvious gaps in the KDE 4 stack, some are being worked on like kbluetooth which has a new maintainer, some don't seem to go anywhere like file sharing, others its less clear what to do such as the web browser question.  But I'm confident that in six months time we'll have something worthy of being LTS.

Hugs to everyone who helped develop and test this release.  Go and <a href="http://www.kubuntu.org/getkubuntu/download">download</a> or <a href="https://help.ubuntu.com/community/KarmicUpgrades/Kubuntu">Upgrade</a> and <a href="http://wiki.kubuntu.org/KarmicKoala/Final/Kubuntu/Feedback">let us know what you think</a>

<img src="http://jasmine.19inch.net/~jr/tmp/released.png" width="849" height="168" />
<!--break-->