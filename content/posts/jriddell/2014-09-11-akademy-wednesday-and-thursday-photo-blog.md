---
title:   "Akademy Wednesday and Thursday Photo Blog"
date:    2014-09-11
authors:
  - jriddell
slug:    akademy-wednesday-and-thursday-photo-blog
---
<a href="https://www.flickr.com/photos/jriddell/15024019887" title="DSC_0769 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3856/15024019887_397700b48a.jpg" width="500" height="281" alt="DSC_0769"></a>
Hacking hard in the hacking room

<a href="https://www.flickr.com/photos/jriddell/15187565146" title="DSC_0773 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3891/15187565146_d830b105f4.jpg" width="500" height="281" alt="DSC_0773"></a>
Blue Systems Beer

<a href="https://www.flickr.com/photos/jriddell/15210595045" title="DSC_0775 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3836/15210595045_81cc05ed60.jpg" width="281" height="500" alt="DSC_0775"></a>
You will keep GStreamer support in Phonon

<a href="https://www.flickr.com/photos/jriddell/15024014677" title="DSC_0780 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3863/15024014677_1ba7ca2cbb.jpg" width="500" height="281" alt="DSC_0780"></a>
Boat trip on the loch

<a href="https://www.flickr.com/photos/jriddell/15207534901" title="DSC_0781 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5556/15207534901_c563f8fd04.jpg" width="281" height="500" alt="DSC_0781"></a>
Off the ferry

<a href="https://www.flickr.com/photos/jriddell/15210198642" title="DSC_0783 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3875/15210198642_c1094b00fa.jpg" width="500" height="281" alt="DSC_0783"></a>
Bushan leads the way

<a href="https://www.flickr.com/photos/jriddell/15210197882" title="DSC_0784 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3913/15210197882_c93f60514f.jpg" width="281" height="500" alt="DSC_0784"></a>
A fairy castle appears in the distance

<a href="https://www.flickr.com/photos/jriddell/15210196512" title="DSC_0787 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3853/15210196512_6f7b4b20d1.jpg" width="500" height="281" alt="DSC_0787"></a>
The talent show judges

<a href="https://www.flickr.com/photos/jriddell/15024006847" title="DSC_0790 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3908/15024006847_b7ed26975c.jpg" width="281" height="500" alt="DSC_0790"></a>
Sinny models our stylish Kubuntu polo shirts

<a href="https://www.flickr.com/photos/jriddell/15207527101" title="DSC_0793 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5593/15207527101_c6618e7959.jpg" width="500" height="281" alt="DSC_0793"></a>
Kubuntu Day discussions with developers from the Munich Kubuntu rollout

<a href="https://www.flickr.com/photos/jriddell/15020612107" title="IMG 9510 v1 by Jonathan Riddell, on Flickr"><img src="https://farm6.staticflickr.com/5563/15020612107_91753e3ed3.jpg" width="500" height="333" alt="IMG 9510 v1"></a>
Kubuntu Day group photo with people from Kubuntu, KDE, Debian, Limux and Net-runner

<a href="https://www.flickr.com/photos/jriddell/15010982058" title="c IMG 8903 v1 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3893/15010982058_752eb49b4f.jpg" width="500" height="333" alt="c IMG 8903 v1"></a>
Jonathan gets a messiah complex
<!--break-->
