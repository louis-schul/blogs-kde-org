---
title:   "Akademy"
date:    2006-09-24
authors:
  - jriddell
slug:    akademy
---
The KDE conference has had a great first day here in Dublin.  The buzz here has an impressive atmosphere because unlike last year where we were just hearing the start of what might become KDE 4 one day, now we have all these projects saying what they've done and where they're going.  KDE 4 really is going to rock!

Great project talks about Plasma, Decibel, Phonon, Solid, Akonadi and KHTML.  Ellen told us how to keep users in mind.  Keith Packard impressed us with his X server code written that morning to get his laptop to display properly on the projector and his desire to make /dev/null a valid xorg.conf file.  Jin from Ricoh told us about the open platform their printers have and Gnome/Freedesktop's J5 convinced us that competition and cooperation are not mutually exclusive.  At the keynotes John from OSDL gave us the stats on the state of the linux desktop and Aaron did the visionary thing asking what KDE is.

Great to see the likes of mjg59, daf, rob from other communities.  Converts to KDE or just wanting an excuse for a weekend in Dublin?

A free Guinness and free pizza from Nokia were very welcome by all last night, thanks Nokia.

<img src="http://static.flickr.com/103/251122189_2123a8a177.jpg?v=0" width="500" height="375" class="showonplanet" />

"Biggest logo I've seen" said OSDL's John Cherry.
<!--break-->
