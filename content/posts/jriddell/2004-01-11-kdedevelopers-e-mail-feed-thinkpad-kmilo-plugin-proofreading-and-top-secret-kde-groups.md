---
title:   "kdedevelopers e-mail feed, thinkpad KMilo plugin, proofreading and top secret KDE groups!"
date:    2004-01-11
authors:
  - jriddell
slug:    kdedevelopers-e-mail-feed-thinkpad-kmilo-plugin-proofreading-and-top-secret-kde-groups
---
<a href="http://www.kde.me.uk">kde.me.uk</a> has some things of mine on it.

kde.com's e-mail feed of KDE dot news died along with kde.com so I set up another one for dot news and added debianplanet.org and kdedevelopers.org.  <a href="http://www.kde.me.uk/index.php?page=email-news-feed">email news feeds</a>.  KDE dot news are planning on adding their own e-mail feed so that one may dissapear.

I made a <a href="http://www.kde.me.uk/index.php?page=kmilo">KMilo plugin for Thinkpads</a> which lets you use the volume and other buttons with KDE.  KMilo is a bit under-documented so that page is also a general KMilo page, if you have a laptop with funny buttons on it (mine has volume, mute, brightness, keyboard light and one just marked 'Thinkpad') consider making a KMilo plugin for it.

I have a list of <a href="http://www.kde.me.uk/index.php?page=top-secret-kde-groups">top secret KDE groups</a> which for whatever reason (mostly good) arn't publisised.  One of them without a good reason is the <a href="http://www.kde.me.uk/index.php?page=KDE_Typos">KDE proofreaders</a> who now also have a page where you can add typos you find either in i18n() strings or documentation, ready for fixing once we're out of a string freeze.