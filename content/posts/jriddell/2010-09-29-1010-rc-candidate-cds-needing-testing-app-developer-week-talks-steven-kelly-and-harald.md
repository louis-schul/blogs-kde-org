---
title:   "10.10 RC Candidate CDs Needing Testing; App Developer Week Talks with Steven Kelly and Harald Sitter"
date:    2010-09-29
authors:
  - jriddell
slug:    1010-rc-candidate-cds-needing-testing-app-developer-week-talks-steven-kelly-and-harald
---
Candidate CDs for our RC need testing <a href="http://iso.qa.ubuntu.com/">see the ISO tracker</a> for what needs testing, download (or upgrade) and report back.  Ask on #kubuntu-devel or #ubuntu-testing if you have questions.

<hr />

The community team at Canonical have come up with another week of IRC talks, this one is <a href="https://wiki.kubuntu.org/UbuntuAppDeveloperWeek">Ubuntu App Developer Week</a>. If you missed it yesterday you can catch the logs of <a href="http://irclogs.ubuntu.com/2010/09/28/%23ubuntu-classroom.html">Harald talking about Qt and its unicorn sparkles</a> (from 18:00).  Coming up tomorrow (Thursday) is "<b>Widgetcraft</b>" with Harald and "<b>Using Grantlee to create application themes</b>" - Stephen Kelly.  Join us on #ubuntu-classroom on freenode IRC for the talks.
<!--break-->
