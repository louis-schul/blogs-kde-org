---
title:   "No Licence Needed for Kubuntu Derivative Distributions"
date:    2014-02-14
authors:
  - jriddell
slug:    no-licence-needed-kubuntu-derivative-distributions
---
Early last year the Linux Mint developer told me he had been contacted by Canonical's community manager to tell him he needed to licence his use of the packages he used from Ubuntu.  Now Ubuntu is free software and as an archive admin, I spend a lot of time reviewing everything that goes into Ubuntu to ensure it has freedom in its copyright.  So I advised him to ignore the issue as being FUD.

Later last year rumours of this nonsense started appearing in the tech press so instead of writing a grumpy blog post I e-mailed the community council and said they needed to nip it in the bud and state that no licence is needed to make a derivative distribution.  Time passed, at some point Canonical changed their licence policy to be called an <a href="http://www.canonical.com/intellectual-property-rights-policy">Intellectual property rights policy</a> and be much more vague about any licences needed for binary packages.  Now the community council have put out a <a href="http://fridge.ubuntu.com/2014/02/13/community-council-statement-on-canonical-package-licensing/">Statement on Canonical Package Licensing</a> which is also extremely vague and generally apologetic for Canonical doing this.

So let me say clearly, no licence is needed to make a derivative distribution of Kubuntu.  All you need to do is remove obvious uses of the Kubuntu trademark.  Any suggestion that somehow compiling the packages causes Canonical to own extra copyrights is nonsense.  Any suggestion that there are unspecified trademarks that need a licence is untrue.  Any suggestion there is compilation copyright is irrelevant in most countries and untrue for derivatives almost by definition.  Any suggestion that the version number needs  a trademark licence is just clutching at straws.

From every school in Brazil to every computer in Munich City Council to projects like Netrunner and Linux Mint KDE we are very pleased to have derivative distributions of Kubuntu and encourage them to be made if you can't be part of the Ubuntu community for whatever reason.

<hr />
In more positive news <a href="http://www.markshuttleworth.com/archives/1316">Ubuntu plans to move to systemd</a>.  This makes me happy, although systemd slightly scares me for its complexity and it's a shame Upstart didn't get the take up it deserved given its lead in the replace-sysv-init competition, it's not as scary as being the only major distro that didn't use it.
