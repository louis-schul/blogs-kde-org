---
title:   "Kubuntu Settings in KDE"
date:    2008-03-20
authors:
  - jriddell
slug:    kubuntu-settings-kde
---
I <a href="http://article.gmane.org/gmane.comp.kde.devel.core/51394">posted the list of Kubuntu settings to kde-core-devel</a> in the hope of getting as many as possible changed for KDE 4.1.  If you have an opinion or any more to add, do reply on the list.
