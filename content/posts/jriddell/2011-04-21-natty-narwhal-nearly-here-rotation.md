---
title:   "Natty Narwhal Nearly Here, On Rotation"
date:    2011-04-21
authors:
  - jriddell
slug:    natty-narwhal-nearly-here-rotation
---
11.04 is nearly here.  If you haven't <a href="https://wiki.kubuntu.org/NattyNarwhal/Beta2/Kubuntu">tried the beta</a> yet you only have a week to go to make sure you catch any last beasties in this release.  KDE Platform 4 is now in a pleasingly stable state so if you haven't tried Plasma or KDE Software for a while and are finding the alternatives a bit too different in their UI changes do give Kubuntu a try.

Personally, I'm looking forward to something different.  I'm going on rotation from my normal position on Canonical's Desktop team working on Kubuntu over to the Bazaar team.  This means a challenging six months working on something completely different.  After more than 5 years working on Kubuntu I'm feeling the need to ensure I can do other things.  I also want to take some distro packager skills to another team to help them integrate more.  I'll be a challenge to the Kubuntu community too, continuing to make a great distro without anyone working full time on it.  But they're excellent folks and I'm sure they will manage for one release without me.

<img src="http://people.canonical.com/~jriddell/nattycountdown7.png" width="270" height="149" />
<!--break-->
