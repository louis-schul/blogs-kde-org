---
title:   "Shared Desktop Notifications from Canonical"
date:    2009-07-24
authors:
  - jriddell
slug:    shared-desktop-notifications-canonical
---
Aurelien Gateau and the rest of the Canonical Desktop Experience folks have been working super hard to get the visual notifications on KDE and Gnome united as part of their Project Ayatana.  This involves uniting the Galago and KDE VisualNotification DBus interfaces.  Along the way freedesktop.org had to get fixed to make such cooperation possible.  But now the KDE half of the changes are in (KDE trunk and Kubuntu Karmic) and Gnome apps running in KDE show visual notifications just like KDE apps.  There's minor issues to be fixed and the other way round (KDE apps running on Gnome) still has some patches to be finished & crashes to be fixed but it works in a lot of cases.

<img src="http://people.canonical.com/~jriddell/popup.png" width="433" height="160" />
Gnome app on KDE.  Why Pidgin, you fit into the KDE desktop now.

<img src="http://people.canonical.com/~jriddell/popup-gnome-update-notify.png" width="506" height="164" />
KDE app on Gnome.  Amarok, your popups are so Gnomeish.
<!--break-->
