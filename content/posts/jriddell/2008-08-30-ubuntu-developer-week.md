---
title:   "Ubuntu Developer Week"
date:    2008-08-30
authors:
  - jriddell
slug:    ubuntu-developer-week
---
<a href="https://wiki.ubuntu.com/UbuntuDeveloperWeek">Ubuntu Developer Week</a> happens on IRC next week.  I'll be giving my introduction to PyKDE with a WebKit web browser tutorial.  There's plenty more interesting talks, see you there.

