---
title:   "ISO Testers Needed"
date:    2010-04-27
authors:
  - jriddell
slug:    iso-testers-needed
---
Candidate images for the Kubuntu 10.04 LTS release have <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">started to appear</a>.  We needs as much testing as possible to make sure these can be given the golden stamp of LTS approval.  Download, install, give us your results on IRC in #kubuntu-devel and the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO tracker</a>.

<img src="http://people.canonical.com/~jriddell/10.10-countdown/kubuntu_2.png" width="180" height="150" />
<!--break-->
