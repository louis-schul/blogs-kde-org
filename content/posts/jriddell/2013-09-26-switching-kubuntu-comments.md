---
title:   "Switching to Kubuntu comments"
date:    2013-09-26
authors:
  - jriddell
slug:    switching-kubuntu-comments
---
Some comments on switching to Kubuntu from a recent Slashdot article...

<i>I switched to Kubuntu ... I haven't looked back. I like *buntu distributions simply because they're the easiest to get up and running. Unless you need a highly customized Linux system, you can't argue with *buntu's simplicity when it comes to installation.</i>

<i>I can't upvote this enough. Kubuntu is a perfectly decent distro. KDE 4 has finally come of age.</i>

<i>I just wiped my box after a drive failure and went Kubuntu. It's early days but so far I love it. It's very polished, responsive and easy to get "functional" IE with binary NVIDIA driver, non-free codecs etc. It feels like a premium product I should have to pay for.</i>
