---
title:   "Desktop Summit"
date:    2011-08-10
authors:
  - jriddell
slug:    desktop-summit
---
<img
src="http://4.bp.blogspot.com/-EBjFCWS5zyY/TkFfSN8owhI/AAAAAAAAAHg/b0OxKtBeS4A/s1600/ds_mine.png" width ="333" height="138" />

I went to the Desktop Summit in Berlin where more than 700 people from Gnome,
KDE and elsewhere met for talks and beer.  Canonical has a sensible policy of needing a report back from any conference we go to so here's mine..

The first non-keynote talk I went to was about Simon, a speech recognition
programme.  This is an impressive aid to accessibility and, according to the
presenter Peter Grasch, fills an important gap not filled by commercial
products because it isn't restricted to a language, so it can be used by people
whose disabilities prevents them speaking a full language.  On the other side of
accessibility, Frederick Gladhorn presented his work on supporting AT-SPI in Qt
so now widgets can be read out with voice readers. It's great to see
accessibilty coming to Qt at last.

Martin Grasslin gave his award winning and Slashdotted talk on porting KWin to
Wayland.  Some nice news here, Canonical has sponsored Martin a Panda Board so
KWin should be working well on ARM.

Sebas presented the status of Plasma Active, a workspace full of slidey bits
designed with help from BasysKom for tablets and phones, which should give
Kubuntu Mobile some exiting times ahead.

Robert Ancell gave a talk on LightDM, his all new cross desktop login manager. 
It's being used in Ubuntu Desktop and is planned for Kubuntu too (although
lacking manpower currently).  It was questioned why if there are already lots
of login managers he started another one, answer was that none of the existing
ones could easily be made cross desktop.

The talk on Rekonq, our default browser in Kubuntu, has a word of warning. 
WebKit is being separated into a separate process model (like Chromium already
is) which would make QtWebKit impossible to expose the same options in the API
that allow KDEWebKit to be made, possible solution would be to move KDEWebKit
inside the WebKit source tree.

Colin Guthrie took us through Pulseaudio and the current state of GUI sound
configuration across Windows, Mac, KDE and Gnome.  I told him about my problems
configuring input devices and the next day a couple of patches turned up in my
indox, lovely.

The KDE Frameworks team presented their plan for splitting up KDE Libs into
three levels of libraries and products.  The bottom tier depends only on Qt and
the next two depend on progressively more depenencies.  The idea is to make as
many parts of KDE Frameworks available to Qt developers from outwith KDE as
possible.

On the Monday I started with John Layt's talk on defining common standards for
calendar and holiday systems, one of those areas where many have done half
hearted attempts before but nobody has solved it in a way which pleases
everyone.  Good luck John.

Gnome's Vincent Untz spoke on a number of issues around Gnome, one being the
lack of engagement with Canonical.  He said it was a shame there were not many
people from Canonical at the summmit, actually I counted about a dozen but we
could have been more noticable, there was no poster except my Kubuntu one and
no sponsors talk from us.

To branch out more I went to a talk on the state of the Enlightenment
libraries.  These have been in development for about a decade without a release
but really honestly are nearing release ready now.  He complained about
freedesktop standards which often don't take performance into account but I
would credit them more if they came up with more solutions rather than just
complaining.

Frank gave a talk on his exciting project OwnCloud, sadly the projector broke
so we couldn't see any demos but it turned the session into more of a
discussion which was interesting.  I reminded them to watch licencing (it
doesn't currently match the KDE licence policy) and usability (after testing it
out with a friend who couldn't upload a file).  There is a big task ahead of
OwnCloud if it is to be able to compete with the likes of Flickr never mind
Dropbox but he says the project has a lot of momentum now with the commits
graph growing exponentially.

Otherwise nice beer and food.  Motel One hotels are a bit
strange, they insist on making you watch the fish channel on TV and don't put
soap in the rooms. I didn't find myself socialising with many Gnome
developers which is a shame, it's hard enough to catch up with the KDE people
you want to never mind a whole second community. 

It was great to catch up with some Kubuntu people and make the real life to IRC match for Jonathan Kolberg and Arthur Schiwon.  They seem to be managing ok with the KDE packaging without me while I'm on rotation to Bazaar but getting a bit behind on some of the features.

I got a few ideas for Bazaar out of discussions with people including a Dolphin plugin so stay tuned for that.

<img src="http://people.canonical.com/~jriddell/DSCF5811.JPG" width="400"
height="300" />
Kubuntu at Desktop Summit - Rohan, Harald, Yofel, bulldog89, Moi - shortly
before our rousing rendition of We Will Rock You at the karaoke.
<!--break-->
