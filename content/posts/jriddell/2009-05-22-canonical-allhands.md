---
title:   "Canonical AllHands"
date:    2009-05-22
authors:
  - jriddell
slug:    canonical-allhands
---
The Canonical AllHands meeting is happening a wee bit outside of Barcelona.  It is a requirement of all buildings in Barcelona that they have weird and interesting architecture.  

<img src="http://www.principal-hayley.com/images/content/header-images/property_landing-alt_1230632835.jpg" width="738" height="248" />
This hotel has more floors underground than it has above ground.  The translucent showers are a curious feature but my roommate is Aurelien who turns out to be as cool a bloke as you could expect being a KDE developer.

<img src="http://people.ubuntu.com/~jriddell/DSCN3504.JPG" width="500" height="375" />
Why Canonical, with these Ferrero Rocher you are really spoiling us no?

<img src="http://people.ubuntu.com/~jriddell/DSCN3508.JPG" width="500" height="375" />
I got to meet Zhengpeng who helped make Kubuntu support CKJ back in the day and now works for the OEM team.

<img src="http://people.ubuntu.com/~jriddell/DSCN3512.JPG" width="500" height="375" />
Aurélien fixed my Gwenview crasher bug without me having to even report it!

I've had a good number of people come up to me to shake my hand and thank me for making the desktop they use happen on Ubuntu, naturally I blush I remind them of the many wonderful people at KDE and Kubuntu who do far more than I ever do.  I've also had quite a few say "I've never used KDE.  Although I use Kopete all the time.  And Amarok is great.  And KMail saved me when my other app broke."  turns out people use KDE without even thinking about it, which is quite pleasing.

Outside AllHands life goes on, I recieved an e-mail with this very cool notice in it "We just installed Kubuntu 9.04 on primary control server for Kuwait National Radio Observatory, and it will be used for control & data acquisition."  Kubuntu goes to the stars.
<!--break-->
