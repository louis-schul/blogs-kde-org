---
title:   "Kubuntu Developer Summit at the Googleplex"
date:    2006-11-07
authors:
  - jriddell
slug:    kubuntu-developer-summit-googleplex
---
The Ubuntu Summit at Google in the US is now underway.  The Googleplex is huge and we are only in one of the smaller buildings on the campus.  Of course this is America and everything is huge, we shared a salad between three people yesterday and still didn't finish it.  

Anyway, the Ubuntu summits are always based around small group discussions and writing specifications to be implemented over the next release cycle.  And we have a lot of <a href="https://features.launchpad.net/sprints/uds-mtv">specifications to be discussed</a>.  The good news is we have a truly excellent Kubuntu/KDE presence here, thanks to all the team for turning up for this, I love you all.  You can even listen in to the discussions by dialing in to the SIP speaker phones we have on each discussion table.  See <a href="https://wiki.kubuntu.org/UbuntuDeveloperSummitMountainView/Participate">the participate discussion page</a> for details.  This is especially fun if you are listening in when the developers forget about the speaker phones and start bitching about other developers, ahem.

There's lots to do with more items turning up each time I overhear someone talking about a spec I havn't yet paid attention to.  So it's going to be a busy 6 months for our feisty release, but certainly a fun time.

<img src="http://static.flickr.com/110/290289158_5afe02912f.jpg?v=0" width="500" height="375" />
<!--break-->
