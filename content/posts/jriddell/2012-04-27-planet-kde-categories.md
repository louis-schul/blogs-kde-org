---
title:   "Planet KDE Categories"
date:    2012-04-27
authors:
  - jriddell
slug:    planet-kde-categories
---
I've had some questions about Planet KDE recently.

Planet KDE is intended for personal blogs of KDE contributors blogging with a KDE focus.  All KDE contributors are welcome to add their feeds to planet by editing the file in svn or filing a bug.  

A while ago I added some categories because people were asking about adding blogs that weren't KDE contributors.  Which categories are shown can be easily edited on your local browser by clicking "Configure" at the top of the page.  KDE.News has the dot.kde.org feed and is on by default, Project News has KDE and KDE friendly projects like Calligra or Kubuntu in it and is also on by default.  There are also categories for blogs not in English and for user blogs.  Finally there's the microblogging category which is for twitter feeds and shows in a box on the side if you turn it on.

Hopefully that provides a way for KDE fans to easily read about what is happening in our community.
<!--break-->