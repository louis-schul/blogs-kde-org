---
title:   "Kubuntu Won't be Switching to Mir or XMir"
date:    2013-06-27
authors:
  - jriddell
slug:    kubuntu-wont-be-switching-mir-or-xmir
---
A few months ago Canonical announced their new graphics system for Ubuntu, Mir.  It's a shame the Linux desktop market hasn't taken off as we all hoped at the turn of the millennium and they feel the need to follow a more Apple or Android style of approach making an OS which works in isolation rather than as part of a community development method.  

Here at Kubuntu we still want to work as part of the community development, taking the fine software from KDE and other upstreams and putting it on computers worldwide.  So when Ubuntu Desktop gets switched to Mir we won't be following.  We'll be staying with X on the images for our 13.10 release now in development and the 14.04LTS release next year.  After that we hope to switch to Wayland which is what KDE and every other Linux distro hopes to do.  

There's been a few <a href="https://plus.google.com/u/0/110095242873945299189/posts">posts of Kubuntu on XMir</a> and other flavours too.  That's nice to see but compositing is very fragile, even Ubuntu shipping the latest Mesa can increase the number of beasties the poor KWin developers have to handle with because of unexpected issues it creates.  Putting another layer between KWin and your monitor is certain to create new issues so I'll be keeping anything to do with Mir off the Kubuntu images.

What's unknown is what happens when an Ubuntu Desktop user installs Kubuntu Desktop or vice versa.  We'll have to find a way to deal with that in a sensible manner.  And maintaining Wayland in the ubuntu archives will be fiddly, even if it is mostly a sync from Debian.  And the live CD system is fiddly with X never mind with three different graphics systems.  But we press on because we love it and we change the world.

Come to <a href="http://blogs.kde.org/2013/05/08/kubuntu-developer-summit-akademy">Kubuntu @ Akademy</a> in Basque Country next month to discuss it.

<img src="http://akademy2013.kde.org/sites/akademy2013.kde.org/files/Ak2013Badge2.png" width="400" height="178" />
