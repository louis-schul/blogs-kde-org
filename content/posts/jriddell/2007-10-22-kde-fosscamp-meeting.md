---
title:   "KDE FOSSCamp Meeting"
date:    2007-10-22
authors:
  - jriddell
slug:    kde-fosscamp-meeting
---
Some KDE people met to discuss next weekend's FOSSCamp on IRC.  <a href="http://kubuntu.org/~jriddell/kde-fosscamp-meeting.log">Reading the logs</a> might be useful if you're going and don't yet know what to expect.
<!--break-->
