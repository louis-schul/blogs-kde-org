---
title:   "This week in Kubuntu"
date:    2008-08-07
authors:
  - jriddell
slug:    week-kubuntu
---
Haven't blogged for a while, so here's some of the changes that have happened in Kubuntu recently.

I finally found some time for the KDE port of system-config-printer, still far from feature parity with the Gnome version and needs plenty of tidying up but should be usable.

<a href="http://kubuntu.org/~jriddell/blog/system-config-printer-kde.png"><img src="http://kubuntu.org/~jriddell/blog/wee-system-config-printer-kde.png"  width="500" height="266" /></a>

mornfall got back to hacking on Adept 3 featuring super fast xapian search.

<a href="http://kubuntu.org/~jriddell/blog/adept3.png"><img src="http://kubuntu.org/~jriddell/blog/wee-adept3.png"  width="500" height="318" /></a>

In other packaging news the Dist Upgrade tool got a pyQt 4 port.

<a href="http://kubuntu.org/~jriddell/blog/dist-upgrade-tool.png"><img src="http://kubuntu.org/~jriddell/blog/dist-upgrade-tool.png" width="359" height="329" /></a>

And gdebi, the .deb installer, got a pyKDE 4 port.

<a href="http://kubuntu.org/~jriddell/blog/gdebi-kde4.png"><img src="http://kubuntu.org/~jriddell/blog/wee-gdebi-kde4.png"  width="500" height="286" /></a>

We're turning on the <a href="https://wiki.kubuntu.org/Apport">Apport</a> crash handler for KDE programmes.  Unlike the KDE crash handler it creates a full backtrace with debug symbols thanks to Launchpad.  Will have to see if the bug team will manage to forward relevant reports upstream.

<a href="http://kubuntu.org/~jriddell/blog/apport-qt.png"><img src="http://kubuntu.org/~jriddell/blog/wee-apport-qt-kde.png"  width="500" height="184" /></a>

We finally got a shiny new website, thanks to Ryan.

<a href="http://kubuntu.org/~jriddell/blog/kubuntu-website.png"><img src="http://kubuntu.org/~jriddell/blog/wee-kubuntu-website.png"  width="500" height="298" /></a>

I turned on compositing by default in kwin, still needs a patch to Compiz so we can use the same blacklist as it does.  Not sure it'll stay on for final, just depends on how reliable it is out in the wild.

<a href="http://kubuntu.org/~jriddell/blog/kwin-compositing.png"><img src="http://kubuntu.org/~jriddell/blog/wee-kwin-compositing.png"  width="500" height="313" /></a>

Worlds finest <a href="http://bazaar-vcs.org/">distributed revision control system, bzr</a> decided to go with the world's finest toolkit for their GUI, I uploaded qbzr today.

<a href="http://kubuntu.org/~jriddell/blog/qbzr.png"><img src="http://kubuntu.org/~jriddell/blog/wee-qbzr.png"  width="500" height="424" /></a>

Next week

<a href="http://akademy.kde.org"><img src="http://kubuntu.org/~jriddell/blog/goingakademy08.png"  width="320" height="178" /></a>

Finally, congratulations to John and SakiFlux.

<!--break-->
