---
title:   "usb-creator in suse; accessibility in Qt, it speaks!"
date:    2011-03-23
authors:
  - jriddell
slug:    usb-creator-suse-accessibility-qt-it-speaks
---
For reasons I haven't worked out, Ubuntu needs a specific tool to write its ISO CD images onto a USB disk.  This is fine if you're on an Ubuntu distro or on Windows where we have usb-creator available to install, but is a problem for users of other distros.  If an openSUSE user who has no CD drive wants to try out Ubuntu (to steal all our great ideas no doubt) they're a bit stuck.  So I learnt how to build RPMs and secrets of the openSUSE build service and put <a href="https://build.opensuse.org/package/show?package=usb-creator&project=home%3Ariddell%3Ausb-creator">Ubuntu USB Creator into an openSUSE archive</a>.  Unfortunately although I patched it to run fine the end result doesn't boot and gets stuck at the syslinux startup credit.  So more work to be done there I'm afraid.

<hr />

More successful was packaging <a href="http://gitorious.org/qt-at-spi">qt-at-spi</a>, a plugin to get Qt talking to AT-SPI2.  Qt 4 has always had accessibility but while it has worked on Windows and Mac it has never been plugged into the AT-SPI interface used by Linux.  This is because AT-SPI used Bonobo which used CORBA and nobody wants to implement CORBA if they can at all avoid it.  So Nokia and others worked on AT-SPI2 which uses DBus.  Unfortunately AT-SPI2 has been a long time coming, but now it is stabalising and getting ready for use.  Frederick Gladhorn has taken up the Qt plugin to talk AT-SPI 2 and you can find it <a href="https://launchpad.net/~kubuntu-ppa/+archive/experimental">in the Kubuntu Experimental PPA</a>.

It takes some fiddling to get it to work and the end result is crashy.  I had to install at-spi2-core, libatk-adaptor, python-pyatspi2 as well as orca the screen reader.  Then set some gconf settings to tell orca to use AT-SPI2 (gconftool-2 --set /desktop/gnome/interface/at-spi-dbus --type bool true; gconftool-2 --set /desktop/gnome/interface/at-spi-corba --type bool false).  Force some environment variables export GTK_MODULES=gail:atk-bridge; export QT_ACCESSIBILITY=1  and finally orca will read my Qt applications.  Yay.

<!--break-->

