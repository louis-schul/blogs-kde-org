---
title:   "Plasma 5 Is Green"
date:    2014-06-12
authors:
  - jriddell
slug:    plasma-5-green
---
On our <a href="http://qa.kubuntu.co.uk/plasma-status/build_status_4.97.0_utopic.html">Plasma 5 build status page</a> most of the packages are now a pleasing green colour.  For the first time today I installed them all and logged in and... it worked!  It took a bit of removing old caches and obsolete installs that'd I'd been making in the months previously and that nice temporary Next wallpaper everyone uses doesn't really get shipped so I had to add that and the icons sometimes work and sometimes don't and there's no plasma-nm release yet so I had to grab a copy and build that before I could use the network.  But with some fiddle and wee bit ay faff, it works!

Today is a beautiful day.

<a href="http://starsky.19inch.net/~jr/tmp/plasma5.png"><img src="http://starsky.19inch.net/~jr/tmp/plasma5-wee.png" width="500" height="313" /></a>

If you just want to try it out <a href="http://neon.blue-systems.com/live-iso/">the Neon 5 ISO</a> is the easiest way still.  But the packages I'm pleased about today are the <a href="https://launchpad.net/~kubuntu-ppa/+archive/next">Next PPA packages</a> which is the packaging Kubuntu (and hopefully Debian) will be using going forward.  It doesn't co-install with Plasma 1 so only use if you want breakage and if you want to help fix breakage, join #kubuntu-devel and help out.
<!--break-->
