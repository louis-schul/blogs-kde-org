---
title:   "Akademy Coming Close"
date:    2007-06-15
authors:
  - jriddell
slug:    akademy-coming-close
---
The year's greatest free software conference, <a href="http://akademy2007.kde.org/">Akademy 2007</a> is only two weeks away.  We have some <a href="http://akademy2007.kde.org/schedule.php">top stuff lined up</a> for you.  Keynotes will be from Mark Shuttleworth of Canonical, Lars Knoll of Trolltech and Dan Kohn from Linux Foundation.  The rest of the <a href="http://akademy2007.kde.org/conference/programme.php">conference programme</a> is packed with goodies including an opening from local member of parliament Patrick Harvie and a closing with the Akademy Awards (the winning names are written inside a sealed envelope on my desk, anyone trying to steal it will have to get through my laser intrusion system).  After the conference on Saturday we'll be having pizza and music at <a href="http://www.chateaugateau.co.uk/">the chateau</a>, remember to fill in your <a href="http://pmurdoch.com/akademy/diet.php">food preferences</a> or you won't get any pizza.

Things are no less busy throughout the week with a <a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">schools day</a> keynoted by Intel with their new classmate PCs.  We have the <a href="http://www.freedesktop.org/wiki/TextLayout2007">text layout summit</a> which will see the brightest minds from across free software projects come together and ponder language scripts.  Then there's a full <a href="http://akademy2007.kde.org/codingmarathon/bof.php">week of BoF sessions</a> to discuss everything that's happening.  

Add in a <a href="http://akademy2007.kde.org/codingmarathon/day-trip.php">Day Trip to Loch Lomond</a> complete with barbeque (no beer for me, I'm driving the food) and a reception courtesy of Glasgow City's Lord Provost (and buffet thanks to Trolltech, yum) and it's going to be the best Akademy yet!  <a href="http://www.kde.org.uk/akademy/">Sign up now</a>, if you name isn't down, you're not getting in.  See you in Glasgow.

<img src="http://muse.19inch.net/~jr/tmp/akademy2007-logo.png" width="300" height="100" />
<!--break-->
