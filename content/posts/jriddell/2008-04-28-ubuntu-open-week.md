---
title:   "Ubuntu Open Week"
date:    2008-04-28
authors:
  - jriddell
slug:    ubuntu-open-week
---
The IRC talks series <a href="https://wiki.kubuntu.org/UbuntuOpenWeek">Ubuntu Open Week</a> is under way now in #ubuntu-classroom.  Coming up at 20:00UTC is Kubuntu Development - Richard Johnson.  See the timetable for a whole week's worth of talks.
