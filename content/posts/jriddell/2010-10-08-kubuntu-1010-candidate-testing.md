---
title:   "Kubuntu 10.10 candidate testing"
date:    2010-10-08
authors:
  - jriddell
slug:    kubuntu-1010-candidate-testing
---
Today it's all hands on board for the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">final candidate testing</a>.  Upgrades need testing too.  Join us in #kubuntu-devel to help.

<img src="http://people.canonical.com/~jriddell/2days.png" width="189" height="157" />
<!--break-->
