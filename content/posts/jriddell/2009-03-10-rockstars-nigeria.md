---
title:   "Rockstars of Nigeria"
date:    2009-03-10
authors:
  - jriddell
slug:    rockstars-nigeria
---
Nigeria!  What a country, what a conference, what a great bunch of lovely people.  We rocked the town.  I've never had to have a bouncer keeping people from swamping me at a conference before, but then I've never been mobbed my so many people asking for photographs and autographs.

<img src="http://farm4.static.flickr.com/3416/3344261940_7a18b31ccd.jpg?v=0" width="500" height="375" />
Before the conference started, not quite sure what was going to happen

Kano is a huge and vibrant town.  The streets are filled with people, cars, motorbikes and stalls to buy anything at.  We arrived late, were met at the airport and taken to fancy guest rooms at Bayero University.  Dinner consisted of a tasty whole chicken and I fell asleep to the sound of the city.  I was woken by the call to prayer at 5 in the morning, and promptly fell asleep again.  

The conference started early with about 500 students and professionals there.  The table on stage had important folks from the university and government.  Me and Ade were requested to join them.  The minister for IT opened with a talk on the wonders of free software and Ade was next with talks on joining free software projects and literacy bridge.  We finished with a Q & A session.  

<img src="http://farm4.static.flickr.com/3342/3344263406_4f4541cc72.jpg?v=0" width="500" height="375" />
Ade spoke for most of the first day

With the first day over we had some idea of what to expect.  Together with the organisers we threw out the programme and re-wrote our talks for day two.  I gave a talk on open source development tools including bugzilla, svn, localise, launchpad and bzr.  

<img src="http://farm4.static.flickr.com/3344/3344262120_f321d4d52c.jpg?v=0" width="375" height="500" />
Demonstrating bzr

In the afternoon we attempted to turn everyone into KDE programmers with Ade introducing Qt and then I took them through my PyKDE WebKit browser tutorial.  We finished with a long Q & A session with questions on everything from installing Kubuntu to programming to using Wine to how to be a dude.  

<img src="http://farm4.static.flickr.com/3647/3344264222_e0aae02f1e.jpg" width="500" height="375" />
Ade and Jon marathon Q & A double act session.

The knowledge and experience of the audience varied widely from hardcore hackers to people completely new to the idea of free and open software.  So for the last day I did what I should have done on the first and gave introduction to KDE and Kubuntu.   I also gave a talk on Open Street Map which I expected to be a fun aside from the main free software topic but was picked up by everyone there as being a must-do project for Kano which currently has no maps at all (Google maps shows this city of 9 million people as a crossroads in the desert).  Our final Q & A double act session lasted even longer than on previous days but we got it all done in time for social events and hob-nobbing with politicians (a blog for another day).

<img src="http://farm4.static.flickr.com/3374/3343430497_8cb301e34e.jpg?v=0" width="500" height="375" />
Group photo

FOSS Nigeria happened and it was a big success.  This time last week plenty people were not expecting it to happen (certainly not for me since my visa had not arrived) but we were all wrong.  It might not have been on time and certainly not to schedule but there are now 500 eager free software developers in Nigeria and next year it'll happen again and there will be even more.

<img src="http://farm4.static.flickr.com/3558/3344265632_cc0f15b802.jpg" width="500" height="375" />
Mustapha, the dude who organised the conference (along with some other dudes).
<!--break-->
