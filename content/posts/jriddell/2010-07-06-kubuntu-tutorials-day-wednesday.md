---
title:   "Kubuntu Tutorials Day this Wednesday"
date:    2010-07-06
authors:
  - jriddell
slug:    kubuntu-tutorials-day-wednesday
---
Coming live from Akademy over IRC later today (Finnish today anyway) is <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a>.

Alan Alpert from Nokia will teach you about <a href="http://blog.qt.nokia.com/2010/02/15/meet-qt-quick/">Qt Quick and QML</a>.  You will need to download and install <a href="ftp://ftp.qt.nokia.com/qtcreator/snapshots/latest/">Qt Creator binaries</a> before the tutorial.  Qt Creator from the archive is not new enough.

Johan Thelin will give an intro to coding in Qt.  Make sure you have Qt development files and Qt Creator installed: <em>sudo apt-get install libqt4-dev qtcreator</em>

We also have sessions on <a href="https://launchpad.net/~kubuntu-bugs">Beastie Hunting</a>, <a href="https://wiki.kubuntu.org/Kubuntu/Ninjas">Packaging and Merging with the Ninjas</a> and Kubuntu Maverick.  Do join us on #kubuntu-devel from 18:00UTC on Wednesday.

<img src="https://wiki.kubuntu.org/KubuntuTutorialsDay?action=AttachFile&do=get&target=kubuntu-logo-lucid-tutorials-day.png" width="400" />
<!--break-->
