---
title:   "No Kubuntu Release Candidate Tonight"
date:    2005-10-07
authors:
  - jriddell
slug:    no-kubuntu-release-candidate-tonight
---
The Kubuntu Release Candidate won't be available tonight (unless you know the right IP address) due to problems with syncing with the mirrors.  Well done to ArkLinux for getting theirs out though.  Ma&ntilde;ana as we say in Malaga.
