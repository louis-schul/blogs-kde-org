---
title:   "Kubuntu: Best KDE distro of 2013"
date:    2013-09-16
authors:
  - jriddell
slug:    kubuntu-best-kde-distro-2013
---
The writer on <a href="http://netrunner-mag.com/?p=3184">Netrunner Mag did a review of the Best KDE Distro of 2013</a> and came to the following conclusion:

<i>
So beauty plus stability plus accessibility, the precarious triple-point equilibrium lands flat in the court of Kubuntu. This is quite an achievement, and the team behind their project should be really proud. Raring Ringtail is an awesome KDE demonstrator, and it tickles all the right glands. If you have friends and family you might want to expose to Linux, then Kubuntu will probably be the best overall choice.
</i>

<img src="http://netrunner-mag.com/wp-content/uploads/2013/09/kubuntu-ringtail.png" width="650" height="365" />
