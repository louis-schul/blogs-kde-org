---
title:   "Metatheme"
date:    2006-02-28
authors:
  - fab
slug:    metatheme
---
<p>I found this really nice project called <A href="http://www.metatheme.org/">Metatheme</A>. From their website:</p>

<p><em>&#034;MetaTheme is a project dedicated to unification of appearance between different graphics toolkits (currently GTK2, QT and Java).&#034;</em></p>

<p><A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot3.png"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/metatheme/scaled_snapshot3.png" border="0"></A></p>

<p><A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot6.png"><IMG class="showonplanet" src="http://www.xs4all.nl/~leintje/stuff/metatheme/scaled_snapshot6.png" border="0"></A></p>


<p>More screenies <A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot1.png">here</A>, <A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot2.png">here</A>, <A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot4.png">here</A>, <A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot5.png">here</A> and <A href="http://www.xs4all.nl/~leintje/stuff/metatheme/snapshot7.png">here</A> as well.</p>

<!--break-->