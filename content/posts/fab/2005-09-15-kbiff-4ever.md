---
title:   "KBiff 4ever"
date:    2005-09-15
authors:
  - fab
slug:    kbiff-4ever
---
<a href="http://www.kde-apps.org/content/show.php?content=28931">This nifty mail notification utility</a> has popped up at KDE-apps.org now! Kudos to Kurt Granroth for maintaining this cool piece of software!<!--break-->