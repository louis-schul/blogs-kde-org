---
title:   "Hi Gaëlle"
date:    2005-08-09
authors:
  - fab
slug:    hi-gaëlle
---
Cool dude and FOSDEM organizer <a href="http://dev-loki.blogspot.com/">Loki</a> has become a father. Congrats on your <a href="http://flickr.com/photos/dev-loki/30704251/in/set-686528/">little daughter</a> Pascal. See you at the next <a href="http://www.fosdem.org">FOSDEM event</a> in 2006!<!--break-->