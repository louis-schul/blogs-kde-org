---
title:   "Hackweek Results"
date:    2008-08-29
authors:
  - cornelius schumacher
slug:    hackweek-results
---
It's Friday now and <a href="http://blogs.kde.org/node/3643">hackweek</a> comes to an end for me. It was exceptionally fun and we got some <a href="http://websvn.kde.org/trunk/playground/base/attica/">decent work</a> done. The team was fabulous, a combination of SUSE and external community guys. <a href="http://blog.karlitschek.de/">Frank</a> was here for the whole week and worked on the API and the opendesktop.org implementation of it. <a href="https://blogs.kde.org/blog/2904">Sebastian</a> joined us for two days and we had a lot of interesting and useful discussion how to put <a href="http://nepomuk.kde.org/">Nepomuk</a> into the picture, <a href="http://wire.dattitu.de/">Dirk</a> started to write a Plasmoid for showing the activity log on the desktop, and <a href="http://zrusin.blogspot.com/">Zack</a> was here this morning giving us moral support and inspiration. We didn't completely realize the <a href="http://idea.opensuse.org/content/ideas/social-desktop">"Social Desktop"</a> yet, but we laid some groundwork. The most visible result currently is this screenshot:

<a href="http://2.bp.blogspot.com/_I0jbESd5Btw/SLfg06ydYjI/AAAAAAAAADA/RbQOsX9qaPA/s1600-h/attica_akonadi.png"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer;" src="http://2.bp.blogspot.com/_I0jbESd5Btw/SLfg06ydYjI/AAAAAAAAADA/RbQOsX9qaPA/s320/attica_akonadi.png" alt="" id="BLOGGER_PHOTO_ID_5239903891207578162" border="0" /></a>

It shows some people from <a href="http://opendesktop.org">opendesktop.org</a> on my desktop. The interesting bit is the technology behind it, because it uses the <a href="http://www.freedesktop.org/wiki/Specifications/open-collaboration-services">Open Collaboration Services API</a> to retrieve the person's data and feeds it into <a href="http://pim.kde.org/akonadi">Akonadi</a> which then is queried by the UI.

Obviously there still is a lot to do. The client library for accessing the Open Collaboration Services API has to be extended to cover more of the API, the Akonadi support has to be completed and then of course we need more and better user interfaces. One thing I would like to provide is something like a person widget, which can be used to easily integrate people into applications. It would give access to the community by providing links to related people or could be used for direct communication.

Another thing which I would love to see is an application to view and interact with groups of people, which is a bit more oriented at the people and not so concentrated on the data as current addressbooks are. This could provide a more natural and useful way to interact with all the data about people which is distributed over the desktop and make it easier to keep in touch with people and keep track of what's going on in the different groups you are involved with.

Finally it would be fantastic to solve the problem of multiple data sets of the same person. The more person data we are able to pull in the more frequent you get multiple entries of the same person in your addressbook. We need a way to merge these, so that there is only one entry per person containing the aggregated information. A possible way would be to use Nepomuk for this and there already is some <a href="http://websvn.kde.org/trunk/playground/base/nepomuk-kde/contactapi/">code</a> which could be used to achieve this goal.

I'm looking forward to do more hacking on this project. There are lots of interesting ways this could develop. Let's see where we can go with it...
<!--break-->
