---
title:   "Looking for a dream job?"
date:    2008-05-24
authors:
  - cornelius schumacher
slug:    looking-dream-job
---
At <a href="http://www.suse.com">SUSE</a> I work in the incubation team. We are exploring new technologies, creating prototypes of future systems, and trying to find and shape some of the features that will be part of upcoming SUSE products and the ecosystem around that. It's a fascinating job, challenging, fun, and always exciting. For somebody like me who loves to create new things and enjoys working with an awesome team of innovative people this is a dream job.

At the moment we are looking for some new team members. So if you are interested in joining a great team, working on technology from tomorrow, making Linux rock the world, see <a href="http://nat.org/blog/?p=828">Nat's blog</a> for more details. If you have questions please don't hesitate to <a href="mailto:schumacher@kde.org">contact me </a> by email or talk to me in person next week at <a href="http://www.linuxtag.org">LinuxTag</a>. I will be there from Wednesday to Saturday.
<!--break-->
