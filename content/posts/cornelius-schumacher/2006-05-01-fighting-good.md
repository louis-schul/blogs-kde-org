---
title:   "Fighting for the Good"
date:    2006-05-01
authors:
  - cornelius schumacher
slug:    fighting-good
---
Aaron took on his asbestos suite and <a href="http://aseigo.blogspot.com/2006/05/case-for-python.html">made a case for Python</a> as a VisualBasic replacement for the free desktop. Ok, let's give him some fire and play the "my language is better than yours" game.

Python is obviously the wrong choice. Ironically it's Aaron in his blog who eloquently tells us why this is the case. There are technical reasons, but there are also social reasons. If the creator of the language is perceived as a blocker to the future of the language there is obviously something wrong.

But in the end it's even not the weaknesses of Python (I actually like the language and have gladly used it in the past), but the strength of the competition, and the strongest competitor is Ruby. It's just amazing what Ruby did in the last couple of months. It's gaining popularity extremely fast. Tim O'Reilly told us end of last year that <a href="http://radar.oreilly.com/archives/2005/12/ruby_book_sales_surpass_python.html">Ruby book sales surpass Python</a>. Many people are adopting Ruby and this is for a reason.

I have used Ruby intensively during the last couple of months and I have to say it just feels right. KDE developers know that feeling. It's the same feeling you had when using Qt for the first time. It feels right, because when you look for something it's actually where you look first. It feels right because you frequently surprise yourself by writing code that works at once. It feels right because it's simple, elegant and still powerful.

You see what Ruby can do for example in <a href="http://www.rubyonrails.org/">Ruby on Rails</a>. Try it yourself. It's really fun to use and a big part of this is due to Rails making use of the dynamic nature of Ruby and putting it all into the right place. Other examples are the fantastic <a href="http://www.ruby-doc.org/stdlib/libdoc/test/unit/rdoc/classes/Test/Unit.html">test framework</a>, the extremely elegant <a href="http://rubyforge.org/projects/builder/">XML builder</a> (I once tried to build something similar in C++, but didn't came even close to the elegance of the Ruby solution) or <a href="http://developer.kde.org/language-bindings/ruby/index.html">Korundum</a>, the Ruby bindings for KDE. For a VisualBasic replacement Ruby has all the necessary ingredients. So as I already stated in a previous blog: The future is <a href="http://blogs.kde.org/node/1765">Ruby, Ruby, Ruby</a>.

But in the end fighting about programming languages usually is as much fun as it is pointless. Deciding which language to use by committee is not going to work. Why do we have this multitude of languages, why are applications written in many different languages peacefully coexisting on each and every desktop out there? Because it actually makes sense to use the tool which fits the problem and which fits the person using the tool. Problems are different, people are different, so let them use the programming languages they chose.

Of course there is a chance for every solution to become widely successfull and push away other solutions. It's hard to tell what it is which makes the difference between success and failure. If I would have to bet, I would put my money on <a href="http://www.ruby-lang.org">Ruby</a>.
<!--break-->
