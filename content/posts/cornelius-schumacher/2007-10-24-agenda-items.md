---
title:   "Agenda Items"
date:    2007-10-24
authors:
  - cornelius schumacher
slug:    agenda-items
---
I was surprised to get so much feedback about the KOrganizer agenda items after I posted the screenshots in my last blog entries. There seem to be some strong opinions about rounded corners. Michael Lentner did the right thing and sent a patch. I applied it and suddenly the agenda items look much more slick.

[image:3066 width="640" height="452" hspace=100]

There is even more space around the items, which seemed to be one of the things which disturbed some people, so I don't know how this change will be accepted. How do you like it?

It also doesn't seem to respect the resource colors anymore, but I'm not sure if that wasn't broken before already. The selection also has a slight problem as it isn't visible when there is no header displayed, but I guess that's fixable.
<!--break-->
