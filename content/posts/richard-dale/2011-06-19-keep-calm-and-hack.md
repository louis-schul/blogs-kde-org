---
title:   "Keep Calm and Hack On"
date:    2011-06-19
authors:
  - richard dale
slug:    keep-calm-and-hack
---
I've just got back from the Qt Contributor's Summit, and I had a really good time.

I arrived on Wednesday evening and we had arranged to meet in a bar called 'Brauhaus Lemke' in Hackescher Markt which is quite near Alexanderplatz. It did look easier to find on the map than it actually was, but Hackescher Markt is a great place. There is a big square with loads of bars that have seats outside. The Lemke is slightly off the main square.

When I got there we had about one and a half giant tables full of KDE people. I was really impressed with the beer menu; they had four brews which they make themselves. A 'Saison' which means seasonal and could be anything, a Pils, a Weiz beer and a very nice 'Lemke Original' which was amber coloured which an interesting depth of taste, and that was my personal favourite. You really need to go through all four at least twice I would say to get started, and you can get a 'test kit' with small samples of each to do that. They have vegetarian food, and I thought the Swiss Rusti - potato cakes with fried eggs on top - was particularly good.

So for the Desktop Summit I don't think you could go far wrong with arranging to meet people at one of the bars in Hackescher Markt. It's even got 'Hacke..' in the name after all..

The following morning we arrived at the Summit for a morning of keynotes. 

The conference was held in an East German 'retro modernist' building (if that is the right word). The rooms were named after space things like 'Vostok' or 'Mars' and there was even a Sputnick sculpture above a mural of 'herioc workers' type stuff at the front. My only criticism was that there were so many attendees that the rooms were packed and the air conditioning couldn't keep up and it got really hot.

After Alex Leisse introduced the conference, the first keynote was from the Nokia guy running the Open Source software program - I didn't take a note of his name. He reassured us that an Open Source plan does actually exist contrary to what you might think from the public reports. One major theme of the conference is that what is really happening in private isn't quite the same as the public image of Nokia although they couldn't tell us anything about what they called 'confidential things.'

Then Lars Knoll gave an account of the plans for Qt5 in detail. This is what I had mainly been looking forward to finding out about, and I was very impressed. They want to make the QML scene graph 3d the main focus of UI development although imperative QPainter based apis for drawing will still be there if you need them (I assume that is in the QtGui module). The biggest change was that QWidgets were being moved out of QtGui into their own module. I don't think the idea was to make them 'legacy only', but instead to optimize memory usage for small devices which don't need them. Other than those major changes most other things were just clean ups and tidying that would be reasonably source compatible.

The aim was to try and move as fast as possible, but not include too much in the 5.0 release so that either quality would be compromised or feature creep would cause delays. From the point of view of KDE we will need to just wait and see, and make sure we don't commit too early. We have plenty of other things to do at the moment, and I don't think we absolutely must switch when Qt 5.0 comes out, and instead just do enough research to have a thorough understanding of what it's about.

The first bindings guy I hadn't met before was Matti who runs the PySide  project. He told me he wasn't too technical, but he could relay any issues we came up with for bindings back to the guys in Brazil.

After the keynotes, we had lunch which was a box with sandwiches and stuff in it. Half the sandwiches and food were vegetarian throughout the conference, which was pretty good, and I didn't end up being distracted by feeling really hungry as I couldn't eat any of the food, like had happened once or twice at the recent UDS conference in Buderpest.

 The first discussion sessions started in the afternoon, and I was particulary interested in one about slots/signals changes in Qt5. I had read a blog on Qt Labs by Olivier Goffart about proposals for new slots/signals (I can't seem to find it via Google at the moment). The proposals looked as though they might be a bit 'boosty', and that they would screw up language bindings projects by changing to entirely statically typed signals/slots. It turned out that wasn't the plan, and in fact the new functor style slots weren't replacements for Qt's QMetaObject based slots, but were in addition to them. Attaching a C++ lamda directly to a signal looked pretty neat, and pretty similar to what we've done with QtRuby with invoking ruby blocks attached to signals.

In the Thursday evening we had a party and listened to the Troll's house band who did some good covers of sometimes 'dodgy' original material. I probably liked Pierre's trumpet riffs best - they were appropriate,  to the point and not overdone. Similar to how I like code I suppose. We had a robot dance from the legendary Knut Yrvin along with some lessons on how to do his basic moves - only feasible for normal humans after quite a few Becks I would say.

On the Friday there was a second QMetaObject session about dynamically generating them. There were some minor issues about how to handle the function that is used by qt_static_metacall(), but mainly it seems pretty good from a bindings point of view and I don't think we're going to need to drastically redesign the slots/signals stuff and it will carry on working much the same.

There was this guy there who talked about Python in the QMetaObjects session, but he had his name badge reversed so I couldn't actually confirm who I suspected it was. After the session I introduced myself and found out that he was indeed Phil Thompson of PyQt fame. I had wanted to meet Phil for a while - every now and then he posts a helpful mail to the kde bindings mailing list and I had been impressed with the PyQt code when I studied it.

We went downstairs outside and had lunch outside sitting on benches in the sunshine which was very pleasant. We had a long chat about bindings issues and agreed that Qt4 was mainly fine from the point of view of bindings and Qt5 looked like it was carrying on being much the same. 

A common issue was what on earth to do about QML integration. We have some problems about how to do custom QML types in bindings languages, but far more important is the effect of mixing two dynamic languages with different syntaxes in the same application. Phil's approach was to provide basic support for QML for those who wanted to use it, but he is working on a very ambitious sounding Python based declarative language that would be used instead of QML, not as well as QML. I had thought about that for Ruby, but decided that it wasn't really option. So we'll have to see how it turns out for PyQt.

On the Friday evening I went back to the Lemke bar for some more 'research' and was pleased to find there were a pile of people from the conference already there.

We finished up on Saturday with more follow on session mainly to expand on what had already been discussed. I had a language bindings session and we went over a few things especially QMetaObject stuff again with Olivier.

So that was it. I have been impressed with what I've learned about Qt5, and think that Berlin is a great place and am looking forward to the Desktop Summit. I really, really must get my flight and hotel booked this week.

Oh, one last thing. Why is this blog called "Keep Calm and Hack On"? It is because we were all given t-shirts with that phrase in big green letters with a pair of crossed swords at the top. Are we all the 'Right Stuff' in other words. I think we will be..