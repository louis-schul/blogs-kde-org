---
title:   "Software Libre in La Laguna"
date:    2007-09-21
authors:
  - richard dale
slug:    software-libre-la-laguna
---
<p>This week I've been at the Jornadas de Software Libre conference in Tenerife. One thing that struck me was that the Spanish don't have a word for 'Open Source', which would be something like 'Codigo abierto'; they always use the term 'Software Libre'. The conference program even had the four freedoms of Free Software written on the back, along with an explanation.</p>

<p>I personally think that we should try and stop using the term Open Source to describe KDE, as it just sounds like it is a slightly tweaked business model, and completely leaves out the community aspects of the project.</p>

<p>What is the difference between Microsoft's 'Shared Source' and 'Open Source'? I don't know and I don't care. What is the difference between 'Shared Source' and 'Software Libre'? Well, that one is obvious because Software Libre is a social movement, and Shared Source is only about letting you read the software source code. Just reading the source code doesn't mean a lot without those four freedoms, because they form the foundations of communities like KDE by providing a common ethic to allow people to work together.</p>

<p>Normally I find Wade Olson's blogs a very good read, and he often writes insightful and interesting comments about the KDE community, but this one where he shows a design for a book, <a href="http://wadejolson.wordpress.com/2007/09/19/what-will-kde-40-be/">Marketing Open Source Software for Dummies</a>, hit a bum note with me. I wish we had re-branded 'Free Software' as 'Software Libre' as it removes the ambiguity between 'free as in beer' and 'free as in freedom' that we have in english, and also emphasizes the international nature of the Free Software community.</p>

<p>In my opinion the <a href="http://www.opensource.org/">Open Source Initiative</a> has been a bit of a disaster because they have encouraged an unnecessary proliferation of incompatible 'Open Source' licences, while at the same time making it harder to explain to people that Free Software is about freedom and building communties, and not just 'cheapest is best'.</p>

<p>In contrast to Spain, the UK is falling further and further behind because the very right wing neoliberal New Labour government completely and utterly fails to get the point. This <a href="http://www.guardian.co.uk/technology/2007/sep/20/guardianweeklytechnologysection.comment1">recent article</a> in the Guardian was pretty depressing:</p>

<p><i>You would have thought that a Labour government, struggling to marry the success of market forces with the socialist endowment of its founding fathers, would have latched on to this new cooperativism which brings people together for a common purpose with a burning zeal. In fact, its wanton neglect could damage our economic prospects.</i></p>

<p><i>The depth of its neglect was made plain by speakers at a seminar last week hosted by Westminster eForum, which tries to make parliament aware of IT issues. It turns out, in contrast to what other governments are doing, that most departments - including Health, Work and the Foreign Office - are so risk-averse they have virtually no open source in their IT infrastructures...</i></p>

<p>Quite.</p>

-- Senor Dale Cervesa

