---
title:   "Is client side Custom Application development dead?"
date:    2004-09-18
authors:
  - richard dale
slug:    client-side-custom-application-development-dead
---
I recently had this email exchange with my friend Geoff. I don't need to add any further commentary, but I've personally bet the farm on custom application development. Hmm.. He's what we said anyway:

<i>Hows things?  I'm in Luxembourg with Finn doing banking b****x for big bucks.
Bit dull here.</i>

I replied:
<p>I went to the KDE developers conference in Ludwigsburg the other week and that 
was really good - I wouldn't mind working in Germany. But it seems that KDE 
custom development is always about 6 months away from taking off, I didn't 
sense there's much in the way of contract work yet.

At least you're earning big bucks - I'm totally broke. But I'm finishing off 
the ruby project, and it's certainly the most awsome RAD environment that's 
ever existed. Not that anyone much knows about it..</p>
<p>
Geoff:
<i>It seems to me that, in professional money making development, that
the client OS is increasingly irrelevant. All work I do is now web
based, not that I know **anything** about Java/HTML. All Oracle and
Busines Objects front ends are now web based. The thin client that was
going to be the big thing a few years ago is coming true. Businesses
like the idea  of no PC (whatever OS you run on it) S/W to maintain,
manage, change versions etc. and like the central control. I do not
see there ever being that much KDE developemnt work for building
custom applications for individual firms. Maybe, like you got the
Objective C work, there will be some visionaries at big banks who
really need the speed for dealing and would choose front end
Linux/KDE. Obviously there are some people writing all this for fun
who are in positions of power. Linux at the server end is booming
though.

You probably disagree with this analysis but this is what I see on
Jobserve and where I go to work.

I hope I'm at least a bit wrong for your sake.</i>
<p>
Richard:
<p>No, I don't disagree. Any desktop app must be network aware, and any client 
apis that assume a standalone PC are dead. That includes Microsoft's win32 
apis, they are irrelevant - MS never understood the internet. Also, they 
don't understand the difference between a systems programming language 'C#' 
and a RAD language like ruby. The KDE desktop is very network aware; you can 
customise the KDE::HTMLPart very easily - see the ruby tutorial in the link I 
sent.

But I still feel unemployable for the reasons you give, which is pissing me 
off. All I need is one bank who isn't brain dead and wants to employ clever 
people, as opposed to code monkeys, and I'm in business again..</p>




