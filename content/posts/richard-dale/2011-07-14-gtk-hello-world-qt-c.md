---
title:   "Gtk Hello World in Qt C++"
date:    2011-07-14
authors:
  - richard dale
slug:    gtk-hello-world-qt-c
---
<p>Recently I've been working on the smoke-gobject bindings in the evenings and weekends. Although I'm working on other things for my job at Codethink during the day, I'm sufficiently excited about these bindings to be unable to stop spending my free time on them. This is at the expense of working on the new version 3.0 of QtRuby sadly. I'll try to explain on this blog why I think the Smoke/GObject bindings will be important for the parties attending the forthcoming Desktop Summit to consider, and why I'm giving them a higher priority than Ruby.</p>

<p>As the title of the blog suggests I've just got a Gtk Hello World working, written in Qt. Here is the code:</p>

<p><pre><code>
#include &lt;QtCore/QObject&gt;
#include &lt;gdk.h&gt;

class MyObject : public QObject {
    Q_OBJECT
public:
    MyObject(QObject *parent = 0);
public slots:
    void hello();
    bool deleteEvent(Gdk::Event e);
    void destroy();
};
...

#include &lt;QtCore/qdebug.h&gt;
#include &lt;gtk.h&gt;

#include "myobject.h"

MyObject::MyObject(QObject *parent) : QObject(parent)
{
}

void MyObject::hello()
{
    qDebug() &lt;&lt; "Hello World";
}

bool MyObject::deleteEvent(Gdk::Event e)
{
    qDebug() &lt;&lt; "delete event occurred";
    return true;
}

void MyObject::destroy()
{
    Gtk::mainQuit();
}
...

#include &lt;gtk.h&gt;
#include &lt;gtk_window.h&gt;
#include &lt;gtk_button.h&gt;

#include &lt;QtCore/qobject.h&gt;

#include "myobject.h"

int main(int argc, char *argv[])
{
    Gtk::init(argc, argv);

    MyObject obj;

    Gtk::Window *window = new Gtk::Window(Gtk::WindowTypeToplevel);
    window-&gt;setBorderWidth(10);
    QObject::connect(window, SIGNAL(deleteEvent(Gdk::Event)),
                     &obj, SLOT(deleteEvent(Gdk::Event)));
    QObject::connect(window, SIGNAL(destroy()), &obj, SLOT(destroy()));

    Gtk::Button *button = Gtk::Button::createWithLabel("Hello World");
    QObject::connect(button, SIGNAL(clicked()), &obj, SLOT(hello()));
    QObject::connect(button, SIGNAL(clicked()), window, SLOT(destroy()));

    window-&gt;slotAdd(button);
    button-&gt;slotShow();
    window-&gt;slotShow();

    Gtk::main();

    return 0;
}
</code></pre></p>

<p>You can compare the Gtk C equivalent example code in this <a href="http://developer.gnome.org/gtk-tutorial/2.90/c39.html#SEC-HELLOWORLD">Getting Started</a> Gtk tutorial. I could spend lots of space explaining what it is all about, but anyone familiar with Qt programming ought to be able to understand the code without any explanation - that is the clever thing in fact!</p>

<p>This demonstrates that an auto-generated binding for GObject libraries described by GObject Introspection .gir files can seamlessly inter-operate with Qt libraries. And it looks much the same as a native Qt library to a Qt programmer.</p>

<p>Normally a language binding for Gtk that allows you to write an app to show a hello world button is a fine thing. But those normal language bindings don't mean that you have united two of the major Linux desktop communities, they just mean that you can write Gtk programs in the language of your choice. So to me, this latest binding is different in kind; it is an opportunity to integrate both libraries and communities, and it isn't limited to being able to program Gtk in your latest nifty language.</p>

<p>I'm going to the <a href="https://launchpad.net/smoke-gobject>GObject Introspection Hackfest</a> in Berlin, which should allow us to nail down the technical details and get close to bringing the Smoke Gobject bindings up to production quality. It will also be about the social side of linking the two communities in conjunction with the technical discussions.</p>

<p>The Smoke GObject apis are described with QMetaObjects, and that means that the apis will 'just work' with QML. I am looking forward to trying out Clutter programming in QML soon, as I believe that should be possible. That shows that these bindings are not just about C++ programming, but languages like QML and QtScript that are driven by QMetaObject introspection too.</p>

<p>You can get the latest version of the gobject-smoke bindings from <a href="https://launchpad.net/smoke-gobject">the Smoke GObject Launchpad project</a>. See the README file for an explanation of what to do with them.</a></p>