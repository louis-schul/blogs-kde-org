---
title:   "GSOC 2009 Progress - Smoke Bindings Generator"
date:    2009-07-10
authors:
  - richard dale
slug:    gsoc-2009-progress-smoke-bindings-generator
---
<p>Yesterday I wore my GSOC tee-shirt at GCDS and got together with Arno Rehn to review his smoke bindings library generator tool. It turns out the project is going great and is pretty much finished.</p>

<p>I can't say I've done a lot of mentoring other than to have an initial discussion about which variant of Roberto Raggi's C++ parsers to use. We chose to use the KDevelop one with some KDE dependencies removed, although in fact from Roberto's comments on <a href="http://arnorehn.de/cgi-bin/weblog_basic/index.php?p=3>Arno's blog</a> it might have been best to use the Qt Creator one.</p>

<p>To test the generator, first I dumped off all methods in the current version of the smoke library for the Qt libs using the 'rbqtapi' tool that comes with QtRuby. Then we built the same lib with the new smoke generator, and dumped off the methods in that into another text file and compared them with Kompare. There we a few minor things wrong like constructors being marked as 'static' methods, but nearly always the new compiler was giving better results than the previous Perl version.</p>

<p>This is really good news for language bindings, as for KDE 4.4 we will be able to install the smoke generator as a binary, put up some instructions on TechBase explaining what to do in order to create a Smoke library for your KDE app or library. Mainly you just need to create a list of the C++ classes you want to wrap in an XML file, give it the headers to parse, and you're done. I've already got a script to create a Smoke lib based Ruby extension to use with QtRuby/Korundum, and we can automate that part too. We will be able to move individual bindings out of the kdebindings module and put them in with the app or lib that they wrap. Then we can just have the bindings tools and the core Qt and KDE libs stuff in kdebindings itself.</p>

<p>So congratulations to Arno for doing such a great job, and also delivering one month ahead of schedule (my summary comment on the Google mid-term review was 'Absolutely excellent!').</p>