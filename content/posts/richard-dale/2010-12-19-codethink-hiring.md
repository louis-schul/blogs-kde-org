---
title:   "Codethink is hiring!"
date:    2010-12-19
authors:
  - richard dale
slug:    codethink-hiring
---
<p>Alberto Ruiz asked me to post a message on Planet KDE about <a href="http://aruiz.synaptia.net/siliconisland/2010/12/codethink-is-hiring.html">Codethink is hiring!</a> He says:</p>

<p><i>"Codethink is currently looking for bright university graduates looking into joining a young open source company. As you may know already Codethink is an Open Source consultancy focused on helping our customers to make the most out of open source and create great innovative products with it.</i></p>

<p><i>The work environment is really flexible and we have a great team of hackers. Our projects cover the whole desktop, mobile and embedded Linux stack. We are particularly looking for people with some involvement in open source (few contributions GSoC...) and an interest in getting involved in low level kernel and graphics development, but if you are focused on other areas that you think might be interesting don’t hesitate "</i></p>

<p>I actually work for 'Codethink Spain' and I'm not based in Manchester. But I can confirm that the company does have some serious hackers working for them, and that being 'Qt/KDE centric' or 'GTK/Gnome centric' isn't much of an issue. It is a great time to be an embedded mobile programmer and Codethink have the infrastructure and contacts for getting lots of really interesting work. We wear some nice green tee-shirts, but in view of the extreme cold and snow at the moment in Manchester I'm sure they might be able to provide the right candidate with an attractive green coat suitable for wearing while shovelling eight inches of snow..</p>