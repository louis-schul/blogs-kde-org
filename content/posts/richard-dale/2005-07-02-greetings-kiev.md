---
title:   "Greetings from Kiev"
date:    2005-07-02
authors:
  - richard dale
slug:    greetings-kiev
---
Alex Dymo has organised a KDevelop Developers conference, and I'm here in Kiev for a week with other KDevelop hackers. 
<!--break-->
We gave a full day of presentations yesterday about all aspects of KDevelop and our future plans. I gave my second public talk ever, about ruby support and the QtRuby/Korundum bindings. It didn't go too badly apart from my iBook not working with the projector, and everything crashing in the middle of demoing the KDevelop ruby debugger. Oh well. I mainly need to get the hang of KPresenter - I wasn't as nervous as in my first talk even if I'm not exactly slick yet. Anyway, it was great go out to another part of the world to encourage Free Software. The audience were interested in what we had to say, and asked some good questions. At the end we took lots of photos of everyone behind the conference banner.

We had an tequila drinking session last night round Alex's place, and are now in a position to compare and contrast tequila with the effects of vodka from the previous night. There was something about the fine and dangerous tequila that Ian brought with him that fired up a good discussion. Curiously they don't have a word for 'sip' here, they like to down their drinks in one, and we needed to teach Alex how to 'sip'. Early this morning Alex's mother and a couple of her friends came round and cooked a great breakfast of Ukranian wonton type things; packets made of pancakes with a potato filling, and then more of them stuffed with cherries. Nice, and they've cooked a Borscht soup and other stuff for us to try tonight.

I like Kiev - there aren't many tourists and parts of it are a bit rundown, with the locks and water supply in Alex's flat being a bit 'iffy'. They have an excellent public transport system, policeman and soldiers wear some great 'big hats' - very Russian looking, and I'm afraid to admit I've got a weakness for the seriously pointy stilleto shoes that seem to be in fashion.

At the moment, I'm hacking on the ruby bison grammar trying to use it as a basis for improving the KDevelop class browser, and maybe enable stuff like refactoring and unit test generation. The current KDevelop ruby parser is a bit simplistic with just regular expression matching, so the intention is to go way beyond that. Roberto Raggi has designed a recursive descent parser generator for KDevelop, so I might use that instead, but I'll get it working with bison first.