---
title:   "Outlaw"
date:    2005-11-11
authors:
  - richard dale
slug:    outlaw
---
I've just spent my first week working at Foton Sistemas Inteligentes in Gran Canaria. I've been learning some Spanish, trying out Ruby on Rails and helping with translating a tourist information site to English. This weekend there is a WOMAD festival in Las Palmas, and I went there last night to see Kanda Bongo Man from the Congo. Wow! Soukous is about my favourite dance music, and being able to just hop on a bus and see that kind of gig for free was amazing. It feels like permanently being on holiday here; the sun shines, people are friendly and you have fun. Yet people work hard and still get lots of stuff done.
<!--break-->
What a contrast to my life in the UK recently. I was quite happy hacking on QtRuby/Korundum until I seriously began to run out of money in the past few months. As I'm 49 now, there is just no point in sending my CV to any sort of normal recruitment consultant as they would just throw it in the bin. Instead, I just had to keep a steady nerve, carry on doing good stuff and hope something would turn up. Which indeed it did - phew!

Over 5 years ago I decided I didn't want to carry on being a 'normal contractor' anymore, working on Fixed Income trading systems as I had been. In March 2000 I discovered the full details of a new tax called 'IR35', which was essentially designed to put contractors out of business and force them to get 'proper permanent jobs'. For some reason the utterly weird New Labour government and the Inland Revenue decided that the only reason someone would become a contractor was as a tax fiddle. 

I had set up 'Lost Highway Ltd' in 1996 with the intention of accumulating enough money in the company, via contracting, to fund some interesting development project. Imagine my amazement and shock when I discovered that this daft new tax would ban contractors' companies from accumulating capital. So you end up with all the hassle of running a company with accountants, complex taxes and so on, but have to pay out 95% of turnover as a personal salary. Anyone who has actually run a company, knows that a limit of 5% on expenses, and no capital to accumulate is just doomed to fail.

What was I to do? I didn't want to be forced to have a permanent job. Nor did I want to give up computing programming altogether as it's about the only thing I'm actually any good at. So I came up with the idea of a new sort of 'strike'. I would withdraw my labour from the UK paid employment market and work flat out on Free Software instead. That way I would minimise the tax I paid to the Revenue, while at the same time keeping my skills at the cutting edge. I've actually managed to get by with only doing 7 months of paid work in that 5 years, which I think is pretty remarkable. I had to go back working for a bank in 2002 to fund the QtRuby development, but I really felt past my sell by date there and didn't enjoy it much. They replaced me with 3 cheaper Indian guys, and got rid of nearly all the other UK contractors. It confirmed my worst fears about staying as a 'commodity contractor', where you have to rely on recruitment by numbers according to your 'skills profile' - that sort of work is a dead end.

I met Augustine from Tenerife at the Malaga conference, who I got on well with, and he told the Foton guys I was there. They already knew about me, as several of them are ruby fans, and they had tried QtRuby. So when they heard I was interested in working in Spain (anywhere outside the UK in fact), they google'd for my name, looked at the stuff I'd done, and getting the job became a 'slam dunk'. 

So there we have it. Work on Free Software, write blogs, check code into public repositories, give talks at conferences, generally use the internet to showcase your work and people will want to employ you. The younger you are when you start doing that, the more solid your career will be. Never get employment via the CV/recruitment consultant route - that was just so 20th century - only losers do that..

Adios mes amigos
Senor Dale Cerveza