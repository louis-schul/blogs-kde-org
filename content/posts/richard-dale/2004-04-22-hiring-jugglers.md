---
title:   "Hiring jugglers"
date:    2004-04-22
authors:
  - richard dale
slug:    hiring-jugglers
---
Here's how I learnt that every programmer should have their own software portfolio from one of my favourite books about professional programmers 'Peopleware' by Tom DeMarco and Timothy Lister.

From HIRING A JUGGLER, Chapter 15 of Peopleware:
 
Circus Manager: How long have you been juggling?
Candidate: Oh about six years

Manager: Can you handle three balls, four balls, and five balls?
Candidate: Yes, yes and yes

Manager: Do you work with flaming objects?
Candidate: Sure.

Manager: ...knives, axes, open cigar boxes, floppy hats?
Candidate: I can juggle anything

Manager: Do you have a line in funny patter that goes with your juggling?
Candidate: It's hilarious

Manager: Well that sounds fine. I guess you're hired.
Candidate: Umm... Don't you want to see me juggle?
Manager: Gee, I never thought of that.

"It would be ludicrous to think of hiring a juggler without first seeing him perform. That's just common sense. Yet when you set out to hire an engineer or a designer or a programmer or a group manager, the rules of common sense are often suspended. You don't ask to see a design or a program or anything. In fact, the interview is just talk"

They then go on to explain how a Canadian CS professor had taught his students to build up a portfolio of samples of their work. Apparently the recruiters were invariably surprised and impressed by the portfolios, and as a result interviewers would converge on this obscure Canadian campus from all over the US.

That was 15 or 20 years ago, and of course nobody needs to bring a portfolio with them today if they have a FOSS project and its associated mailing lists etc out on Sourceforge. If you don't have your own project, all you can do is talk about the closed source projects you worked on for former employers, just like the juggler conversation above. Have recruiters reacted to this new reality? As far as I can see, no they haven't. It's very much a 'hire by numbers' approach, and its getting worse if anything. "We need 3 years J2EE, EJB, CORBA, MSCE pass with distinction blah, blah". And any candidate with those letters on the CV is much the same as any other of course, and one with even more letters is better.