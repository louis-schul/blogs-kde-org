---
title:   "Tom Ball on 'Is Writing Code a Career Limiting Move?'"
date:    2006-08-20
authors:
  - richard dale
slug:    tom-ball-writing-code-career-limiting-move
---
</p>
I found this blog entry on <a href="http://weblogs.java.net/blog/tball/archive/2006/08/is_writing_code.html">coding as a career limiting move</a> interesting, how could being really good at writing code possibly be a 'career limiting move'? I've been a professional programmer for a very long time, and I've come across very, very few people who are brilliant at writing code - maybe a handful before I came across the KDE project where they seem to be all over the place. So how come the Java community thinks you can separate 'architects' who don't code from the lowly coders that the architects tell what to do?
</p>
<!--break-->
<p>
The answer is you can't and the blog quotes Dave Thomas: <i>I've been reading a great book from the Pragmatic Programmer group called Practices of an Agile Programmer. One of its chapters is titled "Architects Must Write Code", which includes this gem: "Real insight comes from active coding."</i>
</p>
<p>
Someone replying to the blog write this:
</p>
<i>
I think I’m suffering from something similar at the moment. I’ve had my head down for the past six years in a small software house as a senior Java hacker. But one sad day we were told the companies going into administration and we got given our notices Since then we’ve spend every day for the past few weeks pouring over the job sites and todays java job wanted ads. It seems to me that most of the Senior Java Programmer positions are asking for only 2 years experience, I’ve even seen some that required as little as ones years exposure to Java to be eligible for a senior role. Other ads will explicitly state they’re after 2-5years of experience and the majority will max out at around £35K (some a bit more with niche skills). With a solid 9 years of Java now under my belt I’m asking myself where do I fit? What comes after a Senior Java Programmer? The job ads suggest nothing. I seem to have wandered into a no-mans land. While you’ll see the odd system architect role they’ll often come in around £50k-£60k, but a good many of them seem to be farming out UML and specs to foreign outsourcing teams, most definitely not hands-on or much fun.

As they’res several of us in the same boat, I find we’re often chasing the same jobs, talking to the same recruitment agents within minutes of each other – but despite having very similar skills it’s the younger team members getting all the calls, 2-3 a day whereas I’m lucky to receive the same in a week. When we put our CV’s forward to the same job, for the same money, at the same time, time and again it’ll be my younger colleague who’ll secure the interview while I just get the cold-shoulder. (No I don’t think it’s the CV six years ago it’d get me all the calls, and I’ve gotten feedback over it everybody thinks its fine). I’ve even been turned down for a job I was perfect for on paper, I didn’t even merit an interview because I was told they cap their experience level at five years max, despite being willing to accept the advertised salary (it wasn’t much less than I was getting already).

So I don’t understand where I’m supposed to go. I’ve tried team leading / management and hated every moment of it. Like most hardcore developers I don’t have the social skills and lack the self-confidence. Do you strengthen your weaknesses or play to your strengths? My passion is for coding not the death by a thousand cuts managing seems to entail. There’s far fewer management jobs than coding ones anyway, where are all the old programmers at? Is there some twilight care home somewhere out there? All comfy slippers, horlicks and sharing fond memories of AmigaOS.. sigh!
</i>
<p>
What can I say? - Java programming is a great career move in the short term, but terrible in the long term. The Ruby community just doesn't just think this way - they don't care about job titles, they don't care about the ease of recruiting commodity programmers, they think programming is a hard skill, and there is a big difference between an ordinary programmer and a 'great' programmer. So programming languages don't just differ between their syntaxes or whether they are statically or dynamically typed, there are big cultural differences that have a large effect on the sort of person that wants to enter the community. I noticed this when I was a NeXT Objective-C programmer, it attracted all the best people just like the KDE project does. So what's the best way to ensure you don't get ignored by clueless recruiters who don't want anyone 'too experienced'?
</p>