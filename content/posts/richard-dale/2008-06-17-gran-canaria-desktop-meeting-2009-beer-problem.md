---
title:   "Gran Canaria Desktop Meeting 2009 - the Beer Problem"
date:    2008-06-17
authors:
  - richard dale
slug:    gran-canaria-desktop-meeting-2009-beer-problem
---
<p>For the past few months we've been working on getting our bid to host GUADEC and Akademy in Gran Canaria for 2009. <a href="http://toscalix.blogspot.com/2008/06/gran-canaria-desktop-meeting.html">
Agustin</a> has done an amazing job in pulling it all together, and <a href="http://aruiz.typepad.com/siliconisland/2008/06/gran-canaria-ca.html">Alberto</a> has been relaying his enthusiasm about the idea of co-located conferences to the Gnome guys.</p>

<p>So OK the venue has perfect weather, plenty of cheap air flights to get there, the Alfredo Kraus Auditorium is one of the most spectacular buildings I've ever seen (as much as a sculpture as a building), there are loads of people here with great expertise in putting on Software Libre conferences, and on top of that the government is keen to help organize and fund it all. So what could possibly be wrong? Our ever sharp dutch/canadian beer drinking friend <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/595-Catch-more-with-beaches-than-with-fog.html">Adriaan</a> has identified a potentially serious problem:</p>

<i>Why yes, I have considered hacking in the Canary islands, but it fails some essential tests. You see, the islands are pleasant. There are beaches. There is sun. You can have fun and be productive at the same time.
 
 Everyone knows that this is patently impossible and therefore such a place cannot exist. Since non-existent places have <b>no beer</b> or internet, I will not go somewhere nice to hack. Heck no.</i>

<p>Yes, he correctly points out the <b>no beer</b> problem because we will be in Spain, and the Spanish sadly have no interest in beer. The main beer here is called 'Tropical' and it is perfectly pleasant refreshing cold lager, and I would say that it is a bit better than San Miguel at least.</p>

<p>What if you get fed up after a couple of days on that cold fizzy stuff? I would just like to reassure people that it is possible to get decent beer in Las Palmas, but you do have to know where to look.</p>

<p>You can start at the Alfredo Kraus Auditorium, and right next to it in the Plaza de la Musica, is a bar that serves Czech Pilsner Urquel, which is very pleasant to sup while admiring the concert hall. After a couple of those you can walk along the Las Canteras beach a few hundred meters to the La Guarida, which serves very fine bottles of Paulaner wheat beer, and generally have some interesting music playing.</p>

<p>Walk on to the Santa Catalina square and look for the Bar Lola, buy a newspaper, and then order a Erdinger Hefe Weisbeer, staying on the wheatbeer theme. Finally, you can catch a bus towards the old part of Las Palmas and find the Bar Metro, which is right opposite the Canarian government buildings. They serve 50 types of beer and I think you will find no need to move out of there for the rest of the evening.</p>

<p>So yes, there is a beer problem here, but it is perfectly tractable. If we are capable of reinventing the Linux Desktop, I feel getting drunk on beer in GC should be well within our grasp.</p>

Señor Dale Cerveza