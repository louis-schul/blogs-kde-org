---
title:   "Adding a Ruby Interpreter menu to Konsole"
date:    2006-05-17
authors:
  - richard dale
slug:    adding-ruby-interpreter-menu-konsole
---
<p>
I've recently added a 'Ruby Interpreter' menu to start an irb shell in Konsole. Just create a file called ruby.desktop in /usr/share/apps/konsole with the following contents:
</p>
<code>
[Desktop Entry]
Type=KonsoleApplication
Name=Ruby Interpreter
Comment=Ruby
Exec=/usr/bin/irb
</code>
<p>
And that's it! Your Konsole will then have menu entry under 'Session' called Ruby.
</p>