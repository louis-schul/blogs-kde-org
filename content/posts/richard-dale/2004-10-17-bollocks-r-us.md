---
title:   "Bollocks R Us"
date:    2004-10-17
authors:
  - richard dale
slug:    bollocks-r-us
---
I love interesting articles about the implications of global collaboration via the Internet, and what effect it will have on working patterns. So when I saw this article <a href="http://today.java.net/pub/a/today/2004/09/09/blacksmith.html"> The Blacksmith and the Bookkeeper</a> I thought it would be just up my street. But no! It's just a pile of utterly pretentious rubbish. I loved this <a href="http://it.slashdot.org/comments.pl?sid=125915&threshold=1&commentsort=0&tid=156&tid=8&mode=thread&cid=10547219"> destruction job</a> on Slashdot.
<!--break-->
<p>
On the other hand, the best article I've read recently is <a href="http://alistair.cockburn.us/crystal/articles/cpanfocisd/characterizingpeopleasnonlinear.html"> Characterizing People as Non-Linear, First-Order Components in Software Development</a>. With a title like that you might think it's a load of pretentious rubbish too. But in fact I think it's spot on, and a very interesting read.

For whatever reason I spend time thinking about what managers do, and whether they are needed on Free Software projects. So this paragraph tantalised me:
<p>
<i>
There was a small-goods and buttons shop nearby that was always in a terrible shape.  The place was a mess, and the girls were either doing their nails or on the phone, and didn't have much time for the customers.  That business closed, and another small-goods and buttons shop opened in its place.  This place was wonderful!  It was clean, tidy, and the girls were attentive to their customers.  The only thing was ... it was the same two girls!
</i>
<p>
What was it that the management did here? I'd just love to know..

-- Richard
