---
title:   "nepomuk.kde.org online"
date:    2008-02-18
authors:
  - trueg
slug:    nepomukkdeorg-online
---
I am proud to announce that finally <a href="http://nepomuk.kde.org">nepomuk.kde.org</a> went online. A owe a big thank you to pinheiro who not only designed the new Nepomuk icon but also did the webpage layout. I also want to thank Luke Parry who adapted pinheiros design for Drupal. Last but not least Dirk Mueller went through the trouble to actually setup Drupal.

So please check it out and especially take a look at the beefed up documentation section: I wrote a bunch of <a href="http://techbase.kde.org/index.php?title=Development/Tutorials#Nepomuk">new tutorials for techbase</a>.