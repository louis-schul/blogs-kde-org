---
title:   "Generic and nice-looking ratings all over KDE (wouldn't that be nice)"
date:    2008-03-03
authors:
  - trueg
slug:    generic-and-nice-looking-ratings-all-over-kde-wouldnt-be-nice
---
I just commited the finalized KRatingPainter to svn trunk which allows to paint a rating value using any QPainter. I think it is quite nice since it allows to specify the alignment, a spacing, a custom icon, the maximum rating, a hover rating, and so forth. And I think it would be great if this class (and its easy-usage widget companion KRatingWidget) would be used throughout KDE whenever we want to display a rating value. Although it is part of the Nepomuk lib at the moment, it has no real dependancy here: the rating is a simple integer value.

A little test application shows the rating widget in action:

[image:3306 size=preview]