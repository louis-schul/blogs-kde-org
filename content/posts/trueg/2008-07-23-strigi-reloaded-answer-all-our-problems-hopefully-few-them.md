---
title:   "Strigi Reloaded - The Answer to all our Problems? Hopefully to a few of them."
date:    2008-07-23
authors:
  - trueg
slug:    strigi-reloaded-answer-all-our-problems-hopefully-few-them
---
It took me one and a half day and Jos will not be happy about it. That is because I have to start this blog entry with apologizing to him:

<i>"Jos, I am sorry, you will probably not like what I am about to present here. But this makes it so much easier for me and all the KDE people. And strigidaemon simply does not provide the needed features, which I can understand since you are doing this in your spare time. But I cannot wait any longer and in the end really want to reuse all the nice KDE features instead of reimplementing it all just to keep away from QT/KDE dependencies. I hope you understand."</i>
<!--break-->
Now that the tension is built up. What did this guy do? Well, essentially I reimplemented strigidaemon as a KDE Nepomuk service. Why would I do that? Why would I reimplement an existing working application? Simple. For the following reasons:

<ul>
<li>The parts that I copied from strigidaemon are rather small since all the work is done in the streamanalyser library. So "reimplementing" is maybe a bit overstating it.</li>
<li>Managing strigidaemon is not that easy as there is no proper method to suspend/resume indexing. You will see below why that is important.</li>
<li>strigidaemon does not inform about what it is doing. Thus, having an information GUI is impossible.</li>
<li>The new service is of course a Nepomuk service and as such, can make use of all our nice Qt/KDE features:
<ul>
<li>It uses KDirWatch to watch all indexed directories for change. In comparision the inotify/fam support in strigi was never completed and also meant to maintain 2 dirwatch implementation: one in KDE and one in Strigi.</li>
<li>It uses Solid to get notified about power state changes - indexing is suspended when your laptop is running on batteries.</li>
<li>It regularly checks the available space on the home partition and suspends indexing if the space runs low (also very simple via KDiskFreeSpace. Using Qt/KDE is so damn great! You really can focus on the important stuff!)</li>
<li>It shows info messages about its status via KPassivePopup. Very KDEish and smoothly integrated with the desktop.</li>
<li>It shows a GUI to inform the user that the initial indexing can take a while and gives the possibility to configure/disable/suspend/resume strigi (see below for a screenshot of the widget for which I'd like your input.)</li>
</ul>
</ul>

For me these are more than enough reasons to commit the new service in the next days. It will solve the Strigi situation for many of our users that always disable/kill strigi because they don't get any information about it from KDE.

As I said above I wanted your input for the GUI. The idea was to make it non-intrusive but have it staying in a corner of the desktop until indexing is done or the user closes it. Here it is in all its uglyness:

[image:3572 size=original]

Please help me to make this widget useful.

<i>Jos, I hope you can understand why I did it. It was rather simple and gives us all the features we need. Without reimplementing all the nice things KDE has to offer.</i>