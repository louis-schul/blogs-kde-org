---
title: New Release Team Blog
authors:
  - release-team
date: 2024-03-15
---

This blog will be used by the Release Team for communally maintained projects which need a release announcement.

KDE Frameworks, Plasma and KDE Gear will remain on kde.org.  But individual releases of apps and libraries which get their own releases can be announced here.
