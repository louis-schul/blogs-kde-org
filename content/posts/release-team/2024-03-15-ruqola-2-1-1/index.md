---
title: Ruqola 2.1.1
authors:
  - release-team
date: 2024-03-15
---

Ruqola 2.1.1 is a bugfix release of the Rocket.chat app.

Improvements:
- Preview url fixed
- I implement "block actions" (it's necessary in RC 6.6.x when we invite user)
- Fix OauthAppsUpdateJob support (administration)
- Fix update view when we translate message (View was not updated in private
channel)
- Show server icon in combobox server
- Fix show icon for displaying emoji popup menu when we display thread message
- Fix jitsi support
- Fix dark mode

**URL:** https://download.kde.org/stable/ruqola/  
**Source:** ruqola-2.1.1.tar.xz  
**SHA256:** `6f089271ef9f2f576aa31ddc404bdbc8ddd5ddecba3cd9ff829641123bceb0ae`  
**Windows Build:** ruqola-2.1.1-windows-cl-msvc2022-x86_64.exe  
**Signed by:** `E0A3EB202F8E57528E13E72FD7574483BB57B18D` Jonathan Esk-Riddell <jr@jriddell.org>
https://jriddell.org/esk-riddell.gpg
