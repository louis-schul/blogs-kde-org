---
title:   "KDE Plasma at the movies"
date:    2014-02-09
authors:
  - eike hein
slug:    kde-plasma-movies
---
For several years, I used to maintain a collection of clippings showing the KDE workspaces in use in interesting settings - popping up on TV shows, on public terminals in odd locations, in articles on leading scientific endeavours. All sorts of cool cases. More recently I haven't been collecting as diligently anymore, though, for perhaps the best possible reason: It's happening so frequently now that individual examples have lost much of their novelty.

But from time to time it still provides me with a (pleasant) shock to the system to see our software help others push the envelope and create amazing things. The latest example occurred just the other day, while I was watching the making-of documentary for Alfonso Cuarón's impressive film <a href="http://en.wikipedia.org/wiki/Gravity_%28film%29"><i>Gravity</i></a>: 

<center><img src="http://www.eikehein.com/kde/blog/gravity/gravity1.png" alt="KDE workstation at Framestore" /><br /><br /></center>

<center><img src="http://www.eikehein.com/kde/blog/gravity/gravity2.png" alt="KDE workstation at Framestore" /><br /><br /></center>

<a href="http://www.framestore.com/">Framestore</a> worked for several years to provide the majority of the film's effects shots, and their London-based offices appear filled to the brim with workstations running KDE Plasma. I think I may have even spotted <a href="http://userbase.kde.org/Yakuake">Yakuake</a> on a panel in there.

Interestingly, Framestore isn't the only London-based VFX house using KDE software. I previously collected a <a href="http://www.eikehein.com/kde/drwhoconfidential/snap.png">snapshot</a> of <i>Doctor Who</i> VFX provider <a href="http://www.themill.com/">The Mill</a> using Plasma Desktop in their work as well.

One factor driving this adoption is perhaps the synergy between our and the industry's extensive use of and familiarity with Qt - many high-end 3D modelling and video editing/compositing packages now use Qt for their UI, and often provide PyQt as convenient extensibility solution for in-house dev teams at the studios. The same is true of KDE. But I'd like to think providing a damn nice desktop experience also has something to do with it :).

In the thick of competition, or looking purely at market share data, it's sometimes easy to forget just how much real and compelling use the stuff we make is seeing in absolute terms. And the degree to which our software permeates the foundations that big chunks of our culture get built on top of. 

It's nice to reflect on that now and then.

<i>Gravity</i> has been nominated for ten Academy Awards recently, including the Best Visual Effects category. I loved it, and have a feeling it has a pretty good chance.