---
title:   "Introducing Kirogi: A ground control application for drones"
date:    2019-09-08
authors:
  - eike hein
slug:    introducing-kirogi-ground-control-application-drones
---
Today I'm in beautiful Milano, Italy, where the KDE community has gathered for its annual user and developer conference, <a href="https://akademy.kde.org/">Akademy</a>. At Akademy I've had an opportunity to present my new KDE project to a larger audience: A ground control application for drones, <b>Kirogi</b>.

<center><img width="640" src="https://kirogi.org/assets/img/kirogi.webp" alt="A screenshot of Kirogi's direct flight controls screen" /></a><br /><small><i>Kirogi's direct flight controls screen</i></small><br /><br /></center>

Kirogi aims to enable the operation of drones of all sorts of makes and models in an approachable and polished manner, with both direct and map-based control modes and many more features planned for the future.

The origin story behind the Kirogi project is a classic open source tale. During this year's Lunar New Year holiday, I was paying a visit to family in Busan, South Korea (the name <i>Kirogi</i> is Korean and means wild goose). During the off-days I ended up buying my first drone. I attempted the first flight in my mother-in-law's living room. Unfortunately the official vendor application immediately started crashing after take off - much to my embarassment, I couldn't land the thing! Eventually it slowly drifted towards a very nice armchair (sorry mom!) and crashed on contact with an emergency engines-off.

Turns out the app I was using had been replaced by a newer, seperate app store upload intended for a newer drone - and the app I had wasn't fully compatible with a newer version of the phone's OS anymore. I realized open source can serve drone owners better there and started hacking on this new KDE application a few days later.

Since then I've received a lot of support and help from many people in the fantastic KDE community, including <a href="https://www.krita.org/">Krita</a>-powered artist L. 'AsmoArael' C., who responded to a request I posted KDE's Artists Wanted forum and helped me realize Kirogi's mascot:

<center><img width="500" src="https://kirogi.org/assets/img/goose.webp" alt="A drawing of Kirogi's mascot, a farm goose and technology enthusiast" /></a><br /><small><i>Kirogi's mascot, a farm goose and technology enthusiast</i></small><br /><br /></center>

If you want to know more about the project's origins and dive further into the technical details, I invite you to check out the <a href="https://share.kde.org/s/aZn6KyqoPioaRAF">slides</a> for the talk I gave today. It was recorded on video as well; I will add a link once it's been made available.

The project also has a <a href="https://kirogi.org/">website</a> already. Along with much additional information on the project it features download links for nightly development builds.