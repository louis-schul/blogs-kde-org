---
title:   "Updates on Kate's Rust plugin, syntax highlighting and the Rust source MIME type"
date:    2015-05-22
authors:
  - eike hein
slug:    updates-kates-rust-plugin-syntax-highlighting-and-rust-source-mime-type
---
The other day I <a href="https://blogs.kde.org/2015/05/18/basic-code-completion-rust-kdes-kate-and-later-kdevelop">introduced</a> a new Rust code completion plugin for Kate, powered by Phil Dawes' nifty <a href="https://github.com/phildawes/racer">Racer</a>. Since then there's been a whole bunch of additional developments!

<h1><b>New location</b></h1>

Originally in a scratch repo of mine, the plugin has now moved <a href="http://quickgit.kde.org/?p=kate.git&a=tree&&f=addons%2Frustcompletion">into the Kate repository</a>. That means the next Kate release will come with a Rust code completion plugin out of the box! (Though you'll still need to grab Racer yourself, at least until it finds its way into distributions.)

For now the plugin still works fine with the stable release of Kate, so if you don't want to build all of Kate from git, it's enough to run <code>make install</code> in <code>addons/rustcompletion</code> in your Kate build directory.

This also means the plugin is now on <a href="https://bugs.kde.org">bugs.kde.org</a> - product <code>kate</code>, component <code>plugin-rustcompletion</code> (<a href="https://bugs.kde.org/enter_bug.cgi?product=kate&component=plugin-rustcompletion">handy pre-filled form link</a>). And you can submit patches via <a href="https://git.reviewboard.kde.org/">ReviewBoard</a> now.

<h1><b>New feature: Go to Definition</b></h1>

In addition to code completion popups, the plugin now also installs <i>Go to Definition</i> action (in the Edit menu, the context menu, and you can configure a keyboard shortcut for it as well). It will open the document containing the definition if needed, activate its view and place the cursor at the start of the definition.

<h1><b>Rust syntax highlighting now bundled with Frameworks</b></h1>

After brainstorming with upstream, we decided together that it's best for Rust and Kate users to deprecate the old <a href="https://github.com/rust-lang/kate-config">rust-lang/kate-config</a> repository and move the syntax highlighting file into KDE's KTextEditor library (the foundation of Kate, KDevelop and several other apps) for good, where it now resides <a href="http://quickgit.kde.org/?p=ktexteditor.git&a=tree&f=src%2Fsyntax%2Fdata">among the many other</a> rules files. With 1.0 out the door, Rust is now stable enough that delivering the highlighting rules via distro packages becomes feasible and compelling, and moving the development location avoids having to sync multiple copies of the file.

The full contribution history of the original repo has been replayed into ktexteditor.git, preserving the record of the Rust community's work. The license remains unchanged (MIT), and external contributions remain easy via ReviewBoard or bugs.kde.org.

KTextEditor is a part of KDE's Frameworks library set. The Frameworks do monthly maintenance releases, so keeping up with the Rust release cadence will be easy, should the rules need to be amended.

<h1><b>It's a MIME type: text/rust</b></h1>

Kate plugins and syntax highlighting files preferably establish document identity by MIME type, as do many other Linux desktop applications. The desktop community therefore maintains a common database in the <a href="http://freedesktop.org/wiki/Software/shared-mime-info/">shared-mine-info</a> project. With the inclusion of a <a href="http://cgit.freedesktop.org/xdg/shared-mime-info/commit/?id=f75cbe0d37c990580dbb6f0694b53bc0c914d933">patch</a> of mine on May 18th, shared-mime-info now recognizes the <code>text/rust</code> type for files matching a <code>*.rs</code> glob pattern.

If you're searching, opening or serving Rust source files, you should be using <code>text/rust</code> from now on.

That's it for today! I still have a bunch of improvements to the plugin planned, so stay tuned for future updates.