---
title:   "Plasma 5.10: Folder View as default desktop mode"
date:    2017-03-01
authors:
  - eike hein
slug:    plasma-510-folder-view-default-desktop-mode
---
<center><img src="https://www.eikehein.com/kde/blog/plasma510_fv_default/fv_desktop.png" alt="Plasma with desktop icons" /><br /><small><i>New defaults: Plasma 5.10 with desktop icons</i></small><br /><br /></center>

<b>A brief history lesson</b>

To set the stage, we need to briefly recap some of the problems with the KDE 3.x desktop that (among others) Plasma initially set out to solve.

In the KDE 3.x desktop, three separate applications performed the duties of today's Plasma shell. <code>kicker</code> was in charge of putting panels on screen edges, <code>kdesktop</code> put wallpapers and desktop icons on screens, and <code>SuperKaramba</code> allowed for (quite shakily) putting widget windows in an extra layer between the desktop and application windows. <code>kdesktop</code> was essentially non-extensible (i.e. you couldn't make it put anything other than icons on the desktop by installing an add-on of some kind), while <code>kicker</code> and <code>SuperKaramba</code> could be extended by new plugins at runtime, but using incompatible plugin formats that couldn't share any code and had to do their work using entirely different sets of APIs.

With Plasma 4.0, things got a lot better. Plasma widgets need to be written only once, using one set of APIs, and can then be added to both the desktop and to panels - tuning their UI to their respective surroundings. Moreover, things like the desktop and panels themselves are plugins as well, and can be similarly swapped out (allowing us to make very different UIs for different devices within the same framework). And multiple plugin can share the same data sources in the back, also cutting down on overhead.

<b>Enter <i>Folder View</i></b>

<i>Folder View</i> is one of the bundled plugins, and it illustrates many of the above concepts. It does what <code>kdesktop</code> used to do - visualize folder contents. But not only can you use it to put icons on the desktop, you can also put additional Folder View widgets on top of that desktop, or put Folder View in your panel, all of them showing different folders. In the panel, it will usually take the form of a button that brings up a popup (which can use either <a href="https://blogs.kde.org/2015/06/04/folder-view-panel-popups-are-list-views-again">list</a> or icon view modes), but it will expand into the panel itself if the panel is roomy enough, allowing even for a file system sidebar.

<b>No more icons on the desktop?</b>

Back in Plasma 4.0, we made the decision not to use Folder View as the default desktop layout (<i>default</i> being the operative term - users could change this in config, returning to a more traditional icon desktop). Instead, we left the desktop surface empty by default, and had Plasma add a Folder View widget on top of it.

We had reasons for that. We wanted to let users know about Plasma's new abilities: highlight its more modular nature, encourage experimenting with new setups and personalized mix-and-match. We didn't want the desktop to be constrained by the skeuomorphic desktop metaphor that at the time looked like it might well be on the way out (as indicated e.g. by a pervasive shift from spatial to browser-style file management across systems). It was exciting to ponder what things might fill the void left by shedding those icons.

<b>Yes - icons on the desktop</b>

More recent history has shown, however: Icons are here to stay. In mobile, there's been an explosion of new platforms that have really doubled-down on the icons-on-the-homescreen thing, making it more popular than ever. Many of those platforms also mix in widgets as we do, meaning nine years after 4.0, both our long-time and our still to-be-recruited users get all of this with ease now. We no longer need to try so hard when it comes to doing the onboarding for Plasma's key concepts.

The evolved landscape leads us to believe that Folder View is now the better default, and this is what Plasma 5.10 will be shipping (some distributions, of course, have beaten us to this, or have always done so). It will still be possible to have an empty desktop (it's that "Layout" option in the desktop settings), but it's a role-reversal in what's opt-in and what's opt-out. Likewise, extending Plasma with new desktop layouts is of course also still possible.

<b>Better icons on the desktop</b>

Given Plasma 5's incarnation of Folder View is about to be put front and center, we're putting a lot of effort into making it really shine throughout the 5.10 development cycle. Performance has been improved greatly. We've been thoroughly auditing for interaction issues, sorting through feedback and talking to people about how they use Folder View. This has lead to numerous small improvements, often subtle, that improve the feel of using Folder View - e.g. tuning the sizes of various hitboxes and hitpoints to make rectangle selections and dragging icons around that extra bit less cumbersome. Sizes and spacing have been tweaked as well in response user feedback, resulting in a tighter icon grid than before. And there's even some <a href="https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work">cool new functionality</a> as well.

<b>How you can help</b>

We're committed to making the default experience of Plasma 5.10 - desktop icons included - rock more than ever. If you want to help, several distributions <a href="https://community.kde.org/Plasma/Live_Images#Shipping_the_latest_code_from_Git">offer live images</a> or packages of current development snapshots. Use them to take a long good look at Folder View, and then let us know about your experience!