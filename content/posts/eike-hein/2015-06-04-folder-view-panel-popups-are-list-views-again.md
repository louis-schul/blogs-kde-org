---
title:   "Folder View panel popups are list views again"
date:    2015-06-04
authors:
  - eike hein
slug:    folder-view-panel-popups-are-list-views-again
---
In later versions of Plasma 4, the Folder View widget adopted a special appearance when placed in a panel: It would arrange folder contents in a simple list instead of the usual icon grid. Folder View had to be rewritten completely for Plasma 5, and while there were various improvements along the way, the list view mode unfortunately went missing. Until now - on popular request, this feature will make a return soon in Plasma 5.4:

<center><img src="https://www.eikehein.com/kde/blog/fvlist/fvlist01.png" alt="Folder View as list view when placed in a panel" /><br /><small><i>Plasma 5.4's Folder View when placed in a panel</i></small><br /><br /></center>

To make up for the wait, list view mode in Plasma 5 is cooler than it was in Plasma 4. Rather than showing preview popups for directory items, activating one will navigate into the directory in-place:

<center><img src="https://www.eikehein.com/kde/blog/fvlist/fvlist02.png" alt="In-place  navigation" /><br /><small><i>One level down</i></small><br /><br /></center>