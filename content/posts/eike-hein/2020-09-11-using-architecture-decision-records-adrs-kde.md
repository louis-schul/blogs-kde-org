---
title:   "Using Architecture Decision Records (ADRs) in KDE?"
date:    2020-09-11
authors:
  - eike hein
slug:    using-architecture-decision-records-adrs-kde
---
Over at <a href="https://akademy.kde.org/2020">Akademy 2020</a>, I just witnessed a fantastic talk by KDE contributor mainstay Kévin Ottens on "Lost Knowledge in KDE". In the presentation, Kévin showed us a series of examples of sophisticated solutions to important problems KDE has innovated and implemented over the years - and subsequently lost knowledge of, applying them sparingly or inconsistently, or developing new solutions redundantly. He also talked about how this is a familiar problem to organizations, with a research field known as knowledge management itself looking to develop solutions and tools to combat this problem since the late 20st century.

He also highlighted how we don't use comm tools with higher knowledge retention factors - blogs, mailing lists, media with more permanent discussion records - as much anymore. So here's a suggestion in blog form! :-)

Within the wider open source as community as well as industry, the idea of writing <a href="https://github.com/joelparkerhenderson/architecture_decision_record">Architecture Decision Records</a> has recently become quite popular. ADRs are not really a new tool - surely forms of it have been around for a long time in various organizations - but giving the renaissance of the concept a name has a lead to ample discourse on its pros an cons, and plenty of resources available.

To give my take, an ADR is a concise write-up of a particular project/community decision. It should have enough detail to make the decision understandable, as well as cover its context and implications. They usually have an owner and co-authors, and are finalized via a light peer-review process. It's a bit like drafting and finalizing a PEP, another popular tool - except instead of motivating a change, it is describing a decision, to serve as a cache and make sure future discussions and changes have a frame of reference to work with. In this sense, it's a bit like <a href="https://manifesto.kde.org/">KDE Manifesto</a>, which has served us rather well.

ADR's aren't perfect - they can't and shouldn't replace other forms of documentation, such as API docs and protocol specs. They also have a problem with indexing - while being able to refer to/link to a particular ADR is extremely useful when someone is aware of the relevant ADR already, they're little information silos that are hard to dive into in flat-list format. But the big pro is that they have a low barrier of entry due to the lower effort and process overhead required compared to other forms of documentation, if implemented well. They make it more likely for documentation to be written.

KDE's design community has recently started making use of this idea, writing down some decisions that otherwise had a tendency to be re-discussed again and again: <a href="https://community.kde.org/Get_Involved/Design/Lessons_Learned">Design Lessons Learned</a>.

I think we could co-opt the ADR idea and apply it to a lot of the examples Kévin showed, to record the needs and thinking that lead to building various solutions and frameworks we have: What problem were we trying to solve? Why was it better than we had before? How does the minimum bar look like now that we have the solution? Where should the solution be applied without fail? What alternatives were discussed and discarded before making the solution?

All we'd need to get started is a wiki space, a good ADR template, a minimal kdereview-like process, and a few good examples to seed the list ...