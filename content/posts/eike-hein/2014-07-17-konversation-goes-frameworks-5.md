---
title:   "Konversation goes Frameworks 5"
date:    2014-07-17
authors:
  - eike hein
slug:    konversation-goes-frameworks-5
---
The Konversation team has started porting the application to <a href="http://kde.org/announcements/kde-frameworks-5.0.php">Frameworks 5</a> earlier this month, getting things to build and run on top of KDE's next-generation libraries.

<a href="https://userbase.kde.org/Konversation/KF5_Port">Here's all the info you should need to help out.</a>

At this stage of porting there are still hundreds of low-hanging fruit (porting away from deprecated APIs, porting to slightly changed APIs, etc.) that don't require extensive knowledge of either the codebase or even the problem space to tackle, so if you were looking to get your feet wet on Frameworks 5 or KDE development in general, consider chipping in!

In fact, one of the major risks associated with any porting is lowering the quality of the application by reducing functionality or just plain breaking things, and of course we'd like to avoid both. So you can even help out by just running the port and logging the things that don't work on KDE's bug tracker (handy link is on the wiki) as tasks for others to jump on.