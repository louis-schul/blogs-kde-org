---
title:   "Measuring performance the buildsystem-guy way..."
date:    2008-04-30
authors:
  - alexander neundorf
slug:    measuring-performance-buildsystem-guy-way
---
Different groups of people use different ways to measure performance of computers. Kernel developers test their kernels by, well, compiling the kernel. KDE developers may measure the startup time of KDE.
So what does the buildsystem maintainer do ?
<!--break-->
He measures how long it takes to build CMake cvs (including curses and Qt4 based interface).

So here are the numbers:

On my desktop machine, which is still an AMD Athlon 2000XP+ (already over 5 years old), 1GB RAM, new hard disk: 5 min 59 s.

On my new Dell Inspiron, Intel Core 2 Duo 2 GHz, 2 GB RAM, 5400 hdd, shipped with Ubuntu (which instantly became a kUbuntu): 2 min 17 s

So although they claim about the same frequency, it's really almost three times as fast !

But, wait, it's a Core Duo, so let's try make -j2: 1 min 15 s. That's only 20 percent of the time the Athlon needs.  Not bad !

(I also tried make -j3, this gave also 1 min 15 s, so no additional gain here).

Alex

P.S. so maybe it's really time to upgrade my desktop machine, I didn't expect such a big difference. OTOH it still feels fast, and so it's a good machine to test the speed of KDE on not too fast hardware.
