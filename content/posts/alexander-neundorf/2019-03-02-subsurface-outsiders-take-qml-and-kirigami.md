---
title:   "Subsurface - an \"outsiders\" take on QML and Kirigami"
date:    2019-03-02
authors:
  - alexander neundorf
slug:    subsurface-outsiders-take-qml-and-kirigami
---
Dirk Hohndel talked about his experiences with QML and Kirigami at LCA 2019:
<a href="https://lwn.net/Articles/780031/">Producing an application for both desktop and mobile</a>

I think that's quite useful for us as feedback from somebody who is not directly part of the KDE community.
