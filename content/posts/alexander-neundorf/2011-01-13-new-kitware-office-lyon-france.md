---
title:   "New Kitware office in Lyon, France"
date:    2011-01-13
authors:
  - alexander neundorf
slug:    new-kitware-office-lyon-france
---
Hi,

just to let you know, our friendly <a href="http://www.cmake.org">buildsystem</a> developers from Kitware are progressing on their plan towards world domination ! ;-)
While their main offices are in the US, in beautiful upstate New York, they recently opened their <a href="http://www.vizworld.com/2010/10/kitware-announces-european-office/">first office outside the US</a>, in Lyon, France, where e.g. their main <a href="http://www.cdash.org">CDash</a> developer is located.

While talking about CDash, did I mention that I'm looking for people interested in setting up nightly or continuous builds of KDE on "exotic" platforms (i.e. not x86 Linux)  ?
I think in the future we really need this to make sure KDE stays working on all platforms (Windows, OSX, mobile). On <a href="http://my.cdash.org">my.cdash.org</a> I have already set up projects for most KDE modules.
If you're interested, drop me a mail or post to the kde-buildsystem (AT) kde.org list.

Alex

P.S. now that they are in Europe, you can also get training courses for their open source packages here: <a href="http://www.kitware.com/products/protraining3.html">ITK, VTK and CMake training</a>.
