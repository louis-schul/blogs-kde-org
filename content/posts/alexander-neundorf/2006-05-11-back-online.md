---
title:   "Back online"
date:    2006-05-11
authors:
  - alexander neundorf
slug:    back-online
---
Ok, so finally I'm back online.
When I came back from LinuxTag last sunday, my development box didn't boot anymore :-/
It took until today to get it working correctly again (RAM, mainboard, power supply were suspect).
Finally the RAM was broken, now it's replaced by nice 1 GB RAM.

So, back to KDE/cmake...

Alex
