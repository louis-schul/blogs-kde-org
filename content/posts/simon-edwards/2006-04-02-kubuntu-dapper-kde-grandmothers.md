---
title:   "Kubuntu Dapper: KDE for grandmothers"
date:    2006-04-02
authors:
  - simon edwards
slug:    kubuntu-dapper-kde-grandmothers
---
It has been a long time since I've posted something here. I've had things that I've wanted to post but pretty much all of my time has gone to working on the Guidance tools for the coming Dapper release of Kubuntu. Thank gawd Dapper has been delayed. I can (am!) really use the extra time for polishing and debugging. It looks like Dapper is going to be an awesome release and will work a lot smoother than the current release (Breezy).

One new feature that I'm glad to see in Dapper is the "adept_install" tool. (Has it got a real name now??) This is a very user friendly alternative to Adept (=Kubuntu's package manager) which allows people (=your grandmother) to browse the software they have installed on their computer and to find and install new software. Yes, it is kind of like Linspire's CNR. Available software is presented divided into a hand full of simple top level categories, Office, Games, Graphics etc. Each category lists the software available and describes what it is in plain English and without all of the extra 'apt' geek talk. Only desktop applications are shown which means that the list of software is not insanely huge and overwhelming and can be browsed through by normal people.

I was planning to discuss this kind of functionality with Riddel at FOSDEM last Febuary for Dapper+1, but he and Mornfall were already waaaaay ahead of me. :-) Cheers and thanks go out to Mornfall who did the heavy lifting of implementing it all.