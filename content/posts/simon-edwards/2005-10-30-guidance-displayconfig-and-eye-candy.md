---
title:   "Guidance, displayconfig and eye candy"
date:    2005-10-30
authors:
  - simon edwards
slug:    guidance-displayconfig-and-eye-candy
---
<p>
I've finally been able to put some much needed time in displayconfig, the screen/monitor/X11 configuration part of Guidance. Other things like writing a paper about Python, Qt and KDE for this NLUUG conference next month kept me distracted. I've now got some eye candy that I can show-and-tell at the end of this post.
</p>
<p>
One of the goals of displayconfig is to support dual-head configurations. This required a lot of hard work to come up with a structure or framework that would support this in addition to the much easier common case of single-head. Why is X11 configuration so hard? I hear you ask. For a start you want to be able to show the user the hardware in their system (gfx card and monitors) and let them tweak and select drivers, gfx card models and monitor models if they want to. This needs to be possible reguardless of how screwed up /etc/X11/xorg.conf is. This means hardware detection. And for the most part this means scanning the PCI bus and using datafiles of PCI, gfx card and monitor information to decode what is there, and also to detect things like dual-head capable gfx cards. Currently Mandriva have the best and most complete datafiles in their ldetect-lst package.
</p>
<p>
The next problem is that object model that /etc/X11/xorg.conf uses is different than how the graphics hardware is organised internally, which makes bridging these two viewpoints quite tricky at times. Yet another problem is that there are different flavours of dual-head with different capabilities and which require different sections and layouts inside /etc/X11/xorg.conf. It isn't as simple as placing a "Option dualhead on" line in /etc/X11/xorg.conf.
</p>
<p>
Finally, you also want to be able to use RandR when available to change the screen resolution and rotation on the fly without having to restart the X server. This also means that normal uses should be able configure their screen, while the administrator can configure the drivers and the default screen settings for everyone. The good news is that most of painful work has been done for implementing a sane layer to support all of this. 
</p>
<p>
Now, that eye candy I promised. Displayconfig also supports normal single-head configurations as well as screen rotation and reflections where available. Of course you need funky preview showing what the user is going to get. And here it is:
</p>
<img src="http://www.simonzone.com/software/guidance/displayconfig_monitor_blog.png" >
<p>
The older version of this preview had some problems. It was implemented using widgets and layouts, and would stretch the dialog layout at times. This new version handles all drawing itself and composites each element of the preview on the fly. Notice how the window and kmenu change relative positions. I'm not just scaling a simple picture here. I'm compositing the screen background, window, kmenu and clock all separately. It is still fake of course but it should give the user a good impression of what they are going to get.
</p>