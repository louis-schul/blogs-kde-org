---
title:   "\"Search\" functionality in GUIs"
date:    2006-05-25
authors:
  - simon edwards
slug:    search-functionality-guis
---
I've been meaning to post about this for literally half a year now. But a recent thread on kde-core about improving the GUI for searching in KDE applications has finally pushed me into action.

At work we're stuck with that other operating system for desktops (not a big deal, our stuff is java and web based) and while looking for a decent text editor I came across jEdit (http://www.jedit.org/). It is ugly as sin but with a little bit of modding and configuration work you can make it acceptable. Anyway, what is interesting is a plugin called "Highlight". Highlight's GUI appears as a docked panel thingy at the edge of the screen. What it does is let you enter a regular expression and then it will simply highlight all matches in your document in the selected colour. Basically you can use highlight as a visual search facility. Yes, much like the highlight button in Firefox's search bar.

This probably doesn't sound too earth shattering right now, but it gets more interesting when you specify multiple regular expressions using different colours. <!--break-->This way you can use it for searching for multiple things at the same time. I now use highlight as a substitute for the find functionality in jEdit. I just highlight what I'm interested in and then quickly page through the file looking for blobs of colour. (The human brain isn't very good for a lot of things, but visual processing is where it excels. Take advantage of this fact! Tip for the day. :-) )

I've also got jEdit configured such that when I press F2 the word under the cursor is added to highlight. Meaning that I can search/highlight things very quickly. This is very useful while trying to debug or comprehend code and when following the use of one or more variables in a complex section of code.

The highlight plugin doesn't support easily jumping from match to match, or replacing the highlighter text with something else. But you can easily see how this and Firefox's search bar can be combined to make a much faster, less intrusive, and useful search functionality.

One other useful piece of the search/highlight puzzle is how Eclipse communicates code errors files. When Eclipse detects an error it highlights the error with a red underline and also highlights it in the space next to the scrollbar. By looking at the scrollbar you can see where any other errors in the current file are. (If my poor explanation isn't working for you and you have never seen Eclipse then look here:  http://help.eclipse.org/help30/index.jsp?topic=/org.eclipse.jdt.doc.user/gettingStarted/qs-7.htm )

Combine these ideas and I think we can come up with a true next generation search GUI.

<img src="http://www.simonzone.com/software/jedit_highlighter.png" />
The little page icon with a plus creates a new expression, and that funny flying stick icon deletes (wipes?) a regular expression. I warned you that it was ugly. ;-)