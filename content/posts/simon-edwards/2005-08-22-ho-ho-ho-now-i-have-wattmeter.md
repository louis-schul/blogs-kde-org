---
title:   "Ho ho ho now I have a wattmeter"
date:    2005-08-22
authors:
  - simon edwards
slug:    ho-ho-ho-now-i-have-wattmeter
---
<p>
I recently moved into my new house, which also means that I have to pay for electricity directly. Wanting to do know how much power things are really using in the computer room, I went out and bought a wattmeter. It measure power use at the wall. I thought that other people might be interested in seeing some "real world" measured values.
</p>
<!--break-->
<p>
I measured energy use at the wall with the device idle and also when turned off but plugged in.
</p>
<ul>
<li>Athlon 64 3000+, 512Mb, 1 HDD, CD/DVD etc drive, low end passively cooled gfx card, NIC. <b>idle/on: 55W, off: 10W</b></li>
<li>"Shuttle", Athon 2800+, 512Mb, 1 HDD, optical drive, high end ATI gfx card, USB sound card. <b>on: 102W, off: 15W</b></li>
<li>Athlon ~1600 (underclocked), 512Mb, 2 HDD, CD burner, NIC. <b>idle/on: 32W, off: 15W</b></li>
<li>LCD monitor 17 inch, NEC LCD1701. <b>on: 41W, off: 8W</b></li>
<li>LCD monitor 17 inch, Samsung SyncMaster 172v. <b>on: 26W, off: 8W</b></li>
<li>Printer, HP Deskjet 930C, <b>idle: 11W, off: 10W</b></li>
<li>Scanner, HP scanjet??. <b>idle: 15W</b></li>
<li>E-tech 5 port switch. <b>idle: 11W</b></li>
<li>Very old 16 port hub (retired). <b>idle: 25W</b></li>
<li>Cordless digital phone. <b>on: 8W</b></li>
</ul>
<p>
It appears that the art of implementing an on/off switch that actually turns the device <b>off</b> is dying out. I'm now turning peripherals and other junk off at the wall using a powerboard with a real switch. And also trying to turn the Shuttle off when GF doesn't need it.
</p>
