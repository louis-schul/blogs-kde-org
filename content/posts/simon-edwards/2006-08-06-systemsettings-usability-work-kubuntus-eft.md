---
title:   "Systemsettings usability work for Kubuntu's Eft"
date:    2006-08-06
authors:
  - simon edwards
slug:    systemsettings-usability-work-kubuntus-eft
---
One of the more unique features of Kubuntu is that it ships with a replacement for the standard KDE Control Center program, called systemsettings. Instead of using a tree in a sidebar on the left hand side of the window, systemsettings shows a large menu of grouped icons for the user to choose from. A lot of people like it (and some people don't), but pretty much everyone agrees that it can be improved even more.

For the coming Edgy Eft release, Ellen of OpenUsability.org fame has done a card sorting experiment with real users (not us geeks!) in order to find out how best to organise the settings modules in systemsettings. "Card sorting" is a fairly simple usability technique. You write down each item on a card and then ask people to sort the cards into piles of related cards. Do this for enough people and you build up a picture of how the typical user organises and thinks how the items a related. Empirical data.

<!--break-->

The usability work and the new specification for Edgy is described here for those who are interested: https://wiki.kubuntu.org/KubuntuSystemSettingsUsability

There is also a link to a spreadsheet describing Ellen's new "information architecture" for the Systemsettings.

In the last week I've taken Ellen's changes and implemented them. Here are some screenshots of what it looks like running on my Dapper system.

<img src="http://www.simonzone.com/software/edgy_settings_general.png">
<img src="http://www.simonzone.com/software/edgy_settings_advanced.png" >

There is still some work to be done. Some icons need changing, and some labels might also be changed. But I think it is a solid improvement over what we now have. Thanks El.

