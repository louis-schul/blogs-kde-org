---
title:   "Footnote to Usability, hierarchies and IO-slaves"
date:    2005-08-16
authors:
  - simon edwards
slug:    footnote-usability-hierarchies-and-io-slaves
---
<p>
It appears that at least one person (Hi Peter!) seems to have misunderstood what I was saying at the end of my last post.
</p>
<!--break-->
<p>
I'm not saying that a new hierarchy should be thought up to supplement (read: in addition to) the current unix filesystem layout. I'm saying that a new hierarchy should <b>completely replace</b> the current unix filesystem layout. This is what GoboLinux and OS X have done. This is also not the same as what system: does. system: is another layer on top of the underlying filesystem layout, which leaves you with a system with a split personality. The GUI presents one version of what the system looks like, while everything below the GUI, such as console programs, uses something completely different.
</p>

<p>
As for the unix filesystem layout being great for sysadmins and programs, *ahrhumm* I beg to differ. :-) Personally I think it is a relict from the past that should be done away with. To put it lightly. Seriously, the scattergun approach to installing software is a liability requiring dedicated software such as RPM to manage just where the h*ll everything got installed. The only reason why it is still used is backwards compatibility. It has no place on a desktop machine.
</p>

<p>
(It is probably best that no one ask me what I think of shell scripts. ;-) or what I think of the idea that unix was based on the idea of "small tools doing one task well", emacs and perl anyone?)
</p>
