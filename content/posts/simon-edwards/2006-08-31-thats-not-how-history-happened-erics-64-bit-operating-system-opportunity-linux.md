---
title:   "That's not how history happened: Eric's 64 bit operating system opportunity for Linux"
date:    2006-08-31
authors:
  - simon edwards
slug:    thats-not-how-history-happened-erics-64-bit-operating-system-opportunity-linux
---
This started as reply to <a href="http://blogs.kde.org/node/2302">Richard's post</a>, but it got too big so I've supersized it. :-)

You would think that <a href="http://www.redherring.com/Article.aspx?a=18176&hed=Linux+Desktop+Window+Closing%3F+">Eric would know his computer industry history a little bit better</a>. The transition from 32 bit to 64 bit won't resemble the past. Even Eric's account of the past doesn't resemble the past.

For those of you who don't remember the 8 bit days, let me describe the situation. The 8 bit generation was a multitude of systems based on 8 bit CPUs such as the Z80, 6502 and 6510. There wasn't one dominant operating system either. There were instead lots of competing systems like the Commodore 64, TRS-80, Apple IIs etc, all running different incompatible operating systems. "Operating system" is a bit of an exaggeration, more like "program loaded" and perhaps a built in interpreted programming language such as BASIC.

The 8 to 16 bit transition [1] was a jump from limited 8 bit systems to qualitatively more capable systems such as the Macintosh, Amiga, Atari ST, and the IBM PC. These systems weren't just bigger and faster, with more memory and more speed. They could do things that were simply not possible on the 8 bit systems, things like run a Graphical User Interface and some kind of multitasking. Operating systems such as AmigaDOS and the combination of MSDOS and Windows 3.11. [2] These systems didn't offer any compatibility with the older generation. They were a complete break from the old.

By the time the 32 bit transition rolled around Microsoft was already established as the dominant platform. MS was able to hold on to the market even though they were years behind the competition before they finally released a real 32 bit OS in the form of Windows 95. Windows 95 was seen as the de facto successor to the 16 bit combination of MSDOS and Windows 3.11. Windows 95 was a 32 bit version of what people were already using. Windows 95 even ran the older 16 bit based software. This was one of the main reasons why it was "business as usual" for most people during that transition.

The same will be true with the 32 to 64 bit transition. 64 bit Windows will be seen as simply a 64 bit version of the same old familiar OS that people have been using for years. People aren't just going to reconsider their choice of OS just because the CPU now works with 64 bit addressing. The 64 bit processors can even run 32 bit code at the same time. People don't even have to be aware that something different is going on under the hood.

Sorry Eric. The hardware transition theory of Linux domination sounds nice, but history certainly doesn't support it.


[1] Actually "8 to 16/32 bit transition" would be more accurate. Most of the new systems were 32 bit based. It was the IBM PC that was running 16 bit code.

[2] Things were not this simple either. The primitive and command line based MSDOS was competing with, and lagging behind, the other GUI based operating systems for a very long time before a GUI for MSDOS called Windows was released and took hold of the market.
