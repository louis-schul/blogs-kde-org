---
title:   "Video: KDE people at openSUSE Conference 2010"
date:    2010-10-22
authors:
  - bille
slug:    video-kde-people-opensuse-conference-2010
---
I couldn't resist snapping as many KDE folk at the openSUSE conference as I could, and editing them together into: <a href="http://bille.blip.tv/file/4281044/">a short video</a>.
<embed src="http://blip.tv/play/A42zVqqkKw" type="application/x-shockwave-flash" width="320" height="270" allowscriptaccess="always" allowfullscreen="true"></embed>

If anyone can tell me how to embed in kdedevelopers.org or enable the download of the OGG version from blip.tv, let me know!