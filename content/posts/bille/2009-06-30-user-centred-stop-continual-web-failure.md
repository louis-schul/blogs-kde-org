---
title:   "User-Centred: Stop Continual Web Failure"
date:    2009-06-30
authors:
  - bille
slug:    user-centred-stop-continual-web-failure
---
KDE needs as an entire project to support a Web browser that everyone can use in 2009.  That's the simple message behind this blog entry and my talk at LinuxTag on Saturday.

If you need any more motivation to go out there and make that happen, read on.
<!--break-->
I have to admit that as a KDE user for the past ten years and a developer for 8 years, I'm writing this blog on Firefox right now.   What do 95% of desktop users actually do after they log in?  They consume content.  That means they use Firefox, use Firefox, use Firefox, use Firefox, and while they are at it they may listen to music using Amarok.  Unless you are reading this in lynx, you know what users are doing - webmail, social networking, youtube, microblogging, editing online documents, uploading to their photo collection, using intranets.  

Anything else they do using a fat local app is pretty much incidental nowadays - using at best, what we used to call 'utilities' to patch their system or change the volume level.

If a desktop or an operating system doesn't have a well integrated web experience, users have no reason to stick around.  

Details. KDE always has been network-transparent environment.  Our apps can just as easily pull content from a web server as load it from the hard disk or save it back to an FTP site.  But Firefox has a completely separate network layer.  Passwords, cookies, sessions are all separate and insulated from KDE and have to be managed separately if you want to use KDE apps, which is hassle.

File associations are managed in most Firefox versions by GVFS now, which means that if you have any g* apps installed, they will be Firefox's first pick to open links to movies, PDFs, and music files you click.  If you are a curious user who installed the whole distro DVD, if every time you view a PDF in KDE, Evince shows up, why bother logging into KDE?

Firefox bookmarks are separate and completely ignored by all KDE's cool semantic technologies like Nepomuk.  They could be indexed, sure, but since we're the integrated Free Desktop environment, why do lists of bookmarks have to be glued to the app they came from?  KDE should have a single Bookmarks app letting users see where they currently are in all their content - Web stuff, documents, podcasts, videos, even Konsole sessions.

And many other smaller integration points break.  Try 'Set as Background Image' for example.  Firefox saves downloaded files in one directory.  Chances are it isn't part of the fancy Places list in the KDE file dialog - another wasted opportunity.

I have nothing but respect for the KHTML developers.  They do an amazing job with very few resources, creating fast, efficient code.  But although KHTML is 'pretty good' that is just not sufficient for real world users.  I tried a selection of modern sites with trunk on Saturday and the majority of them had rendering or ajax errors that provoke a WTF reaction among anyone not very close to the KDE project.  The blame for this is almost certainly with the site developers, but a reality check says that they will never test for KHTML and never fix it.  

So I suggest, as strongly as I can, that KDE gets behind a widely used web engine that works, puts it in Konqueror, integrates Rekonq, and makes it all work as well with the rest of KDE as Konq+KHTML did.  I don't think this even changes anything for the KHTML developers - the same tiny subset of KDE users will continue to choose to use Konq+KHTML, but the majority will be able to have a web experience that strengthens KDE, rather than weakening it.

By not having an integrated browser that anyone uses, KDE is on the same path to irrelevance that Microsoft started when they ignored the World Wide Web in the mid 90s.  Apple gets it, Microsoft now gets it, GNOME gets it.  Yet KDE continues to look away from this unsightly hole in its portfolio.