---
title:   "Hello Trysil, goodbye world cup"
date:    2006-07-02
authors:
  - bille
slug:    hello-trysil-goodbye-world-cup
---
Got into Trysil, Norway for KDE Four Core at 10pm and it was still sunny.  Now (00:17) it's still quite light outside which is playing silly beggars with my head, I've lost the ability to speak German.  Anyway, I'm here to work on Akonadi, Solid's networking support and any other jobs that come up.  The team that the Technical Working Group have assembled here are a highly motivated group and we're going to work our socks off for eight days to get kdelibs to a position where the rest of the KDE community can pick it up and start to write KDE 4 apps without the rules changing every week.  

With 22 hackers and 200000 midges, we can do it!

Just don't mention the football...

This is the view from the chalet:

[image:2159]

