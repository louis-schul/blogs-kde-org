---
title:   "NetworkManager support in Solid for KDE 4"
date:    2007-03-09
authors:
  - bille
slug:    networkmanager-support-solid-kde-4
---
The NetworkManager backend for Solid in KDE 4 is ready to announce.  Since writing the frontend in September I hadn't gotten round to making it useful, but after a lot of encouragement from ervin I picked up my tools again.  

For readers who are just joining us, what's really cool about Solid is that it separates the function (in this case, information about network devices and controls for them) from the facility that provides that function.  This means that KDE 4 apps on Windows will be able to just use Solid without caring about the details of Windows dial up networking etc. 

Back to today, the NetworkManager daemon provides user configurable networking in openSUSE and Fedora and is coming to kubuntu, so supporting it is important to us.  Using what I learned when working on knetworkmanager for SUSE (mainly, that NetworkManager's DBus API is  a bit toss) I've been using QtDBus to implement the Solid backend.

QtDBus really makes DBus programming a joy.  Even when you have no Introspect method on the object you're talking to, and its methods are irregular and take and return arbitrary sets of parameters, you can still handle this with QtDBus and stay sane.  

To use the Solid NetworkManager backend, you need SVN.  At present the solidshell command line tool has been extended to let you control NM - as far as I know this is a World First, as nm-tool is read only. For example 

<code>solidshell network listdevices</code>

returns a list of Unique Network Interfaces, and 

<code>solidshell network listnetworks /org/freedesktop/NetworkManager/Devices/eth1</code>

returns all the networks visible to eth1, and 

<code>solidshell network set network  /org/freedesktop/NetworkManager/Devices/eth1 /org/freedesktop/NetworkManager/Devices/eth1/Networks/bedrock authentication wpapsk wpa tkip bettyrubble</code>

activates the wireless network 'bedrock'. <code>solidshell --commands</code> gives you all the info.