---
title:   "You know you work at SUSE when..."
date:    2006-09-22
authors:
  - bille
slug:    you-know-you-work-suse-when
---
... your fridge contains less than the one at work.   A couple of years ago, when I was new in Nuremberg, <a href="http://en.opensuse.org/User:AdrianSuSE">Adrian</a> told me that most people at SUSE never eat at home, and some people who had newish flats still had the cellophane on the kitchen hardware and appliances.  I laughed at the time, being a cook-it-yourself type of guy, but now, on my way to <a href="/conference2006.kde.org">Akademy</a> and making sure that nothing will go off, I realised that there was pretty much nothing to spoil there.  So, in an obvious attempt to kickstart another geek blog meme, here's the contents of my fridge:

<ul>
<li>One bottle of bloke coke</li>
<li>Half a bottle of Müller-Thurgau</li>
<li>Maple syrup</li>
<li>Thai curry paste (never goes off and I WILL cook with it again, I swear</li>
<li>1L Chocolate soya milk (now consumed)</li>
<li>1L fresh orange juice (now consumed)</li>
<li>One yogurt that I'm not supposed to eat (now consumed></li>
</ul>

And now I'm ready for a week of happy hacking on Akonadi and network management at Akademy.  So I ask - what's going off in your fridge while you're there?
I'm off to get my flight now.  See you in Dublin!<!--break-->