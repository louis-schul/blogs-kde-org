---
title:   "KPodcasted"
date:    2007-07-28
authors:
  - bille
slug:    kpodcasted
---
A couple of weeks ago we had the <a href="http://idea.opensuse.org/content/">openSUSE hack week</a> and at last it was the newly renamed <a href="http://en.opensuse.org/KDE_Team">KDE Team</a>'s turn to share our thoughts with the <a href="http://www.novell.com/feeds/openaudio/">Novell Open Audio</a> team.  They're Novell's podcast unit, who travel the empire of the big red N finding what the company and its community are doing, record it then mix it all up with their idiosyncratic wit and what I've come to believe is the Novell rock anthem.  So I jumped on the opportunity to tell our team's story, communicate everything that's possible with KDE on openSUSE, and highlight all the good things that KDE 4 will bring to the community distro and Novell's enterprise products.  <a href="http://www.novell.com/feeds/openaudio/?p=170">Tune in</a> here.
<!--break-->