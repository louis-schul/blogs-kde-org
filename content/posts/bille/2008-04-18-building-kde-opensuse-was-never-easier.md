---
title:   "Building KDE on openSUSE was never easier"
date:    2008-04-18
authors:
  - bille
slug:    building-kde-opensuse-was-never-easier
---
I've just published the <a href="http://en.opensuse.org/KDE/Developing/Guide">Building KDE on openSUSE guide</a> over at the openSUSE wiki.  It makes it insanely easy to build latest KDE 4.1 in a minimal number of steps, but the goal is not just to make it easy, but to give people the tools and the skills to go from just building KDE to developing it.  If you're the type of person who always stays up to date with the latest alphas/betas, or are a Power Bug Reporter who wants to report bugs with full debug output and maybe try applying a patch from a developer or twiddle a few bits yourself, this is a way to get the freshest KDE 4 Plasma or Amarok around.

The guide uses Qt 4.4 and the kdesupport packages from the <a href="http://build.opensuse.org">openSUSE build service</a> so you don't have to build them yourself.  The openSUSE KDE Team are so obsessive about keeping those packages up to date that the only way to get them fresher is to smuggle yourself into Trolltech (I hear the big N810 disguise works).  So there's no need, if you want to work with the main KDE modules or extragear, to build those yourself.  For the record, with a 4 year old HP zt3000 - Centrino 1.5Ghz, 512Mb RAM - laptop, I was able to build a minimal KDE 4 desktop in 4.5 hours.

And if you run into any problems or have feedback, talk on the Discussion page attached to the Guide or look for 'wstephenson' in #opensuse-kde on Freenode.
<!--break-->