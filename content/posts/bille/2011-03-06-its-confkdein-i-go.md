---
title:   "It's off to conf.kde.in I go!"
date:    2011-03-06
authors:
  - bille
slug:    its-confkdein-i-go
---
I'm feeling very lucky today.  Why?  Because in a few hours I'll be getting on a plane to Bengaluru, India and attending <a href="http://kde.in/">conf.kde.in</a>.  <a href="http://pradeepto.livejournal.com/">Pradeepto</a> has been asking me for years to look outside the cosy confines of the US-Europe Axis of KDE, and thanks to my role in the <a href="http://en.opensuse.org/openSUSE:Boosters_team">openSUSE Boosters team</a>, this has finally become possible.  

I'll be giving a talk on contributing to KDE in the openSUSE project and a long talk/practical workshop on using the <a href="http://build.opensuse.org">openSUSE Build Service</a> as used by openSUSE, Novell, Dell, Intel, Nokia, Broadcom and Cray, to spread free software: your own, update existing software on openSUSE, or package for it and for many other distros at one go.  But mainly I'm looking forward to meeting the people who make up a whole side of KDE.  So if you haven't made up your mind what you're doing next week, how about coming to the RV College of Engineering in Bengaluru?  

Like many others,

<img src="http://kde.in/files/media/badge.png">

PS: I'm bringing a load of openSUSE loot to give away, so just look for the guy staggering under the huge carton!
<!--break-->
