---
title:   "Wanna work on openSUSE?"
date:    2010-08-05
authors:
  - bille
slug:    wanna-work-opensuse
---
Yes, this is basically a job ad. The <a href="http://en.opensuse.org/openSUSE:Boosters">openSUSE Boosters team</a> is expanding again (when will it ever stop?) and we're looking for another member.  If you want to work full time on Linux, enjoy the idea of building a community around the distribution and think you have <a href="http://news.opensuse.org/2010/08/05/were-a-happy-family-the-boosters-are-hiring">the right skills</a> why not <a href="http://www.novell.com/jobs">apply</a> and have the chance to work with me, Lubos Lunak, Stephan Kulow, Klaas Freitag and many other people you know from the KDE and wider Free Software scene?
<!--break-->
