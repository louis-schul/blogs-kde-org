---
title:   "Desktop Pattern reimplemented in Plasma"
date:    2008-11-30
authors:
  - bille
slug:    desktop-pattern-reimplemented-plasma
---
<p>Those of you've been around desktop computers for a while know the calming effect of a simple desktop.  One of the things I've missed is the ability to have a basic tiled pattern with user defined colours.  So since I wanted to learn a bit more about Plasma wallpapers for another project, I rolled up my sleeves, had a look at the old kdesktop sources, and reimplemented it using the latest technologies.  The result is in playground.  Everything old is new again: 

<p><a href="http://blogs.kde.org/node/3780?size=_original"><img src="https://blogs.kde.org/files/images/plasma_wallpaper_pattern.thumbnail.png"/>

<p>Watch out, panel. You're next.
<!-- break -->
<!--break-->
