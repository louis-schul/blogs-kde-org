---
title:   "Don't believe the FUD"
date:    2007-05-23
authors:
  - bille
slug:    dont-believe-fud
---
With apologies to Public Enemy, but KDE has now got recognition of its usability, in that <a href="http://dot.kde.org/1179818755">KDE 3 officially meets ISO 9241</a>.  We might not be the best at marketing it, but more and more people are finding out for themselves about KDE's goodness by picking it up themselves.

While I'm skeptical generally of the application of broad certifications and kitemarks to software, it should help us get more users among those large organisations who do need the "boxes to be checked".

Usability in KDE continues to progress through a mixture of expert advice, as was used in producing the 'Kickoff' menu for openSUSE 10.2, and <a href="http://blogs.kde.org/node/2811">grassroots participation in usability improvements</a>.  I'm enjoying watching and <a href="http://software.opensuse.org/download/KDE:/KDE4/openSUSE_10.2">being a part of the process<a/>.
<!--break-->