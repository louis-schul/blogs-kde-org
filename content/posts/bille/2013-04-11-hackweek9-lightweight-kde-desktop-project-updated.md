---
title:   "hackweek9: Lightweight KDE Desktop project - updated"
date:    2013-04-11
authors:
  - bille
slug:    hackweek9-lightweight-kde-desktop-project-updated
---
It's Hack Week 9 at SUSE, and I'm working on a cracking project this time around. I've codenamed it 'KLyDE', for K Lightweight Desktop Environment, and it's an effort to point KDE at the lightweight desktop market.  Surely some mistake, you say?  KDE and lightweight kan't fit in the same sentence.  I think they can.

This project has been bouncing around my head for a couple of years now, starting on a train ride back from the KDE PIM meeting in Osnabrück in 2010, then <a href="http://www.youtube.com/watch?v=6lfAjdtwECc">I presented it at COSCUP 2012</a> in Taiwan last August. But work commitments and family always got in the way of completing/finishing it.  SUSE's hack week gives me 40 hours to throw at it and this time I wasn't going to tackle it alone, so I enlisted my bro(grammer)s Jos and Klaas.

As has been repeated on Planet KDE over the past decade, KDE is not intrinsically bloated.  At its core, it jumps through a lot of hoops for memory efficiency and speed, and is modular to a fault. But most packagings of KDE take a kitchen sink approach, and when you install your KDE distribution you get a full suite of desktop, applets and applications.  The other major criticism of KDE is that it is too configurable.  The KlyDE project applies KDE's modularity and configurability to the challenge of making a lightweight desktop.  However, what I don't want to do is a hatchet job where functionality is crudely chopped out of the desktop to fit some conception of light weight. Read on after the break to see how we're doing it.

<!--break-->
We're approaching problem from 3 sides:

<b>Minimal footprint</b>

The first method of attacking this is by packaging. It involves factoring optional components of the KDE desktop out of the base installation into subpackages that the main packages only have weak dependencies upon, allowing a minimal installation without them.  This targets big lumps of ram/cpu usage and objects of user hatred like Nepomuk and Akonadi, but also smaller items like Activities and Attica (social desktop support) and non-core window decorations/styles/etc.  The actual KDE build includes everything; the optional components are always available, so those who do need one of them can just add the package and start using it.

The second approach is by configuration.  This allows different profiles of KDE desktop with the same installed packages.  We've collected sets of configs that represent these profiles, but I'm not entirely sure how to package this yet.  One way would be to ship default profiles as X sessions.  Another would be a first run wizard or KCModule so users can select profile and apply it to their configuration after login.

<strong>Simple config</strong>
Is a mixture of usability and perception.  A simplified configuration presents fewer choices and is therefore easier to understand.  It also looks faster and more lightweight, because people equate visual simplicity with efficiency.  This is incorrect, of course, but I'm not above exploiting this fallacy to give people what they want. For this aspect, we're providing an alternate set of System Settings metadata to give it a cut down tree.  The full set remains available, if needed.

<b>Fast startup</b>

Is the most high-risk-for-reward effort.  It's mostly a perception/first impression thing.  A working desktop shouldn't need to be started up all the time.  But for people trying out KLyDE for the first time, a fast startup supports the claim to minimalism.  The interesting thing I note so far is that the package splitting and configuration in 1) makes very little different to startup time.  The optional components of KDE are already highly optimised to not affect startup time.  So I'm investigating alternate startup systems; refactoring startkde, Dantti's systemk, Plasma Active's startactive, and a systemd-managed startup.

<b>Progress</b>

The packaging effort is mostly done; we have packages in an <a href="https://build.opensuse.org/project/show?project=home%3Awstephenson%3Aworkbench%3Aklyde">Open Build Service project</a>, that give you a basic 'KlyDE' Plasma Workspace when installed on top of a minimal X SUSE 12.3 installation with --no-recommends.

Jos has <a href="https://plus.google.com/u/0/117563705675081959469/posts/C2omqikDQfT">put a great effort</a> into understanding System Settings and has produced a simple layout, I just need to complete my patch to allow it to use the alternative metadata scheme at runtime.  If we have time, we'll also customise some KCMs to provide a simple way to control KDE's theming.

I've been busy converting systemd, kdeinit and ksmserver into a native systemd startup by defining systemd unit files.  It's a steep learning curve as it exposes a number of assumptions on both sides, but I'm getting there.  The unoptimised systemdkde.target starts up in 4s here, vs 6s for the same .kde4 started by startkde.  That might be due to legacy/fault tolerance parts of startkde being left out, so I won't give more detailed numbers yet.

<i>Update Thursday 9pm</i>
The freedesktop.org sprint is happening at the SUSE offices this weekend so I had a long discussion with Lennart Pöttering about the systemd session effort.  It left us with a number of open questions such as how to perform XSMP session restore and XDG autostart of apps, tasks which align with what systemd does, and I got some useful tips on how to start up a real session without losing configurability

<b>Next steps</b>

You can see the state of the project on <a href="https://trello.com/board/klyde/515be014e9d9a5cf48007e01">Trello</a>. I'd like to see if there is a startup time  win by parallelizing kded and ksmserver starting modules and apps. I'd like to make an openSUSE pattern for existing installations, and an iso or a disk image for testers.  I've also submitted a talk on the subject for Akademy, so I'd like to work on that and get some real data to support this work.

&nbsp;