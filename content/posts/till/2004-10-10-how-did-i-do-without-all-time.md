---
title:   "How did I do without this, all this time?"
date:    2004-10-10
authors:
  - till
slug:    how-did-i-do-without-all-time
---
I spent a few hours yesterday finally implementing something I've been meaning to do for a long while. Basically since I switched from mutt years ago. You can now assign shortcuts to folders, which means when I hit Alt-I KMail now selects my Inbox. Rather convenient if you have many folders and visit some of them regularly. Of course it also opens subtrees and moves the viewport so the selected folder is visible. After using it for half a day today I really would not want to miss it again, so I imagine a few folks out there will like it as well. :)<br>You can also put the actions on your toolbar, but that is currently not remembered across restarts. I'll fix that and maybe also make a "Favorites" menu which lists them, which I know people have also requested. Feels good to actually scratch your own itch again, for a change, instead of taking care of other people's gripes.<br>On a related note I gave in and actually read many of the /. comments on the eweek KDE/Gnome comparison and was really positively surprised to see that the vast majority of comments was very appreciative of KMail and Kontact. People do seem to like it after all. Nice.<!--break-->