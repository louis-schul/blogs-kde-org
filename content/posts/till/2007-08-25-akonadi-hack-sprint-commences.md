---
title:   "Akonadi Hack Sprint Commences"
date:    2007-08-25
authors:
  - till
slug:    akonadi-hack-sprint-commences
---
Once again we have friends visiting, here at the KDAB Berlin office. So far Will Stephenson, Bruno Virlet, Thomas McGuire, Volker Krause and Kris Koehntopp have arrived for a weekend of Akonadi hacking. Kris (of MySQL) has kindly agreed to have a look at our usage of their system and point out the various errors of our ways. It's already been very productive, we now have a much better idea of what not to do and how to debug what we are currently doing. The rest of the guys are working on benchmarking the other layers, with the goal of proving that Akonadi can actually deliver the kind of performance we need. Bruno has been doing great work on the models for Qt4's model/view framework, on top of Akonadi, as part of his Summer of Code project, and it's great to meet him in person. The picture below shows him in animated discussion with Kris. More exciting things to come as the weekend progresses, I'm sure.
<!--break-->
<img src="http://hubbahubba.de/images/KrisBrunoAkonadi.jpg" />

