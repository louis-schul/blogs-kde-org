---
title:   "A-Tumblin'"
date:    2008-12-30
authors:
  - till
slug:    tumblin
---
Did some tumblin' a few days ago, as a result of me and some tram tracks at <a href=http://en.wikipedia.org/wiki/Hackescher_Markt>Hackescher Markt</a> disagreeing as to the general direction of the front wheel of my bike. This resulted in a banged up wrist and rib, but no damage to the laptop or head. So while I'm still able to perform my primary function at work, namely talk, I am somewhat useless in my primary function at home, namely as a holder of hands while our daughter learns to walk. She has decided that this dire situation necessitates desperate measures and promptly started standing by herself. This works ok, as long as the realization does not hit her that no one is holding her, at which point we're back to tumblin', this time into a pile of cute making happy gurgling sounds, which I am convinced mean "Dad, check out what I did!".<br>

My year has been awesome, folks, and the next one is promising to be even awesomerer. I wish you all the very best, for the new year, out there in KDE land.<br>

I'm also, like, totally stoked to be attending Camp KDE in Jamaica in a couple of weeks time, where I will be preaching the gospel of Akonadi and, more importantly, meeting my Zack! There shall be hugs! There will also be sun and a great bunch of other assorted KDE friends, both of which I can never get enough of. Did I mention my life rocks? :)<br>

<img src=http://people.fruitsalad.org/adridg/pics/dec2008/campkde.png>