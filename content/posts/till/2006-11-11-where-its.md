---
title:   "Where it's at"
date:    2006-11-11
authors:
  - till
slug:    where-its
---
Since <a href="http://blogs.kde.org/node/2530">Ellen's wish is my command</a>, I'm happy to report that as of yesterday I can be considered a bonafide Berliner, having received the keys to our lovely new apartment in what is rapidly becoming the KDE capital of the world. My wife and I are looking much forward to having about twice as much space as before, 17 million times as many decent food and coffee places, galleries, bookstores, concert venues, clubs, and friends and KDE hackers within walking and biking distance. Of course there's the actual move to take care of first, which is about as much fun as debugging random memory corruptions without valgrind. It is for the greater good, though, and the prospect of spending my working days at the brand new KDAB office in Berlin soon, with a bunch of friends (most of them fellow KDE hackers, yes, we're hiring ;)), is an exciting one. Good times. :)<!--break-->