---
title:   "Proud husband"
date:    2006-06-02
authors:
  - till
slug:    proud-husband
---
Since I've already written a very long on-topic entry today, I feel I can get away with an utterly off-topic personal entry, and a shameless plug. After years of translating fiction from English to German, writing articles and being a romance magazine editor, my wife managed to sell a couple of short stories to an American e-book publisher, and they were released on Monday. They've indicated that they'll be wanting to publish more of her stuff. I'm insanely proud of her and happy to see her succeed at what she loves doing. It's cool that she chose my last name (with an added "s", since the Americans always append that anyhow) as a pen name, after refusing it when we got married. ;) Check out her <a href="http://www.lilibadams.com">website</a> for details. Gotta love a woman who writes smut for a living and codes her own CSS. :)<!--break-->