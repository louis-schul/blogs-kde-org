---
title:   "Publishing Calendar Events directly from KOffice"
date:    2010-01-11
authors:
  - robertm
slug:    publishing-calendar-events-directly-koffice
---
One of the great things about KDE 4 is how powerful the APIs for the central components are.  In particular, Akonadi and Nepomuk have become very easy to use in custom software and third party applications.  I recently discovered another very powerful set of libraries: the plugin API for Koffice.  Using those libraries, I recently wrote a little "docker" that lets you attach the documents you are currently working on in koffice to a new calendar event which can be used by any Akonadi-enabled application.  For instance, you could publish minutes of a meeting to korganizer so that they are easier to find . . . and then sync them to your Palm Lifedrive using kpilot.

I initially wrote this plugin to address some of my own needs: I have a submission web site which allows my students to turn in their projects electronically.  The backend is a postgres database which tracks the due dates for the projects in each of my courses.  It also uses CGI scripts to post a list of projects and their due dates on the web page for each course.

Over the summer, I took the time to explore Akonadi by writing an "Akonadi Resource" which uses my project submission web site as a storage backend.  The resource can log in to my submit system, obtain a list of projects, and read them into Akonadi as calendar events.  The project handout (usually a PDF file) is displayed as an attachment to the calendar event.  I can also use it to add, remove, or modify events in the same way.  I can post a new project to my server simply by dragging and dropping the handout file onto korganizer.  

I was amazed at how easy it is to interface custom software with Akonadi.  Writing a resource is ridiculously simple and straightforward.  Most of the work is done for you by the "kapptemplate" tool.  The remaining work only took me a few hours (with some help from the developers on #akonadi).

I have been happily posting projects using my resource, but decided it would be nice if I could post them directly from kword.  After all, I write almost all of my handouts in kword.  Wouldn't it be great if I could just click a "publish" button and let koffice do all the work?

It turns out that the koffice developers have created a very powerful plugin interface which allows you to write "Dockers" that can be embedded in the sidebar.  So this week, I created a docker that does exactly what I wanted -- it pulls up a dialog which asks for project information (such as a due date) and uses that information to create a new calendar event in Akonadi.  It also attaches any currently open (and saved) documents to the event.

While this suits my needs, I wanted to make it more general.  So I wrote an "akonadi docker" plugin that will let you attach all your currently open documents to a new calendar event.  

<img src="http://narnia.homeunix.com/~robert/Screenshots/Kword.png"/>

The "publish" button is in the sidebar on the right, but it can easily be moved wherever you want it to be.

<img src="http://narnia.homeunix.com/~robert/Screenshots/Dialog.png"/>

The publishing dialog allows you to select an Akonadi storage folder and choose a title, location, start date, and end date for your event.  You can also add comments.  

<img src="http://narnia.homeunix.com/~robert/Screenshots/Korganizer.png"/>

You can see that all of the information I entered into the dialog now shows up in Korganizer.  There is a "Streemer Meeting" event at the correct time and place.

I've posted the source code at http://gitorious.org/akonadi-docker/akonadi-docker.  To compile it, simply grab the source with git and then do:

cd akonadi-docker
mkdir build
cd build
cmake ..
make
sudo make install

(You may need to use the -DCMAKE_INSTALL_DIR=[blah] flag to cmake to get everything installed in the correct place).

The code is still pretty rough.  I need to add some checks for exceptional conditions (end date occurs before start date, for instance).  It would also be nice if I could make it possible to only attach the "current" document, not all open documents and ensure that the document has been saved before it is published.  I haven't yet mastered the koffice API to that extent, unfortunately.

Feel free to modify this code in any way you like.  It's not very well commented, so if you want to shoot me questions, I'd love to help.  