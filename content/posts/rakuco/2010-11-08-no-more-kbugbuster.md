---
title:   "No more KBugBuster"
date:    2010-11-08
authors:
  - rakuco
slug:    no-more-kbugbuster
---
Yesterday, I moved KBugBuster from trunk/KDE/kdesdk to tags/unmaintained/4. For those unfamiliar with it, it is/was a frontend to KDE's <a href="https://bugs.kde.org">Bugzilla</a>, so that you could interact with it without a web browser.

Unfortunately, KBugBuster had not seen any bug fix or feature commit in several years, and did not work at all -- the URLs it used to communicate with KDE's Bugzilla were wrong to start with.

Personally, I find the idea of non-browser frontends to our bug tracking system very interesting (I seem to belong to the shrinking group of people who prefer non-web-based alternatives whenever they exist, even when we are talking about something as non-fancy-web2.0-blingy thing as Bugzilla :) ).

Some time ago, <a href="https://metelliuscode.wordpress.com/2008/08/18/deskzilla-what-do-you-think/">Harald blogged</a> about Deskzilla, a Java-based Bugzilla GUI. Even though they have <a href="https://metelliuscode.wordpress.com/2008/08/18/re-deskzilla-they-gave-us-a-site-license/">granted</a> a site-wide license for us to use with our Bugzilla installation, Deskzilla is proprietary software and the fact that it is Java-based does make it feel like it is not properly integrated with the rest of my desktop.

I also remember some people worked on a KBugBuster rewrite in Python some years ago, but it has never left our playground, which means it is still very preliminar work.

So until someone steps up to maintain KBugBuster, it will not be present in KDE SC 4.6.0 or in any further release.
<!--break-->
