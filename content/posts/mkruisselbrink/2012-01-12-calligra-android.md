---
title:   "Calligra on Android"
date:    2012-01-12
authors:
  - mkruisselbrink
slug:    calligra-android
---
<a href="http://heap.kogmbh.net/mek/device-2012-01-12-003719.png"><img align="right" src="http://heap.kogmbh.net/mek/device-2012-01-12-003719_1.png"></a>As most of you are probably well aware, since quite a while it is not very hard to write Qt applications to run on Android devices. A couple of weeks ago we at <a href="http://kogmbh.com">KO GmbH</a> decided to look into how hard it would be to get KDE applications to run, and more specifically, if it would be possible to run Calligra with one of its mobile UI's on an android device.

So after some (sometimes frustrating) hacking, I've got the first results: Calligra Mobile running on an android tablet. There are still lots of rough edges, and not everything works correctly, but as you can see in these screenshots, it does actually run and work. To get to this point I had to make some rather ugly hacks though to work around some of the android limitations.

<b>D-Bus</b>
First of all, I had the problem that android does not come with dbus. Or, to be more precise, dbus is only used internally in android as an implementation detail and 3rd party applications should not try to use it. Because of this lack of d-bus, I basically ifdef'fed out all d-bus usages in the parts of kdelibs that I needed to run Calligra. I wasn't too worried about lack of dbus causing actual issues with Calligra, since from for example the Documents application on the Nokia N9 (which uses Calligra internally) I knew that Calligra doesn't really need any of the runtime deamons/processes that a KDE application generally tries to communicate with via dbus (none of the kde deamons are present on the N9).

<a href="http://heap.kogmbh.net/mek/device-2012-01-12-003825.png"><img src="http://heap.kogmbh.net/mek/device-2012-01-12-003825_1.png"></a>

<b>KSyCoCa</b>
The second major problem (and one I couldn't just get rid of by ignoring it) was figuring out how to get KSyCoCa to work. Calligra is heavily plugin based and relies on the ksycoca functionality to determine what plugins are available, and which are needed to display different filetypes. What I'll probably end up doing (and what is also basically what the N9 does) is include a prebuilt ksycoca file in my package. I tried various ways of getting kbuildsycoca4 to run nicely when needed (one hack even uglier than another), and none of them really worked nicely. Also I don't think it is really too big of a loss to not be able to install extra plugins outside of the application package for a mobile version of Calligra.

<a href="http://heap.kogmbh.net/mek/device-2012-01-12-004230.png"><img src="http://heap.kogmbh.net/mek/device-2012-01-12-004230_1.png"></a>

DBus and KSyCoCa are really the two big problems I had while getting to the point I am now. To get it all to work relatively smoothly I of course had lots of other small things to worry about:<ul>
<li>Removing SOVERSION's from libraries. The way necessitas is designed it kind of fails if your libraries have a SOVERSION set in CMake.
<li>Renaming plugins from foosomething.so to libfoosomething.so. I'm still not sure if it is an android limitation or a necessitas specific thing, but only libraries that start with "lib" seem to end up installed where I can dlopen them.
<li>I'm currently including $prefix/share as part of the "assets" of my android package, this works fine and even somewhat transparantly with the way necessitas wraps this in a QAbstractFileEngine, but since these are not real files on the actual filesystem, I had to patch KStandardDirs to use QDir/QFile instead of direct filesystem access. A cleaner solution might be to extract all the files to some place in the filesystem first, but that would mean duplicating them, since they would also still be in the .apk.
<li>Calligra Mobile used to (unnecesary) link against libQtOpenGL, this caused Necessitas to pick the OpenGL graphics engine, which unfortunately seems to be very buggy. After removing this dependency it reverted back to the raster engine and lots of things suddenly started working.
</ul>

The current state of my code can be found in the following repositories:
<a href="http://quickgit.kde.org/index.php?p=scratch%2Fmkruisselbrink%2Fcalligra-android-build.git&a=summary">git://anongit.kde.org/scratch/mkruisselbrink/calligra-android-build.git</a>
This repository has a bunch of scripts I use to build kdelibs and calligra for android. Lots of hardcoded paths, so don't expect it to actually work for you. Also heavily based on the android build scripts for Quantum-GIS. The existence of those scripts helped me a lot to get KDE to build.

<a href="http://quickgit.kde.org/index.php?p=clones%2Fkdelibs%2Fmkruisselbrink%2Fandroid.git&a=shortlog&h=refs/heads/android">git://anongit.kde.org/clones/kdelibs/mkruisselbrink/android.git</a>
The android branch in this repository has all my current hacks to kdelibs to make it build&run on android. Here also lots of hardcoded paths around as well as a whole bunch of ugly hacks and things that don't realy work yet.

<a href="http://quickgit.kde.org/index.php?p=calligra.git&a=shortlog&h=refs/heads/mobile-android-mek">git://anongit.kde.org/calligra.git</a>
In the mobile-android-mek branch you can find the changes I made to calligra to make it run on Android. Nothing too spectacular here.