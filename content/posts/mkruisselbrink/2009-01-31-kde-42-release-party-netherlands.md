---
title:   "KDE 4.2 Release Party in The Netherlands"
date:    2009-01-31
authors:
  - mkruisselbrink
slug:    kde-42-release-party-netherlands
---
It is of course a bit late to announce, but this sunday (february first), there will be a KDE 4.2 release party in The Netherlands. Everyone is welcome to join us at <a href="http://www.commanderie-nijmegen.nl/index.html">De Commanderie van Sint Jan</a> in Nijmegen, starting from around 14:00.