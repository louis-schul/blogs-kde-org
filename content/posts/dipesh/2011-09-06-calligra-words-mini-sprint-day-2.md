---
title:   "Calligra Words mini-sprint day #2"
date:    2011-09-06
authors:
  - dipesh
slug:    calligra-words-mini-sprint-day-2
---
Today is the second and last day of the Calligra Words mini-sprint. After spending yesterdays <a href="http://blogs.kde.org/node/4476">first day</a> on planing and implementation of decisions made we spent today on solving all kind of problems that showed up since then.

In particular today we land;
* Fix wrong placement of anchored object if the object is anchored in a paragraph that spans two pages, and the anchor is in the part of the paragraph that is on the first page.
* Be sure to only try position an anchor once per layout-run.
* Implement anchor stacking.
* Proper sort paragraph-anchors to fix the paragraph left/right stacking results.
* Fix auto-grow-(height|width) and svg:(height|width) are mutable exclusive.
* Made page up and page down keys work
* Made shift + page up/down work too

Especially the page-spaning, anchor-stacking and auto-grow-height/auto-grow-width changes make huge differences in lot of documents. Together with the ground-breaking decisions made yesterday this makes a real long-term difference. All that was the result of rather productive mind-sharing sessions we had within last 2-3 days. I would even think the produced patches have a higher quality then usual thanks to those sessions.

Compared with the regular Calligra sprints we have from time to time this mini-sprint was different. First we where concentrating on one specific thing we liked to address: ODF Anchors in Calligra. Focusing on that topic helped us to make huge steps forward in that specific area. Second the small number of people attending and the place chosen for that (a participants house rather then the usual unpersonal hotels and/or meeting-rooms) was giving us a way different range of social activities then the usual "party till the early morning" incidents at the sprints.

I can only suggest other hacking-groups within KDE to do the same, become focused on specific things for some days and get to know each other a bit better. Have a look at the <a href="http://ev.kde.org/rules/reimbursement_policy.php">KDE e.V. Travel Cost Reimbursement Policy</a> to get such a focused meeting done.
