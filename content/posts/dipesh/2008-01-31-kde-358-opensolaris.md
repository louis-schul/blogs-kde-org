---
title:   "KDE 3.5.8 on OpenSolaris"
date:    2008-01-31
authors:
  - dipesh
slug:    kde-358-opensolaris
---
See http://blogs.sun.com/moinakg/entry/kde_3_5_8_on

Seems it's possible to give OpenSolaris a try now where a recent KDE3-version is up and running there. Thanks for that SUN/Moinak :)
