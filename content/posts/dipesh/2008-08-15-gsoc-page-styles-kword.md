---
title:   "gsoc: Page-styles in KWord"
date:    2008-08-15
authors:
  - dipesh
slug:    gsoc-page-styles-kword
---
It's a while I blogged last time about the progress on the <a href="http://code.google.com/soc/2008/kde/about.html">goggle summer of code</a> project to <a href="http://blogs.kde.org/node/3457">Improve OpenDocument in KWord</a>. Mainly cause after working on a lot of smaller things till the mid-term we picked one of the bigger tasks and spend last weeks on it: pagestyles also known as masterpages.

In OpenDocument pagestyles define special properties like the size of a page or if headers/footers are visible for one page or a group of pages. The big problem we had was, that KWord was from the ground up never designed to support that and just did handle each page separat rather then allowing to define a group of pages and manipulate there pagestyle in one go. While working it was overall complicated and did introduce lot of problems and limitations in KWord 1.x.

Inherited from the KWord 1.x codebase we did port the design-problem a while back to the new <a href="http://wiki.koffice.org/index.php?title=Flake">flake</a> codebase and just did discover the design-flaw a bit to late. The result was, that a lot of code was already build up on top of those design-mistake what made the refactor-job very difficult since we had to touch a lot of code.

After a long time of planing how to get that task done the right(TM) way we created an own branch and started to break KWord from the ground up. While that part made lot of fun, it was only the beginning to integrate pagestyles and the real work (and pain) started. The initial steps where rather slow and we did not see any light at the end of the tunnel for quit some time. My own believe was, that the amount of work is just to much to finished it in time for the 2.0 release and therefore I did count it as long time project with 2.1 as target.

Planing did not match reality like so often and <a href="http://pinaraf.blogspot.com/2008/08/dipesh-i-think-im-gonna-sincerely-hate.html">Pierre</a> just surprised me with a series of great work to progress and much faster then believed, or to be more exact just today, we where able to merge the changes done on pagestyle within an own branch back to trunk and addressed one of the most annoying problems within the KWord-codebase in time for the <a href="http://wiki.koffice.org/index.php?title=Schedules/KOffice/2.0/Release_Plan">upcoming</a> 2.0 release. So, finally KWord will come with full support for ODF-pagestyles (no GUI to manipulate them there yet but that's a rather small task since the foundation is done and in place now) :-)

All in all; thank you Pierre for the work done during the gsoc (and specially on pagestyles) and thank you google for supporting KOffice and the open AND vendor-independent [1] OASIS [2] OpenDocument standard.

[1] compared to other broken "standards" (hint: I did spend last few months on OOXML and it *is* a broken standard independent of whatever other sources, who did not try to implement it, are writing with a much nicer english then mine)
[2] which does a great and <a href="http://lists.oasis-open.org/archives/office-comment/200806/msg00055.html">open</a> job to improve OpenDocument. Let's hope the ISO will do some day a similar good and open (ok, someone still can dream) job and doesn't continue to risk to lose any remaining trust they may still enjoy and <u>at least</u> does listen to those countries that did speak up against the <a href="http://wikileaks.org/wiki/IEC_central_office_on_the_OOXML_fast_trac_process_2007">OOXML fast track</a> joke.
