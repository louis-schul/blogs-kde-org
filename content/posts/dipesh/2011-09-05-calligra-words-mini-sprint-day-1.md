---
title:   "Calligra Words mini-sprint day #1"
date:    2011-09-05
authors:
  - dipesh
slug:    calligra-words-mini-sprint-day-1
---
Actually it's day #2 already since Boemann and myself already started hacking away yesterday evening when I arrived at his place in Denmark.

The main goal of our small <a href="http://www.calligra-suite.org/news/mini-sprint-about-anchoring-and-text-run-around/">mini-sprint</a> is to address the various problems we have within Calligra Words with anchors and anchoring objects and text running around such anchored objects.

The root of the problem's we where fighting with within last weeks and which did bring us endless pain had it's origin in a <a href="http://lists.oasis-open.org/archives/office/200409/msg00018.html">mail written in 2004</a> to the OASIS mailing-list. SUN suggested introducing the new draw:wrap-influence-on-position element into the OpenDocument specs to allow using different anchoring-modes. This made it possible that all 3 different anchoring-modes supported by OpenOffice.org are part of the official OpenDocument standard.

Back then Calligra representents where already actively participating at OASIS to move the OpenDocument standard forward. But it seems back then we didn't see how much pain adding support for different anchoring modes can bring to consumers of the standard.

Within last hours we came to the conclusion that it's just not possible to support all of the anchoring methods. We never ever could reach a state where our iterative anchoring method would produce the exact same results OpenOffice/LibreOffice do. That is cause the iterative anchoring method is so complex and depends on so much different states, factors, conditions that we would end with spending significant time to figure out how it's done in OpenOffice/LibreOffice Writer to finally reconstruct and rebuild the same in Calligra Words.

The additional complexity, time spend on this and bugs we would introduce is not worth the result. Instead we decided to do the same Microsoft Office does; we only support one of the many anchoring modes; the once-successive. That anchoring method is very simple and super-fast. Since that anchoring method is the only one supported by both, OpenOffice/LibreOffice and Microsoft Office, we have our lowest common denominator. Microsoft Office itself only supports that single anchoring mode in general even if processing the OpenDocument format (supported since 2007 SP2 and the quality is good).

After weeks of pain we know why now. Personally I think it was a mistake to push different anchoring methods to the OpenDocument standard. That makes the standard more complicated for no good reason. But then you probably only come to that conclusion after spending significant time to support all/different anchoring modes.

By sharing this outcome I hope others will come to the same conclusion. So, either support all anchoring mode or focus on one and if you plan to focus on one, let it be the once-successive one cause that one is supported by all office suites.
