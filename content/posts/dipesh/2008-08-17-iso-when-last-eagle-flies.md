---
title:   "ISO, when the last eagle flies..."
date:    2008-08-17
authors:
  - dipesh
slug:    iso-when-last-eagle-flies
---
Just two days ago I was still <a href="http://blogs.kde.org/node/3611">partly optimistic</a> in the hope the ISO would do it's job. Yesterday we got it <a href="http://www.iso.org/iso/pressrelease.htm?refid=Ref1151">official</a>, they don't.

To break the own rule of consensus for things running though the fast-track process, to ignore all the provided feedback, to ignore any protest, to cloud as much of the decision-making as possible while hide-out any details or reasons leaves only one conclusion: The <a href="http://homembit.com/2008/08/openxml-end-of-story-appeals-rejected.html">dying of the ISO</a> starts...

We are enjoying interesting times and are probably allowed to see the beginning of the end of an era of something that was designed to get worldwide standards done. Something we need, we depend on so much. A "fork" of what the ISO was can be seen as opportunity for a fresh restart. The demand to have an <b>open</b> international organization responsible for good, vendor-neutral and usuable industry standards is valid, now even more then before. We will see if attempts like <a href="http://www.digistan.org">The Digital Standards Organization</a> will succeed, if the ISO does pass things on to other organizations who are <a href="http://www.jtc1sc34.org/repository/0940.htm">able to do there job</a> [1] or if it takes more time, more such cases and bigger damages. As usual, only time will tell...

[1] I recommend to read that link to have an idea why it would be such dangerous to just stick with the current situation.
[2] For those that did not recognize the meaning of the title of this blog, I would suggest to listen to <a href="http://www.youtube.com/watch?v=fEIXUNKA9NY">this song</a> and while we are on it, the <a href="http://www.youtube.com/watch?v=RERXiliJfdI">Yoshida Brothers</a> are rocking too [3] :)
[3] Flash is not ISO (yet?) plus I don't know anything about the quality of the format but it can't be more worse then OOXML which does include various other formats like CustomXML, client-server sharepoint communication, etc. and all of them are in the "ISO standard" construction-kit now. If they had any clue <a href="http://weblog.infoworld.com/openresource/archives/2006/05/the_future_of_l.html">what</a> [4] they signed there? I somehow still doubt so.
[4] Read the comments as well. Now it may clear why OOXML is NOT about a file-format but a new protocol. Now it may also clear why e.g. IBM, google and Apache are interested in that protocol (pure guess for sure) as well cause it's there to expand from the desktop to the network and internet (that's btw also the or one of the reasons why the EU forced Microsoft to open there formats including the OOXML-format and why Outlook uses the Word-engine to write mails and so on). Lock-in everywhere, welcome new and still unpublished ISO-standard.
