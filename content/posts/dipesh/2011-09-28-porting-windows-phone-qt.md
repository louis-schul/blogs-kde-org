---
title:   "Porting Windows Phone to Qt"
date:    2011-09-28
authors:
  - dipesh
slug:    porting-windows-phone-qt
---
Since so far I missed any blog on our beloved planet pointing to this interesting resource I do it myself now;

<a href="http://www.developer.nokia.com/Resources/Library/Porting_to_Qt/windows-phone-to-qt.html">Porting Windows Phone to Qt</a>

A rather cute way to escape your Windows Phone lock in :-)
