---
title:   "KSpread 2.0 Scripting"
date:    2006-11-16
authors:
  - dipesh
slug:    kspread-20-scripting
---
In the category of scripting in KSpread 2.0 using Kross2 the <a href="http://kross.dipe.org/kspread2scripting/">KSpread 2.0 Scripting</a> article provides an overview of what's possible with KSpread based on Qt4+KDE4 this days.

Seems KOffice isn't that far away from world-domination :)