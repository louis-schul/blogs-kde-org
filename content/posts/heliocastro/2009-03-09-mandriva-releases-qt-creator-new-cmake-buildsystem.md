---
title:   "Mandriva releases qt-creator with new cmake buildsystem.."
date:    2009-03-09
authors:
  - heliocastro
slug:    mandriva-releases-qt-creator-new-cmake-buildsystem
---
Yes, sounds strange. Why should i did it ?
Well, first is known among the distros that qt-creator in first release is not the most friendly to packaging.
Not only beacuse qmake, but because some distros rely on things like splitted packages or 32/64 bits coexistent libs and plugins.
Second, was quite a challenge for me do this, because i need to learn some new tricks ( thanks dfaure for automoc4 help ).
Third, dressing my packager hat, i would love to see qt-creator been in the main Mandriva distro, compliant with our policies, and been adopted as well i expect for kdevelop4 when be released. Qt Creator is one of the amazing IDE's around and i thought that worth the effort to give some love in buildsystem to have it in a full Mandriva way, including our flags and standard cmake build.
During the process, i could manage to fix all bugs open in our bugzilla about packaging, and at same time enable the "most wanted" feature here, the designer plugin, which for some reason was not enabled in our qmake previous compilation.

To summarize what i did:
- Write whole cmake buildsystem
- Backported icons and mime and desktop files from upstream binary Qt Software package ( as same as Kubuntu did )
- Make the plugin standard dir be the Qt plugins standard dir, which make possible have 64 and 32 bits plugins instalable
- Make qt-creator easily compliant to /usr linux install
- Splitted libraries in subpackages as usual, and installed in our standard libdir instead of a subdir
- Reenabled designer plugin.

So, now i'm totally happy with all the changes, barely minimal in code ( most moc include additions and add standard Qt plugin path ).

For who is interested, our svn repository is <a href="http://svn.mandriva.com/cgi-bin/viewvc.cgi/packages/cooker/qt-creator/current/">Qt Creator Package Repos</a>
For who wants to try our marvelous distro, today we will be releasing RC1 from Mandriva 2009 Spring, so you will be able to see how hard we are working to make a better distro for you...
<!--break-->