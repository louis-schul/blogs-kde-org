---
title:   "KDE 3.4.1 Solaris 10 IA32/UltraSPARC-II Released!"
date:    2005-07-14
authors:
  - stefan teleman
slug:    kde-341-solaris-10-ia32ultrasparc-ii-released
---
This past Sunday (July 10) i finally released KDE 3.4.1 for Solaris 10, for both Intel/AMD and UltraSPARC-II. Both of them built with the Sun Studio 10 compilers. Yes, you can install Solaris 10 on an Intel based PC and run KDE 3.4.1 on it. Pentium IV minimum, or AMD. Or UltraSPARC-II minimum.

You can see screenshots <a href="http://www.flickr.com/photos/skipjackdes/">here</a>, or <a href="http://impliedvolatility.blogspot.com/">here</a> in larger sizes.

Unlike the last time, this time around you can really see the screenshots :-P

There's a lot of good talk about KDE <a href="http://www.opensolaris.org/jive/thread.jspa?threadID=1052&tstart=15">here</a> and <a href="http://www.opensolaris.org/jive/thread.jspa?threadID=1151&tstart=0">here</a>, which are part of the discussions at <a href="http://www.opensolaris.org/os/">OpenSolaris</a>. And there is also a KDE Solaris link <a href="http://www.opensolaris.org/os/links/">here</a>. The inevitable conclusion of all this KDE activity at <a href="http://www.opensolaris.org/os/">OpenSolaris</a>, is that us OpenSolaris dudes are a friendly bunch. Right ? :-)

So, i was wondering, in a very existential way, maybe the KDE 3.4.1 Solaris distro deserved a link on the KDE binary packages page ? :-P

Seriously now, it came out really nicely. i still have to finish KOffice.

And feel free to take this blog as an invitation to drop by <a href="http://www.opensolaris.org/os/">OpenSolaris</a>, and say "hi, i'm a KDE developer". :-)






