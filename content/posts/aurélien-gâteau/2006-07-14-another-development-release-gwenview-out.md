---
title:   "Another development release of Gwenview is out"
date:    2006-07-14
authors:
  - aurélien gâteau
slug:    another-development-release-gwenview-out
---
<p>
I am happy to report that I was finally able to release another development release less than one month after 1.3.91 :-). So here it is: another <a href="http://gwenview.sf.net">development release of Gwenview: 1.3.92</a>.</p>

<p>Most fixes from this version are bugs reported on bugs.kde.org.<br>
I must confess I neglected Bugzilla for a while, but I am trying to get through the current list of bugs now, marking already-fixed bugs as closed, fixing others when possible... If you made a bug report some (hopefully not too long) time ago, you might hear from me... just don't hold your breath though, the list is quite big!</p>

<p>Speaking of such a big list of bugs, I would be interested in hearing about the way other developers deal with Bugzilla. Do you visit it often, use some bookmarked queries? do you prefer working with the mails it sends? or do you work in another way I haven't heard about?
<!--break-->