---
title:   "Gwenview progress"
date:    2007-06-24
authors:
  - aurélien gâteau
slug:    gwenview-progress
---
Long time no blog (It seems most of my blog entries begin like that...). In case you missed it, Gwenview has moved to kdegraphics, yeah! I am very happy about it.

Has I keep saying here and there, the KDE4 port of Gwenview is more a rewrite than a port, at least from the user perspective (I try to port as much as possible of the available internals). Since everybody love screenshots, here is one of the current SVN version:

[image:2845 width=300]

Main change compared to the <a href="http://blogs.kde.org/node/2660">Gwenview 2 mockups</a> I posted some time ago is that the thumbnail view has moved from the bottom edge to the left edge. After using Gwenview 2 for a while, I realized having this view on the left edge was a much better way to use screen space because nowadays screens are getting wider, while digital pictures are still using 4:3 ratio. Of course new digital cameras now support wide picture formats, but I am not sure this feature is going to be used for every picture, and anyway we will still shoot portrait-oriented pictures, which really need as much vertical space as possible.

In case you're wondering, it's already usable in its current state, even if it's a bit limited and contains a few unwanted features (aka bugs) for now.
<!--break-->