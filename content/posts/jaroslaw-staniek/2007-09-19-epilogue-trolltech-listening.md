---
title:   "Epilogue: Trolltech is listening"
date:    2007-09-19
authors:
  - jaroslaw staniek
slug:    epilogue-trolltech-listening
---
Not even 2 days after out <a href="http://blogs.kde.org/node/2997">meeting in Berlin</a>, there is an epilogue of the story: <a href="http://labs.trolltech.com/blogs/2007/09/18/qtwindows-open-source-edition-to-support-vs-express/">Qt/Windows Open Source Edition will support VS Express</a>. 
<!--break-->

Big thanks for considering this!

There are always many comments about boundary between free and non-free tools. I can give two notes here:
<ul>
<li>Q: Hey, why to add dependency on these tools?<br/>
A: The word "dependency" here looks like manipulated. Qt technology is about single-source-code projects and clean portable design. For example in my recent test KWord <a href="http://blogs.kde.org/node/2997">compilation</a> I have not added even one #ifdef. If there are any specifics in such portable development, they usually come from operating system, not compiler. In the worst case most often you fall back to Qt equivalents of the given KDE technology instead of relying on anything built in-house. For example in KDE 3/Windows I have uses QFileDialogs in place of KFileDialogs. In KDE 4 there is improvement in form of abstract file dialog interface. But again, this is not related to compilers.</li>
<li>Q: Oh, but I use mingw and it compiles quite fine!<br/>A: Many of us are concerned about all the development tools (fast compilation, linking, debugging), not just compilation. I wish I can see some large entity like Google or a Linux vendor supporting gcc project heavily to make its internals become a present-day technology (surprise, we have more than 32MB RAM and 500MB HDD today, but still gcc, being unbeatable in cross compilation, unfortunately creates unoptimized temporary files with linear lookup cost). So far, some people do abandon mingw project (see the Christian's link below), and even <a href="http://developers.slashdot.org/comments.pl?sid=299969&cid=20638303">not satisfied</a> with gcc as a project someone from the outside would like to contribute to.</li>
<li>Q: But I don't especially admire vendor of this compiler... Does msvc Express allows for Open Source evelopment at all?<br/>A: Yes it does. And there are also Intel's and SUN's compilers available for KDE you may want to try... Again, in the worst case if the vendor stops the support, you still have _single-source-code_ project, and abstracted in terms of design (you pay attention to this, do you?), so you can move with your development to other tools or even operating system. It's especiallny not a problem for folks like me, who do the development in parallel on Linux, Windows and sometimes on the Mac.
</ul>

For some notes about gcc on windows read <a href="http://chehrlic.blogspot.com/2007/09/two-beer-or-not-two-beer-or-what.html">Christian's blog entry</a>.
