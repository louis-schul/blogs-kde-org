---
title:   "CHIP, better than ever"
date:    2006-07-27
authors:
  - jaroslaw staniek
slug:    chip-better-ever
---
Do you remember my <a href="http://blogs.kde.org/node/1571">complaints</a> about KDE or KDE apps mentioned nearly *zero* times in CHIP?

<!--break-->
Now looks like the magazine has been improved in the area. At least the current 08/2006 issue. There's even a "KDE or GNOME, what's better?" article.

SUSE folks cannot be upset too. SUSE 10.1 on the cover is called by CHIP no less than <i>"The world's safest operating system"</i>. 

Funny, the subtitle says <i>"Functionality of Windows XP & stability of Linux in one"</i>, while the article inside admits KDE flexibility and configurability easily beats Windows XP. KDE has been also picked over GNOME by CHIP editors for availability of easy steps for making it look like XP. Well, better than nothing. (click for the <a href="http://magazyn.chip.pl/images/1372480_65af9569da.jpg" target="_blank">cover</a> and <a href="http://magazyn.chip.pl/chip_174421.html" target="_blank">table of contents</a>).

At the very last sentence an editor wrote that there are chances for KDE4 to oust Vista. On the next page there is a column named "WinFS dropped from Vista". So what is the magical Vista technology KDE4 has to defeat? 3D desktop expose function in Vista? Forgive me, but it is considered as an average copy of already shipped <a href="http://en.wikipedia.org/wiki/Xgl" target="_blank">XGL</a> or <a href="http://www.sun.com/software/looking_glass/" target="_blank">Project Looking Glass</a>. Yes, I recently learned that editors sometimes do not know about each other's articles until these are already printed...

I am curious whether, say, german edition of CHIP is behind the polish one in the area of noticing KDE or GNOME? Are advertisers push more aggresively there against standards and open source? Or is this just a slack season in Poland what I encountered?
