---
title:   "Yeah, Mediawiki rules"
date:    2006-04-24
authors:
  - jaroslaw staniek
slug:    yeah-mediawiki-rules
---
As I agreed with most of these complaints (http://blogs.kde.org/node/1952#comment-4773) and people know I am complaining about the current wiki.kde.org engine as often as possible - here are news/issues on the topic:

<ul>
<li>Yes, http://appeal.kde.org/wiki/Appeal Mediawiki skin already exists and I'd say I love it. Is this going to be the new web style for KDE4 (Appeal-compatible, etc.)? I could help with a compliant skin for Mediawiki in terms of design/development. 
<li>For instance, look at http://www.kexi.pl/wiki/ (content and menu in polish but it's irrelevant) - it already uses <a href="http://wiki.kde.org/tiki-index.php?page=Colors">KDE4 color palette</a>, it's valid XHTML and nice CSS. Images are not clickable here, and thanks to some subtle changes, the web pages are more like regular pages than Wikipedia content.
<li>It is relatively easy to develop plugins in a (relatively) clean way that, e.g. to add auto-links to Qt or KDElibs API docs if you write QWidget or KApplication word.
<li>Look how the layout of kexi.pl Mediawiki pages were modified: unlike  Wikipedia pages, there is no left-hand menu that takes too much space (you can switch it on again of course), so the layout is _cleaner_, you can publish docs containing large images, you are able to keep your content inside ~800-pixels-wide page. After logging in, editing menu appears (Special Pages, Last changes, etc.), take a look <a href="http://iidea.pl/~js/kexi/kexi_pl_editing.png">here</a>. Below that, Content menu is presented with Edit, Discuss, etc. links.
<li>The last but not least, recently I've managed to find a developer contributing with PHPWiki->Mediawiki converter for http://kexi-project.org/wiki/wikiview/ ; after that maybe Tikiwiki->Mediawiki for wiki.kde.org will be possible.
</ul>

Summing up: nothing special but I believe it's a good way to make the wiki pages clean and pleasant for both authors and readers.

See also http://dot.kde.org/1110293679/1110315655/
