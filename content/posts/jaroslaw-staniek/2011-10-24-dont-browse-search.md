---
title:   "Don't browse, search"
date:    2011-10-24
authors:
  - jaroslaw staniek
slug:    dont-browse-search
---
"Don't browse, search" is already well known demand and it changes the way people expect to use your app.

Recently I needed to finalize first incarnation of my <a href=http://community.kde.org/Kexi/Search>Global Search</a> feature in Kexi. Being late by two days after Calligra beta 3 tagging, I made it fully working only for RC1 now. But it's here.
<!--break-->
What I needed is line edit with completion. You'd say KDE has KCompletion. Well, <a href="http://lists.kde.org/?l=kde-core-devel&m=131923627114896&w=2">it could be improved</a> e.g. in areas like model/view support. 

So I switched my interest to Qt4's QCompleter. It's flexible regarding model/view but lacks some overloading options for searching so no full text search. It's not secret that Qt Creator has the lead in such features but even Qt Creator guys created something outside of Qt API.

So what I did is a light temporary fork of QCompleter and friends now available as private <a href="http://quickgit.kde.org/?p=calligra.git&a=tree&f=kexi/kexiutils/completer">kexi/kexiutils/completer</a> and extension to KLineEdit (based QLineEdit's internals related to completion) as <a href="http://quickgit.kde.org/?p=calligra.git&a=blob&f=kexi/main/KexiSearchLineEdit.h">KexiSearchLineEdit</a>. I am open to contribute all or parts of that either to KDE or Qt Frameworks (yay, <a href="http://qt-project.org">http://qt-project.org</a>!).

It's all generic enough and finished. You'll get <a href="http://kexi-project.org/pics/2.4/rc1/google-suggest.jpg">Google Suggest</a>-like or <a href="http://kexi-project.org/pics/2.4/rc1/firefox-bar.jpg">Firefox URL/search bar</a>-like widget with highlighting matched areas and full-text search. Neither of these features were available in KDE or Qt libs. 

What's more, there is thin interface that you implement to make any area of your app's UI searchable using the very same global search. So far Kexi's Project Navigator is such area so searching locates objects like tables, queries, forms in real time and highlights them. Press enter and you go to given item so it can be opened quickly. 

<img src="http://kexi-project.org/pics/2.4/rc1/kexi-2.4-global-search.png">

Next area for the global search can be the Tabbed Toolbar. Executing actions by typing (parts) of their names would be then available. And again, it's already pretty generic framework. But this is for Calligra 2.5 or 3.0. Now back to bug fixing.