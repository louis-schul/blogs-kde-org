---
title:   "Anniversary"
date:    2009-12-06
authors:
  - jaroslaw staniek
slug:    anniversary
---
<p>
Today we enjoy 11th anniversary of disclosure of Microsoft's best business and development practices:
</p>
<p>
<a href="http://lh6.ggpht.com/_FBlhhjjfQMQ/SxpWwLizqOI/AAAAAAAAGms/pXzb-JexFNc/s912/bill_gates-nq8.png"><img src="http://lh6.ggpht.com/_FBlhhjjfQMQ/SxpWwBgzryI/AAAAAAAAGmw/UJHG0oZMXuA/bill_gates-nq8s.png"/></a>
</p>
<!--break-->
<p>
The concept has been reportedly in development since early 90-ies:
</p>
<p>
<a href="http://lh3.ggpht.com/_FBlhhjjfQMQ/SxqxizdtZ2I/AAAAAAAAGm4/iT1FJrCkiTw/s800/bradsi.png"><img src="http://lh5.ggpht.com/_FBlhhjjfQMQ/SxqxjMf20II/AAAAAAAAGm8/gJiolaGquLQ/bradsi_s.png"></a>
</p>
<p>
The practical and consistent extension of that achievement is introduction of the MSOOXML format:
<ul>
<li>It is <a href="http://www.theopensourcerer.com/2008/02/20/the-deprecated-%E2%80%9Csmoke-screen%E2%80%9D-of-ms-office-open-xml-ooxml/">not compatible</a> with ISO standard Office Open XML</li>
<li>But those that use MS Office exclusively and upgrade frequently do not really care; moreover it is not a problem even for them to hear the mistyped OpenOffice XML name from time to time</li>
<li>Office Open XML unlike MSOOXML has no implementation so far</li>
<li>It is not clear whether Office Open XML is even implementable because of contradicting parts</li>
<li>There is no something like MSOOXML specifications, instead there is something like "report from the current state of implementation", which has already been updated at least once and will be again for MS Office 2010</li>
<li>MSOOXML is advertised as a dump of the binary DOC/XLS/PPT formats into the XML layer, and thus is a great advantage according to the creators</li>
<li>The advantage is so big that as authors suggest, breaking XML principles is not a big problem (there is no known attempt to address the issues)</li>
<li>Since MSOOXML, unlike ODF, was created behind closed doors, there was no planned or possible harmonization between the two; now since it is too late (there are implementations of ODF and MSOOXML) "interoperability" term is the new modern buzzword, but at least in Poland noone knows what this word really means (and yes, this is an apparent advantage)</li>
<li>For making the binary->XML dump extensive documentation of the former was needed; so since finally Microsoft created docs, and the binary formats got declared as obsolete anyway, publishing is a double PR win ("we are so open", and "we're now OK in court cases")</li>
</ul>

The binary DOC/XLS/PPT formats are now somewhat documented in the public but this is not the end of the story.
I've been mentioning this before and I'll trow this chair again: there are still MS Access file formats, as closed as in 1992, except for the huge work of never finished reverse-engineering. The current attitude is as follows:

<blockquote>
<p><b>When will Microsoft release the binary specs for Access MDB files?</b></p>

<p>Q: I need to be able to access these files on Unix / Linux and possibly mainframe based computer systems. Read access is all that is required. I need official Microsoft documentation or as an alternative routines written in C++ that can b used to compile on a Unix platform and access the MDB files.</p>

<p>A: Currently, we do not have any plans to document the Access MDB file format, or the SQL Server MDF file format.</p>

<a href="http://social.msdn.microsoft.com/Forums/en-US/os_binaryfile/thread/72fd99c6-ece0-4e74-9c32-c65603739a30">more...</a>
</blockquote>

EOT.

Assuming nobody like a bigger government player or European Commission is interested in openess of the format (our data!), it can still be kept as closed forever.