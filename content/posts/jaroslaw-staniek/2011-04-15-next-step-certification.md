---
title:   "Next Step in Certification"
date:    2011-04-15
authors:
  - jaroslaw staniek
slug:    next-step-certification
---
Even if you are certified Qt developer already, I definitely do not recommend to stop but <a href="http://blog.qt.nokia.com/2011/04/15/new-certification-level-launched">continue the certification effort</a>.

One extra point is that for someone like me preparing for exams means having a closer look at technologies I do not use so extensively everyday, e.g. QtScript. Nothing beats the developer's practice of course but learning for exams is almost as good excuse for taking the time as preparing Qt presentations. You have a chance to <i>accidentally</i> learn something relatively new and unnoticed.

<a href="http://kexi-project.org/pics/ads/qt-cert.jpg"><img src="http://kexi-project.org/pics/ads/qt-cert_sm.jpg"></a>

I can imagine next steps: 1. Qt Contributor certification which would play nice with the Open Governance; 2. Qt-style API Designer certification (materials <a href="http://developer.qt.nokia.com/wiki/API_Design_Principles">here</a>). For improved transparency both papers would ideally be issued by Linux Foundation...

What area of certification would be interesting for you? Please leave a comment.
