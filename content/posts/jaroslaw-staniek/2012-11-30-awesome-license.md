---
title:   "Awesome License"
date:    2012-11-30
authors:
  - jaroslaw staniek
slug:    awesome-license
---
Good news. The <a href="http://fortawesome.github.com/Font-Awesome/">Font Awesome</a> project's founder responded at speed of light to my request and now he plans to release the icon set under the <a href="http://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License">LGPL</a> within a month or so, making the icons as accessible for reuse as the default Oxygen set.

After that the "only" remaining matters would be adaptation to KDE and artistic vision. Here's a mockup of how <a href="http://www.kde.org/applications/utilities/kwrite/">KWrite</a>'s look could be transformed. Original:
<img src="http://kexi-project.org/pics/blog/2012/awesome-icons-kwrite-orig.png"/>

To make it play more right I also reworked the Oxygen style quite a bit. Nearly no gradients or shines, post-Vista aesthetics inspired by web and mobile UIs where contrast is king. "Awesome" style:
<img src="http://kexi-project.org/pics/blog/2012/awesome-icons-kwrite.png"/>

Last week fri13 <a href="http://blogs.kde.org/comment/9345#comment-9345">commented</a>: <i>"Monochrome icons are terrible from usability standpoint. They don't offer at all same information as colorful skeuomophisim icons."</i>

Whoa about the colorizing the icons to give benefits of <a href="http://en.wikipedia.org/wiki/Color_code">color coding</a>?
