---
title:   "KEXI 3"
date:    2016-06-01
authors:
  - jaroslaw staniek
slug:    kexi-3
---
Two pictures worth thousands of <strike>SLOCs</strike>^w words.

All the modernization KEXI 3 receives these months may be enough to start using upper case letters for the name :)

Spolier: 0% of mockups here, top picture: kexi.git master, bottom picture: to-be-published GUI. 

<a href="/sites/blogs.kde.org/files/kexi-3.0-compare.png"><img src="/sites/blogs.kde.org/files/kexi-3.0-compare.png"></a>

Key words for the GUI:
- Qt-Creator like global view selector
- better suited for Mac and Windows versions (native menus are back!)
- superflat style with 1990's paddings removed but with with some "material" touch
- content-is-king approach: darker side panes don't stay in the way
- QWidget-based but styled beyond what QWidgets typically can do
- Breeze style and icons-optimized (planned as the default on all OSes for 100% of portability)
- icons styling beyond Breeze: some icons lighten and get colorful on dark backgrounds
- the Kexi/Create/Design button bar will be replaced with something else