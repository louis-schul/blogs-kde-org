---
title:   "Can Web Developers Scale?"
date:    2010-10-11
authors:
  - jaroslaw staniek
slug:    can-web-developers-scale
---
McGrath's dream, linked by <a href="http://bergie.iki.fi/blog/the_web_and_the_free_desktop/">Henri</a>:

<b><i>"There's a huge pool of web developers with HTML and JS skills that can now contribute to desktop apps. Watch how fast GNOME and KDE advance when this happens."</i></b>

Can Web Developers Scale?

It is obviously not a new story. Nicely <a href="http://lwn.net/Articles/408052/#CommAnchor408105">commented by elanthis</a>:

<b><i>"That's like saying that there's a huge pool of high-school physics teacher who can now contribute to solving cold fusion. [..]"</i></b>

Really I admire talented web devs. But I think while most of developers are web developers, most brilliant developers are not the web ones. And Oh, I remember Aaron having some fun when noticing people are <a href="http://aseigo.blogspot.com/2007/05/on-mistaking-internet-for-interface.html">excited that drag and drop works in their nice browser-is-my-desktop paradigm</a>. I had too. And still have, say, after noting that just dragged random static graphical element in a browser by accident.
<!--break-->
