---
title:   "Google Gears"
date:    2007-05-31
authors:
  - jaroslaw staniek
slug:    google-gears
---
Google uses the same db backend as Kexi for offline operations. 
<!--break-->
I mean: SQLite.

http://code.google.com/apis/gears/api_database.html

Interesting times. This makes it possible to implement live connectivity to offline resources from KOffice and KDE itself. Then, after going online, the data can return to Google's server. With <a href="http://www.kexi-project.org/docs/svn-api/html/namespaceKexiDB.html">KexiDB</a> it could also be automatically exported/replicated to your very own database, no matter it is MySQL, PostgreSQL or ODBC-compliant backend. 

The idea of integrating Web Services and Desktop was also covered by "<a href="http://blogs.kde.org/node/2744">Web Interface for KDE Data Forms</a>" KDE GSoC project.
