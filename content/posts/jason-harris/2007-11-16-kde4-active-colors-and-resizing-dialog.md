---
title:   "KDE4 \"active\" colors, and resizing a dialog"
date:    2007-11-16
authors:
  - jason harris
slug:    kde4-active-colors-and-resizing-dialog
---
I need help on some things which ought to be trivial.  I'm working on the KStars Observing List tool, depicted here:
<br>
<img src="http://www.30doradus.org/kstars/obslist.png">
<p>
Ain't it pretty?  The first trouble arises if the TableView listing the objects loses input focus.  Then it looks like this:
<!--break-->
<img src="http://www.30doradus.org/kstars/obslist_unfocus.png">
<p>
So it's just about impossible to see what objects are selected, and the only way to restore the active colors without altering the selection or column ordering, is to click on the vertical scrollbar (if there is one!).  If these "disabled" colors appeared when the <b>dialog</b> lost focus, that would maybe be acceptable.  However, as it is now, if any other widget in the dialog gets focus, I get these "disabled" colors in the TableView.  This is obviously a usability nightmare; how do I make the "active" colors persist as long as the dialog has focus (or heck, keep the active colors no matter what...that would be better than the current situation)?  Any advice would be most appreciated.
<p>
The second thing I need help with is resizing the dialog.  The button in the upper right is supposed to present a more compact version of the tool, in case the user would like to keep it open without it taking up too much of the screen.  This button hides all columns except the first, and shrinks the Action buttons so they only contain one letter each.  This all works, but it simply refuses to resize the dialog itself to match the contents:
<br>
<img src="http://www.30doradus.org/kstars/obslist_minbad.png">
<p>
What I want to happen is the following (which I achieved by manually resizing the Dialog):
<br>
<img src="http://www.30doradus.org/kstars/obslist_mingood.png">
<p>
Does anyone know how to make it do this resizing automatically?  I've tried resize(), adjustSize(), everything I could think of.
<p>
Thanks!
