---
title:   "Back to Gentoo!"
date:    2005-10-28
authors:
  - jason harris
slug:    back-gentoo
---
Update from my last post: After failing to install the "easy" distros (Suse 10.0 and Kubuntu), I decided to install Gentoo again.  Went flawlessly, and I'm loving emerge.  Now, why did I ever want to try another distro?  Can't remember :)

Porting KStars to Qt4 is going pretty slowly; I don't have much time these days.  Still, it's coming along.  The new SkyComponents architecture is mostly in place, and should make the code much easier to maintain (thanks Thomas!).  Now that that job is done, I've started actually debugging compile errors.  It's repetitive, boring work, but I just keep thinking of seeing KStars runnning under Qt4 for the first time, and that keeps me going.  I'm dying to see the sky display with native antialiasing.  :)
