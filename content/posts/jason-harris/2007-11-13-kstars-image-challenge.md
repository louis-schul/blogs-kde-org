---
title:   "KStars Image Challenge"
date:    2007-11-13
authors:
  - jason harris
slug:    kstars-image-challenge
---
Do you want to help improve KStars, but don't want to do programming or debugging?  Do you like pretty pictures?  

Over at the KStars Community Forums, I have announced the <a href="http://kstars.30doradus.org/viewtopic.php?p=239">KStars Image Challenge</a>.  We need more pictures of celestial objects for our Details window.  Right now we have most of the Messier objects and about 40 NGC objects; you can see the current images here: http://www.30doradus.org/kstars/detail_thumbs/
<!--break-->

Here's an example of what the image looks like in the Details window:
<img src="http://www.30doradus.org/kstars/kstars_detail.png">

See the Forums post for details on how to submit images.  Have fun!


