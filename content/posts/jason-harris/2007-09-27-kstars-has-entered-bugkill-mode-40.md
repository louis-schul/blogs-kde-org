---
title:   "KStars has entered BugKill mode for 4.0"
date:    2007-09-27
authors:
  - jason harris
slug:    kstars-has-entered-bugkill-mode-40
---
Howdy KStars fans,

Just a quick note to let you know that we are now in BugKill mode: no more new features for 4.0, just polishing and bugfixes.  Here's what KStars looks like these days:
<!--break-->
<p>
<img src="http://www.30doradus.org/kstars/kstars4.png">
<p>
I encourage you to try it out for yourself.  It's quite useable; most things now work as they should.  Thanks to heroic efforts by James Bowlin, it's now more efficient than kstars 3.5.x in terms of CPU usage, and the new HTMesh architecture should allow us to scale up the number of stars in a future version (thus silencing a frequent complaint by our critics).
<p>
Anyway, we could use your help.  If you want to help us uncover bugs and misbehaviors, we're organizing our QA efforts <a href="http://wiki.30doradus.org/index.php/KStars_Quality_Check">here</a>.  You can also feel free to report any findings on our mailing list.
<p>
Thanks a lot!
