---
title:   "I love the desert"
date:    2006-05-09
authors:
  - jason harris
slug:    i-love-desert
---
I live in <a href="http://maps.google.com/maps?f=q&hl=en&q=Tucson,AZ&ll=32.221667,-110.925833&spn=4.980797,10.83252&t=h&om=1">Tucson, Arizona</a>, in the heart of the <a href="http://en.wikipedia.org/wiki/Sonoran_desert">Sonoran desert</a>, among the <a href="http://en.wikipedia.org/wiki/Saguaro">Saguaro</a> and <a href="http://en.wikipedia.org/wiki/Javelina">Javelina</a>.  I love it here; the desert has a unique kind of beauty: vast, sparse, ancient, and enduring.  I love the spectacular sunsets, and the spicy smell after a long-needed rain.  I love the tough resilience of desert life, which still somehow manages to reveal a <a href="http://marvin.as.arizona.edu/~jharris/gallery/v/tucson/tuscon_outdoors/sabino_blooms/0_G.jpg.html">fragile beauty</a> now and then.

Last weekend, I did a ten-mile hike to the summit of Mt. Kimball, in the <a href="http://en.wikipedia.org/wiki/Santa_Catalina_Mountains">Santa Catalina mountains</a>.  This hike goes past Finger rock, which is a really distinctive feature that can be seen from all over Tucson.  

Here's a picture of me at the top of Mt. Kimball, with Mt. Lemmon (the apex of the Catalinas) in the background:
<a href="http://marvin.as.arizona.edu/~jharris/gallery/v/miscellany/brief/Jason+on+Mt_+Kimball.html"><img src="http://marvin.as.arizona.edu/~jharris/gallery/d/31313-2/Jason+on+Mt_+Kimball"></a>

There's nothing like being on a mountain!
