---
title:   "Using KDevelop 3 for KDE 4 development"
date:    2006-08-25
authors:
  - amantia
slug:    using-kdevelop-3-kde-4-development
---
I wanted to write down my experience with how good KDevelop 3 is for KDE4 development since a long time, but never had the time for it. I used it since I started to work on KDE4 porting, and I can say that it is usable with some shortcoming and rough edges.
 First of all you have to use the upcoming KDevelop 3.4, not the latest stable release. This is an improved version of KDevelop 3.3.x, with big changes in debugging - it is supposed to work better with Qt4, but there are some issues left - and autocompletion. It also has better cmake support, which is important for KDE 4 development. The KDevelop 3.4 can be found at /home/kde/branches/kdevelop/3.4 in the KDE subversion repository.
 Once you have it you have to generate a KDE 4 project file for it. I use as an example an existing project, namely Quanta which is in kdewebdev module. So check out kdewebdev from trunk and run cmake in the source directory by specifying the -G KDevelop3 option, which will generate a KDevelop3 project file. I use the following cmake command:
 cmake -DCMAKE_INSTALL_PREFIX=path_to_kde4_install_dir -DCMAKE_BUILD_TYPE=debugfull path_to_source_dir -G KDevelop3

 You can see that there are *.kdevelop and *.kdevelop.filelist files in *every* subdirectory holding applications from the module. As I'm interested only in Quanta, I need only kdewebdev/quanta/quanta.kdevelop and kdewebdev/quanta/quanta.kdevelop.filelist. As the project is a "custom project" (cmake is not directly supported), the list of files that are part of the project are not read from CMakeFiles.txt, but from this filelist.
 Now you can load the project into KDevelop. Once you did, go to Project->Project Options and set up the following items:
1) in C++ Support add the Code Completion database for KDE and Qt4 headers. Also there for Qt options select Qt4 and specify the correct path to Qt. I also disable automatic code completion and use CTRL+SPACE instead when I want, as it seems that automatic completion can be slow some time and annoys me.
2) in Run Options: select Run from Build directory and choose the main program (for me it is src/quanta). The most important settings come here for the Environment Variables. You don't want to mess up your KDE3 installation, so you need separate local and temporary directories for KDE4. So set the following variables:
KDEDIR : path_to_KDE4 (/opt/kde4 for me)
KDEDIRS: path_to_KDE4 (/opt/kde4)
KDEHOME: path_to_local_KDE4 folder (/home/user/.kde4)
KDETMP: path_to_KDE4 temp dir (/tmp/user-kde4)
KDEVARTMP: similar to the above in /var (/var/tmp/user-kde4)
LD_LIBRARY_PATH: path_to_kde4/lib:path_to_qt4/lib:$LD_LIBRARY_PATH (don't use $KDEDIR here, as it is not guaranteed that it is set before LD_LIBRARY_PATH). I have /opt/kde4/lib:/opt/qt4/lib:$LD_LIBRARY_PATH
PATH: path_to_kde4/bin:path_to_qt4/bin:$PATH (/opt/kde4/bin:/opt/qt4/bin:$PATH)
QTDIR: path_to_qt4 (/opt/qt4)
QT_PLUGIN_PATH: path_to_kde4/lib/kde4/plugins (/opt/kde4/lib/kde4/plugins)
XDG_CONFIG_DIRS: set to a single space (unset it)
XDG_DATA_DIRS: set to a single space (unset it)

Doing the same in a console session gives you the possibility to run KDE4 apps from under KDE3 using the same user. 

In /home/user/.kde4/env you should have a script (let's call it dbus.sh), set to be executable with the following content:
#!/bin/sh
eval `dbus-launch --auto-syntax`

(set the path to dbus-launch if you have it in an unusual place).

I had to disable "Automatic compilation before execution" as it didn't start the program...

3) in Build options: select Make as a tool and set the build directory.

Now you should be able to build your project (Build->Build Project or F8), install it (Build-Build Target-Install) and run it or debug it. In most case you must install at least once your application and if you wrote shared libraries or kpart for it, you must install every time before running.

Autocompletion for KDE and Qt4 classes should work as well, but the documentation is not yet set up. First you have to create your kdelibs trunk API docs. Go to kdelibs sources and run "doc/api/doxygen.sh ." which will take a while and once the kdelibs-apidocs directory is created, copy this or link to $KDEDIRS/share/doc/HTML/en.
Now you can add the documentation to KDevelop in Settings-Configure KDevelop-Documentation. Choose Add, set the type to Doxygen Documentation Collection, select the index.html from the above location and you're done. Enable indexing or full text searching if you want. Do the same for Qt4, but there choose Qt Documentation Collection as type and select $QTDIR/doc/html/qt.dcf.

If you want Full Text Search, set up the next tab correctly to htdig/htmerge and htsearch. To regenerate the search index, in the Documentation toolview on the right use Search->Update Config and Update Index. This will take some time as well.

From now on you can search for classes and methods in the Documentation toolview or directly from source if you right-click on an item.

Adding/removing files to/from the project needs some handwork. If you use the new file dialog, it will add for you to the project, but not to the CMakeFiles.txt. That one you must *always* modify by hand. If a file was added to the CMakeFiles.txt, you have to add to the KDevelop project in the File Selector toolview (right click on the file, and see the last entry). The same is true if the file was removed.

Possible problems:
1) the current version has problems displaying QString member variables from parent classes in the debugger.
2) debugging takes a lot of memory (~512MB here)
3) if I want to automatically rebuild the project before running, I get a "Compilation aborted" message and the application is not run. I have to build and run separately.
4) install always install the full project. If you want to install only from a subdir, you have to do it from a console.
5) autocompletion makes typing to stop from time to time. Use manual completion with CTRL-SPACE.
6) when your application crashes in the crash dialog you might not see the Debug in KDevelop button. The workaround for me is to delete the KDE4 version of drkonqi (/opt/kde4/bin/drkonqi).
7) KDevelop tends to crash on project loading. Disabling the Konsole plugin seems to help.

Some of the problems from above might be solved soon, so update your KDevelop 3.4 from time to time.

Good luck and enjoy KDevelop!
