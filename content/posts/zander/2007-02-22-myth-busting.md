---
title:   "Myth busting."
date:    2007-02-22
authors:
  - zander
slug:    myth-busting
---
Just the other week I was talking to someone that is a big supporter of the Open Document Format. He was arguing that we need to find a way to get KOffice and OpenOffice to open all the documents made in MSOffice without any loss and any change in layout.  He argued, if that's impossible, then what is the advantage of ODF?

The not so straight forward answer is that the Open Document Format was not created to provide a solution to the billions of binary objects that exist now. That's not the reason we started working on ODF. This is a recurring myth.

The problem is best described with the analogy that <a href="http://blogs.sun.com/jonathan/entry/microsoft_vista_microsoft_office_and"> Jonathan blogged</a> about;
<ul> Just like your family photos, the last thing you'd want is a camera company demanding payment before you could see your photos. </ul>
Its an apt analogy, in my opinion.  Everyone would snort and ignore any such camera company. But how many people gladly pay upgrade after upgrade for the same privilege of using Microsoft Office?

The linked blog is certainly worth a read!


<b>Important note</b>: the problem of migrating from the billions of binary documents created in old versions of Office is a real one, one that is being attacked at several fronts. This blog in no way means to say that its not important. Its just not the main reason for using and creating ODF.
<!--break-->