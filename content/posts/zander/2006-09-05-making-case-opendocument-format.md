---
title:   "Making the case for OpenDocument Format"
date:    2006-09-05
authors:
  - zander
slug:    making-case-opendocument-format
---
Sometimes you have to let the end-users themselves do the talking on why what we are creating is important for them. And member dylunio did exactly that on libervis.com

<ul>Since academics don't have time to fight against the norm, which in their institutions are proprietary file formats, the only way I see to fix this problem is to change the norm. If the norm were to use free and open file formats such as the Open Document Format they would not be tied into an operating system. </ul>

<a href="http://www.libervis.com/proprietary_file_format_lock_in">www.libervis.com/proprietary_file_format_lock_in</a>
<!--break-->