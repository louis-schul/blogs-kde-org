---
title:   "Annoying mailinglists."
date:    2006-08-05
authors:
  - zander
slug:    annoying-mailinglists
---
There are two basic settings for a mailinglist with regards to where replies go to. The one KDE uses is that replying goes to the list.  An older standard is that replying goes to the person that send the mail. This distinction is very annoying to make each time you switch mailboxes.  KMail already makes it easier for you by having a feature that a mailinglist can be associated with a folder. But that one is not very foolproof, and I like playing a fool when it comes to software :)
For example; private emails I get normally go into my inbox, but I still need to archive them, so I move them to the mailinglist folder if they are appropriate. Pressing reply on such a mail will make kmail send my reply to the mailinglist. I already send some pretty embarrassing mails like that to publicly archived places.
So, I don't use that feature anymore.

I found out a new way to do this, its pretty darn simple. In KMails filter dialog I sort the incoming email to the right folder and I <b>also</b> set the reply to. That's a neat feature I only recently discovered!

Open your filter dialog (settings->configure filters...) for an existing rule that sorts your mail to a folder, click on 'more' in the 'filter rules' section.
Then alter the new combobox to 'Set Reply-to To'. And then copy paste the email address of the mailinglist in the line edit. Press ok.

I hope it will save you as much frustration as it did me :)
<!--break-->