---
title:   "Flake progress"
date:    2006-05-15
authors:
  - zander
slug:    flake-progress
---
The KOffice objects-manipulation library is progressing nicely; since my last blog (2 weeks ago) I have added and fixed lots of things.  Not nearly 2 weeks hacking worth, though, with LinuxTag in between.

Here a list of significant Flake enhancements; made the library support multiple views, where the different views update automatically and they can have different zoom levels and different selections.

We now have 3 types of groups; one for simple groups (think: all the shapes in a butterfly to look and select like one shape), another for nested shapes (think: image flowing inside text) and the final one for 'related shapes' used for things like an image that is anchored to a text paragraph, but shown outside of the actual text.

Full support for any sort of border, where borders can be as simple as a line to as complex as some ornaments being drawn around the shape.  Naturally we support non-square shapes here.

Previously we could only move objects by mouse, this has been made more mature by adding rotation & scaling and making it aware of a grid while moving. As well as cool things like scaling by keeping aspect ratio and scaling from center.
Skewing is still to be added in this department.

I also Made repainting a lot more intelligent so we only repaint what has actually changed. This should make things look a lot smoother for those on slower machines.

Printing to PDF now works flawlessly, which shows only how good a job the Trolls did with Qt4, but still, its very important for KOffice :)

The library already reached the level of features that is needed to make KWord start using it, most work now is in fixing bugs and rough edges plus fixing internal bookkeeping to get fast enough to handle loads and loads of shapes.  Not hard, just work.

Obligatory screenshot;
<img src="https://blogs.kde.org/system/files?file=images/Flake-screeny_0_0.png"><br>
A screenshot of the test app showing the flake library features with multiple views (and different zoom) where I am currently rotating 4 shapes in the bottom view. Note that the 3-colored shape is a group.

Code is in trunk/playground/office/flake for the people that want to get in on the fun!
<!--break-->