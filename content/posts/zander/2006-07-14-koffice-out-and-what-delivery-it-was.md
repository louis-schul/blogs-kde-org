---
title:   "KOffice is out; and what a delivery it was :)"
date:    2006-07-14
authors:
  - zander
slug:    koffice-out-and-what-delivery-it-was
---
The 1.5 series of KOffice has been a good one for the suite; with several new applications and a much improved stablity for its main applications one thing became clear, we got new people that started working on KOffice. You have to admit; for such a small team of people its a pretty big suite to maintain. After all we truthfully state we have the suite with the most components on our website!.

As you can read in the <a href="http://www.koffice.org/releases/1.5.2-release.php">announcement</a>, the 1.5.2 release is out.  A real team effort as we actually got not one but 2 release managers cooperating on this one due to time constraints of Boudewijn, who was the driving force behind the 1.5.0 and the 1.5.1 releases.  Both Sebastian Sauer (dipesh) and Martin Ellis have taken up the task with great success. Certainly counting since this was a first for both of them. It was a bit of an experience, getting such a release out; fighting with svn for the tags, getting the translators to commit their work before the release and all that.  But we all pulled it off. Something to be proud of!

KOffice continues on the 1.6 route with a very small chance of another 1.5 release. Some applications and the libraries are simply skipping the 1.6 release and are working on 2.0 which is based on Qt4 and KDE4. Where we see beautiful things being done. Exciting times.<!--break-->