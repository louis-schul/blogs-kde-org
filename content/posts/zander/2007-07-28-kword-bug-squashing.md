---
title:   "KWord bug squashing."
date:    2007-07-28
authors:
  - zander
slug:    kword-bug-squashing
---
Every now and then I start kbugbuster and browse the bugs database for KWord.  I can then happily close a dozen or so bugs without having to code anything.  The reason for this is that basically KWord has undergone a rewrite for 2.0; when we switched to Qt4 and the basic text engine was replaced with Qt4s-scribe and the frames were replaced with Flake it turned out it wasn't useful to refactor the code but more to merge in old code when and where possible.

At the same time I redesigned a lot of core components. Can't let an opportunity like that pass.  I had the experience on how it would have to look and then its just a matter of writing the code.

Now, if you start the new alpha release of KOffice and see a KWord that still misses loading and saving, instead I see a KWord that got its guts ripped out and replaced with shiny new components. Really 2.0 like.
And I got some numbers to prove it!  If I check the amount of bugs closed in the 16 months (since the redesign was started and work on 1.6 stopped) I note that 312 reports were closed.  So without actually trying to fix bugs the redesigns just 'magically' closed over 300 bug reports and wishes.

I only closed bugs that are actually properly working now; with working GUI items and all. Which means that redesigns which are started, but are not yet fully finished still give me room for more closing of bugs.

So, when I can close 2084 days old bugs, I think the work I did on Kword 2 was due :)<!--break-->