---
title:   "eloquent rants"
date:    2007-04-29
authors:
  - zander
slug:    eloquent-rants
---
eloquent rants are actually nice to read :)

This is a nice one on how media spin can not only point fingers at innocent people, it can also mask the evildoings of the real culprits;
<ul><a href="http://www.robweir.com/blog/2007/04/sometimes-i-need-to-remind-myself.html">Sometimes I need to remind myself -  Rob Weir</a></ul><!--break-->