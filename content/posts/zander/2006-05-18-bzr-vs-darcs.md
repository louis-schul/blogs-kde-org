---
title:   "bzr vs darcs"
date:    2006-05-18
authors:
  - zander
slug:    bzr-vs-darcs
---
After reading <a href="http://www.omat.nl/drupal/?q=node/76">Toms blog</a> I installed and tried Bazaar (bzr). I wanted to anyway since this is Mark Shuttleworths answer to svn and cvs.

I tried it and I must say I'm disappointed. Not because of the ideas it has, they are very good and they address the problems cvs and svn have head on, which Tom also noted.

Some years ago there was a revolutionary invention called ‘tla’. This was the first open source revision management software that allowed decentralized repositories. Great news; now someone working on KWord does not need an svn account anymore, but just a personal copy of the sourcetree where he can then record changes made and sent them upstream if he’s ready.

The problem was (and still is) that tla was over designed and completely unusable without a weeks long on-site training.

See, nobody wants to become an expert in their revision management software, so it should be really easy to learn (flat learning curve) and still very powerful to do all the things you need and want to do.

There were at least 2 spin offs from tla; the first was Darcs and its been in development for a couple of years. The first commit made of itself in its own revision management software was in Oct 2002. Its started with 2 things in mind; excellent merging capabilities and easy to use.

The second is Bazaar. Its written to be an easier version of tla. Well, that’s easy, but it seems to have made the same mistakes of tla and it still takes a lot of work to use. It has no less then 47 individual commands, where svn has 30.

Comparing Darcs and Bazaar brings to light that there are a couple of notable differences that influence the whole. First is that Darcs obviously is more mature, its been sharpened by the teeth of time (there actually <i>are</i> docs) and various usability people have actually helped in creating its command set. In contrary to Bazaar which 	not only has a huge set of top-level commands it has copied a lot of the hard to learn concepts from tla which Darcs has managed to handle internally instead of forcing upon the user.

In working with Bazaar you are made aware of the fact that it still is based on patch and diff. Two patches that are reasonably close to each other will cause a conflict. In effect its not much better than <a href="http://blogs.kde.org/node/1028">svn</a> when merging between branches.
As an example; if you change one file in two different repositories you can record those changes locally using commit. The other branch will not notice that until you do a ‘pull’ to get the data from the other branch. This will fail if there is a merge conflict, even when not the same line is changed, and in the case of a rename it always causes a conflict. You are then told to use the command ‘merge’ which makes you merge manually and commit a ‘merge’ log afterwards. At this time it gets sticky since you have to push the merge back to the branch you copied it from, even while there was absolutely no change made!

If you lost me here, that’s OK since that exactly the reason why Bazaar gets a verdict of being too complex.

If you take the alternative, Darcs, then you can see a really revolutionary way of doing disconnected revision management. The authors of Darcs recognized the above problem and found out that there is a better way of storing and merging patches that completely sidesteps the above problem and allows for a far easier way of operation since the user does not have to be aware of the common patch/merge pitfalls anymore.

The effect is that the simple user can move patches between repositories and not get any merging problems unless exactly the same lines were changed. Where file-renames and method renames are no probelem and don't cause conflicts. It also allows you to rollback one patch you made a week ago. Even after you already made a lot of commits after that.

The last area that Bazaar and Darcs differ in is the user interface. In Darcs all commands have an interactive conversation between the application and the user.

So, doing a revert in darcs will allow you to say 'yes' or 'no' as well as 'all' etc to each individual change.  Making it trivial to remove those kDebug() statements you added while debugging the latest problem. In Bazaar a revert is even worse than in svn, it just reverts ALL files without any indication which they were, or if there were any at all.

In darcs there is a great symetry in commands, if there is a revert, there is an unrevert to undo the revert you did earlier.  Again with a good patch-based interface. Missing in Bazaar.

The concept of tagging is essential in release management, missing in Bazaar. I guess they expect you to use the same concept of svn here; just create a new branch and call it a tag.  Sounds good in theory, but in contrary to svn you will end up with a full copy of the sources on your disk!

In the end darcs allows a lot more flexibility and features and a lower learning curve. So I’d say it wins hands down. I don’t know what made the bazaar people start yet another RC system, but they got into the game late, have a design that’s not up to the design of either git or darcs and I think it's a stillborn. Well, there are users for every technology, but will it be the next best thing? I don’t think so.