---
title:   "Release schedules and development cycles."
date:    2004-05-31
authors:
  - zander
slug:    release-schedules-and-development-cycles
---
In the recent Poll on <a href="/node/view/463">KDEPims release schedule</a> I wanted to answer, but realized the answer is not that simple; and KDE has been running in the same circle for too long to see that there have been developments out there which change the release stuff completely; in other words, time for my first BLog here :)
<!--break-->

<br><br>This blog can be classified as a rant, but I have a little bit of a reservation about that. The simple thing is that I intend to tell nothing new here; there are millions of developers already using a development cycle which make the whole point of release schedule quite simple and obsolete.<br><br>

My point? A release schedule is based on development cycle. KDE still has a (bluntly said) cycle of <ol>
<li> Refactor and wreak havoc -- all is broken and everyone knows.</li>
<li> Week before feature freeze -- all is even more broken, but at least the features are in!</li>
<li> Feature freeze, we fix what we broke before</li>
<li> We release</li>
</ol>
The total time of this list takes anywhere between 4 to 8 months, and is global for the millions of lines of KDE code.  There are those that like this way of working, but I sure as hell don't!  I can't follow KDE on a stable box since I have to worry about loosing data all the time.  What if this coding cycle could be brought back to, say, 2 weeks ?  Sound stupid? Just make the tasks smaller. Even smaller then that. Still having a problem seeing how this could work, make the tasks even smaller (ad infinitum).<br><br>

<b>Keep Changes Small</b><br>
What I have been experimenting with is a way of working that puts all your TODOs in an ordering of priority. If your TODO can't be done in a cycle (including debugging) of less then two weeks. Then you have to split the TODO into smaller pieces. This is explained better in an article here: <a href="http://today.java.net/pub/a/today/2004/04/27/smallchanges.html">Keep Changes Small</a>.<br>
If you grasp the concept, and actually try it, I'm pretty sure that all but the most-complete-rewrites can be finished, and debugged in under a month (taking hacker time in account here).<br><br>

<b>Fix your bugs fast</b><br>
With shorter cycles people will suddenly start to be able to use your software fully again before its actually released. When a fellow KDE finds a bug or annoyance in your application, he can fix it or ask you to fix it directly; since the change that you are still hacking on that is very very small.  In the end the feature freeze may still be needed; but it can probably be something like 3 weeks.  Remember that the one finding most bugs is the developer himself; and if you have more then 2 weeks of bugfixing, then your methodology on keeping changes small is flawed<br><br>

<b>Prioritize your features and bugs</b><br>
Its simple, really, if you do the most important features and TODOs first, then you won't have too much of a hassle releasing a month early since only the lowest priority TODOs and bugs are left.<br>
I've been to a department that had all bugs in a system on those yellow sticky-notes. Each had a small description of the feature or bug and it was positioned on a (big) wall according to priority.  The highest prio at the top, and similar priorities at the same level.<br><br>
Every couple of days the priorities were re-done. Some things might have gained a lot of priority due to others depending on it. Some suddenly did not seem so important after all.  Reshuffeling galore!  Naturally the amount of reshuffeling got less and less after the project progressed.<br><br>

In summery; with projects like KDEPim and KOffice having seperate release schedules we loose a lot of marketing value for those respective projects.  All we need is developers developing in a lot smaller coding cycles which means that the software is stable at all times, just misses features that might impede it from an 1.0 release.<br>
Stabler software means more debuggers and basically higher developer satisfaction.<br>
More importantly, it means any project can release 'next month' if the release coordinator wants.


Do you want to know what I've been smoking (hint; I don't), or do you want to know more? Just hit reply.