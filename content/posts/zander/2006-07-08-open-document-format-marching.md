---
title:   "Open Document Format marching on"
date:    2006-07-08
authors:
  - zander
slug:    open-document-format-marching
---
Open document format is that new fileformat for Office suites, ISO certified and genuinely an open standard.
Its been busy in ODF land, since early may the ISO certification came through we have seen the market accept this standard in an amazing speed. Governments are not well known for moving fast, and yet we have this long list of successes.

<a href="http://news.zdnet.co.uk/software/applications/0,39020384,39276978,00.htm">Belgian</a> government chooses OpenDocument, ODF in <a href="http://gotze.eu/2006/05/odf_in_denmark.html">Denmark</a>, <a href="http://www.ciol.com/content/search/showarticle1.asp?artid=85632">Key India official</a> endorses OpenDocument and naturally <a href="http://www.weeklydig.com/index.cfm/fuseaction/article.view/issueID/5461f599-4327-4ef8-9df3-a7aed9312a07/articleID/52ac5c49-7f28-46be-bce1-f28750835a0a/nodeID/4b1339d1-be3a-44a2-be8b-1484963a003a">Massachusetts</a> is still going strong.  The <a href="http://www.odfalliance.org/">ODF Alliance</a> certainly is not <a href="http://www.govtech.net/magazine/channel_story.php/100143">sitting still either</a>.

Oh, and this one is nice too; <a href="https://addons.mozilla.org/firefox/1888/">ODFReader: Firefox Add-on Released</a>. How is that for support :)

This probably means that we got critical mass. Enough people take the idea seriously to start that snowball effect and get more people on board to fix problems and to continue getting new people to hop on the bandwagon.  At this point I should probably state why this idea is better than any other.  ODF is an open format where a lot of different companies and groups have collaborated to create a good fileformat usable across different suits.  Its not perfect and its not complete, but thats not the reason why a government wants this solution so bad. The reason for that is that it allows anyone to fix those problems and it allows anyone to keep reading those files even 100 years in the future, guaranteed!

As we know the first office-suite to have a release out with support for OpenDocumentFormat was KOffice (1.4), and we continue to support it with a lot of new work being done in 2.0 that will just rock the world when it comes out.  Next to KOffice there are a lot of other office suits that support ODF in various degrees. OpenOffice, naturally, but also the IBM Productivity Editors (part of Lotus Notes soon) which together already take up a big chunk of the market.

The biggest chunk of the office-market is taken up by Microsoft Office, though.  Microsoft has various different 'standard' file formats.  And their new release switches to another fileformat again.  This creates a pretty big problem; it can divide the industry into 3 parts.  The old-fileformat of MSOffice (which is where the most docs are saved in today), second is ODF and 3th is the new closed-format from MS.

With the goal being one fileformat that can be relied upon for many years to come, this is an unwanted fork in the road.  Everyone looking ahead, with the idea of keeping documents readable into the future, will have to choose between MS-XML and ODF.

No longer!  Microsoft has seen that its in everyones best interrest if MSOffice can work together with ODF based suites.  Microsoft has issued a <a href="http://www.microsoft.com/presspass/press/2006/jul06/07-06OpenSourceProjectPR.mspx">newsreport</a> stating it has started a project on sourceforge to convert documents.

The result of this backing is not just one more company jumping on the bandwagon.  Its eliminating the choice that everyone was forced to make between ODF and MSOffice.  Since whatever you choose, the path to ODF is still there.  This is a pretty significant backing of the format and critical comments from Microsoft on the format are more then welcome.  Afterall, without bugreports the problems can't be fixed :-)

Now, I'm left with the idea that forcing MS to back an open format, despite the likely result of that significantly loosening its grip on the office market, is a feat that the whole industry has achieved. This is a clear use of the idea that the pen is stronger than the sword.  If enough people choose a better solution even the nay-sayers will have to follow or be left behind.  Hmm, globalism isn't so bad afterall, if you count on opensource :-)
<!--break-->