---
title:   "The Future of Kontact"
date:    2015-10-01
authors:
  - htietze
slug:    future-kontact
---
Supplemental to what we reported previously about the work in Randa [<a href="http://user-prompt.com/look-and-feel-of-plasma-sidebars/">1</a>, <a hef="http://user-prompt.com/baloo-kcm/">2</a>] there was a session on the future of Kontact, KDE’s personal information manager (PIM). Over the years this tool has evolved into a monster making both development as well as usage sometimes tricky. It’s time to cut hydra’s arms.

TL;DR: Sorry for the long posting. This blog post is about requirements for Kontact mobile. It also shows a couple of mockups based on the preliminary mobile HIG that illustrate what the requirements mean.

<u><b>Vision</b></u>
Formerly, Kontact had a rather simple <a href="https://community.kde.org/KDE_PIM">vision</a> which was updated at the last PIM sprint:

<i>“The KDE PIM Framework allows to easily create personal information management applications ranging from personal to large enterprise use, for any target device and major platform. It seamlessly integrates data from multiple sources, while aiming to be rock-solid and lightning-fast....” </i> (read more at Thomas Pfeiffer's <a href="https://sessellift.wordpress.com/2015/01/18/ideas-for-a-project-vision-for-kde-pim-and-improvements-for-search-in-kmail/">blog</a> and the new <a href="https://community.kde.org/PIM">KDE PIM</a> wiki page) 
(Probably we do not talk about a framework anymore but a full-featured PIM suite)

In addition the generic “simple by default and powerful on demand” should be true. That means among others to drop all non-core features. Since KDE software will run on mobile and touch devices the next version of Kontact has to runs smoothly on all devices and form factors. And the special aim of the developers is that Kontact will become the first tool when privacy and security is of primary interest. 

<u><b>Persona and Scenario</b></u>
Kontact will stick to the <a href="https://techbase.kde.org/Projects/Usability/Principles/KDE4_Personas">KDE personas</a>. The main area of application is in medium to large size companies with a focus on security. So we talk about people like Berna or Santiago as the primary persona. The secondary persona is the private user with average knowledge (Susan) but also those with special needs like mailinglists (Philip).

There shouldn’t be much to clarify about the desktop scenario. In respect to mobiles we have to consider first phones with ~4 to 5” in both vertical and horizontal orientation. For instance, the phone can be used vertically to get an overview of incoming emails and provide details on the selected item when rotated. Additionally we have to take tablets with 10-12” and wearables into account. The latter might be a small wrist watch but also a head-mounted display with a larger visualization.

<u><b>Requirements</b></u>
Most relevant in this early stage of development is to define the intended features clearly. So this blog post is actually about requirements. 
<ul>
<li>Essential for Kontact+ are email, calendar, and address book. Additional modules like notes, to-do lists, feeds etc. will be made available via plugin.
<li>It has to be easy to switch between these modules
<li>Modules have to allow interactions in terms of adding an appointment to the calendar that was received by email.
<li>Configuration of the app is important
<li>Comply with KDE phone (swipe up/down from top and bottom; extend the to<li>olbar wrt. the form factor)
</ul>

<b>KMail</b>
Some time ago an analysis was done in respect to <a href="https://community.kde.org/Plasma/Active/mail">Plasma Active</a>, which shouldn’t be outdated too much. 
<ul>
<li>Received emails have to be shown in a comprehensive list that allows to focus on important items.
<li>Discussions (mailinglists) are shown in threads, which is one of the killer feature of today’s KMail.
<li>Emails can be sorted and filtered by date/sender/receiver/size/flags with all flexibility
<li>Security/privacy is of major interest.
<li>Core technical features are:
<ul>
<li>Support multiple accounts
<li>Support standard protocols (IMAP, POP3, SMTP)
<li>Supports authentication via NTLM (Microsoft Windows) and GSSAPI (Kerberos)
<li>Supports plain text and secure logins, using SSL and TLS
<li>Native support for inline OpenPGP, PGP/MIME, and S/MIME encryption
<li>Filter spam (Integration with popular spam checkers, e.g. SpamAssassin, Bogofilter, etc.)
<li>Provide synchronization features (e.g. owncloud)
<li>Notification for new messages
<li>Deal with text and HTML formatted emails
</ul></ul>

UI relevant features with relevance and suggested realization:
<ul>
<li>Receive/update: core > auto push or swipe down</li>
<li>Overview as well as details view and the complete email: core/performance > selection and touch again
<li>Distinguish read from unread messages: core > grey out
<li>Reply/reply all/forward: performance (reading is preferred on mobiles) > context drawer / handle (reply to all only for mobiles)
<li>Compose: performance > global drawer
<li>Delete: performance > context drawer
<li>Move to folder (archive): buzz > context drawer
<li>Save/add attachment: buzz > context drawer
<li>Link item (add sender to/from address book, add appointment etc.): performance > context drawer
<li>Toggle html on/off: exotic / buzz > context drawer
<li>Show full header info: exotic / buzz > link
<li>Handle long lists of receivers: exotic (at overview)/  performance wrt. to security > cut by default
<li>Search for items: exotic > not implemented
<li>Sort items by property (time, sender, receiver...): exotic > not implemented
<li>Import/Export emails: exotic > not implemented
</ul><small>core/basic features = program does not work without, performance = not absolutely necessary, but create the impression of a good product, buzz = not expected by default, exotic = not really needed</small>
For more about context drawer or global (or menu) drawer you may read the preliminary <a href="https://techbase.kde.org/Projects/Usability/HIG/Patterns/CommandPatterns#Patterns_for_a_simple_command_structure">HIG</a> or wait for the upcoming blog post by Thomas Pfeiffer.

<b>Calendar & Addressbook</b>
We talked also in short about the calendar app. Ideas will be presented later to not overload this posting.

<u><b>Mockups</b></u>
Requirements are essential for products. But since people tend to not read walls of text- and because it’s often not easy to follow the ideas, it makes sense to visualize with simple mockups.

<b>Multiple level of details</b>

<a href="http://i.imgur.com/zEhdeIV.png"><img src="http://i.imgur.com/zEhdeIV.png" alt="Figure 1" height="300px"></a>
<i>Figure 1: Several level of detail in order to support both a fast overview as well as having a preview and a full-size read mode. The figures also illustrate the idea to add graphical guidance a la GitHub.</i>

The probably most shining feature is the graph supporting the orientation. It is inspired by GitHub with the idea to flatten the current system of reply indention. If Alice communicates with Bob there would be a straight line. And if John joins and “branches” the discussion this would be illustrated respectively.
Threads are identified by the topic, highlighted in blue here. With the goal to show as many (incoming) emails as possible we start from the left with the highly condensed view that lists sender and time of the email. Items expand on selection (indicated also by a bigger sign in the graph) providing a quick preview. The actual email address is shown here for security reasons. If the user touches the item once again the app provides inline access with almost the complete functionality. However, from time to time it might be necessary to read the message in full size with for example all HTML features, as illustrated in the right picture.
By the way, the bottom panel with the red cross is a placeholder for global plasma functions on mobiles.

<b>Interactions</b>

<a href="http://i.imgur.com/j66746c.png"><img src="http://i.imgur.com/j66746c.png" alt="Figure 2" height="300px"></a>
<i>Figure 2: Context functions with the selected item and global interactions with the app. In the right mockup the idea of dynamic filters is shown.</i>

Context relevant functions are located in the right drawer. It opens when the user swipes from right to left offering all functions with the current list as well as the selected item. If the user wants to have quick access to a particular function, such as reply (reply to all in case of mobiles) and delete, these features may be selected for having them at the handle, which is shown on slid sideways. The user can decide what he or she wants to have there but with a limit to a few functions only (here two).
Access to global functions, i.e. compose for email, and other modules are available in the left, global drawer. And there is also a core feature of the email app: <i>Dynamically combined filters</i>.

Normal email programs work with a couple of fix folders like inbox, sent, and trash plus user-defined folders. But reading an email in a conversation without reference to own contributions is weird. And using predefined folders is also somewhat outdated. The idea is to have filters like “all incoming unread messages” (replacing the inbox), “emails with reference to KDE in the header or sent from *@kde.org” (replacing personal folders). The creation of those filters (like smart folders in the Mac OS world and also similar to the stored search in the current KMail) could be done on the desktop for convenience. The mobile app provides checkboxes to have any combination of those filters.

<b>Form factors and orientation</b>

<a href="http://i.imgur.com/dIbPbpM.png"><img src="http://i.imgur.com/dIbPbpM.png" alt="Figure 3" height="200px"></a>
<i>Figure 3: Horizontal orientation.</i>

Different form factors like in rotated orientation or in case of a larger displays have to be taken into consideration. While the number of shown emails is reduced it is possible to read the content of the selected item, at least in terms of the preview.

<a href="http://i.imgur.com/SnnYVjU.png"><img src="http://i.imgur.com/SnnYVjU.png" alt="Figure 4"></a>
<i>Figure 4: Desktop or tablet size version of Kontact+.</i>

With more size the screen may get organized like in conventional email applications. The left, global drawer (aka sidebar in desktop apps) offers navigational and global features (still providing access to dynamic filters), there is an overview of all emails (here with more information compared to the mobile version), and the details below. Large screens makes it also possible to show a toolbar with labeled buttons instead of the right context drawer.

It should be kept in mind that users may want to switch to the mobile version on the desktop, for example in order to have a small app running on a secondary screen.

<u><b>Alternative version</b></u>

<a href="http://i.imgur.com/pztl7fj.png"><img src="http://i.imgur.com/pztl7fj.png" alt="Figure 5" height="300px"></a>
<i>Figure 5: Alternate, even more simple version.<(i>

Of course you can (and should) refuse requirements that make no sense to you. For instance, if threading is less important and if the major focus is not on as many items as possible at once, the user interface may look like in figure 5. Colors can be added to support navigation and orientation, but must not be used as primary indicator. That means, similar to existing apps the items may have a small indicator bar, or the like, for the respective filter.

<u><b>Configuration</b></u>
Finally, and to make it really confusing, the app is made for KDE. And since we offer all freedom the configuration should allow flexibility and individualization. However, the mobile app needs to be simple so we must not add all the features available currently.
Here are some ideas for the configuration:
<ul>
<li>Sender name: ( ) First name Surname, (o) Surname, First name, ( ) First name only, ( ) Surname only
<li>[x] Show sender’s full address
<li>[x] Allow HTML emails
<li>[x] Show navigation graph
<li><1> Number of preview lines
<li>[Breeze|Oxygen|Rainbow|B/W] Color set
<li><0.5> Time to mark as read (steps in 250ms)
</ul>

<u><b>Conclusion / Participation</b></u>
What do you think? Are the requirements sufficient for your workflow or do you think we have to add or remove something? We are looking forward your ideas. Please join the discussion at the KDE forums thread <a href="https://forum.kde.org/viewtopic.php?f=285&t=128565">"The Future of Kontact"</a>.

Finally kodus go to Michael Bohlender, Christian Mollekopf, Andreas Kainz, Uri Herrera, and Jens Reuterberg who discussed the topic at Randa. Also big hugs to the mobile HIG team with Thomas Pfeiffer and Alex L. 
Images were made with Balsamiq Mockups. The source can be downloaded from <a href="https://share.kde.org/index.php/s/AkDa1o871pIhDP3>KDE share</a>.