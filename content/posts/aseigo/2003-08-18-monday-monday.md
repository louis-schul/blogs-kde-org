---
title:   "Monday, Monday!"
date:    2003-08-18
authors:
  - aseigo
slug:    monday-monday
---
posted the first installment in my new "Non-Programmer's Guide to Participating in KDE" series, title <a href="http://urbanlizard.com/~aseigo/whatsthis_tutorial/">Adding WhatsThis Help To KDE Applications</a>. was posted to theDot, where it got the usual inane comments, but also lots of positive feedback. very happy about it. will be moving it to the KDE support page eventually. next topic will be Bug Triage.

also committed a fix for a long standing huge memory leak in the taskbar and kscd related to tooltips. dirk got on my case about it briefly, but we chatted on IRC and he eventually came around ;-) but seriously, as annoying as getting called out on the rug erroniously can be at times, it's more than made up for by the knowledge that the peer-review process is in full effect.

another hot and smokey day, another bunch of work. and i'm behind because i'm complete distractable today. stupid heat. i have an early evening footbal/soccer game, hopefully it cools down a bit. Mahlah already picked up the beer for the aftergame superlatives. we just need to win two more games (out of three) to clinch the division beyond any of the other team's reach, so hopefully tonight is another good showing for us. go Drillers! (what a silly name)