---
title:   "to launch or not to launch"
date:    2004-03-04
authors:
  - aseigo
slug:    launch-or-not-launch
---
ok.. so, i'm sick of waiting several seconds for kwrite to pop up whenever i do a View Source in konqueror. yes, one might be correct in saying kwrite should open faster. but i've got a different idea: why not have a simple viewer window that runs IN the konqi process ala Mozilla? i've had this on my slower box at home for some time now and BOY does it make a nice difference speed-wise. i need to add some basic viewer functionality to it like Search and Print. perhaps just a nice little toolbar with a couple of buttons on it (and an incremental search line? hrm...) anyways... i'm pondering whether it's worth polishing up and attempting to convince the Konqi maintainers that this is a better approach than launching an external editor. here's my benefit/cost summary:

benefits:

 o fast. and i mean fast.
 o simple. you can't edit the source in any truly meaningful way (e.g. edit it and then have it appear on the web) so why have a full editor when it's really only a viewer?
 o fewer dependencies. kwrite is supposedly guaranteed to exist. but is it necessary to rely on it?

costs:
 o loss of advanced functionality, like syntax highlighting.
 o another class in Konqueror, meaning more code. it's not a lot, but it's there.

i suppose it could also be made an option whether to use the "embedded" viewer or to launch a full-on editor. or a button in the toolbar could be provided to allow launching the editor from the viewer window.. hrm......