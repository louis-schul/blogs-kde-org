---
title:   "Doing KDE on the run"
date:    2003-07-23
authors:
  - aseigo
slug:    doing-kde-run
---
gah! too busy these last two weeks... i need to lead a life of luxury so i can nurse my many non-paying but oh-so-enjoyable interests, like KDE. perhaps i'll ask Santa for one this year ;-)
<!--break-->
did up a quick UI for Zack's kspell work, based on a mock up by Henrique Pinto with a bit of a twist. still ruminating on whether or not to add a progress bar. yeah, maybe i will.

discussion on kde-usability is actually pretty high on the signal side of the signal/noise scale the last few days. huzzah for that.

been receiving patches for KJots. was going over one and fixing it up a bit before committing, only to see that someone else had decided to commit it. i just love resolving CVS conflicts. =( oh well, slow but steady progress.

too slow, really. just too much happening. must convince people to leave me alone.