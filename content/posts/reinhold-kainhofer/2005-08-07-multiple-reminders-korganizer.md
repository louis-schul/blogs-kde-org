---
title:   "Multiple reminders in korganizer"
date:    2005-08-07
authors:
  - reinhold kainhofer
slug:    multiple-reminders-korganizer
---
Actually, today I wanted to do some serious bug fixing and reduce the number of items on my to-do list, but then I didn't get beyond one of them: Multiple reminders in KOrganizer. 
Actually, like so many other features, multiple reminders were already supported by libkcal as well as by korgac (the reminder daemon), it was just the GUI component that was missing. I added that today, so it's now possible to have :
<ul>
<li>reminders before / after the start / end date of an event
<li>multiple reminders per event
<li>repeating reminders
</ul>

Seems like korganizer in kde 3.5 will also have some new stuff to show off...

Actually, this feature implements our second-most requested wish for korganizer... And it fixes some problems identified by our usability guys / gals. (BTW, they are doing a tremendous job! Just look at http://openusability.org/projects/kdepim/ and http://www.userbrain.de/kdepim/. The only problem is that this keeps my to-do list of open issues growing immensely ;-) )
<!--break-->