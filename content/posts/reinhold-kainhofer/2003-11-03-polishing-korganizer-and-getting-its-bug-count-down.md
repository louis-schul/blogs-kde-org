---
title:   "Polishing KOrganizer and getting its bug count down..."
date:    2003-11-03
authors:
  - reinhold kainhofer
slug:    polishing-korganizer-and-getting-its-bug-count-down
---
<p>So, finally KOrganizer's bug count got down from almost 95 bugs in July/August to 44 right now. The last few weeks I was really busy producing patches at quite a rate. But every time I tested a patch, I ran into the next bug, and then the next, and so on.<br>
But I think, korganizer is really getting into shape now for the KDE 3.2 release. 
As far as I'm concerned, all the important issues are resolved / fixed (I don't know about Cornelius, and he's the maintainer of korganizer, so this list is only "unofficial"):
<ul>
<li>The resource calendar replaced the file based system korganizer had in previous releases, so each user now has his very own system-wide calendar consisting of parts from several sources (birthdays, files, shared network calendars etc). I really think this resource framework is one of the best things that could happen to kdepim... Whoever is responsible for this (Cornelius?) really deserves an award and a spot in KDE's hall of fame!
<li>Alarm daemon works again with the resource calendar (due to Cornelius' work)
<li>KOrganizer's printing system got a lot more options than it used to, and it was factored into separate classes, so KDE 3.3 will get a print plugin system. Then it will be able to easily extend the available print styles by a simple plugin which implements your own style.
<li>The resource calendar now uses the correct time zone.
<li>Journals now also work with the resource calendar.
<li>KOrganizer should now work with all foreign encodings (it uses kind-of-utf8 as specified in the rfc 2445), also for group scheduling. I just submitted that patch this morning, so if something breaks on your next kdepim update (in particular on systems with non-western locale), I'm the one to blame (please also tell me about such problems so we can fix them).
<li>I also implemented moving multi-day items (items with a time range that go over midnight) in agenda view. There is still one little crash in there, which I need to resolve before the release...
</ul>
</p>

<p>
That was the past, now it's time to think about new features to implement after the release (not that korganizer's wishlist at bugs.kde.org would be too short). The biggest part will be the groupware release in a few months, where we need to produce a full-feature groupware solution consisting of all the great KDE apps. Will be a lot of work, but also a lot of fun...
