---
title:   "create call graphs with Doxygen"
date:    2008-09-06
authors:
  - tstaerk
slug:    create-call-graphs-doxygen
---
Since some years, I have searched for an elegant solution to generate <a href=http://en.wikipedia.org/wiki/Call_graph>call graphs</a> out of C++ source code. Today I found it. It is doxygen. I have evaluated doxygen years ago, but I threw it away. The reason is that you have to know two things about doxygen:
<ul>
<li>Do not call doxygen, start with doxywizard and everything goes straightforward.</li>
<li>You will run into an error message "Error opening map file *.map for inclusion in the docs!" and you need to resolve it as described on <a href=http://mihirknows.blogspot.com/2008/03/error-opening-map-file-map-for.html>http://mihirknows.blogspot.com/2008/03/error-opening-map-file-map-for.html</a></li>
</ul>
So, have fun with your call graphs!