---
title:   "Desktop Developers' Conference 2005 impression"
date:    2005-07-19
authors:
  - chouimat
slug:    desktop-developers-conference-2005-impression
---
<b> DISCLAIMER: this blogs containt some sarcastic comments. Reader discration is advise </b>

I had the chance, or the bad luck, to attends to the Second Ottawa Desktop Developers' Conference. I say bad luck because almost all the talk where about Gnome and freedesktop technology, and even those which weren't about the gnome stuff where geared toward it. I was looking in learning something but it was not the case.

Here a more detailled description and comment about the talk (didn't write this yesterday so it's from memory so ...)

<b>monday talks</b>

<b>XCB and Xlib:</b>
this talk was about the X C Bindings, a replacement for the xlib. 
since xcb is a X protocol binding you can implement X over it.

<b>What's new in Eclipse 3.1</b>
basicly they presented eclipse 3.1 new features like improved Gnome integration, no word about kde
the port of swt (their widget set) to cairo and the reduction of the flickers in the user interface. Beside those minor improvements, they didn't improve 
the usability
of their interface =)


<b>Desktop Development In China </b>
This talk was somewhat interesting, it explained what is happening in china since the decision of the government to use linux.
the talk was more targeted to managers than to developers. to make a long story a short one: we will need to learn Chinese.

<b>Bloat of Data in the Unicode Era</b>
this article focused on the use the use of gnome and xml to implement localization.
didn't really listened to it since I was busy trying to get my internet connection working.

<b>Evas & Edje for Enlightment - Independent Structural Objects for Gui driving</b>
This talk was interesting, presented a lot of cool window manager feature, like fast window drawing,
powerpoint like transition from on desktop to another or when changing the background, 
background image resizing without lost of quality in the picture. animated menu hilighting.
But even if those feature where cool and can be used from a scripting language embedded in the window manager, they make more sense in a video game than on my desktop


<b>Inkscape</b>
I missed part of this one, because we took more time for the lunch. 
Basicly he presented inkscape features and explained a little svg.


<b>X on Gl</b>
this talk was interesting only because x on gl is a weird and intersting beast. instead of targeting an hardware memory buffer they reimplemented the X 
server to the opengl api. but since not all video card support opengl (yes I know the new ones do) I believe it's a dead end in the long run.


<b>Modularizing the X window system source tree</b>
The roadmap to X11R6.9 (monolythic) and X11R7 (modules based) and beyond.


<b>Free Software for digital photography</b>
this talk was about lingphoto2 and a big rant against digital camera makers being uncooperative and a bunch of liars.
the only point that need to be noted is he talked a little about the Exiv2 library which has 2 majors problems even if it relatively complete
1) it's in C++ 2) it's GPL ...

<b>Introduction to the Openclipart Library</b>
Was to tired so I decide to get home at this point
 
<b>Tuesday talks</b>

<b>Rapid Linux Desktop Development With KDE and Kdevelop</b>

No gnome developers attended even the eclipse guys where
not here


<b>Bringing X.org's GLX support into the modern age</b>
this talk was about the changes that was made to the glx support since xfree86 4.0.0 basicly they are changing the
mesa spec files and python script by an easier to maintain combination of xml and python scripts.

<b>Moving Bottlenecks: CPU Cycle Reduction Using Liboil</b>
the presenter works on gstreamer and esound ...
the stuff is really lowlevel.
Liboil is a Library of Optimized Inner Loops, in other word a collection of simple functions operating on arrays, they are tested and profiled.
One of the goal of liboil is to make easy to use MMX, SSE or altivec extension in your application
the library containts - math functions, type converstion, colorspace conversion, codec, image manipulation , misc (UTF-8 Checking, md5sum)
the most interesting part of the library, according to the author is the init part ...


<b>Helping third-party developers</b>

this talk was about how to attract and keep 3rdparty developer, and that we need to concentred on support not the technology
and we also need to reduce the learning curve of the developer. Even this presenter made the common error about the common specs and standard which need to 
be supported (eg is freedesktop.org supported). He also  said that we need to keep uptodate documentation, explain what is obsoleted and when it will be 
removed. and also we need to "potty train" the ISV by giving them a clear path to follow if they want to use our technology over any other available. And 
since not all the developers are not using C or C++ or java ... it's bad to force them to read the C++ api documentation. it even better to add examples in 
all the supported language in the documentation. and support more than the trivial hello world example or point to an available model real-world application. 
It seems that trolltech get this right in their documentation.


<b>Putative software engineering and the x window system</b>

Sotfware engineering in the X team ... 
review of diverse source control and tla was tested but it's not ready for X :)
imake is getting replaced by make/autotools/pkgconfig (wow 3 tools to replace one ... nice improvement :)
some talk about bugzilla and source code management integration ...

<b>an insider's  guide to cairo</b>

Computer graphics lecture !!! 
so far what I understand computer graphics systems SUCK
he explained all the problems you can have with graphics primtives ... spline etc etc ...
He said that postscript have all of them, inkscape too and so cairo ... so what is the need for a new computer graphics library then?


<b>toward a gnome office suite</b>

this talk is what need to be done to get gnome office suite, seems they don't like openoffice.org because it doesn't use standard library (read here gnome)
for what I understand, every thing will be in libraries ... the application will be less than 100 lines of source code
Actualy only present, a presentation viewer, is available. and the project is started by some persons at ximian 

<b>Uniconf, gconf,kconfig,dbus,elektra, oh my! - or -DConf A configuration framework for everyone</b>

They explained the Mother of All Configuration systems and why all the currently used ones sucked
basicly it's a common abstraction layer over any existing configuration systems that will offers:
permission and access control,notification of changes, transactions, standalone and daemon mode, and pluggable configuration backends
uniconf also provide a network protocol so we don't need to use the library. The pluggable backends can be used by a kde application to use gconf or the 
opposite a gnome application to use kconfig ... The only thing i find interesting in this is the network protocol, maybe I can use it or be inspired by it 
for m2/arrggh, but that will depend if it's what I need :)


<b>Possible futures of linux kernel/user graphics drivers</b>
this talk was about the problem existing in the X window world.
they described the current situation and talk about the new ddx.

- 2 new 2d based DDX
  - EXA.
    overall he said it was a good thing because it's basicly an extension of the current drivers, (easy to implement)
 - X over openGL
    need new drivers ...

another possibility is to use a kernel drivers and one of the suggested approach is the merging of drm and fbdriver.
interesting but make more sense for embedded developers or the X server developers than for desktop application developers.	
 basicly he explained how to get the same performance from linux/X that we get on Mac OS X or windows

<b>Conclusion</b>
Some people might think I'm paranoid when all the talks are about gnome or freedesktop, you wonder what are real motivation of the organizers. And after watching the non gnome related talks, i mean the eclipse one and the freedesktop ones it's an evidence that freedesktop got hijacked by gnomes, all the desktop stuff they talked about integrated with gnome and not with kde, because according to one of the attendee I overheard: "It's normal that we don't see any KDE stuff because the last person using it died 3 months ago."

I'm not sure I will attend next year. Only positive point: I got a free t-shirt. :D

update: there was an eclipse guy during the kdevelop talk. I apologise for any problem my mistake might have provoked 
<!--break-->