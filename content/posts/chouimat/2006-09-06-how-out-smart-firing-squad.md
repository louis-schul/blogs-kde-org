---
title:   "How to out smart a firing squad"
date:    2006-09-06
authors:
  - chouimat
slug:    how-out-smart-firing-squad
---
This blog is the first of a new series which goal is to make the world a better place  using laughter ... so here today's joke :D

Bill Clinton is placed against the wall, and just before the order to
shoot him is given, he yells, "Earthquake!" The firing squad falls  
into a panic and Bill jumps over the wall and escapes in the confusion .

John Kerry is the second one placed against the wall. The squad is
reassembled and John ponders what his old pal Bill has done.  
Before the order to shoot is given, John yells, "Tornado!"  Again the squad  
falls apart and Kerry slips over the wall thus making his escape.

The last person, George W. Bush, is placed against the wall. He is
thinking, "I see the pattern here, just scream out a disaster and hop over
the wall!" As the firing squad is reassembled and the rifles raised in his
direction, he smirks his famous smirk and yells, "Fire!"

<!--break-->