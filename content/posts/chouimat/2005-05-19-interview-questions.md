---
title:   "interview questions"
date:    2005-05-19
authors:
  - chouimat
slug:    interview-questions
---
I had another interview lately and I'm still wondering why they always those stupid question like
"Where do you see you in 10 years?" I find answering this type of question very very difficult, because it's so easy to give a wrong answer, the person in front of you, can believe anything from the lack of ambition to anything on the other extreme (if such thing as too much ambition can exists) so I tried to say that with the way the job market and the world  is changing that I can't say anything about where I see myself in 10 years because it's too uncertain, but on the positive side I might be able to watch all the Star Wars movies (including the clone war cartoons) in the right order :) But it seems that I answered wrongly once again :D
<!--break-->