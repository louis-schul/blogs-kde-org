---
title:   "A little social experiment"
date:    2005-01-25
authors:
  - njaard
slug:    little-social-experiment
---
I'm going to have <a href="http://ktown.kde.org/~charles/badtrend.jpg">this</a> shot, and we will see how much sense I make at the end of this journal entry.  Or for that matter, at the beginning.
<br><br>
Mmmm... Anise.
<br><br>
Anyway, continuing, in my two week long operation to track down the <a href="http://bugs.kde.org/show_bug.cgi?id=95704">Jitter Bug</a>, I got very close, but, alas, all I did was provide enough information for someone that actually knows (slightly more about) how khtml works.  Do I get half credit for that?  My next is <a href="http://bugs.kde.org/show_bug.cgi?id=97806">something to do with RTL</a>.  I hope that Allen doesn't get to it first.  Yes, you heard me Allen, that one's mine.  I only claimed it because I have already seen how some of that part of khtml works.  I'm also the one that made the test-case.
<br><br>
Fixing khtml bugs consists of reading code for hours/days/weeks and after a while, changing a line or two, and wondering "what will this do?" -- really.  Sometimes, if I feel really confident, I even think "maybe this will make a change to the better."  Usually it doesn't but if you're lucky you might have learned a little about how khtml works.  Who is to blame for the following code?:

<pre>if (child && (flow->enclosingLayer() == enclosingLayer()))
    // Set noPaint to true only if we didn't cross layers.
    r->noPaint = true;
</pre>
That has to be the lamest comment ever.  Please, please, coders: don't comment what you are doing, comment why you are doing it.  The place to explain what you are doing is in the header file as API-docs.
<br><br>
Next month, Chris Howells and Jeff Snyder are going to see Nightwish and Tristania in London.  Well, to that, I say nya nya, and I'm going to see them a whole five days prior in Birmingham.  Jeff even said that they were sold out on tickets just to discourage me.  <i>That monster</i>.
<br><br>
While I'm at it, note my new hackergotchi on <A href="http://www.planetkde.org">planet kde</a>.  I'm not exactly artistically talented, but I do know how to use the gimp.
<br><br>
Also, I fixed a couple bugs in Photobook (and then chose to delay a couple of features until KDE 4).  Looks like Photobook will have to be moved to kdebase, or libkonq will have to move to kdelibs.
<br><br>
Noatun make-it-snow branch is also progressing (no thanks to myself).  Stefan's been doing a super job on doing stuff and I've just been lazy as hell. I'll work more on it.  I did, however, get around to porting the noatun audioscrobbler <a href="http://www.sdonag.plus.com/ntscb/">plugin</a> to Noatun3.  If for no other reason because I think it's <a href="http://www.audioscrobbler.com/user/njaard/">cool</a>.
<br><br>
Oh, today was the first week back in university after the holiday, and exams.  I'm sorry, but I find it more rewarding to make no progress on khtml bug-fixing than to attend lectures in which nothing is learnt.
<br><br>
I do not have a nice bit of literary artwork for this week, so I will share a grammatical lesson for you:
<ul>
<li>"a lot" is two words.</li>
<li>Charles is made into a possessive as «<i>Charles's</i>», not «<i>Charles'</i>» (the rule of adding just an apostrophe only applies to plurals).  I'm not making that up.</li>
<li>The word "Idea" is pronounced with three syllables: Eye-Dee-uh.</li>
<li>"since" is used only for an absolute time or date, "for" is used for a period of time.  "I have been a KDE developer <i>since</i> January 2000" versus "I have been a KDE developer <i>for</i> four years".</li>
</ul>
(If English is not your native language, those last two will be of particular interest to you).
<!--break-->