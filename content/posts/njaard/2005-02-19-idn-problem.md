---
title:   "the IDN problem"
date:    2005-02-19
authors:
  - njaard
slug:    idn-problem
---
Filed against every single major web browser is the bug of "unicode blindness injection" security vulnerability.  In short, Unicode letters can look the same as their ascii-equivalents, but lead to a different URL (thereby permitting man-in-the-middle attacks).
<br /><br />
My <a href="http://ktown.kde.org/~charles/ucompare.png">solution</a> consists of verifying that unicode glyphs look different from ascii glyphs (yes, I like the word "glyph").  In my example screenshot, words in parentheses are entirely ascii, those preceding them have a "wrong letter:"
<ul>
<li>the K in KDE is Cyrillic (U041A)</li>
<li>the S in RULES is Cyrillic (U0405) (which I'm aware doesn't even exist in Cyrillic)</li>
<li>the P in APPLE is also Cyrillic (U0420)</li>
<li>the T in MICROSOFT is Greek (U03A4)</li>
</ul>

However, it does a poor job of identifying K. If this is considered useful, I may be inclined to fix it for inclusion into KDE, otherwise, I'll leave it to be abandonware as I do with everything else.
<br /><br />
The idea is that if there's a unicode letter, and the "error report" is high enough, you might warn the user prior to visiting the page.
<!--break-->