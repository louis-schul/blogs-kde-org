---
title:   "A web interface to digikam"
date:    2006-04-25
authors:
  - antonio larrosa
slug:    web-interface-digikam
---
Currently I have 21458 photos according to digikam's database (which are using 13 Gb according to du ). When someone asks me to put some album in my web server I hate to lose my time exporting to html, generating thumbnails and resized 1024x768 versions of my pictures which occupy space in my HD that I'm usually hesitant to remove.

Also, when kimdaba exports the images to html, it includes the associated tags, but digikam doesn't. which makes digikam's web exports be a bit unusable for me.

So last saturday evening I started a new project: Creating a web interface to digikam. Currently it's nearly done. The current completed features are:
* python cgi based
* In the config file you specify the location of digikam's albums folder, and it opens directly digikam's db as well as the original pictures
* It creates resized images and thumbnails on demand from the original picture and cache them in a special directory (which can be safely removed, since the script regenerates the cache from the original files when needed)
* It keeps track of the number of times each image has been seen (and the persons who saw them)
* The albums that can be seen from the web are easily configurable in a simple text file which contains a like for each Album.
* Uses cookies to always paint images at the same size for each user. Those images (usually in lower resolution than the original) are also generated in run time and cached.
* Shows the thumbnails of the previous and next pictures when seeing one.
* When generating a thumbnail or resized version of a movie, it adds a movie indicator to the image.

<P>
There are still some things that I'd like to fix before publishing the cgi scripts (like making the interface localized), but if anybody wants to know how it looks like, here are a couple of screenshots . Yes, my css could be better, but I'm not a designer.  If someone has any idea on how it should look like, I'll be happy to read it (or even better, getting an example html file of how it should look like).
<!--break-->

[image:1958 size=original] [image:1959 size=original]
