---
title:   "Kickoff: Talk Video, SVN Branches, More Options"
date:    2007-01-14
authors:
  - beineri
slug:    kickoff-talk-video-svn-branches-more-options
---
It's again time for some <a href="http://en.opensuse.org/Kickoff">Kickoff</a> news: the video of the <a  href="http://akademy2006.kde.org/conference/talks/33.php">talk Coolo gave at aKademy 2006</a> is finally online since start of this year. The <a href="http://websvn.kde.org/branches/work/suse_kickoff/">work/suse_kickoff/</a> branch in SVN saw no activity since mid-November as we branched it to <a href="http://websvn.kde.org/branches/work/suse_kickoff_qstyle/">work/suse_kickoff_qstyle/</a> branch at that time to reimplement the tabbing with QTabWidget - still <a href="http://www.sabayonlinux.org/">other distros</a> shipping with Kickoff and Kickoff <a href="http://kde-apps.org/content/show.php?content=50240">packages for distros</a> seem to continue to ship the old pixmap based version. You will find bugfixes and the new options only in the new branch!

The unsupported <a href="http://en.opensuse.org/OpenSUSE_News/10.2-Release">openSUSE 10.2</a> kdebase packages in <a href="http://software.opensuse.org/download/KDE:/KDE3/openSUSE_10.2/">the KDE:KDE3 project</a> of the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a> contain the bugfixes and the non-GUI options listed below. Feedback is requested and welcome so we can decide what to maybe release as online update. Most of them have to be added to kickerrc and get only active after a panel restart (relogin or "dcop kicker Panel restart"):

<pre>
[General]
ScrollFlipView=false
KickoffFontPointSizeOffset=2
KickoffSwitchTabsOnHover=false
KickoffTabBarFormat=IconOnly
</pre>

The first disables the scrolling in the application browser, KickoffFontPointSizeOffset takes positive and negative values and is added to the calculated font sizes (which are relative to your system font size), the third makes you require a click to switch the tabs and the last takes LabelAndIcon, LabelOnly and IconOnly as valid keywords.

The confirmation dialogs for the "Leave" actions are now configurable in ksmserverrc:

<pre>
[General]
confirmLogoutDelay=0
confirmRebootDelay=11
confirmShutdownDelay=11
</pre>

A value of 0 makes the corresponding confirmation box not appear at all, other values define the seconds. Have fun!
<!--break-->