---
title:   "KOffice 1.6"
date:    2006-10-17
authors:
  - beineri
slug:    koffice-16
---
Another great <a href="http://dot.kde.org/1161037713/">feature release of KOffice</a> and this time it fits fine into the release schedule of <a href="http://en.opensuse.org/Roadmap/10.2">next upcoming openSUSE release</a>:

<a href="http://koffice.org"><img src="http://koffice.org/pics/koffice1.6-458.jpg" /></a>

You can find fully functional rpms of it in Factory and <a href="http://software.opensuse.org/download/repositories/KDE:/Backports/SUSE_Linux_10.1/">for SUSE Linux 10.1</a>. The koffice-illustration package for older SUSE Linux versions in KDE:Backports (a build service project which has the goal to not force you to update your system libraries or KDE to get the newest application versions) are missing Krita though as its developers explicitely disable the build of Krita because they think the version of libcms on these distros is too old/too buggy. They rather prefer to have less users than getting reports about some maybe seldom occuring bug caused by that libcms you would never notice.<!--break-->