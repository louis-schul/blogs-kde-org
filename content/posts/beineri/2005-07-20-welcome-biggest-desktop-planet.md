---
title:   "Welcome on the Biggest Desktop Planet!"
date:    2005-07-20
authors:
  - beineri
slug:    welcome-biggest-desktop-planet
---
You are reading the <a href="http://planetkde.org/">Free Software Desktop Planet</a> which aggregates the most voices. Especially nice to have also several female voices on it. :-) And looking forward to see selected Trolltech blogs also become integrated in future.<!--break-->