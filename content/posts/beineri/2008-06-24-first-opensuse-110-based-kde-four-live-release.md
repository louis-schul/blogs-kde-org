---
title:   "First on openSUSE 11.0 Based KDE Four Live Release"
date:    2008-06-24
authors:
  - beineri
slug:    first-opensuse-110-based-kde-four-live-release
---
<img src="http://home.kde.org/~binner/kde-four-live/KDE-Four-Live.i686-1.0.83.png" width="640" height="480" hspace="0" alt="KDE Four Live 1.0.83"/>

<a href="http://dot.kde.org/1214311382/">KDE 4.1 Beta 2</a> + <a href="http://news.opensuse.org/2008/06/19/announcing-opensuse-110-gm/">openSUSE 11.0</a>  = <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> 1.0.83 :-)