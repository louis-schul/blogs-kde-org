---
title:   "First Work Day"
date:    2005-09-01
authors:
  - beineri
slug:    first-work-day
---
Today I had my first work day - at <a href="http://www.novell.com/">Novell</a>/<a href="http://www.suse.com/">SUSE</a>, packaging KDE as successor of <a href="http://www.opensuse.org/index.php/User:AdrianSuSE">kAdrian Schr&ouml;ter</a> (please note the part about "poor successor" on that page!) who became a lead for the <a href="http://www.opensuse.org">openSUSE</a> project. So call me biased what is "the world's most usable Linux distribution" from now on. :-)

<a href="http://www.opensuse.org/index.php/Download#Development_Build">SUSE Linux 10.0 OSS Beta 4</a> was released today, the right thing to try on my brand-new work place hardware. Explored the countless internal web pages and services of Novell/SUSE. Started to get myself familiar with the build system - even submitted the first, not yet fully written by myself, package.

Originally I wanted to write something about SUSE LINUX Products GmbH being one of the two coolest companies in the KDE universe but today's office temperature let me hesitate: it felt about as warm as being outside in Malaga. :-(
<!--break-->