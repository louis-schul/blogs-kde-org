---
title:   "Also This Week"
date:    2005-08-13
authors:
  - beineri
slug:    also-week
---
Took the last exam of my studies at long last. :-) Received a KDE-related job offer and accepted it. :-) Booked my flights for Akademy. :-)

Unfortunately I will not be able to attend whole Akademy but have to depart on the evening of the last conference day because my work, more on that later, starts on 1st September. So I will miss one or two talk slots (not so important as Fluendo will likely stream/record them), the coding marathon week with all its BoFs (pretty please write summaries for all who cannot attend!), the common city tour and maybe the GPG keysigning party depending on when it will happen. :-(

If I should owe you a drink (or vice versa) be sure to catch me during the first five days.
<!--break-->