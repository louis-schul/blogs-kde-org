---
title:   "The openSUSE 10.2 Release"
date:    2006-12-11
authors:
  - beineri
slug:    opensuse-102-release
---
On Thursday the download edition of <a href="http://en.opensuse.org/Product_Highlights">openSUSE 10.2</a> was <a href="http://lists.opensuse.org/opensuse-announce/2006-12/msg00004.html">announced</a> (<a href="http://en.opensuse.org/Screenshots/Screenshots_openSUSE_10.2">screenshots</a>). The <a href="http://distrowatch.com/stats.php?section=popularity">interest seems to be high</a> but from what I read users experienced a good download speed thanks to FTP mirrors being prepopulated the week before, some strong initial Torrent seeders and usage of Metalink. This week also the debuginfo repository will be filled and the Live-DVD will be uploaded. Development in <a href="http://en.opensuse.org/Factory/News">Factory has also started again</a> but syncing of the Factory tree has been disabled until above mentioned are also on the mirrors.

The reactions on IRC and <a href="http://en.opensuse.org/In_the_Press">first reviews</a> have been positive about this release (except for <a href="http://ati.cchtml.com/show_bug.cgi?id=478">poor ATI users</a> which have to wait for a driver update from ATI). And it's really a fine release imo too: the default package management got faster (and supports delta rpms), non-ZMD based command line tool and a KDE update notification applet have been added, latest KDE and Krita 1.6, KNetworkManger, KPowersave, of course the newly developed start menu and better Beagle integration everywhere (Kerry, file/save dialogs, Konqueror, new start menu) and better Xgl support. And with X.org 7.2rc2, D-Bus 1.0 and cmake 2.4.3 the base is laid for KDE 4 development.

The retail box version of openSUSE 10.2 has already gone to production and this week we see <a href="http://www.novell.com/products/opensuse/">the product pages being updated</a> and more merchants will list it. In Europe it will be available through the normal retail channels while for North America it will be available at <a href="http://shop.novell.com/">shop.novell.com</a>.
<!--break-->