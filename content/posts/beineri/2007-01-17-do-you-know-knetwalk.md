---
title:   "Do you know KNetWalk?"
date:    2007-01-17
authors:
  - beineri
slug:    do-you-know-knetwalk
---
You think that you know every game that KDE ships? How about KNetWalk? It's a nice little game - not just for system administrators. The chance that you have it already installed is high, it's in the kdegames module! It entered SVN in May 2005, received bug fixes, got ported to KDE 4. But only the upcoming KDE 3.5.6 release will fix the biggest bug: the non-appearance of KNetWalk in the K menu. Have a nice play...<!--break-->