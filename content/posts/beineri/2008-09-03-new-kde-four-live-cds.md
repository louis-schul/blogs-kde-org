---
title:   "New KDE Four Live-CDs"
date:    2008-09-03
authors:
  - beineri
slug:    new-kde-four-live-cds
---
New versions of <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> are up to accompany the <a href="http://www.kde.org/announcements/announce-4.1.1.php">KDE 4.1.1 release</a> and give an impression of the development in trunk half-way to the first KDE 4.2 Alpha release. These CDs are built with k*-branding-upstream instead of k*-branding-openSUSE packages (see <a href="http://en.opensuse.org/SUSE_Package_Conventions/Branding">openSUSE branding policy</a>) so their desktop appears less green than usual. :-)<!--break-->