---
title:   "Introducing the KDE:Community Project"
date:    2006-09-16
authors:
  - beineri
slug:    introducing-kdecommunity-project
---
You may think that I have blogged about every KDE-related <a href="http://www.opensuse.org/Build_Service">openSUSE build service</a> project by now. Not quite yet. :-)

Besides the unsupported backports of <a href="http://en.opensuse.org/Factory">openSUSE Factory</a> packages in <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/repodata/">KDE:KDE3</a> & <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/repodata/">KDE:Backports</a> and the experimental stuff in <a href="http://software.opensuse.org/download/repositories/KDE:/Playground/SUSE_Linux_10.1/repodata/">KDE:Playground</a> (like KOffice 1.6 Beta and digiKam 0.9 Beta 2) various people started to package additional applications using the build service in the last founded <a href="http://software.opensuse.org/download/repositories/KDE:/Community/SUSE_Linux_10.1/repodata/">KDE:Community</a> project. With the help of the community it's not absurd to expect this repository to become the biggest of all KDE projects over time.<!--break-->