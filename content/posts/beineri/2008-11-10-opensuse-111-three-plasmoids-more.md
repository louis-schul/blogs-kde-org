---
title:   "openSUSE 11.1: Three Plasmoids More"
date:    2008-11-10
authors:
  - beineri
slug:    opensuse-111-three-plasmoids-more
---
openSUSE 11.1 will include (besides the to be expected plasmoids from kdebase4-workspace and kde4-plasma-addons) three more plasmoids to be closer to KDE 3.5 functionality: Quick Access, Quick Launcher and Keyboard Status Applet:

<img hspace=20 src="https://blogs.kde.org/files/images/oS-11.1-plasmoids.png">

And many more plasmoids are available from the <a href="http://en.opensuse.org/KDE/Repositories">KDE:KDE4:Community</a> openSUSE Build Service repository thanks to the <a href="http://en.opensuse.org/KDE/Team">openSUSE KDE Team</a>. :-)

<a href="http://en.opensuse.org/openSUSE_11.1"><img hspace=20 src="http://counter.opensuse.org/11.1/small" /></a>
<!--break-->
<!--more-->