---
title:   "Localizing the &quot;Klax&quot; Live-CD"
date:    2005-05-20
authors:
  - beineri
slug:    localizing-klax-live-cd
---
A powerful feature of the &quot;<a href="http://ktown.kde.org/~binner/klax/">Klax</a>&quot; Live-CD, inherited from its <a href="http://slax.linux-live.org/">Slax</a> and <a href="http://www.fsl.cs.sunysb.edu/project-unionfs.html">UnionFS</a> base, are &quot;modules&quot;: you can <a href="http://slax.linux-live.org/modules.php">download a module</a> like Gimp or Firefox and either load them into the running Live-CD <a href="http://slax.linux-live.org/doc_modules.php#useonfly">on the fly</a> or by adding them to the /modules/ directory on the CD (without the need to remaster the whole compressed file system) before burning the ISO image.
<br><br>
Today I created <a href="http://ktown.kde.org/~binner/klax/modules/">KDE translation modules</a>  for 17 languages (chosen by amount of native speakers, share in Web content and KDE translation progress). I want to explain you how you can create within 3 minutes a Live-CD supporting your favorite languages.
<ol>
<li>Download the generic k3b-i18n*.mo module and the two kde-i18n*.mo and koffice-i18n*.mo modules for all languages you want to have supported. I will only take German as example:
<pre>
wget http://ktown.kde.org/~binner/klax/modules/k3b-i18n-0.11-noarch-2.mo
wget http://ktown.kde.org/~binner/klax/modules/kde-i18n-de-3.4.0-noarch-1.mo
wget http://ktown.kde.org/~binner/klax/modules/koffice-i18n-de-1.3.5-noarch-1.mo
</pre>
</li>
<li>Mount the &quot;Klax&quot; ISO image to somewhere (requires being root):
 <pre>mount -o loop klax-kde-3.4.iso mountpoint</pre></li>
<li>Copy its content to a writeable directory: <pre>cp -a mountpoint newcontent</pre></li>
<li>Copy the downloaded modules into the modules/ directory: <pre>cp *.mo newcontent/modules/</pre></li>
<li>Create the new ISO image: <pre>cd newcontent
./create_bootiso.sh ../localized_klax.iso</li>
</ol>

If you are using MS Windows you can use the graphical tool <a href="http://www.myslax.rabidhutch.co.uk/">MySlax Creator</a> for steps 2 to 5 instead. Now burn the ISO image as usually, boot the CD and you should be able to select added languages (and applications) after starting KDE.
<br><br>
If you are a local KDE Group, a Linux user group or someone else wanting to distribute the modified Live-CD for promotion, you likely want to make more modifications including defaulting to your local language or adding additional information material. Not much more difficult, but please somewhere make it obvious that you modified the Live-CD: you can modify the under MS Windows visible files under newcontent/ (boot splash=slash.lss, boot text=splash.cfg, boot help=splash.txt).
<br><br>
Create a directory structure under somewhere/ (best is when running the Live-CD because of required tools and to be matched guest user-id) which contains all to be modified and added files like:
<pre>
root/.kde/share/config/kdeglobals
root/.kde/share/config/kdesktoprc
root/Desktop/README.html
home/guest/.kde/share/config/kdeglobals
home/guest/.kde/share/config/kdesktoprc
home/guest/Desktop/README.html
opt/kde/share/wallpapers/promowallpaper.jpg
</pre>

&quot;Klax&quot; knows two users, do the same for both (don't worry: it will be compressed). Define in kdeglobals to which of the supported languages the user shall default (in this case: "[[Locale]] Language=de:en_US"). In kdesktoprc you can define the wallpaper settings and README.html is a file which shall appear on the user's desktop. Make sure that guest's files are owned by the guest user! With "dir2mo somewhere/ module.mo" you turn everything into a module which you only have to copy also into newcontent/modules/ in step 4.
<br><br>
I hope this short HOWTO will inspire someone to create a localized Live-CD and spread the lore of KDE to his neighbour, the next village, town, state, ... :-)
<!--break-->