---
title:   "Distributor Patches: Linspire 5.0 and Some Others"
date:    2005-04-02
authors:
  - beineri
slug:    distributor-patches-linspire-50-and-some-others
---
First, greetings to everyone who is still (already 2nd April here) reading this entry on http://planet.gnome.org. :-)

I wanted to complete my <a href="http://ktown.kde.org/~binner/distributor-patches/">collection of distributor patches</a> with the ones Linspire has applied to the Qt and KDE packages in their Five-0 release. So I contacted them because I didn't find a hint where to get the sources. Their support offered to send me a CD with the source code. I again stated that I'm only interested in Qt and KDE related patches and would prefer a download. Seems I was the first asking for that or for sources at all. :-) They put up two Source CD images on a slow server which they linked in their warehouse (http://my.linspire.com My Products/CD Downloads). So I registered there, downloaded 1.2GB 10+ hours long and then had to diff myself against the original tarballs. I guess the (L)GPL is not talking about getting patches, convenient and fast. :-( The result of all efforts are compressed 845KB big (size is partly due to release->branch diffs) <a href="http://ktown.kde.org/~binner/distributor-patches/Linspire/5.0/">Linspire 5.0 patches</a>. On first sight Linspire didn't patch much, at least much less than SUSE or Mandrake in comparison.

Other recent patches additions include the patches of ArkLinux 2005.1, Mandrake 10.2-rc2 and Fedora Core4-test1. The <a href="http://www.novell.com/coolsolutions/feature/11878.html">SUSE 9.3</a> (note the mention of my K menu entries right-click options :-)) srpms should also be available shortly on ftp.suse.com when the first SUSE subscription customers receive their package.

Have fun exploring the patches!
<!--break-->