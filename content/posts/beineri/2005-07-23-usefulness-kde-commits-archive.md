---
title:   "The Usefulness of kde-commits Archive"
date:    2005-07-23
authors:
  - beineri
slug:    usefulness-kde-commits-archive
---
I know that not everyone is subscribed to and reading the <a href="https://mail.kde.org/mailman/listinfo/kde-commits">kde-commits</a> mailing list (hint: if you work on or are interested only in a specific area of KDE visit the <a href="http://commitfilter.kde.org/">CommitFilter</a> site).  All the more important is having an <a href="http://lists.kde.org/?l=kde-commits&r=1&w=2">archive of kde-commits</a>. With a search in the bodies you can quickly track down possible regression introducing commits, see who is working on something, when last, what branches exist for it, on what someone is working and so on. 

Please do this also when <a href="http://developer.kde.org/documentation/other/developer-faq.html#q1.8">requesting a SVN account</a> as new developer before you state "project xy seems dead" as reasoning when in fact it isn't. Gives a bad impression of you and makes me worry about future conflict/cooperation especially if there is a major rewrite in a branch happening which you don't seem to be aware of.
<!--break-->