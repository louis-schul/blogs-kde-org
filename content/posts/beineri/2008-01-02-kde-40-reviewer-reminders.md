---
title:   "KDE 4.0 Reviewer Reminders"
date:    2008-01-02
authors:
  - beineri
slug:    kde-40-reviewer-reminders
---
Before everyone starts to spread his opinion about KDE 4.0 let me spread some reminders:

<ul>
<li>KDE 4.0 is not KDE4 but only the first (4.0.0 even non-bugfix) release in a years-long KDE4 series to come.</li>
<li>KDE 4.0 is known to have missing parts or temporary implementations (eg printing, PIM, Plasma).</li>
<li>Most changes happened under the surface and cannot be discovered in a "30 minutes usage"-review anyway.</li>
<li>User interfaces being unchanged in 4.0 compared to 3.5 may be still changed/improved during KDE4 life time.</li>
<li>KDE 4.0 will not be the fastest KDE4 release, like for KDE2 most speed optimizations will happen later during KDE4.</li>
<li>Most applications (many are not even fully ported yet) will take advantage of new features which the new Qt/KDE libraries offer only later.</li>
<li>Don't measure portability success (eg MS Windows) by current availability of application releases, they will come.</li>
<li>KDE 4.0 is only expected to be used by early adopters, not every KDE 3.5 user (and IMHO KDE 4.0 shouldn't be pushed onto other user types like planned for Kubuntu ShipIt [btw said to have only 6 months support for its packages]).</li>
<li>KDE 4.1 development will not require the same amount of time as the big technology jump 4.0, expect 4.1 later this year.</li>
</ul>
Last, again: KDE 4.0 is not KDE4. :-)
<!--break-->
