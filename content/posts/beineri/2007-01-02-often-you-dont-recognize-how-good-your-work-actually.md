---
title:   "Often you don't recognize how good your work actually is..."
date:    2007-01-02
authors:
  - beineri
slug:    often-you-dont-recognize-how-good-your-work-actually
---
Often you don't recognize how good your work actually is unless you take the time to look at what others do. The reason I say this is that I just read the first issue of a new promising <a href="http://dot.kde.org/">dot.kde.org</a> series called "The Road to KDE 4". Together with the other series like "People Behind KDE", "KDE Commit-Digest", "All About the Apps", sub project news letters, KDE://radio and countless other stories (18 total in December) I think that KDE is the Open Source project which keeps its users and own community best informed and entertained.

At one of the last fairs I asked someone from the OpenOffice.org community if there would be a general news site because I found it hard to follow what happens with OOo next and within the OpenOffice.org project. It turned out I didn't miss it, it simply doesn't exist. And their homepage contains an only occasionally updated "In the media" news box - no original content at all. For Mozilla I feel enough informed by reading mozillaZine although it could be more entertaining. And GNOME? Well, the project homepage seems to be last updated in September, advertizes some "Upcoming Event" in October 2006 and FootNotes (5 stories in December with 17 comments total) is not even linked. 

In summary: keep up the fantastic work everyone involved in promotion of KDE on dot.kde.org and elsewhere!
<!--break-->