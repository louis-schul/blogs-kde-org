---
title:   "How to Browse openSUSE's Source Packages"
date:    2008-01-05
authors:
  - beineri
slug:    how-browse-opensuses-source-packages
---
This works for a long time but I don't think that it's widely known: you can browse/view/download all the latest patches of openSUSE development in the <a href="https://build.opensuse.org/package/show?project=openSUSE%3AFactory">openSUSE Build Service Factory project</a> (yes, you have to login). That's more comfortable, comprehensive (3000+ source packages) and up-to-date (real-time) than me  dumping our KDE patches once every release <a href="http://ktown.kde.org/~binner/distributor-patches/SUSE/">somewhere</a>. :-)