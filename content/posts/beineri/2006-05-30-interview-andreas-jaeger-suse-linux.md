---
title:   "Interview with Andreas Jaeger, SUSE Linux"
date:    2006-05-30
authors:
  - beineri
slug:    interview-andreas-jaeger-suse-linux
---
Short reading hint: this <a href="http://distrowatch.com/weekly.php?issue=20060529">week's issue of Distrowatch Weekly</a> features an <a href="http://distrowatch.com/weekly.php?issue=20060529#interview">interview with Andreas Jaeger</a>, the project manager of SUSE Linux about the 10.1 release process and more.
<!--break>