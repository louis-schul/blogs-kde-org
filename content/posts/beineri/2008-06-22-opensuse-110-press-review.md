---
title:   "openSUSE 11.0: Press Review"
date:    2008-06-22
authors:
  - beineri
slug:    opensuse-110-press-review
---
<a href="http://en.opensuse.org/OpenSUSE_11.0"><img src="http://files.opensuse.org/opensuse/en/thumb/1/16/11banner.png/800px-11banner.png" width="800" height="316" /></a>

The launch of <a href="http://news.opensuse.org/2008/06/19/announcing-opensuse-110-gm/">openSUSE 11.0</a> was a big success and positive <a href="http://en.opensuse.org/In_the_Press#openSUSE_11.0">reviews</a> keep showing up :-), two especially nice ones:

<a href="http://arstechnica.com/news.ars/post/20080620-first-look-opensuse-11-out-offers-best-kde-4-experience.html">ars technica</a>: <i>This is a very strong OpenSUSE release with a lot of compelling improvements. OpenSUSE 11 offers the best KDE 4 experience out there and will continue to be our reference distribution for KDE testing.</i>

<a href="http://www.linux.com/feature/139073">linux.com</a>: <i>The openSUSE version of KDE 4 alone is worth the download [..] Including KDE 4 is not as big of a risk for openSUSE as it might be for other major distributions because of the conservative and intuitive way KDE 4 is set up. openSUSE has given me hope that I could actually like KDE 4.</i>
<!--break-->