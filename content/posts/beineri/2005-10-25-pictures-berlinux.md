---
title:   "Pictures of Berlinux"
date:    2005-10-25
authors:
  - beineri
slug:    pictures-berlinux
---
Last Friday I had my first free day. And what did I do? Likely not what normal people would do. I participated on Friday and Saturday the <a href="http://www.berlinux.de/">Berlinux 2005</a> event. Berlinux took place the second time under that name and location and has only regional impact - but it's growing. I took <a href="http://ktown.kde.org/~binner/Berlinux2005/">some pictures</a> with still the same old shitty camera. The KDE booth was (wo)manned by me, Ellen and Ossi. As usual for these events, I didn't find time to attend <a href="http://ktown.kde.org/~binner/Berlinux2005/dscf0082.jpg">the talks</a> - except my own about KDE 3.5 (no slides available as I used none). Spent most time at the booth answering questions or talking to other exhibitors. But I didn't fail to attend the social event with <a href="http://ktown.kde.org/~binner/Berlinux2005/dscf0041.jpg">tasty buffet</a> and the GPG key signing. And just before departing I paid a ransom to free a Geeko from BSD slavery (task: <a href="http://ktown.kde.org/~binner/Berlinux2005/dscf0103.jpg">spot Geeko on the pic</a>).
<!--break-->