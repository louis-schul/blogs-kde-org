---
title:   "KOffice 1.5.1 rpms for SUSE Linux"
date:    2006-05-23
authors:
  - beineri
slug:    koffice-151-rpms-suse-linux
---
<a href="http://www.koffice.org/releases/1.5.1-release.php">KOffice 1.5.1</a> has been released and as usual there are rpms for SUSE Linux available: either as <a href="http://download.kde.org/stable/koffice-1.5.1/">directly built against the distros' packages</a> for SUSE Linux 10.0 and 10.1 (these packages were btw built within the progressing <a href="http://en.opensuse.org/">openSUSE</a> <a href="http://en.opensuse.org/Build_Service">Build Service</a>) or as part of the <a href="http://download.suse.com/i386/supplementary/KDE/">KDE supplementary repository</a> for SUSE Linux 9.2 to 10.1.

The KOffice release announcement mentions some post-release discovered issues with Kexi - the SUSE rpms already contain the patches to fix these so don't hesitate to upgrade your KOffice because of that.
<!--break-->