---
title:   "Less Than 50 Days to openSUSE 11.1"
date:    2008-10-30
authors:
  - beineri
slug:    less-50-days-opensuse-111
---
No blog for over a month as everyone is busy with openSUSE 11.1. The release countdown counter got kicked off too again. What will openSUSE 11.1 bring? In short the best shipped KDE desktop yet (KDE 4.1.3 with many backports and patches, no KDE3 or even Gtk+ apps running by default, Will is heavily busy with NetworkManager-kde4 plasmoid) as well all other <a href="http://en.opensuse.org/Factory/News">latest and greatest versions</a> (like OpenOffice.org 3.0 and Gimp 2.6).

<a href="http://en.opensuse.org/openSUSE_11.1"><img hspace=20 src="http://counter.opensuse.org/11.1/" /></a>