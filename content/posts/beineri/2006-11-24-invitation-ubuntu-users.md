---
title:   "Invitation to Ubuntu Users"
date:    2006-11-24
authors:
  - beineri
slug:    invitation-ubuntu-users
---
<p>In less than two weeks openSUSE 10.2 will be released <a href="http://en.opensuse.org/Product_Highlights">with really edgy components</a>. And you can download and try <a href="http://lists.opensuse.org/opensuse-announce/2006-11/msg00008.html">its release candidate</a> today and help to <a href="http://bugzilla.novell.com/">find the last bugs</a>! You will experience a complete system management suite and the best-engineered and most improved KDE and Gnome desktops available - with <a href="http://beagle-project.org/">desktop search</a>, <a href="http://en.opensuse.org/Xgl">3D effects</a> and <a href="http://blogs.kde.org/node/2331">interface usability improvements</a> in both desktops.</p>

<p>openSUSE is the distribution developed by the growing <a href="http://opensuse.org">openSUSE.org community</a> and is sponsored by Novell to create the world's most usable Linux. openSUSE has the same roots and is the base of the enterprise-ready <a href="http://www.novell.com/linux/">SUSE Linux Enterprise</a> desktop and server products.</p>

<p>Another reason: the openSUSE project is determined to keep a clear separation between patent-free Open Source (also no binary only kernel stuff) and proprietary closed-source parts. If you want to have only the former, don't download and install the optional Addon-CD containing the latter. The openSUSE project does not plan to make these two worlds <a href="http://www.desktoplinux.com/news/NS7895189911.html">indivisible</a> in the future.</p>

<p>This blog is a response to the <a href="http://en.wikipedia.org/wiki/Mark_Shuttleworth">Ubuntu dictator for life</a>'s cheap <a href="http://lists.opensuse.org/opensuse-project/2006-11/msg00108.html">try to poach openSUSE's developers</a>. But of course everyone is invited to put openSUSE 10.2 to the test. :-)</p>
<!--break-->