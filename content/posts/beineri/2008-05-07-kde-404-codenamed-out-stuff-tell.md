---
title:   "KDE 4.0.4, Codenamed Out-Of-Stuff-To-Tell"
date:    2008-05-07
authors:
  - beineri
slug:    kde-404-codenamed-out-stuff-tell
---
The usual monthly game: a <a href="http://dot.kde.org/1210150521/">new KDE bugfix release</a>, <a href="http://en.opensuse.org/KDE4">openSUSE packages</a> and a <a href="http://home.kde.org/~binner/kde-four-live/">new Live-CD</a> which as <a href="http://blogs.kde.org/node/3368">already said</a> looks more and more less like KDE 4.0 but like our <a href="http://en.opensuse.org/Screenshots/openSUSE_11.0_Beta2#KDE_4">openSUSE 11.0 KDE4 desktop</a> (while still being based on openSUSE 10.3):

<img src="http://home.kde.org/~binner/kde-four-live/KDE-Four-Live.i686-1.0.4.png" width="640" height="480" hspace="20" alt="KDE Four Live 1.0.4"
" />

<a href="http://en.opensuse.org/openSUSE_11.0"><img hspace=20 src="http://counter.opensuse.org/11.0/small" /></a> 
<!--break-->