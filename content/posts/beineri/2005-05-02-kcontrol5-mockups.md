---
title:   "kcontrol5 Mockups"
date:    2005-05-02
authors:
  - beineri
slug:    kcontrol5-mockups
---
Yesterday I suddenly realized with horror that I missed a trend: everyone seems to rewrite kcontrol or create mockups how it should look like but not me. :-) First I had to find a project name, I chose kcontrol5 because kcontrol3 and kcontrol4 already exist in kdenonbeta. After having this difficult task finished I created <a href="http://ktown.kde.org/~binner/kcontrol5/">these mockups</a> which meanwhile incorporate first feedback from #kde-usability.

Note that his is only a proposal for a new kcontrol interface, how to navigate. It is not (again) about restructuring and renaming the modules (like trying to flatten it to two levels), cleaning up modules (later more about this), introducing a task-based approach, improving how good the search works or introducing context between options (that will be left to Tenor). In short, only what could be easily improved in KDE 3.x series.
<!--break-->'