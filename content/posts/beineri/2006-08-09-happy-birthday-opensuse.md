---
title:   "Happy Birthday openSUSE!"
date:    2006-08-09
authors:
  - beineri
slug:    happy-birthday-opensuse
---
<a href="http://opensuse.org" title="openSUSE.org"> <img src="http://files.opensuse.org/opensuse/en/c/c7/Opensuse_4.gif" alt="openSUSE.org" width="180" align="right" height="60"></a>
Exactly one year ago Novell announced <a href="http://en.opensuse.org/">the openSUSE project</a> to develop its Linux distribution, which is also the base for the Linux enterprise products, in an open way. I was one of the first to download Beta 1 of SUSE Linux 10.0, shortly after I switched the sides (btw there are several <a href="http://www.suse.de/jobs/">open jobs at SUSE in Germany</a>).

Since then <a href="http://distrowatch.com/stats.php?section=popularity">the interest in SUSE increased</a> significantly. In some areas we received great community involvement like the Wiki now being available in <a href="http://en.opensuse.org/Translation_Team">not less than 14 languages</a>. Also the <a href="http://en.opensuse.org/Build_Service">build service</a> while still being in Alpha phase attracts developers and will be a central part during next months for more openness of the development. Other stuff like the <a href="http://i18n.opensuse.org/">localization portal</a> is still under development and will allow even more people to participate in the development of the distribution (and derivates).

Sometimes things seem to progress a bit slowly (especially if you look at them every day) and bumpy but I think until the end of the year we will have all central parts of the project nicely running and have a cute openSUSE 10.2 release under your christmas tree. Beyond I can't imagine yet how openSUSE will look like when it becomes two years old. That also depends on your involvement! :-)
<!--break-->