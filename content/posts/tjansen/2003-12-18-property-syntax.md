---
title:   "Property syntax"
date:    2003-12-18
authors:
  - tjansen
slug:    property-syntax
---
Today I realized that the property syntax of in all C-based languages sucks. Lets a assume a very simple class called 'SubString' that describes a fragment of a string and has two properties: its length and the index of the first character of the string. The class implementation has two field members, one containing the first character's index, and the other the index of the first character after the string. One problem is that there is no field member corresponding to the second property. This is what I call a virtual property. In this simple example there is no real reason for having a virtual property, but in reality this happens quite frequently when you want to expose a value of an aggregated object.
Let's try to implement this in Qt/C++, Java and C&#35;.<br><br>

<b>Qt/C++</b><br>

Qt's situation is a little bit difficult, since there is no language support for properties. moc does not help with the syntax, so the implementation is macro-based. The example looks like this:

<pre>
class SubString : public QObject {
        Q_OBJECT
        Q_PROPERTY( int beginIndex READ beginIndex WRITE setBeginIndex )
        Q_PROPERTY( int length READ length WRITE setLength )
private:
        int mBeginIndex;
        int mEndIndex;
public:

        /// Documentation!      
        int beginIndex() const {
                return mBeginIndex;
        }
        /// Documentation!      
        void setBeginIndex(int value) {
                mBeginIndex = value;
        }
        /// Documentation!      
        int length() const {
                return mEndIndex - mBeginIndex;
        }

        /// Documentation!      
        void setLength(int value) {
                mEndIndex = value + mBeginIndex;
        }
};
</pre>

The problems of the Qt approach are:
<ul>
<li>You need to write 2 redundant functions (beginIndex() and setBeginIndex())
<li>You need to document 4 functions, instead of only 2 properties
<li>The macro Q_PROPERTY is ugly and almost guarantees bad error messages when you get the syntax wrong
</ul><br>
<br>
<b>Java</b><br>
<br>
Java uses a very simple solution for properties that makes use of its reflection mechanism: for a property X you only implement two methods getX() and setX(). The SubString class implemented in Java looks like this:<br>
<br>
<pre>
class SubString {
        private int mBeginIndex;
        private int mEndIndex;
        /// Documentation!      
        public int getBeginIndex() {
                return mBeginIndex;
        }
        /// Documentation!      
        public void setBeginIndex(int value) {
                mBeginIndex = value;
        }
        /// Documentation!      
        public int getLength() {
                return mEndIndex - mBeginIndex;
        }
        /// Documentation!      
        public setLength(int value) {
                mEndIndex = value + mBeginIndex;
        }
};
</pre>
<br>
The advantage of the Java solution is that it only uses Java 1.0 features. No macros were needed or new language features added when JavaBeans have been introduced with Java 1.1. Disadvantages are:

<ul>
<li>You need to write 2 redundant functions (getBeginIndex() and setBeginIndex())
<li>You need to document 4 methods instead of 2 properties
<li>Some people feel uneasy about encoding the property name into the get/set methods. It also creates some (theoretical) problems with property names that differ only in capitalization
</ul><br>
<br>
<b>C&#35;</b><br>
<br>
C&#35; has built-in support for properties, but with a somewhat strange syntax (note the implicitly defined value variable in the example below). They will not be accessed like a function, but like a field member. By omitting the get or set part it is possible to create read-only and write-only functions. The example looks like this:
<pre>
class SubString {
        private int mBeginIndex;
        private int mEndIndex;
        /// Documentation!      
        public int beginIndex {
                get {
                        return mBeginIndex;     
                }
        
                set {
                        mBeginIndex = value;
                }
        }

        /// Documentation!      
        public int length {
                get {
                        return mEndIndex - mBeginIndex;
                }

                set {
                        mEndIndex = value + mBeginIndex;
                }
        }
};
</pre>
<br>
Properties in C&#35; have the following disadvantages:
<ul>
<li>You need to write redundant get/set accessors for the 'beginIndex' property (you can not convert a regular field into a property without losing binary compatibility, which is the main reason for making them, nor can a field appear as property)
<li>Because properties are accessed like fields, many APIs in .Net become a strange mixture of fields and methods, and it is hard to remember which value is exposed as a property and which as a method
<li>Many people, including me, find the syntax quite awkward, and it blows up the language's syntax somewhat. It is quite brief though, compared to the Java and Qt alternatives
</ul><br>
<br>
<br>
<b>The property syntax should be short and crisp</b><br>
Most property accessors are used to access a field member of the class. They are used either to make the properties browsable, e.g. for a GUI editor or to allow automatic mapping to some file format, or to keep binary compatibility even when the implementation changes in the future. Thus syntax of properties should be short to promote their use. When you have 20 potential properties in a class many people, including me, hesitate to use them. The reason is that then would have to write and document 40 methods, declare 20 field variables and 20 properties. Too much work, and that's why people start working around this by using maps/hash tables for the values (which makes it impossible for the compiler to find many bugs). <br>
<br>
<b>How easy could it be?</b><br>
A very simple solution for a property would be to add a keyword to promote a field member to a property:
<pre>
property int beginIndex;
</pre>
Then the compiler could create default accessor methods that can be overridden by the user, if the property should need specialized accessors. For properties that do not need a corresponding field member, like 'length' in the example, the property could be declared as 'virtual'.
<pre>
virtual property int length;
</pre>

The class would look like this:
<pre>
class SubString  {
        private int mEndIndex;

        /// Documentation!      
        public property int beginIndex;

        /// Documentation!      
        public virtual property int length;

        public int length() const {
                return mEndIndex - beginIndex;
        }

        public void setLength(int value) {
                mEndIndex = value + beginIndex;
        }
};
</pre>
For most properties this reduces the number of lines down to 1. The disadvantage is that the implicitly defined accessor methods may confuse a reader who isnt aware of them.<br>

<br><br>
<b>Update (03/12/22):</b> Please look at the comments, I think panzi's syntax for accessor methods (and my Java'ized variant for a language closer to Java) are much nicer than the original example.