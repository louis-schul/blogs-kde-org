---
title:   "4.0 Wishlist: KURL -> KDE::Net::URI + KDE::Net::URL"
date:    2003-09-14
authors:
  - tjansen
slug:    40-wishlist-kurl-kdeneturi-kdeneturl
---
I hope this becomes a loose series of various things that  I would like to change for KDE 4.0. The code may appear in kdenonbeta/kdeutil where I keep utility classes for the KDE namespace.

Here is number one: KURL. <!--break-->KURL basically describes HTTP URLs which happen to be also usable for all other URIs that use the Common Internet Syntax from <a href="http://www.faqs.org/rfcs/rfc1738.html">RFC 1738 Section 3.1</a>. But KURL is not suitable for a growing number of URIs (read <a href="http://www.w3.org/Addressing/">this</a> if you do not know the difference between URLs and URIs) that use different syntaxes, like <a href="http://www.faqs.org/rfcs/rfc2141.html">URNs</a>, <a href="http://www.faqs.org/rfcs/rfc2397.html">data: URIs</a> and <a href="http://www.faqs.org/rfcs/rfc2608.html">service: URIs</a>. So I'd like to move some code into a super class called KDE::Net::URI that implements the scheme part that all URIs have in common, and then rename KURL to KDE::Net::URL, following the Namespace convention <a href="http://blogs.kde.org/node/view/172">described in a previous post</a>. This would make it possible to create classes for other schemes. Network-transparent code like KIO should use the KDE::Net::URI class, to allow the use of protocols that do not follow the Common Internet syntax.
