---
title:   "KPhotoAlbum Development Sprint"
date:    2008-08-29
authors:
  - blackie
slug:    kphotoalbum-development-sprint
---
Tomorrow it starts! The (at least for four of us) long awaited KPhotoAlbum development sprint!

Tuomas Suutari,  Jan Kundrát,  Henner Zeller, and I will all by together at my place in Hjørring, Denmark the coming week to bring the development of KPhotoAlbum forward.

Stay tuned!