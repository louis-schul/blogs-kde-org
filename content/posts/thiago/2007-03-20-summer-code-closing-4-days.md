---
title:   "Summer of Code closing in 4 days"
date:    2007-03-20
authors:
  - thiago
slug:    summer-code-closing-4-days
---
So this is the time to send your proposal if you haven't already. If you want to work on something for KDE 4, you're a student and would like to get paid, this is your chance.

You still have 4 days (and a few hours) to send it using the <a href="http://code.google.com/soc/student_step1.html">Google web interface</a>. It closes for new submissions on March 24th, 23:59 UTC. After that, you will have to wait for next year and KDE 4.0 will already be out.

With 4 days to go, we're still nowhere remotely close to the application count of last year. Then again, last year, I was also very surprised when the gross of the applications turned up in the final days. I recommend students <b>not</b> to wait: your application doesn't have to be perfect, just good enough. And, besides, applications sent early get more review and, probably, more votes.
<!--break-->