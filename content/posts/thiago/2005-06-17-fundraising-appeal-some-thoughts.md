---
title:   "Fundraising Appeal: Some thoughts"
date:    2005-06-17
authors:
  - thiago
slug:    fundraising-appeal-some-thoughts
---
When I started reading <a href="https://blogs.kde.org/user/view/418">pipitas</a> blog about the <a href="http://blogs.kde.org/node/view/1173">fundraiser</a> he's driving, my first thought was, “sure, how can I help, <i>non-financially</i>?".

It's easy to donate time and help, and it's very appreciated. But sometimes you have to make the effort to do something more.

Oischi looks like a fine developer, and his work with KDE 4 code is pretty amazing. Just look at it for yourself:

<img src="http://home.degnet.de/hans.oischinger/kompose2.gif" class="showonplanet">

So I am going to make an effort and see how much I can do to help. I'm not rich, and you'd probably laugh you if you knew how much I make (compared to U.S. or Europe standards). But we should help one another.
<!--break-->