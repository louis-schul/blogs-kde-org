---
title:   "Qt D-Bus bindings"
date:    2006-02-28
authors:
  - thiago
slug:    qt-d-bus-bindings
---
I'd like to thank J5 <a href="http://www.martianrock.com/?p=198">making the D-Bus 0.61 release</a>. For those that don't know it yet, I've been the maintainer of the Qt bindings for almost two months now -- half of which as a Trolltech project.

Yes, this means it's actually Trolltech code. It means Trolltech decided to support D-Bus: we here in Oslo think D-Bus will play a very important role in the context of the <a href="http://www.freedesktop.org/wiki/PortlandIntegrationTasks">Portland</a> integration project, so we might as well offer our customers and the community the best API possible.

Release 0.61 of the D-Bus library is the first one to contain the new code. Most of the code is there, but I warn you! Not only is it not <a href="http://lists.freedesktop.org/archives/dbus/2006-February/004189.html">feature-complete</a>, but I've already broken binary compatibility again with my new code.

On the other hand, I'm happy to announce I've now finished implementing all features I had in my original plan. The last one of which is a nice tool which I (intentionally mis)named <tt>dbusidl2cpp</tt>, whose intent is to replace <tt>dcopidl2cpp</tt>. (misnamed because it currently doesn't read an IDL, but XML).

I'll spend the rest of this week documenting the code and writing more autotests. The next week, I'll submit the code to an API review here at Trolltech. And then I'll merge the new features back into the D-Bus CVS for the next release.

In other, non-related news, life here in Oslo is doing just fine, though it has just started snowing again, after almost being able to see the streets' pavements after the snow melted (again).
<!--break-->
[Disclaimer: this is a blatant abuse of the "DCOP" category in kdedevelopers.org]