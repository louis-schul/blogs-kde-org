---
title:   "kdelibs port to D-Bus Completed"
date:    2006-05-21
authors:
  - thiago
slug:    kdelibs-port-d-bus-completed
---
So after a few weekends of work (and some not working), some friends and I have finally finished porting kdelibs from DCOP to D-BUS. It compiles, links and installs. I've just made the final commit on Subversion (I had forgotten to remove a few files from installation).

I'd like to thank Harald Fernengel, Simon Hausmann, Ben Meyer, Roberto Raggi (thanks for the Tiramisu!) and Kévin Ottens for the help on porting. At least if I drop off the face of the earth tomorrow, there's someone with knowledge :-)

You're going to ask: was it difficult? What was the trickiest code?

Well, I'd have to say that the most tricky things in DCOP to port are hand-written code, especially hand-written signals. KDELibs had a lot of hand-written DCOP calls and even a few hand-written DCOP objects. In other words: they marshalled and de-marshalled the arguments themselves, which means a lot of code to be ported.

The problem with signals is that the low-level DCOP (i.e., not using generated skeletons and stubs) has no concept of signals. It only has broadcast calls. On the other hand, D-BUS has no such thing as broadcast calls: it only has signals. So I've had to convert those broadcast calls into proper signals. Sometimes, the code ended up being very ugly because of that.

I'll start taking a look at how to port kdebase now. If you want help, I've put a simple HOWTO in <a href="http://wiki.kde.org/tiki-index.php?page=Porting%20KDELibs%20to%20D-Bus">the wiki</a>.
<!--break-->