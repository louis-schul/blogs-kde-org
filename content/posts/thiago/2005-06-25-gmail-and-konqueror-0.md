---
title:   "On \"GMail and Konqueror\""
date:    2005-06-25
authors:
  - thiago
slug:    gmail-and-konqueror-0
---
I was reading carewolf's <a href="http://blogs.kde.org/node/view/1195">blog</a> and I decided to post a comment there. Here it is reproduced for greater visibility:

<dl>
<dt>Then stop</dt>
<dd>Stop making the effort of reading such obviously and blatantly obfuscated code. You've done quite a lot of work, and the community surely thanks you many times over for it. However, if they keep breaking it, then it'll never end.<br>
<br>
Instead, everyone who has a GMail account and wants to use Konqueror, mail Google <b>now</b> and make them write Konqueror support.</dd>
</dl>
<!--break-->