---
title:   "Food for thought"
date:    2006-06-07
authors:
  - cristian tibirna
slug:    food-thought
---
A good friend of mine recommended to me <a href="http://bostonreview.net/BR31.3/byrne.html">What Mind–Body Problem?</a>, an excellent read on the age-old puzzle of consciousness. His recommendation came interestingly as a complement to the book of <a href="http://en.wikipedia.org/wiki/John_D._Barrow">John D. Barrow</a>, <i>Theories of Everything</i>, which ask a strong question about the involvement of conscious observers in the foundation of any universal theory. It's satisfying to read clear and captivating renderings of long lasting personal fuzzy reflections.
