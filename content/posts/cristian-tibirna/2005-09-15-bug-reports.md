---
title:   "Bug reports"
date:    2005-09-15
authors:
  - cristian tibirna
slug:    bug-reports
---
(Yes, blogs are slowly becoming what mailing lists always were anyways... It's my secret theory that blogs were invented by people a) discontent with their visibilities on dedicated mailing lists and b) not willing to mix their greatness with the commoners ;-)

The problem of the fuzziness of bug reporting (in KDE, but I suppose the same applies everywhere) was recently brought to discussion (<a href="http://aseigo.blogspot.com/2005/09/kde-and-bug-reports.html">Seigo</a>, <a href="http://www.christian-loose.de/2005/09/re-kde-and-bug-reports.html">Loose</a> and <a href="http://www.omat.nl/index.php?option=com_content&task=view&id=30&Itemid=0">Albers</a>).

I want to refer only to the mentioning of a probably useful merit system for bug reporters, as proposed by aseigo. I must agree that some parts of this idea are useful and needed in an eventual process of improving the automatisation of b.k.o. But there is an aspect that needs to be spotted out. A merit system, where maintainers could assign quantified trust to bug reports creates the (negative) potential of artificially narrowing the kinds of bug reports favored/preferentially considered. Even from the examples given by aseigo, it is evident that we tend to favor people we know and trust. Which happen more often to be developers. Which have rather well determined and somehow uniform manners of work and environment exploration. Thus, narrowing trust based on such criteria would make us easily miss probable kinds of more "real world" problems.

I give an example: I need to maintain kdeprinting technologies. Michael Goffioul did a wonderful job at making these very flexible and very powerful. The code is quite clean and nice too. Thanks to these qualities, the bug reports currently in the database are most often coming from really twisted ways of use, that I dare say aren't very probable to be tested by casual KDE developers that I would tend to trust. Or not even by general developers that would be able to come out with a patch. Casual developers rarely print, in my experience, given their special relationship with their computers (don't need no stinkin' paper...) Or if they do, they usually print pure text in dull formats. No special needs.

That makes the important bug reports in the kdeprint product come dominantly from non-technical users.

Where I come to is that having a high quality triage of bug reports remains key. And of course this means high need of qualified manpower. <i>I still think the second most important challenge in the next 5 years for KDE will be recruiting, marketing and presence</i> (the first being writing meaningful code of course).
<!--break-->