---
title:   "The bitter failure named \"safari and khtml\""
date:    2005-04-29
authors:
  - cristian tibirna
slug:    bitter-failure-named-safari-and-khtml
---
It's a sad thing to read intelligent people uttering frivolous and unsubstantiated affirmations, harpooning around with caustic near-to-arrogance addressings and pulling a Demostene from the "height" of their perceived more inner understanding of the "grand scheme".

Yes, Zack, the colaboration between the KHTML team and Safari seems from here (read from far outside) at least superfluous if not disastrous. 

Yes, I agree with most things you had to say about how hard things are, and how hard things are <b>made</b> to be.

But I can't agree with your tone and with your approach. The eagerness people have to see Safari modifs merged into KHTML is, I dare to think, legitimate. It was big news when Apple decided to take the KHTML code and run with it. It remained big news when things <i>seemed</i> to flow in both directions inbetween. But we, those who were there, with their collective nose in the code all the time, knew from the beginning that the collaboration smelled fishy. You don't need to slice at your own people, you should speak out the ugliness of the Apple behavior or you shall stay out of it at all.

So, we lost another occasion of pulling the PR we deserved. We played the careless and the kind, we forgot that the authors of gifted code have a right to refuse despising acts or even words, even though they oughtn't ask for praise or thanks.

Don't shout at the still hopeful and ingenuous ones, which are hoping to see Apple say thanks. They act from hope for Apple's actions, not from despise for KHTML team's work. Don't punish hope! Lash at the corporate treason!

We should stay a community. We should try partnering with outside players (outside because they choose so) but keep a wide open eye for abuse. 

We shouldn't be afraid of saying that yes, Safari works on maps.google.com and Konqueror doesn't, but this is so because somebody took thousands of hours of coding's worth of fine library code and threw heavy money at it in a diverging effort meant to instill mischeavious competition. (At least that's my interpretation of what happened even though I can't understand why they'd choose so).

Be kind to your kin, Zack.