---
title:   "LinuxWorldExpo San Francisco 2005 - the day after"
date:    2005-08-15
authors:
  - cristian tibirna
slug:    linuxworldexpo-san-francisco-2005-day-after
---
LWE-SF 2005 is over. I'm back to the normal schedule now. Here is the fasttrack of getting back home.
<!--break-->

5h30 - wake up, shower, check room, check baggages.
6h30 - breakfast, alone.
7h00 - check out and fix the bill. On my way to the airport.
8h30 - checkin to the flight back.
9h30 - I find out from a gentle family of french people that the barril of oil blew up to 66$. Harsh times await us all...
10h30 - takeoff
15h00 (18h00 Mtl. time) - landing. Very nice flight.
19h20 - finally get the baggages, after 1h20 of stupid waiting.
19h30 - find my car in perfect shape in the long-time parking lot.
22h00 - home!

I have 2655 emails and exactly 1000 rss titles to go through. This will prove to take most of the next 2 days (I just ended all those while writing this, on sunday evening).

In the evening of the <a href="http://blogs.kde.org/node/1328">day 3</a> at LWE-SF, the KDE booth was visited by Mark Sobbell from <a href="http://bookpool.com">bookpool</a>. He promised to mention us in the coverage of the show and <a href="http://www.bookpool.com/ct/98048">he did so here</a> (photo included).

This was a full and fruitful week. I want to underline the instrumental role of <a href="http://www.linuxprinting.org/till/">Till Kamppeter</a> in having me invited to the LWE-SF 2005 and have my travel expenses covered by Ricoh. I believe we managed to have a rich and useful communication. It always helps very much to know people personally and realize how to calibrate your contact and your work relation with them.

This concludes my series about LinuxWorldExpo San Francisco 2005.
