---
title:   "Oughta... write... some... applications."
date:    2005-05-19
authors:
  - cristian tibirna
slug:    oughta-write-some-applications
---
Yes, it's already bad to be forced to own and run <a href="http://blogs.kde.org/node/view/1063">16 different linux distros</a> just so as to have one's application available for "linux". But it's even worse when there isn't an application at all.

Way too often I hit the wall with my stubornness to use only Linux all the time. I don't speak of getting the "you're weird, to say the least" looks when I send latex documents to my colleagues. Nor do I whine about not being able to run Duke Nukem or the latest "look ma', 2 tons of fresh blood" game of the hour. I mean, <b>where are the applications</b>?

Today, authoring a small home movie (complete with titles and sound track) is a job that even the most typical subpar Joe Bloe is proud to do <b>in a blink</b> with his stinking overpriced windows xp. Ditto creating a dvd complete with menus and animations (as much as I hate them).

Getting my DV movies from the camera tape to the HDD is a definite no no, and this after 3 years since the DV cameras became as ubiquitous as automobiles.

Creating a publication quality scientific plot or graph on linux requires a PhD in the matter. How could I ask my wife to defect her trusty windows machine when she does such graphs by the dozens <b>per day</b>, knowing that all the tweaking she performs on her Origin or Sigmaplot in 5 minutes would require 2 hours with gnuplot? She doesn't stinkin' want to become professional in gnuplot programming, she just wants her bloody graphs up and out to the printer.

The problem is the usual big ugly NIH syndrom. <b>That</b> explains this <a href="http://distrowatch.com/">abomination</a> (not the distrowatch site, mind you; this is very useful, unfortunately; but the topic of the site). And our inability to cooperate enough. Why create 42 different IM clients (each with its own cherished degree of unfinishness) when we could put the efforts together and speed the development process by incredible factors of 2 or 4 magnitude orders and rather get damn VoIP work with at least one OSS IM client. Talk about resource waste.

The other problem is lack of communication (yes, the paradox of the internet). Instead of looking at what is already done and just start from there with helping (redesign if needed), most of us just proudly reinvent the wheel.

And the roots of evil are really deeply dipped both in the past and in the landscape of technology that we develop. Wanting to start an application today means doing a set of 5 to 10 <i>a priori</i> choices: base distro, packaging, build system, docu system, base GUI library, base DE and so on. This efficiently scinds the already small attention market. And proficiently denies the deserved success to apps that rather get cornered into a niche section of the landscape.

No, I don't see a solution (other than an orwellian style one). The problem is very intricate. The distro proliferation that Kurt Pfeifle rightly criticized is only a (big) chunk of the problem, but only the tip of the iceberg (penguin pun intended). And I understand very well the "pro-choice" arguments. But I feel (deep inside) that if something doesn't change quick, we might loose our toys. And soon apart that.