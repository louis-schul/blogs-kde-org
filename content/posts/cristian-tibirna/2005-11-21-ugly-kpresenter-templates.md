---
title:   "Ugly KPresenter templates"
date:    2005-11-21
authors:
  - cristian tibirna
slug:    ugly-kpresenter-templates
---
I want to use <a href="http://koffice.org/kpresenter/">KPresenter</a>. Really, I do. And of course, this being a presentations software, I want to do with it a presentation. By its very own implicit definition, a presentation has to be attractive yet simple, suggestive yet discrete. But mostly, it has to be aesthetically pleasing. Beautiful. Well, KPresenter's default templates are darn ugly. All of them. The only ones remotely usable are the KDE3 one and, somehow, the KDE2 and KDE1 ones. And... ugly surprise, the KDE2 and KDE1 were made by ... me! Back in 1999 (this is in antiquity, for who doesn't care to do the math). If 2/3 of the acceptable templates of KPresenter are made by an aesthetics-challenged, artistically-incompetent semi-doct developer aeons ago, we're really in bad shape. 

Isn't there any young artist in quest for glory that wants to have his name and creations shot in the stratospheres together with a successful KOffice??
<!--break-->