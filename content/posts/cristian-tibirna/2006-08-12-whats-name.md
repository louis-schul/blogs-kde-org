---
title:   "What's in a name"
date:    2006-08-12
authors:
  - cristian tibirna
slug:    whats-name
---
Kurt promptly <a href="http://blogs.kde.org/node/2241">provided me with a nice surprise</a>. <a href="http://www.kbarcode.net/">There are(!) GUI (even KDE) applications that print labels</a>.

But... what's in a name! I know about KBarCode since long (and, being old, don't remember exactly since when). But given all the bar-code centered information in all descriptions I've put my eyes on in relation to it, and given my lack of interest in bar-codes presently (and in the past), it's, IMO, not a surprise that I didn't know it did labels.

Torsten Rahn <a href="http://blogs.kde.org/node/2241/5261">already marked all good points</a> about the situation. And other comments mentioned the dire need to put this application into a desktop-wide service.

I want to touch, in addition, another important point, IMHO: KDE's ecosystem seems to need taxonomy and taxonomers. Beyond what <a href="http://www.kde-apps.org">kde-apps</a> offers. But I know this is a frustrating endeavour and the result is prone to become frustrating in itself (I am still traumatized by recalls of my tucows perusing back in the '90s). So, we get to rely on the huge power of the Internet as a collaborative information enhancer. Slow and error/omission prone, but definitely working. At least, it worked well this time around. Thank you, Kurt, thank you, KBarCode creators!
<!--break-->