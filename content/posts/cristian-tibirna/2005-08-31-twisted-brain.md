---
title:   "Twisted brain"
date:    2005-08-31
authors:
  - cristian tibirna
slug:    twisted-brain
---
Since the move to SVN, I have a perception problem that was not present with CVS.

I physically <b>cringe</b> when I realize I have to fix a commit that I somehow botched (e.g. with a typo). This is so, it seems (from what my brain tells me), because of the global revision number. When I do an error and I need to commit again (thing which would have been avoided with a bit more care in the first place), I increase the global release number by <b>2</b>. I think the problem spawns from the fact that our SVN already has a <i>huge</i> release number and this accounts for the sheer giantness and complexity of it (the SVN). Maybe I secretely fear a possible failure caused by complexity.

Well, bah, I think this just means that I'm not Ossi or Coolo.
<!--break-->