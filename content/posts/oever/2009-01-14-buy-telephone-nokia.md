---
title:   "Buy a telephone from Nokia"
date:    2009-01-14
authors:
  - oever
slug:    buy-telephone-nokia
---
Do you need a new telephone? A mobile one without a wire? Please consider buying a Nokia telephone.
When you buy a Nokia telephone you support free software. Do not buy an iPhone, do not buy a Palm, buy a Nokia.
Were you considering buying a Motorola or an HTC telephone? Think again. Think Free Software. Buy a Nokia.
Avoid Windows Mobile. Avoid the iPhone. Avoid WebOS. Buy a Nokia with Free Software.

And use Qt and KDE.
