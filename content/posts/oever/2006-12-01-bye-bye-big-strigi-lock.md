---
title:   "Bye bye Big Strigi Lock"
date:    2006-12-01
authors:
  - oever
slug:    bye-bye-big-strigi-lock
---
In the CLucene backend of Strigi, I was being conservative about allowing concurrent reads and writes to the index, hence making indexing slower if you were looking at the status. So if you were monitoring the indexing speed, it would be slower.

This has now been fixed and it makes a huge speed difference if you are indexing and searching at the same time.
