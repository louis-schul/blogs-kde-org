---
title:   "KDE4 is very attractive for software service companies"
date:    2007-08-01
authors:
  - oever
slug:    kde4-very-attractive-software-service-companies
---
The next version of KDE4 will run natively on Mac OS X and Windows XP and Vista. This means that it is a very attractive platform for software development. No other cross-platform toolkit looks as good as Qt and has an equally appealing API.

Linux and Mac OS X are steadily gaining popularity. I know from experience what a delight it is to administrate a large park of Linux computers, desktop and server. There is a tremendous software choice that can easily be updated and managed and kept in check. Departments or whole companies that can get away from windows, do so more and more. And in the wake of the successful iPod, Macs are reappearing on the workfloor. This means, that the market for cross-platform tools is growing again. And this is where the KDE development environment is a very credible proposition.

The foundation of KDE is a <!--break--><!-- break --> professional toolkit, Qt, that is used for many enterprise applications already. Once a software development company has bought a Qt license for releasing software under a non-GPL license, KDE is a free added bonus. This is the path many companies will take and KDE endorses this option by releasing the KDE source code under the LGPL license.

If a company is confident enough to use the GPL license for their software, they can use Qt without paying a license fee. In the community, this is a controversial issue. Some hold that companies will not want to pay a license for Qt or are afraid to develop with the GPL. However, companies that want to sell shrink-wrapped software work on a scale where the cost of the Qt license is not an issue. This leaves companies that develop special solutions for a relatively small number of seats.

These companies should have no problems with releasing their code under the GPL. It is a sign of confidence and quality to do so. By releasing code under the GPL you are saying that your code can live up to the scrutiny of the user. You are proud of your code and you are not afraid to show it. And there is no reason to be afraid of the competition taking the code and running with it. Because if they do, they must also release under the GPL. Moreover, the cost of taking over a project with a limited deployment scale is much larger than the risk of someone copying your business. The value of special software is in the service, not in the code. And if your customer decides to switch to another company because they are not happy with your service, then the competitor will not want to touch that code either. Releasing code under the GPL is sign of confidence and quality.

KDE4 will be a big step towards an enterprise software business based on free software. The software on the big servers is there already, the desktop is next.

