---
title:   "Comparing text style support in Calligra, Abiword and LibreOffice"
date:    2014-11-30
authors:
  - oever
slug:    comparing-text-style-support-calligra-abiword-and-libreoffice
---
This weekend I spent time on preparing for the ODF Plugfest <a href="/2014/11/25/odfautotests-gearing-towards-10th-odf-plugfest-london">again</a>. The test software <a href="https://github.com/vandenoever/odfautotests">ODFAutoTests</a> now has many more tests. Most new tests are for text styles. I've created tests for each of the possible attributes in <a href="http://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part1.html#__RefHeading__1416402_253892949">&lt;style:text-properties/></a>.

You can have a look at the results <a href="http://www.vandenoever.info/tmp/odfautotests/publish/report.html">*here*</a>. If you want to recreate the tests, you can download a <a href="http://www.vandenoever.info/tmp/odfautotests/publish/odftester.jar">compiled version</a> of the test software. It can be run like this:
 <pre>usage: java -jar odftester.jar [-c &lt;arg>] -i &lt;arg> -r &lt;arg> -t &lt;arg>
 -c,--run-config &lt;arg>   xml file that contains the instructions for running the
                         loading and saving of the documents in the various ODF
                         implementations. If this option is absent, the files
                         will not be loaded and saved, but the files from
                         results-dir will be analyzed into a report
 -i,--input-dir &lt;arg>    the directory for the documents that are generated from
                         the test definition
 -r,--result-dir &lt;arg>   the directory for the results of the loading and saving
                         of the files from input-dir
 -t,--tests &lt;arg>        xml file that contains the tests
</pre>

To run it you'll also need to download the <a href="https://github.com/vandenoever/odfautotests/raw/master/config.xml">configuration</a> and <a href="https://github.com/vandenoever/odfautotests/raw/master/tests/text-properties.xml">test description</a> files.

I'm happy to receive test files for more ODF tests or configuration settings to run the tests with more ODF software. It's only one more week to the <a href="http://plugfest.opendocumentformat.org/2014-london/">ODF Plugfest</a>.