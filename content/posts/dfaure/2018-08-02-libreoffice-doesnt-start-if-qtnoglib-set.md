---
title:   "LibreOffice doesn't start if QT_NO_GLIB is set"
date:    2018-08-02
authors:
  - dfaure
slug:    libreoffice-doesnt-start-if-qtnoglib-set
---
That was a surprising find. Libreoffice wouldn't start anymore (just some splash screen with a progressbar, then nothing happens).

After some debugging (comparing my normal zsh session where it wouldn't start, with a clean `sudo -u dfaure bash` session where it worked), I found the culprit. I had QT_NO_GLIB=1 in my environment for some reason, and that's what breaks libreoffice...

Posting this in case it helps someone one day ;)