---
title:   "Upgrading to OpenSUSE Leap 15.0"
date:    2018-07-21
authors:
  - dfaure
slug:    upgrading-opensuse-leap-150
---
Upgrading from Leap 42.3 to Leap 15.0... How hard can it be? Well, there was a bit of fighting necessary. One fight due to an encrypted root partition, another one due to NVIDIA and libGL.

I used the <a href="https://doc.opensuse.org/documentation/leap/startup/html/book.opensuse.startup/cha.update.osuse.html#sec.update.zypper">zypper upgrade method</a>, from a text virtual terminal (after stopping X), which always seems safer to me than booting on a USB disk and hoping the partition setup is correct.

After a successful zypper dup, the first reboot led to an error like
"Failed to start systemd.cryptsetup@long_name_here.service, see systemctl status blah blah" (yeah right, with no root partition mounted, there are no logs anywhere)

Booting in rescue mode on a USB key and doing `cryptsetup luksOpen /dev/sda6 cr` worked, so no issue with crypto libraries or something, it was just systemd being stupid. I mounted the root partition into /mnt and edited its /etc/crypttab. Turns out the last argument on each line was timeout=0 (due to something I tried a long ago, it's supposed to work, but it doesn't seem to), which I replaced with none, and while at it I also replaced the /dev/disk/by-id device name with a UUID (found with `blkid /dev/sda6`), as suggested on a forum. After the usual `mkinitrd` and `update-bootloader --refresh`, and a reboot, it worked, it's now asking my for my crypto password! Much better. I'm not happy about systemd-cryptsetup's error handling here....

Next problem: I got a mouse cursor, but sddm doesn't show up, can't log in.
`journalctl -S today` says sddm-greeter crashed.
gdb on a core dump shows it crashed in glXMakeCurrentReadSGI
`rcxdm stop` and `startx` gets me an X session, but kwin_x11 crashes often, also in glXMakeCurrentReadSGI

Clearly a GL problem. glxinfo says:

libGL error: No matching fbConfigs or visuals found
libGL error: failed to load driver: swrast

Using `strace -e file glxinfo |& grep -i libgl`, I found the problem.
/usr/lib64/libGL.so.1 was a symlink to libGL.so.1.2.0, which rpm -qf said it didn't come from any RPM.
On the other hand there was a libGL.so.1.0.0 in that directory too, coming from Leap 15's libglvnd.
Removing libGL.so.1.2.0 and making the symlink point to libGL.so.1.0.0 fixed the issue. After a reboot I finally have sddm and plasma working.

Maybe this helps someone out there....