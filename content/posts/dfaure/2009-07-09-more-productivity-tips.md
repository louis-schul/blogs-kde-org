---
title:   "More productivity tips"
date:    2009-07-09
authors:
  - dfaure
slug:    more-productivity-tips
---
I learned a number of useful productivity tips during this akademy, so as the self-appointed guy-who-shares-productivity-tips-with-the-rest-of-kde, I thought I would share them here :-)

Read more below for yokadi, ack-grep, autojump, and cgdb.

The first one is yokadi, a command-line Todo-List manager program, which allows me to finally start applying the principles of "Getting things done" so I stop forgetting important tasks. http://yokadi.github.com - it's developed by two KDE contributors, actually: Aurélien Gateau of gwenview fame, and Sébastien Renard, coordinator of french translations.

The second one is ack-grep, a grep alternative with more human-readable output, builtin skipping of unwanted files (.svn subdirs, backup files, etc), ability to grep only certain file types, perl regexps, and more. Didn't use it much yet, though.

The third one is autojump: a "cd" command that learns, over time, where you go most often. So very quickly you can jump into the most commonly used folders by simply typing their name rather than their full path. Get it from "git clone git://github.com/joelthelion/autojump.git"

Last but definitely not least: "cgdb", a very useful (text-based) wrapper around gdb, which shows the source code in a splitter above the usual gdb prompt. This is really great, I was looking for this for a very long time. Typing "list" all the time in gdb is so cumbersome. gdb in emacs can also show source code, but I always forgot to run gdb in emacs, while now I have alias gdb=cgdb ;), and I didn't figure out how to attach gdb-in-emacs to a running process, and the keybindings were different, etc. cgdb, on the other hand, can be started exactly like gdb, and is editor-independent.

With thanks to Aurélien and Sébastien for all these useful tips!