---
title:   "Menu Musing:  Stepping Further Back"
date:    2006-09-13
authors:
  - scott wheeler
slug:    menu-musing-stepping-further-back
---
I found <a href="http://weblog.obso1337.org/2006/reflections-on-information-structures-–-mental-effort-and-task-appropriateness/">Celeste's recent post</a> interesting as it took to breaking out the different tasks that are currently lumped under the menu.  As I read it, I found myself stepping a bit further back and rephrasing the tasks as questions.  Here's they are, in blather-rific format, in hopes that they may serve as a bit of food for thought:

<ul>
<li><b>What can my computer do?</b></li>
<li><b>How do I accomplish this task?</b></li>
<li><b>How do I launch this application?</b></li>
<li><b>How do I do this task with this application?</b></li>
</ul>

I found that in stepping back a bit the meta-question that I wanted to look at was, "Do these suggest a start menu?"  I'm not convinced on all points, or really, any of them.  Specifically, some of the questions are very logically distinct, and once conceptually stepping away from our current start menu, I'm not sure I'd step back.

Going through them point by point:

<b>What can my computer do?</b>

This, I believe, is one of the most central functions of the start / K-Menu at present.  But I'm not convinced that it's anywhere close to an optimal solution.  I'm not quite sure what an optimal solution looks like, but if you'll venture with me a bit through some thought experiments I think the results are kind of interesting.

When I'm trying to figure out the capabilities of something there are some broad starting points.  First, I believe there's a subconscious categorization that goes on.  "What category of object is this?"  I'd take a different line of inquiry were I inspecting a person, a location or a machine.  I believe that things get interesting as a computing desktop embodies some of all three of these.

If I want to find out what a person is capable of, in broad terms, I might set up an interview with them.  "What have you done before?"  "Where are your strengths?  Your weaknesses?"  "What do you like to do?"  If they wanted to give me this sort of information in a concise format, they might first provide me with a CV that could be used as a starting point for further questioning.  Applied to a computer this idea intrigues me, but I've yet to pin down how that might translate into an interface.

In a location, I tend to look around.  I try to take in the surroundings -- maybe see what other people are doing there, see where I can go from there and so on.  I think this is probably the closest to the current metaphor.  But while the location-metaphor of computer navigation certainly has its usefulness, I believe it may be possible to inform that with other inquiry methods, as well as to explore a bit of the depth of location-ness.

A machine I usually play with.  I can usually feel what's going to break it or not and avoid the things that will.  Everything else is fair game.  I push buttons, turn knobs, do all kinds of wacky stuff and just see what happens.  But I approach a power-saw much more cautiously than I do a CD player.  Why?  Because I don't want to lose a finger.  I think one of the things that we can do to make figuring out the capabilities of the desktop more user-friendly is to find a way to ensure that when people are exploring that they won't lose a proverbial finger.  That also takes some of the strain off of attempting to enumerate capabilities.

<b>How do I accomplish this task?</b>

This has a few things about it that aren't obvious on first inspection.  The first, to my mind, is that this isn't an enumerated set, like, say, the list of applications.  Apple has enumerating tasks with the services menu, and in my opinion, largely failed.  Tasks are often composed of multiple discreet actions and user specific.  I also don't think that users will go to the effort to manually encode these tasks in macro recorders.

The other notable thing, hinted at above, is that tasks are rarely single-step.  An example of a "task" that I undertook in real life recently was, "How do I adjust the bridge height on a double bass?"  An application developer, looking around my apartment, might have seen things like a saw and seen that I could "cut wood", or a ruler and enumerated "measure distance", but going from there to the steps of measuring string height, drawing a line from a proposed string height to the bridge, cutting away a bit of wood from the bridge and re-filing the string grooves would be a composed task that likely would not be enumerated given my tools.

When I wanted to find out how to do this, I googled, looked at multiple suggestions on how to get there, all of which were composed of several steps and then mostly knew how it should happen.

In the computer world, I had another "task" recently when I wanted to figure out how to request vacation at work.  I asked a couple of people and then had a specific set of tasks to get that done.  I doubt the Samba developers were thinking of me requesting vacation when they implemented the ability for me to grab a form for such from a share at the office.

Generalizing that to an interface is tricky, but a task repository with a memory and some sort of high level tracking might be an interesting starting point.  I'm not sure what sort of structure would be appropriate for browsing these, but I think it would need to be multi-step and allow for multiple paths to the same result.

Again, no answers, but perhaps some more interesting questions.

<b>How do I launch this application?</b>

While I'm not a fan of application-centric-ness, this is an ingrained enough metaphore that it certainly should be supported for users.  Here I'm willing to give full points to Apple.  I do this two ways on my Mac, and am pretty happy with both.  I either look through the folder with all of my applications in the file browser, or I type the name into Spotlight.  Both of these are easy to get to and relatively easy to search.  Our current K-Menu complicates things by the necessity of screen real-estate and lumps things into sub-menus which don't always (or even usually) map to where I look by default.  On the Mac if I feel the need to group things, I can do so on my own by creating my own folders.  That's somewhat impractical in a start-menu-ish thing, so once again, I'd go for abandoning the metaphore.

<b>How do I do this task with this application?</b>

This really suggests two different starting points -- the piece of information to be worked with in a given application verses an application and a piece of data at some other point.  When starting with the data, a file, web site, mail, whatever -- I think our service menus do a pretty good job (aside from being sometimes overcrowded).  For clarity, those are the ones that show up when you right click on something in Konqueror -- i.e. "Open With..." or "Add to JuK Playlist..."

Starting from the application, we typically delegate this to the application itself.  To an extent, again, for composed tasks, that will remain so, but here I think we can strive for making it easier for application developers to expose their central functionality.  We get halfway there with our "File" menu.  Usually the upper left menu in an application exposes its main functionality (especially given our goal of being as document-centric as possible), but we're not consistant enough there presently.  Here, again, doing idle speculation, I wonder if reserving that spot for a small list of primary tasks might be better.  For examples of what this might look like, see KEdit (where I'm writing this entry):

<ul>
<li>New</li>
<li>Open</li>
<li>Open Recent</li>
<li>Save</li>
<li>Save As</li>
<li>Print</li>
<li>Mail</li>
<li>Close</li>
<li>Quit</li>
</ul>

With the exception of "Quit", those are the main things that I would want to do with a simple text document.  On the other hand see Konqueror:

<ul>
<li>New Window</li>
<li>New Tab</li>
<li>Duplicate Window</li>
<li>Open Location</li>
<li>Send Link Address</li>
<li>Send File</li>
<li>Save Background Image As</li>
<li>Print</li>
<li>Print Frame</li>
<li>Open With Firefox</li>
<li>Quit</li>
</ul>

"Open Location" and "Print" are the only two that I would identify as core tasks there.  "Search", "Bookmark" and so on would probably fit better.

If the concept of an application menu with core functionality and potentially some visual differentiation catches on, I think it would make it much easier to expose "How do I do this with the given application?"
<!--break-->