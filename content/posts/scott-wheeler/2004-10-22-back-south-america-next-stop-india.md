---
title:   "Back from South America, next stop India."
date:    2004-10-22
authors:
  - scott wheeler
slug:    back-south-america-next-stop-india
---
So, as I'm sure a handful of folks have noticed I'm back from Chile now.  Vacation was good; a little too good honestly -- I'm having trouble readjusting to normal life.
<br><br>
While I was there I met up with
<a href="/node/view/684"><img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/thumbs/thumb_d85323389c4d0378d7055b2d7355363c-684.jpg" align="right" border="" hspace="" vspace="" alt="Me, Duncan, Duncan's girlfrind (whose name I always forget...) in Valparioso." /></a>
Duncan (of Kopete fame) several times who it looks like we'll have joining us in the veritable land-o-KDE (err, Germany) in the next relatively short while.  Between Spanish classes, drinking Pisco and general ignoring of technology when possible I did end up doing a talk similar to my talk
<a href="/node/view/685"><img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/thumbs/thumb_43a88f224872f0f1be04a8959dd0c8ec-685.jpg" align="left" border="" hspace="" vspace="" alt="Me doing a talk at the Pontificia Universidad CatÃ³lica." /></a>
from aKademy at one of the universities there.
<br><br>
Things were good -- I didn't even look at code for three weeks, which was the longest stint of such in a few years.  My Spanish is back to being almost at a usable level again; and unfortunately I seem to be mixing it into my German at the moment, which doesn't really work at all.  Also had a lot of fun just socializing with non-geeks for a while.  I've always kind of been a softie for the social sciences, so this was a good chance to dust off a few of my rants.
<br><br>
Oh, and sorry Roberto -- I really planned on making it to Buenos Aires, but didn't get much further than Mendoza
<a href="/node/view/686">
<img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/thumbs/thumb_80b6acc438da0d94a4fb827199d4e3bd-686.jpg" align="right" border="" hspace="" vspace="" alt="Me and a crew of fine folks from the language school somewhere not too far into Argentina in the Andes (between Mendoza and the border)." /></a>
.  At some point I shall return though.  When I had finished budgeting all of my time I realized that going to Buenos Aires for just three days would be a little silly, so it's been postponed for my next trip to that part of the world.
<br><br>
Strangely just as I'm starting to get back into things from Chile I'm already having to organize things to go to <a href="http://linux-bangalore.org/2004/">Linux Bangalore</a>, which I submitted my talk for yesterday.  I actually really need to head to the US embassy (in Frankfurt) to get my passport extended (I'm out of visa pages -- 14 countries so far this year) and get my visa organized in the next few days.  As it turns out one of my old college roommates is now studying in Bangalore, so it'll be fun meeting up with him there.
<br><br>
Blah, this is rather scatterbrained with few real details on anything, but at any rate -- aside from bumming around and yacking about KDE I'm also back to doing a few things:

<ul>
 <li><b>JuK</b> (cleaning out some of the accumulated bugs, mostly)</li>
 <li>Getting back into some of the design stuff for the <b>link / searching framework</b> that I'm cooking up with a few others; I'd like to have another proof-of-concept hack ready for LB <small>(Sidenote -- I actually had a chance to meet with Ricardo Baeza, author of <i>Modern Information Retrival</i> while in Chile to discuss some of this stuff, which was rather cool.)</small></li>
 <li>Ideally this weekend I'll get the <b>GStreamer</b> backend written for the KDE multimedia stuff that we've got going in the <tt>make_it_cool branch</tt> targeted for KDE 4.</li>
</ul>

Yeah, so them's the plans.  Or something.  Probably I'll sit at home watching movies instead and wishing I was back in South America.  ;-)
<!--break-->