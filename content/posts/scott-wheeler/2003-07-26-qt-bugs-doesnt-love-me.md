---
title:   "qt-bugs doesn't love me"
date:    2003-07-26
authors:
  - scott wheeler
slug:    qt-bugs-doesnt-love-me
---
Ugh, another bugfix rejected.  This one affects at least KAlarm, JuK, KGPG, KGet and Kopete.  Bug was acknowledged; patch acknowledged; won't fix before 3.3 (probably more than a year and no help for KDE 3.2).

Basically if you drag a toolbar to make it float and then dock on of these apps you can't get the toolbar back without either enabling it manually (in the apps that support this) or editing the config file (which is the case for 3 of the apps).

Anyway, I'll get over it and hack around it in kdelibs, but I get frustraded when I can't just fix things (More often patches just get igored; this one at least I got a responce, but a not the one that I wanted.).  I joked earlier that I love Trolltech about 6 days of the week.  This is the one where I rant about them.  ;-)

Full corespondance <a href="http://bugs.kde.org/show_bug.cgi?id=60645">here</a>.