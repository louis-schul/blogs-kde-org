---
title:   "Konquering Berlin"
date:    2006-04-19
authors:
  - scott wheeler
slug:    konquering-berlin
---
I decided to change the post title when I looked at the Planet and saw that Matthias's post about the Berlin office is right up there at the top.

So, my news for the day, is...wait for it...yep, I'm moving to Berlin.  And no, not to work for Trolltech -- though Rich joked that I seem to be the only KDE person switching jobs and not going there.  I'll be leaving the SAP LinuxLab, after almost four years, at the end of June to work for a pro-audio software company in Berlin.  In a meeting today I announced the news to the rest of the LinuxLab (previously only my manager had known) so I can finally mention it publicly, though I made the decision a few months back.

While I'm looking forward to the change, being in Berlin, which I've fallen in love with over the course of the 10 or so visits in the last year, and a little bit of a change in scenery in the office scene (as well as working on multimedia software), I feel like I should at least take a moment to say thanks to the folks at SAP that I've been working with for the last several years.  While there have been ups and downs during my time here, on the whole it has been an experience where I've learned a lot (more organizationally than technically) and I'm glad that I'm leaving on a positive note.

Berlin is really in the process of becoming KDE-Central.  We've had a number of oldies (though some are inactive these days) that have been there for a while, Jan has managed to get it established as the KDE usability capital with Jan and Ellen there and Tina and Florian there off and on, the new Trolltech office will no doubt pull in a couple of new KDE-ish faces, and there are a couple of other KDE folks that either have moved there recently or will be doing so this summer.  Just from names I could scratch out there are 12 KDE folks that will be around by the end of the summer.

(And now, for the first time since creating this blog a few years back, I've found a use for the "new job" category on kdedevelopers.org.)
<!--break-->