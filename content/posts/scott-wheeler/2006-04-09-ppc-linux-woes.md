---
title:   "PPC Linux Woes"
date:    2006-04-09
authors:
  - scott wheeler
slug:    ppc-linux-woes
---
I still haven't managed to settle into a PPC distro that I like for my iBook.  I just tried the latest OpenSUSE RC and YaST segfaulted before the installer really got anywhere.  This is not dissimilar from my experiences the last time I tried to install it.  (Though this time around at least it authoritatively crashed during the install with no hope of recovery -- unlike a few months back when I tried it and it gave cryptic and conflicting error messages and still wouldn't install.)

Kubuntu is still on there, and the install went without a hitch, but while I'm rather excited about the existence and growing popularity of Kubuntu, it's just not my kind of party.  Having used SUSE for around 5 years (and Redhat for about 4 before that), I found it hard to get my development environment set up the way that I liked it.

I've decided for the first time in years to give Fedora a whirl.  It's conceptually closer to SUSE and I had several years experience with Redhat-ness before my SUSE conversion.

I don't get really excited one way or the other about distros.  Mainly I want a working toolchain, X, etc. and ideally some configuration tools.  My KDE I tend to build myself, as well as a pile of other things.  Gentoo and other source based distributions don't really appeal to me in the least.

We'll see how things go with Fedora.

Next task will be getting Icecream working with a cross-compiler so that my Athlon 64 3200+ desktop can help out the poor little G4 1.33 GHz. 
<!--break-->