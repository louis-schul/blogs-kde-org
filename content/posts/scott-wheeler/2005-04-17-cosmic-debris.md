---
title:   "Cosmic Debris"
date:    2005-04-17
authors:
  - scott wheeler
slug:    cosmic-debris
---
Uhm, yeah, random notes on my online life as of right now:

<ul>
<li>Matthias Kretz and I had our <i>KDE Multimedia Roadmap</i> talk accepted for LinuxTag.  He came over a yesterday and we spent some time today hashing out the outline for our paper and deciding who would write what.</li>
<li>I've got 801 <i>Tenor</i> related things that I'm in the middle of, but the basic structures are pretty close to working properly.</li>
<li>I really need to get around to doing a TagLib 1.4 release with the standard run of bug fixes beforehand.</li>
<li>Michael (Pyne) seems to be back.  Coolness.  ;-)</li>
<li>Like Boudewijn I've been messing around with painting lately.  I'm much worse than him.  It reminds me of when my college friend Ruth would come over and dye her hair in our bathroom.  The whole bathroom was splashed with red crap, but her hair always came out exactly the same color as it was before she started.  My painting is like that -- I've got a mess of champs, but my paintings are, well, just saying <i>bad</i> is kind.  At any rate, it's fun getting a feel for the brushes, blending colors and whatnot.</li>
<li>In other artistic endeavors I've taken to recording some of my improvisations on bass so that I can prove when I'm old that I once was actually good at the thing.  I've decided to call them <i>Memes for Bass</i> and have been throwing them <a href="http://home.arcor.de/scott.wheeler/memes-for-bass/">here</a>.</li>
<li>Seeing usability talked about so much on the <a href="http://planetkde.org/">Planet</a> lately makes me happy.</li>
<li>I'm in serious need of KExtendedDay lately.</li>
</ul>

That will be all for now.
<!--break-->